import * as React from 'react'
import { batch, useSelector, useDispatch } from 'react-redux'

import Paper from '@material-ui/core/Paper'
import { Theme } from '@material-ui/core/styles'
import HelpIcon from '@material-ui/icons/HelpOutline'
import LoadIcon from '@material-ui/icons/FolderOpen'
import ResetIcon from '@material-ui/icons/Clear'
import UndoIcon from '@material-ui/icons/Undo'
import RedoIcon from '@material-ui/icons/Redo'
import SplashIcon from '@material-ui/icons/Explore'
import { makeStyles } from '@material-ui/styles'

import { Actions, ReduxState } from '../../../reducers'
import { loadState } from '../../../utils/persist'
import ElementButton from '../../Toolbar/ElementButton'
import StateButton from './StateButton'
import TransitionButton from './TransitionButton'
import PositionButton from './PositionButton'
import SetAlgorithmButton from './SetAlgorithmButton'

import SaveButton from './SaveButton'
import { stateCanvasActions } from '../../../reducers/stateCanvas'
import { CanvasToolbar } from '../../Toolbar/CanvasToolbar'
import { loadData } from 'utils/statemaker/import'
import positionForce from 'utils/positioning/force'
import positionLayer from 'utils/positioning/layered'
import { upload } from 'utils/upload'
import ImportButton from './ImportButton'
import ExportButton from './ExportButton'

const { deselect, setCursorMode, zoom, toggleGrid } = stateCanvasActions
const { setErrors } = Actions

const {
	set,
	setConfirmText,
	delete: deleteAction,
	toggleShowHint,
	setShowExplore,
	undoStatemaker,
	redoStatemaker
} = Actions

const useStyles = makeStyles((theme: Theme) => ({
	root: {
		maxHeight: '100%',
		overflowY: 'auto',
		paddingRight: 8,
		'& > *': {
			marginLeft: 8,
			marginTop: 8,
			display: 'flex',
			flexDirection: 'column',
			overflow: 'hidden',
			'& > button': {
				borderRadius: 0,
				color: 'rgba(0, 0, 0, 0.54)',
				'&:hover': {
					color: theme.palette.primary.light,
					backgroundColor: 'transparent'
				},
				'&[data-selected="true"]': {
					color: theme.palette.primary.main
				}
			}
		},
		'& > *:first-child': {},
		'& > *:last-child': {
			marginBottom: 8
		}
	},
	main: {
		backgroundColor: theme.palette.primary.main,
		'& > button': {
			color: 'rgba(255, 255, 255, 0.84)',
			'&:hover': {
				color: theme.palette.primary.contrastText
			},
			'&[data-selected="true"]': {
				color: 'rgba(255, 255, 255, 0.84)',
				backgroundColor: theme.palette.primary.light
			}
		}
	}
}))

/**
 * Panels with buttons from the main toolbar
 *
 * @returns
 */
function StatemakerToolbar() {
	const dispatch = useDispatch()
	const classes = useStyles()
	const { cursorMode, grid, algorithm } = useSelector((state: ReduxState) => ({
		cursorMode: state.stateCanvas.cursorMode,
		grid: state.stateCanvas.gridOn,
		algorithm: state.stateUI.algorithm
	}))

	const handleSetCursorModeFactory = React.useCallback(
		(mode: 'select' | 'move') => () => dispatch(setCursorMode(mode)),
		[dispatch]
	)

	const handleSelectCursorMode = React.useCallback(
		handleSetCursorModeFactory('select'),
		[handleSetCursorModeFactory]
	)

	const handleMoveCursorMode = React.useCallback(
		handleSetCursorModeFactory('move'),
		[handleSetCursorModeFactory]
	)

	const handleZoomIn = React.useCallback(() => {
		dispatch(zoom(0.25))
	}, [dispatch])

	const handleZoomOut = React.useCallback(() => {
		dispatch(zoom(-0.25))
	}, [dispatch])

	const handleLoad = React.useCallback(() => {
		const data = loadState()
		if (data) {
			batch(() => {
				dispatch(set(data))
				dispatch(setConfirmText('Automata loaded'))
			})
		}
	}, [dispatch])

	const handleReset = React.useCallback(() => {
		batch(() => {
			dispatch(deleteAction())
			dispatch(deselect())
		})
	}, [dispatch])

	const handleToggleGrid = React.useCallback(() => {
		dispatch(toggleGrid())
	}, [dispatch])

	const handleToggleHint = React.useCallback(() => {
		dispatch(toggleShowHint())
	}, [dispatch])

	const handleToggleExplore = React.useCallback(() => {
		dispatch(setShowExplore(true))
	}, [dispatch])

	const handleUndo = React.useCallback(() => {
		dispatch(undoStatemaker())
	}, [dispatch])

	const handleRedo = React.useCallback(() => {
		dispatch(redoStatemaker())
	}, [dispatch])

	const handleImport = React.useCallback(async () => {
		const { filename, content } = await upload()
		const filenameArr = filename.split('.')
		// Loads and converts data to JSON
		const result = loadData(content, filenameArr[filenameArr.length - 1])
		let chosenPosition = positionLayer
		if (algorithm === 'force') chosenPosition = positionForce

		// If there was error during load, notify user
		if (result.errors && result.errors.length > 0) {
			dispatch(setErrors(result.errors))
		} else if (result.data) {
			// Otherwise position data and show them
			dispatch(set(chosenPosition(result.data)))
			dispatch(setConfirmText('Automata imported'))
		}
	}, [dispatch, algorithm])

	return (
		<div className={classes.root}>
			<Paper className={classes.main}>
				<StateButton variant="default" />
				<StateButton variant="initial" />
				<StateButton variant="final" />
				<TransitionButton />
			</Paper>
			<CanvasToolbar
				cursorMode={cursorMode}
				gridOn={grid}
				onSelectCursorMode={handleSelectCursorMode}
				onMoveCursorMode={handleMoveCursorMode}
				onZoomIn={handleZoomIn}
				onZoomOut={handleZoomOut}
				onGrid={handleToggleGrid}
			/>
			<Paper>
				<PositionButton />
				<SetAlgorithmButton />
			</Paper>
			<Paper>
				<ElementButton
					Icon={UndoIcon}
					label="Undo"
					onAction={handleUndo}
				/>
				<ElementButton
					Icon={RedoIcon}
					label="Redo"
					onAction={handleRedo}
				/>
				<SaveButton />
				<ElementButton
					Icon={LoadIcon}
					label="Load from memory"
					selected={false}
					onAction={handleLoad}
				/>
				<ElementButton
					Icon={ResetIcon}
					label="Reset canvas"
					selected={false}
					onAction={handleReset}
				/>
			</Paper>
			<Paper>
				<ExportButton />
				<ImportButton onImport={handleImport} />
			</Paper>
			<Paper>
				<ElementButton
					Icon={HelpIcon}
					label="Show hint"
					selected={false}
					onAction={handleToggleHint}
				/>
				<ElementButton
					Icon={SplashIcon}
					label="Show explore screen"
					onAction={handleToggleExplore}
				/>
			</Paper>
		</div>
	)
}

export default StatemakerToolbar
