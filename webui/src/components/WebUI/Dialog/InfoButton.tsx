import { IconButton, Tooltip } from '@material-ui/core'
import HelpIcon from '@material-ui/icons/Help'
import React from 'react'

interface InfoButtonProps {
    content:  JSX.Element
}

const InfoButton = (props: InfoButtonProps) => {
    const { content } = props

    return (
        <Tooltip
            title={content}
            placement='right'
        >
            <IconButton><HelpIcon/></IconButton>
        </Tooltip>
    )
}

export default InfoButton