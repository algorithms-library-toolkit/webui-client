import React, { useCallback } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { algorithmDataActions } from '../reducers/algorithmData'
import { ReduxState } from '../reducers'
import { Actions } from '../reducers'
import { useEvaluate } from './useEvaluate'
import { algorithmCanvasActions } from '../reducers/algorithmCanvas'
import { useAlgorithmExport } from './useAlgorithmExport'
import { useAlgorithmImport } from './useAlgorithmImport'
import { useAlgorithmDelete } from './useAlgorithmDelete'
import { algorithmUIActions } from '../reducers/algorithmUI'

const { toggleGrid, setCursorMode } = algorithmCanvasActions
const { clear } = algorithmDataActions
const { toggleHint } = algorithmUIActions
const { undoAlgorithm, redoAlgorithm, setAlgorithmToolbarOpen } = Actions

export const useAlgorithmKeybinds = () => {
	const dispatch = useDispatch()
	const {
		algorithmToolbarOpen
	} = useSelector((state: ReduxState) => ({
		algorithmToolbarOpen: state.algorithmUI.toolbarOpen
	}))

	const handleDelete = useAlgorithmDelete()

	const evaluate = useEvaluate()
	 const handleExport = useAlgorithmExport()
	 const handleImport = useAlgorithmImport()

	const onKeyDown = useCallback(
		(e: React.KeyboardEvent) => {
			e.stopPropagation()
			e.preventDefault()

			switch (e.key) {
				case 'Delete':
					handleDelete()
					break
				case 'z':
					if (e.ctrlKey) dispatch(undoAlgorithm())
					break
				case 'y':
					if (e.ctrlKey) dispatch(redoAlgorithm())
					break
				case 'a':
					dispatch(setAlgorithmToolbarOpen(!algorithmToolbarOpen))
					break
				case 'e':
					evaluate()
					break
				case 'g':
					dispatch(toggleGrid())
					break
				case 'm':
					dispatch(setCursorMode('move'))
					break
				case 'l':
					dispatch(setCursorMode('select'))
					break
				case 's': 
					if (e.ctrlKey) handleExport()
					break
				case 'o':
					if (e.ctrlKey) handleImport()
					break
				case 'x':
					if (e.ctrlKey) dispatch(clear())
					break
				case 'h': 
					dispatch(toggleHint())
					break
			}
		},
		[dispatch, handleDelete, algorithmToolbarOpen, evaluate, handleImport, handleExport]
	)

	return {
		onKeyDown
	}
}
