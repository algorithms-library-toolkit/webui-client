/**
 * Represents state machine element transition
 *
 * @export
 * @interface ITransition
 */
export default interface ITransition {
	id: string
	name: string
	/** Id of state transition is coming out */
	startState: string
	/** Id of state transition is coming into */
	endState: string
}
