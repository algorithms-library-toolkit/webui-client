#include <cstdlib>
#include <iostream>
#include "evaluator.hpp"

int main(int argc, char* argv[])
{
    std::string jsonString((std::istreambuf_iterator<char>(std::cin)), std::istreambuf_iterator<char>());
    try {
        std::cout << evaluate(jsonString) << std::endl;
    } catch (const std::exception& e) {
        std::cerr << e.what() << std::endl;
        return EXIT_FAILURE;
    }

    return EXIT_SUCCESS;
}
