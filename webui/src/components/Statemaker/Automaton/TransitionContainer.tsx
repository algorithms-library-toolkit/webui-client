import * as React from 'react'
import { batch, useDispatch, useSelector } from 'react-redux'

import useStatePosition from '../../../hooks/useStatePosition'
import { Actions, ReduxState } from '../../../reducers'
import { getEdgePoints } from '../../../utils/positioning'
import { hasTransitionError } from '../../../utils/errors'
import Transition from './Transition'
import { stateCanvasActions } from '../../../reducers/stateCanvas'

interface Props {
	/**Id of transition from data */
	id: string
	canvasRef: React.RefObject<SVGSVGElement>
}

const { setInput } = Actions
const { selectEdge: selectTransition } = stateCanvasActions

/**
 * Selects attributes from state
 *
 * @param {string} id Id of the transition
 */
const transitionSelector = (id: string) => (state: ReduxState) => {
	const states = state.stateData.present.states
	const transitions = Object.values(state.stateData.present.transitions)
	const transition = state.stateData.present.transitions[id]
	if (!transition) return null // Transition with id not found
	const { startState, endState } = transition

	// Find if there are transitions with same start and end states (or flipped ones)
	const hasReverseTransition = transition
		? transitions.some(
				t => t.startState === endState && t.endState === startState
		  )
		: false

	// State is being positioned
	const positioningState = state.stateCanvas.positioningNode
	const useLocalMove = [startState, endState].includes(positioningState)

	// Array of transitions going between the same states as this transitions
	// [...from -> to, ...to -> from]
	const sameTransitions = [
		...transitions.filter(
			t => t.startState === startState && t.endState === endState
		),
		...transitions.filter(
			t => t.startState === endState && t.endState === startState
		)
	]

	// Index of current transition on the array
	const offset =
		sameTransitions.length === 0
			? 0
			: sameTransitions.map(t => t.id).indexOf(id) -
			  Math.floor(sameTransitions.length / 2)

	return {
		data: transition,
		isSelected: state.stateCanvas.selectedEdge === id,
		startState: transition ? states[transition.startState] : undefined,
		endState: transition ? states[transition.endState] : undefined,
		hasError: hasTransitionError(
			state.stateData.present.transitions,
			transition
		),
		hasReverseTransition,
		cursorMode: state.stateCanvas.cursorMode,
		positioningState: state.stateCanvas.positioningNode,
		useLocalMove,
		offset
	}
}

function TransitionContainer(props: Props) {
	const { id, canvasRef } = props
	const dispatch = useDispatch()
	const state = useSelector(transitionSelector(id))

	const position = useStatePosition(canvasRef, state ? state.useLocalMove : true)

	const tId = state ? state.data.id : ''
	const tName = state ? state.data.name : ''
	const cursorMode = state ? state.cursorMode : 'select'
	const handleClick = React.useCallback(
		(e: React.MouseEvent<SVGGElement>) => {
			e.stopPropagation()
			// Disallow if mode not select
			if (cursorMode !== 'select') return
			// Select transition
			batch(() => {
				dispatch(selectTransition(tId))
				dispatch(setInput(tName))
			})
		},
		[dispatch, tId, tName, cursorMode]
	)

	if (!state) return null

	const {
		data,
		startState,
		endState,
		hasReverseTransition,
		isSelected,
		hasError,
		positioningState,
		useLocalMove,
		offset
	} = state

	// Return if start or end states are missing
	if (!startState || !endState) return null

	// Use local position if start or end states are being positioned
	if (useLocalMove) {
		if (startState.id === positioningState) {
			startState.position = position
		} else {
			endState.position = position
		}
	}
	// Find points on the edge of states
	const points = getEdgePoints(startState, endState)

	return (
		<Transition
			data={data}
			offset={offset}
			{...points}
			isSelected={isSelected}
			isIdentity={data.startState === data.endState}
			hasReverseTransition={hasReverseTransition}
			hasError={hasError}
			onClick={handleClick}
		/>
	)
}

export default TransitionContainer
