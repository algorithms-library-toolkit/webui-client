import * as React from 'react'
import { batch, useDispatch, useSelector } from 'react-redux'

import { makeStyles } from '@material-ui/styles'
import Input from '@material-ui/core/Input'
import Paper from '@material-ui/core/Paper'
import SvgIcon from '@material-ui/core/SvgIcon'
import RemoveIcon from '@material-ui/icons/Delete'

import { ReactComponent as InitialIcon } from '../../../icons/state_initial.svg'
import { ReactComponent as FinalIcon } from '../../../icons/state_final.svg'
import { Actions, ReduxState } from '../../../reducers'
import ElementButton from '../../Toolbar/ElementButton'
import { stateCanvasActions } from '../../../reducers/stateCanvas'


const { deselect } = stateCanvasActions

const {
	setInput,
	updateState,
	updateTransition,
	removeState,
	removeTransition,
	reset,
	toggleFinalState,
	toggleInitialState
} = Actions

const useStyles = makeStyles({
	root: {
		marginTop: 8,
		marginLeft: 16,
		paddingTop: 10,
		paddingBottom: 10,
		minHeight: 40,
		display: 'flex',
		alignItems: 'center'
	},
	input: {
		padding: '0 16px'
	},
	button: {
		height: 32,
		padding: '4px 12px',
		borderLeft: '1px solid #ddd',
		borderRadius: 0,
		'&:hover': {
			color: '#4285F4',
			backgroundColor: 'transparent',
			opacity: 0.8
		}
	},
	actions: {
		marginTop: 8,
		marginLeft: 16,
		alignSelf: 'flex-start',
		color: 'rgba(0, 0, 0, 0.54)'
	}
})

// Custom Initial state icon
const InitialStateIcon = () => (
	<SvgIcon>
		<InitialIcon />
	</SvgIcon>
)

// Custom Final state icon
const FinalStateIcon = () => (
	<SvgIcon>
		<FinalIcon />
	</SvgIcon>
)

/**
 * Menu shown when state or transition are selected
 * Allows manipulation with current element
 *
 * @returns
 */
function TextInput() {
	const dispatch = useDispatch()
	const classes = useStyles()

	const state = useSelector((state: ReduxState) => ({
		selectedState: state.stateCanvas.selectedNode,
		selectedTransition: state.stateCanvas.selectedEdge,
		input: state.stateUI.input,
		initialStates: state.stateData.present.initialStates,
		finalStates: state.stateData.present.finalStates
	}))

	const {
		selectedState,
		selectedTransition,
		input,
		initialStates,
		finalStates
	} = state

	// Sets selected element's name based on input value
	const handleChange = React.useCallback(
		(e: React.ChangeEvent<HTMLInputElement>) => {
			// Don't propagate key events further
			e.stopPropagation()
			e.nativeEvent.stopImmediatePropagation()
			const value = e.currentTarget.value

			batch(() => {
				dispatch(setInput(value))
				if (selectedState) {
					dispatch(
						updateState(selectedState, {
							name: value
						})
					)
				} else {
					dispatch(
						updateTransition(selectedTransition, {
							name: value
						})
					)
				}
			})
		},
		[dispatch, selectedState, selectedTransition]
	)

	// Don't propagate click
	const handleClick = React.useCallback(
		(e: React.MouseEvent<HTMLInputElement>) => {
			e.stopPropagation()
			e.nativeEvent.stopImmediatePropagation()
		},
		[]
	)

	// Set state as initial
	const handleInitialClick = React.useCallback(
		(e: React.MouseEvent<HTMLButtonElement>) => {
			e.nativeEvent.stopImmediatePropagation()
			dispatch(toggleInitialState(selectedState))
		},
		[dispatch, selectedState]
	)

	// Set state as final
	const handleFinalClick = React.useCallback(
		(e: React.MouseEvent<HTMLButtonElement>) => {
			e.nativeEvent.stopImmediatePropagation()
			dispatch(toggleFinalState(selectedState))
		},
		[dispatch, selectedState]
	)

	// Delete selected element and deselect it
	const handleRemoveClick = React.useCallback(
		(e: React.MouseEvent<HTMLButtonElement>) => {
			e.nativeEvent.stopImmediatePropagation()
			batch(() => {
				if (selectedState) {
					dispatch(removeState(selectedState))
				} else {
					dispatch(removeTransition(selectedTransition))
				}
				dispatch(deselect())
				dispatch(reset())
			})
		},
		[dispatch, selectedState, selectedTransition]
	)

	// Handle keyboard events when focused on text input
	const handleKey = React.useCallback(
		(event: React.KeyboardEvent<HTMLInputElement>) => {
			// Used for deselect
			if (
				(event.key === 'Enter' && !event.shiftKey) ||
				event.key === 'Escape'
			) {
				event.preventDefault()
			} else if (!event.ctrlKey) {
				// Catch ctrl key events
				event.stopPropagation()
				event.nativeEvent.stopImmediatePropagation()
			}
			// Use Shift Eneter as new line
			if (event.key === 'Enter' && event.shiftKey) {
				const value = event.currentTarget.value

				batch(() => {
					dispatch(setInput(value))
					if (selectedState) {
						dispatch(
							updateState(selectedState, {
								name: value + '\n'
							})
						)
					} else {
						dispatch(
							updateTransition(selectedTransition, {
								name: value + '\n'
							})
						)
					}
				})
			}
		},
		[dispatch, selectedState, selectedTransition]
	)

	// Don't show if no selected state or transition
	if (!selectedState && !selectedTransition) {
		return null
	}

	return (
		<React.Fragment>
			<Paper className={classes.root}>
				<Input
					className={classes.input}
					type="text"
					onChange={handleChange}
					onKeyDown={handleKey}
					value={input}
					onClick={handleClick}
					disableUnderline={true}
					autoFocus={true}
					multiline={true}
				/>
			</Paper>
			{selectedState && (
				<Paper className={classes.actions}>
					<ElementButton
						label="Set as Initial state"
						selected={initialStates.includes(selectedState)}
						onAction={handleInitialClick}
						Icon={InitialStateIcon}
					/>
					<ElementButton
						label="Set as Final state"
						selected={finalStates.includes(selectedState)}
						onAction={handleFinalClick}
						Icon={FinalStateIcon}
					/>
				</Paper>
			)}
			<Paper className={classes.actions}>
				<ElementButton
					label="Remove element"
					onAction={handleRemoveClick}
					Icon={RemoveIcon}
				/>
			</Paper>
		</React.Fragment>
	)
}

export default TextInput
