import { Algorithm, AlgorithmCategory, Overload } from 'interfaces/Algorithms'
import { Filter } from 'interfaces/Filter'

/**
 * Check if overload match some filter
 * @param overloads
 * @param filters
 * @param value
 */
export const filterAll = (overloads: Overload[], filters: Filter[], value: string) : boolean => {
    for (let filter of filters)
        if (overloads.some((overload) => filter.filter(overload, value)))
            return true

    return false
}

/**
 * Check if category matches the name filter
 * @param category
 * @param value
 */
export const filterCategory = (category: string, value: string) : boolean => {
    return category.toLocaleUpperCase().includes(value.toLocaleUpperCase())
}

/**
 * Check if filter matches some overload in list of algorithms
 * - doesn't use name filter, this function is filtering only in algorithm's overloads
 * @param algorithms
 * @param filters
 * @param value
 */
export const filterAlgorithms = (algorithms: Algorithm[], filters: Filter[], value: string) : boolean => {
    const algoFilters = filters.filter((filter) => !filter.isNameFilter())
    return algorithms.some((algorithm) =>
        algorithm.overloads.some((overload) =>
            filterAll(overload, algoFilters, value)))
}

/**
 * Look the filter match up in different levels of categories - name of category, subcategory, algorithms
 * @param filterValue
 * @param algorithmCategory
 * @param filters
 */
const filterLevels = (filterValue: string, algorithmCategory: AlgorithmCategory, filters: Filter[]) => {
    //check category name if name filter is applied
    let include = false
    if (filters.some((filter) => filter.isNameFilter())) {
        include = filterCategory(algorithmCategory.categoryName, filterValue)
    }

    //check subcategory
    const includeSubcategories = filterSubcategories(algorithmCategory, filters, filterValue)

    //if current subcategory contains algorithms check matches with all filters
    let includeOverloads = false
    if (algorithmCategory.algorithms.length > 0) {
        includeOverloads = filterAlgorithms(algorithmCategory.algorithms, filters, filterValue)
    }

    return { include, includeSubcategories, includeOverloads }
}

/**
 * Check if something matches filter somewhere in the subcategory
 * @param algorithmCategory
 * @param filters
 * @param value
 */
export const filterSubcategories = (
    algorithmCategory: AlgorithmCategory,
    filters: Filter[],
    value: string
): boolean => {
    return algorithmCategory.subcategories.some(
        (algorithmSubcategory) => {
            const { include, includeSubcategories, includeOverloads } =
                filterLevels(value, algorithmSubcategory, filters)

            return include || includeSubcategories || includeOverloads
        }
    )
}

/**
 * Check if (sub)category will be rendered deciding on matching some of the filters
 * - return '' for matching name filter and continuing with subcategories
 * - return filterValue when filter matches something in the subcategory
 * - return null for category that does not match any filter and shouldn't be rendered
 * @param filterValue
 * @param algorithmCategory
 * @param filters
 */
export const nextFilterValue = (filterValue: string, algorithmCategory: AlgorithmCategory, filters: Filter[]) => {
    if (filterValue !== '') {
        const { include, includeSubcategories, includeOverloads } =
            filterLevels(filterValue, algorithmCategory, filters)

        if (!(include || includeSubcategories || includeOverloads))
            return null

        if (!include && (includeSubcategories || includeOverloads))
            return filterValue
        return ''
    }

    return ''
}

