export const automaton = `
start
	= _ type:type _ alphabet:alphabet _ nl states:(row nl)* eof
	{
		return {
			type: type,
			alphabet: alphabet,
			states: states.reduce(function(result, element) {
				return result.concat(element[0]);
			 }, [])
			}
	}

alphabet
	= contents:( symbol_or_epsilon ( _ symbol_or_epsilon )* )?
		{
			return contents === null ? [] :
				contents[1].reduce(function(result, element) { return result.concat(element[1]); }, [contents[0]]);
		}

row
	= _ props:state_props? _ name:state _ tr:transitions _
	{
		return { props: props || undefined, name: name, transitions: tr }
	}

state_props
	= "><" { return "inout"; }
	/ "<>" { return "inout"; }
   	/ "<" { return "out"; }
	/ ">" { return "in"; }

state
	= symbol

transitions
	= contents:( outgoing_transition ( _ outgoing_transition )* )?
	{
		return contents === null ? [] :
			contents[1].reduce(function(result, element) {
				return result.concat([element[1]]);
			}, [contents[0]]);
	}

outgoing_transition
	= "-" { return []; }
	/ head:state tail:( "|" state )*
	{
		return tail.reduce(function(result, element) {
			return result.concat(element[1]);
		}, [head]);
	}

comma_delimited_symbol_sequence
	= _ head:symbol? tail:( _ "," _ symbol )* _

	  {
		  return tail.reduce(function(result, element) { return result.concat(element[3]); }, [head]);
	  }

symbol_or_epsilon
	= symbol
	/ "#E"

symbol
	= symbol:symbol_seq cont:symbol_cont?
    { return symbol + (cont !== null ? cont : ""); }

symbol_seq
    = characters:[_a-z0-9]i+ { return characters.join(""); }
    / _ "{" _ symbols:comma_delimited_symbol_sequence _ "}" _ { return "{" + symbols.join(", ") +  "}"; }
    / _ "(" _ symbols:comma_delimited_symbol_sequence _ ")" _ { return "(" + symbols.join(", ") +  ")"; }
    / _ "<" _ symbols:comma_delimited_symbol_sequence _ ">" _ { return "<" + symbols.join(", ") +  ">"; }
    / _ "[" _ symbols:comma_delimited_symbol_sequence _ "]" _ { return "[" + symbols.join(", ") +  "]"; }

symbol_cont
	= [']

type
	= 'DFA'
	/ 'NFA'
	/ 'MISNFA'
	/ 'ENFA'
	/ 'MISENFA'

_
	= [ \\t]*

nl
	= [\\r\\n]*

eof
	= !.
`

