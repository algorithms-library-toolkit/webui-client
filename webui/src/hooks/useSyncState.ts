import { useState } from 'react'

/**
 * UseState wrapper with getter of current value
 * source: https://dev.to/bytebodger/synchronous-state-with-react-hooks-1k4f
 * @param initialState
 */
export const useSyncState = <T> (initialState: T): [() => T, (newState: T) => void] => {
	const [state, updateState] = useState(initialState)
	let current = state
	const get = (): T => current
	const set = (newState: T) => {
		current = newState
		updateState(newState)
		return current
	}
	return [get, set]
}