#include <activemq/library/ActiveMQCPP.h>
#include <argparse/argparse.hpp>
#include <chrono>
#include <filesystem>
#include <iostream>
#include <json/json.h>
#include <json/value.h>
#include <thread>
#include "introspection/introspect.hpp"
#include "utils/json.hpp"
#include "utils/log-sinks.hpp"
#include "utils/log.hpp"
#include "utils/process.hpp"
#include "worker/MessageProcessor.hpp"

using namespace std::string_literals;

namespace {
static const auto ARG_BROKER = "--broker";
static const auto ARG_EVALUATOR = "--evaluator";
static const auto ARG_TIMEOUT = "--timeout";
static const auto ARG_LOG_LEVEL = "--log-level";
static const auto ARG_LOG_LEVEL_SHORT = "-v";
static const auto ARG_LOG_STDERR = "--log-stderr";

}

void validateEvaluator(const std::filesystem::path& pathToEvaluator)
{
    if (!std::filesystem::exists(pathToEvaluator))
        throw std::runtime_error("Path to evaluator is not a valid path");
}

int main(int argc, char* argv[])
{
    argparse::ArgumentParser program("alt-webui-worker");

    program.add_argument(ARG_BROKER)
        .help("URL of the broker to connect to.")
        .default_value("tcp://localhost:61616"s);

    program.add_argument(ARG_EVALUATOR)
        .help("Path to the graph evaluator executable")
        .default_value("./alt-webui-worker-evaluator"s);

    program.add_argument(ARG_TIMEOUT)
        .help("Timeout (in milliseconds) of a single job.")
        .action([](const std::string& val) { return std::stoul(val); })
        .default_value(static_cast<long unsigned int>(15000));

    program.add_argument(ARG_LOG_LEVEL_SHORT, ARG_LOG_LEVEL)
        .help("Log level. Values: 0 (off), 1 (error), ..., 5 (trace).")
        .default_value(3)
        .action([](const std::string& val) { return std::stoi(val); });

    program.add_argument(ARG_LOG_STDERR)
        .help("Log warnings and errors to stderr instead of stdout.")
        .default_value(false)
        .implicit_value(true);

    int returnCode = EXIT_SUCCESS;
    std::thread thrAlgorithmListWorker;
    activemq::library::ActiveMQCPP::initializeLibrary();

    try {
        program.parse_args(argc, argv);

        if (program.get<bool>(ARG_LOG_STDERR)) {
            auto sink = std::make_shared<loglevel_dist_sink_st>();
            sink->add_sink(std::make_shared<spdlog::sinks::stdout_sink_st>(), spdlog::level::trace, spdlog::level::info);
            sink->add_sink(std::make_shared<spdlog::sinks::stderr_sink_st>(), spdlog::level::warn, spdlog::level::n_levels);

            // set default logger's sink
            spdlog::default_logger()->sinks().clear();
            spdlog::default_logger()->sinks().push_back(sink);
        }

        auto brokerURL = program.get<std::string>(ARG_BROKER);
        auto timeout = program.get<long unsigned int>(ARG_TIMEOUT);
        auto evaluator = program.get<std::string>(ARG_EVALUATOR);

        auto logLevel = parseLogLevel(program.get<int>(ARG_LOG_LEVEL));
        spdlog::set_level(logLevel);
        validateEvaluator(evaluator);

        spdlog::info("Connecting to broker {}", brokerURL);
        spdlog::info("Timeout set to {} milliseconds", timeout);
        spdlog::info("Evaluator is: {}", evaluator);
        spdlog::info("Log level is: {}", spdlog::level::to_string_view(logLevel));

        auto evaluateGraphWorker = std::make_unique<MessageProcessor>(brokerURL, "EvaluateGraph", "response", [&timeout, &evaluator](const long& id, const std::string& message) {
            std::string resultJsonText;
            Json::Value reply;

            try {
                resultJsonText = utils::execAndWait(evaluator, {}, message, std::chrono::milliseconds(timeout));
                reply = parseJson(resultJsonText);
            } catch (const utils::ExecTimeoutError& e) {
                reply = Json::objectValue;
                reply["error"][0] = "Time limit exceeded.";

                spdlog::warn("{}: Evaluator timeouted", id);
                spdlog::warn("{}: {}", id, message);
            } catch (const utils::ExecReturnCodeError& e) {
                reply = Json::objectValue;
                reply["error"][0] = "Internal error. Something went bad on our side.";

                spdlog::error("{}: Evaluator did not exited successfully ({})", id, e.what());
                spdlog::error("{}: {}", id, message);
            }

            auto jsonReply = writeJson(reply);
            spdlog::trace("{}: {}", id, jsonReply);
            return jsonReply;
        });

        auto introspectCache = introspect();
        auto algorithmListWorker = std::make_unique<MessageProcessor>(brokerURL, "WorkerDefinitions", "response", [&introspectCache](auto, auto) {
            return introspectCache;
        });

        thrAlgorithmListWorker = std::thread([&]() { algorithmListWorker->listen(); });
        evaluateGraphWorker->listen();
        thrAlgorithmListWorker.join();
    } catch (const std::exception& e) {
        spdlog::error("{}", e.what());
        returnCode = EXIT_FAILURE;
    }

    spdlog::info("Shutting down");
    activemq::library::ActiveMQCPP::shutdownLibrary();

    return returnCode;
}
