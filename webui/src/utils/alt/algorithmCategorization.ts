import { Map } from 'immutable'
import { Algorithm, AlgorithmALT } from 'interfaces/Algorithms'
import { AlgorithmCategory } from 'interfaces/Algorithms'
import { categorizeOverloads } from 'utils/alt/overloads'

const createCategory = (
	categories: string[],
	algorithms: Algorithm[]
): AlgorithmCategory => {
	const [category, ...subcategories] = categories

	if (subcategories.length === 0)
		return {
			categoryName: category,
			subcategories: Map(),
			tags: [],
			algorithms,
		}

	return {
		categoryName: category,
		subcategories: Map<string, AlgorithmCategory>().set(
			subcategories[0],
			createCategory(subcategories, algorithms)
		),
		algorithms: [],
		tags: [],
	}
}

const addAlgorithm = (
	algorithmCategories: Map<string, AlgorithmCategory>,
	categories: string[],
	algorithm: AlgorithmALT
): Map<string, AlgorithmCategory> => {
	const [category, ...subcategories] = categories
	const algorithmCategory = algorithmCategories.get(category)

	if (!algorithmCategory) {
		const resultAlgorithm: Algorithm = {
			name: algorithm.name,
			templateParams: algorithm.templateParams,
			overloads: categorizeOverloads(algorithm.overloads),
			docs: algorithm.docs
		}
		// create new category
		return algorithmCategories.set(
			category,
			createCategory(categories, [resultAlgorithm])
		).sort((a, b) => a.categoryName.localeCompare(b.categoryName))
	}

	if (subcategories.length === 0) {
		// no more categories to delve into, add algorithm here
		const resultAlgorithm: Algorithm = {
			name: algorithm.name,
			templateParams: algorithm.templateParams,
			overloads: categorizeOverloads(algorithm.overloads),
			docs: algorithm.docs
		}
		algorithmCategory.algorithms.push(resultAlgorithm)
		algorithmCategory.algorithms.sort((a, b) => a.name.localeCompare(b.name))
		return algorithmCategories
	}

	algorithmCategory.subcategories = addAlgorithm(
		algorithmCategory.subcategories,
		subcategories,
		algorithm
	)

	return algorithmCategories
}

export const categorizeAlgorithms = (algorithms: AlgorithmALT[]) =>
	algorithms.reduce((acc, algorithm) => {
		const categories = algorithm.name.split('::')

		if (algorithm.templateParams.length > 0)
			categories.push(
				algorithm.templateParams
					.reduce((acc, typeParam) => `${acc}, @${typeParam}`, '')
					.slice(1)
			)

		return addAlgorithm(acc, categories, algorithm)
	}, Map<string, AlgorithmCategory>())

