import GrammarData, { dataToGrammar, grammarToString, GrammarType, Rule } from 'interfaces/GrammarData'
import { Set } from 'immutable'

//TEST DATA FOR RIGHT_RG
const initialSymbolRG: string = 'S'

const typeRG: GrammarType = 'RIGHT_RG'

const rulesRG: Rule[] = [
	{ left: 'S', right: 'a A | bb B |' },
	{ left: 'A', right: 'a A | bb B' },
	{ left: 'B', right: 'bb C' },
	{ left: 'C', right: 'ca A | a' }
]

const expectedNonterminalsRG: Set<string> = Set.of('S', 'A', 'B', 'C' )
const expectedTerminalsRG: Set<string> = Set.of( 'a', 'bb', 'ca')

const grammarRG: GrammarData = {
	initialSymbol: initialSymbolRG,
	type: typeRG,
	rules: rulesRG,
	nonterminals: expectedNonterminalsRG,
	terminals: expectedTerminalsRG
}

const stringGrammarRG = 'RIGHT_RG(' +
	'{S,A,B,C},' +
	'{a,bb,ca},' +
	'{' +
	'S -> a A | bb B |,' +
	'A -> a A | bb B,' +
	'B -> bb C,' +
	'C -> ca A | a' +
	'},' +
	'S)'

//TEST DATA FOR RIGHT_RG
const initialSymbolRG2: string = 'S\''

const typeRG2: GrammarType = 'RIGHT_RG'

const rulesRG2: Rule[] = [
	{ left: 'S\'', right: 'a Abcde | bb B\' |' },
	{ left: 'Abcde', right: 'a Abcde | bb B\'' },
	{ left: 'B\'', right: 'bb C' },
	{ left: 'C', right: '[c Abcde | a' }
]

const expectedNonterminalsRG2: Set<string> = Set.of('S\'', 'Abcde', 'B\'', 'C' )
const expectedTerminalsRG2: Set<string> = Set.of( 'a', 'bb', '[c')

const grammarRG2: GrammarData = {
	initialSymbol: initialSymbolRG2,
	type: typeRG2,
	rules: rulesRG2,
	nonterminals: expectedNonterminalsRG2,
	terminals: expectedTerminalsRG2
}

const stringGrammarRG2 = 'RIGHT_RG(' +
	'{S\',Abcde,B\',C},' +
	'{a,bb,[c},' +
	'{' +
	'S\' -> a Abcde | bb B\' |,' +
	'Abcde -> a Abcde | bb B\',' +
	'B\' -> bb C,' +
	'C -> [c Abcde | a' +
	'},' +
	'S\')'

//TEST DATA FOR RIGHT_RG
const initialSymbolRG3: string = 'S'

const typeRG3: GrammarType = 'RIGHT_RG'

const rulesRG3: Rule[] = [
	{ left: 'A', right: 'b' }
]

const expectedNonterminalsRG3: Set<string> = Set.of('S', 'A')
const expectedTerminalsRG3: Set<string> = Set.of('b')

const grammarRG3: GrammarData = {
	initialSymbol: initialSymbolRG3,
	type: typeRG3,
	rules: rulesRG3,
	nonterminals: expectedNonterminalsRG3,
	terminals: expectedTerminalsRG3
}

//TEST DATA FOR CSG
const initialSymbolCSG: string = 'S'

const typeCSG: GrammarType = 'CSG'

const rulesCSG: Rule[] = [
	{ left: '| S |', right: 'a S B C | a B C' },
	{ left: '|B| C', right: 'a a C' },
	{ left: 'a |B| c', right: 'a C C c' },
	{ left: '|C|', right: 'c' }
]

const expectedNonterminalsCSG: Set<string> = Set.of('S', 'A', 'B' )
const expectedTerminalsCSG: Set<string> = Set.of( 'a', 'c')

const grammarCSG: GrammarData = {
	initialSymbol: initialSymbolCSG,
	type: typeCSG,
	rules: rulesCSG,
	nonterminals: expectedNonterminalsCSG,
	terminals: expectedTerminalsCSG
}

const stringGrammarCSG = 'CSG(' +
	'{S,A,B},' +
	'{a,c},' +
	'{' +
	'| S | -> a S B C | a B C,' +
	'|B| C -> a a C,' +
	'a |B| c -> a C C c,' +
	'|C| -> c' +
	'},' +
	'S)'

test('Create GrammarData from initial symbol, rules and data', () => {
	expect(dataToGrammar(initialSymbolRG, typeRG, rulesRG)).toEqual(grammarRG)
})

test('RIGHT_RG GrammarData to ALT string format', () => {
	expect(grammarToString(grammarRG)).toEqual(stringGrammarRG)
})

test('CSG GrammarData to ALT string format', () => {
	expect(grammarToString(grammarCSG)).toEqual(stringGrammarCSG)
})

test('Create GrammarData from initial symbol, rules and data with various characters', () => {
	expect(dataToGrammar(initialSymbolRG2, typeRG2, rulesRG2)).toEqual(grammarRG2)
})

test('RIGHT_RG GrammarData with various characters', () => {
	expect(grammarToString(grammarRG2)).toEqual(stringGrammarRG2)
})

test('Initial symbol is not in the rules', () => {
	expect(dataToGrammar(initialSymbolRG3, typeRG3, rulesRG3)).toEqual(grammarRG3)
})