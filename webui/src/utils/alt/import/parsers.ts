import { Set } from 'immutable'
import peggy from 'peggy'
import Automaton from 'interfaces/Automaton'
import GrammarData from 'interfaces/GrammarData'
import { grammar as pegForGrammar } from 'utils/alt/import/grammars/grammar'
import { automaton as pegForAutomaton } from 'utils/alt/import/grammars/automaton'

function createParser(peg: string) {
    return peggy.generate(peg, {
        grammarSource: peg,
        output: 'parser',
    })
}

function checkForDuplicates(array: string[]) {
	let map = new Map<string, number>()

	for (let symb of array) {
		if (!map.has(symb)) {
			map.set(symb, 0)
		}
		map.set(symb, map.get(symb)! + 1)
	}

	let res: string[] = []
	map.forEach((v, k) => {
		if (v > 1) {
			res.push(k)
		}
	})

	return res
}

const grammarParser = createParser(pegForGrammar)
const automatonParser = createParser(pegForAutomaton)

export function parseGrammar(input: string): GrammarData {
    const res = grammarParser.parse(input)
	return { ...res, nonterminals: Set.of(...res.nonterminals), terminals: Set.of(...res.terminals) } // FIXME: Create set in the parser
}

function toStateData(parsedAutomaton: any): Automaton {
	if (!parsedAutomaton.states.every((state: any) => state.transitions.length === parsedAutomaton.alphabet.length)) {
		throw Error("Invalid row transitions length")
	}

	if (parsedAutomaton.states.filter((state: any) => state.props === "in" || state.props === "inout").length === 0) {
		throw Error("No initial state")
	}

	const duplicateStates = checkForDuplicates(parsedAutomaton.states.map((state: any) => state.name))
	if (duplicateStates.length > 0) {
		throw Error(`States ${JSON.stringify(duplicateStates)} are specified multiple times`)
	}

	const duplicateSymbols = checkForDuplicates(parsedAutomaton.alphabet)
	if (duplicateSymbols.length > 0) {
		throw Error(`Symbols ${JSON.stringify(duplicateSymbols)} are specified multiple times`)
	}

	const transitions = parsedAutomaton.states.reduce((acc: any[], state: any) =>
		[
			...acc,
			...state.transitions.reduce((acc: any[], targets: any, idx: number) => [
									...acc,
									...targets.map((target: any) => {
										return {
											symbol: parsedAutomaton.alphabet[idx],
											from: state.name,
											to: target
										}
									})], [])
		], [])

	return {
		type: parsedAutomaton.type,
		initialStates: parsedAutomaton.states.filter((state: any)=> ["in", "inout"].includes(state.props)).map((state: any) => state.name),
		finalStates: parsedAutomaton.states.filter((state: any) => ["out", "inout"].includes(state.props)).map((state: any) => state.name),
		states: parsedAutomaton.states.map((state: any) => state.name),
		alphabet: parsedAutomaton.alphabet,
		transitions: transitions,
	}
}

export function parseAutomaton(input: string): Automaton {
    return toStateData(automatonParser.parse(input))
}
