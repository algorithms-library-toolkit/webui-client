export interface EvaluateRequest {
  customFunctions: string[],
  graph: {
    algorithms: {
      [id: string]: {
        name: string
      }
    }
    inputs: {
      strings?: {
        [id: string]: {
          value: string
        }
      }
      ints?: {
        [id: string]: {
          value: number // must be integer
        }
      }
      doubles?: {
        [id: string]: {
          value: number // must be double
        }
      }
      bools?: {
        [id: string]: {
          value: boolean
        }
      }
    }
    outputs: string[] // array of IDs
    pipes: {
      from: string // ID of the source node
      to: string // ID of the destination node
      paramIndex: number // index of the parameter in the destination node
    }[]
  }
}