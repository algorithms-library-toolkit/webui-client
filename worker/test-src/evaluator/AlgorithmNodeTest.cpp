#include "testing.hpp"
#include <environment/Environment.h>
#include <list>
#include <string>
#include "graph/nodes/AbstractNode.hpp"
#include "graph/nodes/AlgorithmNode.hpp"
#include "graph/nodes/BoolNode.hpp"
#include "graph/nodes/IntNode.hpp"
#include "utils/alt.hpp"

class AlgorithmTestNodeMock : public AbstractNode {
public:
    AlgorithmTestNodeMock()
        : AbstractNode("", ext::vector<std::string>())
    {
    }

    explicit AlgorithmTestNodeMock(const std::string& value)
        : AbstractNode("", ext::vector<std::string>())
    {
        this->setParam(std::make_shared<abstraction::ValueHolder<std::string>>(std::string(value), true), 0);
    }

    void evaluate(abstraction::TemporariesHolder& environment) override
    {
        passResultToChilds(params[0]);
    }

    bool isOutput() const override
    {
        return false;
    }

    bool isInput() const override
    {
        return false;
    }
};

TEST_CASE("AlgorithmNode evaluate")
{
    std::string automaton = "NFA 0 1\n>0 0|1 1\n<1 - -\n";
    auto node = std::make_shared<AlgorithmNode>("1", "string::Parse", ext::vector<std::string> {"automaton::Automaton"});
    auto mockInputNode = std::make_shared<AlgorithmTestNodeMock>(automaton);
    auto mockOutputNode = std::make_shared<AlgorithmTestNodeMock>();

    mockInputNode->createEdge(node, 0);
    node->createEdge(mockOutputNode, 0);

    cli::Environment environment;
    mockInputNode->evaluate(environment);
    node->evaluate(environment);


    REQUIRE(mockOutputNode->getParams()[0]);
    REQUIRE(abstractValueToString(mockOutputNode->getParams()[0]) == "(NFA states = {0, 1} inputAlphabet = {0, 1} initialState = 0 finalStates = {1} transitions = {((0, 0), 0), ((0, 0), 1), ((0, 1), 1)})\n");
}

TEST_CASE("AlgorithmNode multiple params evaluate")
{
    auto alphabetSize = std::make_shared<IntNode>("", 4, ext::vector<std::string>());
    auto randomizeAlphabet = std::make_shared<BoolNode>("", true, ext::vector<std::string>());
    auto lowerCase = std::make_shared<BoolNode>("", false, ext::vector<std::string>());
    auto randomAlphabet = std::make_shared<AlgorithmNode>("", "alphabet::generate::GenerateAlphabet", ext::vector<std::string>());

    alphabetSize->createEdge(randomAlphabet, 0);
    randomizeAlphabet->createEdge(randomAlphabet, 1);
    lowerCase->createEdge(randomAlphabet, 2);

    auto stringSize = std::make_shared<IntNode>("", 10, ext::vector<std::string>());
    auto randomString = std::make_shared<AlgorithmNode>("", "string::generate::RandomStringFactory", ext::vector<std::string>());
    auto mockOutputNode = std::make_shared<AlgorithmTestNodeMock>();

    stringSize->createEdge(randomString, 0);
    randomAlphabet->createEdge(randomString, 1);
    randomString->createEdge(mockOutputNode, 0);

    cli::Environment environment;
    alphabetSize->evaluate(environment);
    randomizeAlphabet->evaluate(environment);
    lowerCase->evaluate(environment);
    randomAlphabet->evaluate(environment);
    stringSize->evaluate(environment);
    randomString->evaluate(environment);
    mockOutputNode->evaluate(environment);

    REQUIRE(mockOutputNode->getParams().size() == 1);
    REQUIRE(mockOutputNode->getParams()[0]);
    REQUIRE(abstractValueToString(mockOutputNode->getParams()[0]).starts_with("(LinearString content"));
}
