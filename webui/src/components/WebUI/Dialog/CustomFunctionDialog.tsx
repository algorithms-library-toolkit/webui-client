import React, { useCallback, useState, useMemo, useLayoutEffect } from 'react'
import { batch, useDispatch, useSelector } from 'react-redux'
import {
	Dialog,
	DialogActions,
	Button,
	DialogTitle,
	IconButton,
	makeStyles,
	Typography,
	Divider,
	TextField,
	DialogContent,
	Grid,
	InputLabel
} from '@material-ui/core'
import CloseIcon from '@material-ui/icons/Close'
import { ReduxState } from 'reducers'
import { algorithmUIActions } from 'reducers/algorithmUI'
import {
	dataToCustomFunction,
	functionParamsToString,
	isEmptyFunction
} from 'interfaces/CustomFunction'
import { algorithmDataActions } from 'reducers/algorithmData'
import {useSnackbar} from "notistack";

const {
	closeCustomFunctionDialog
} = algorithmUIActions

const {
	setCustomFunctionNode
} = algorithmDataActions

const useStyles = makeStyles({
	dialogPaper: {
		width: '100%',
		overflow: 'visible'
	},
	dialogTitle: {
		display: 'flex',
		justifyContent: 'space-between',
		alignItems: 'center',
		padding: '4px 4px 0px 24px'
	},
	textField: {
		width: '100%',
		height: '100%'
	},
	dialogContent: {
		display: 'flex',
		flexDirection: 'column'
	}
})

const CustomFunctionDialog = () => {
	const [name, setName] = useState<string>('')
	const [parameters, setParameters] = useState<string>('')
	const [resultType, setResultType] = useState<string>('')
	const [functions, setFunctions] = useState<string>('')

	const dispatch = useDispatch()
	const classes = useStyles()
	const dialogClasses = useMemo(
		() => ({
			paper: classes.dialogPaper
		}),
		[classes.dialogPaper]
	)

	const { open, initialValue, editedNode } = useSelector(
		(state: ReduxState) => ({
			open: state.algorithmUI.customFunctionDialog.open,
			initialValue: state.algorithmUI.customFunctionDialog.initialValue,
			editedNode: state.algorithmData.present.nodes.get(
				state.algorithmUI.customFunctionDialog.editedNodeId
			)
		})
	)

	const { enqueueSnackbar } = useSnackbar()

	useLayoutEffect(() => {
		if (!open) return

		if (!isEmptyFunction(initialValue.name, functionParamsToString(initialValue.params), initialValue.resultType, initialValue.body)) {
			setName(initialValue.name)
			setParameters(functionParamsToString(initialValue.params))
			setResultType(initialValue.resultType)
			setFunctions(initialValue.body)
		} else {
			setName('')
			setParameters('')
			setResultType('')
			setFunctions('')
		}

	}, [open, initialValue])

	const handleClose = useCallback(() => {
		dispatch(closeCustomFunctionDialog())
	}, [dispatch])


	const handleKeyDown = useCallback(
		(e: React.KeyboardEvent) => e.stopPropagation(),
		[]
	)

	const handleNameChange = useCallback(
		(e: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>) =>
			setName(e.currentTarget.value),
		[]
	)

	const handleParameterChange = useCallback(
		(e: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>) =>
			setParameters(e.currentTarget.value),
		[]
	)

	const handleResultTypeChange = useCallback(
		(e: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>) =>
			setResultType(e.currentTarget.value),
		[]
	)

	const handleFunctionChange = useCallback(
		(e: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>) =>
			setFunctions(e.currentTarget.value),
		[]
	)

	const checkFields = useCallback(() => {
		if (isEmptyFunction(name, parameters, resultType, functions)) {
			enqueueSnackbar('Please fill in all fields', { variant: 'error' })
			return false
		}
		return true
		}, [name, parameters, resultType, functions]
	)

	const handleApply = useCallback(
		() =>
			batch(() => {
				if (!checkFields())
					return

				if (editedNode)
					try {
						dispatch(setCustomFunctionNode(editedNode.id, dataToCustomFunction(name, parameters, resultType, functions)))
					} catch(e: any) {
						enqueueSnackbar(e.message, { variant: 'error' })
						return
					}

				handleClose()
			}),
		[
			dispatch,
			handleClose,
			editedNode,
			name,
			parameters,
			resultType,
			functions
		]
	)

	return (
		<Dialog
			hideBackdrop={true}
			disableEnforceFocus
			style={{ pointerEvents: 'none' }}
			PaperProps={{ style: { pointerEvents: 'auto' } }}
			classes={dialogClasses}
			fullWidth
			scroll="body"
			open={open}
			onClose={handleClose}
			onKeyDown={handleKeyDown}
			maxWidth="sm"
		>
			<DialogTitle disableTypography className={classes.dialogTitle}>
				<Typography variant="h6">Custom function editor</Typography>
				<IconButton onClick={handleClose}>
					<CloseIcon />
				</IconButton>
			</DialogTitle>
			<Divider />
			<DialogContent className={classes.dialogContent}>
				<Grid container direction={'column'} spacing={2}>
						<Grid item xs={12}>
							<InputLabel id='functionNameLabel'>Function name</InputLabel>
							<TextField
								className={classes.textField}
								variant='filled'
								color='primary'
								size='small'
								value={name}
								onChange={handleNameChange}
								placeholder='myFunction'
							/>
						</Grid>
						<Grid item xs={12}>
							<InputLabel id='parametersLabel'>Parameters</InputLabel>
							<TextField
								className={classes.textField}
								variant='filled'
								color='primary'
								size='small'
								value={parameters}
								onChange={handleParameterChange}
								placeholder='type $name, ...'
							/>
						</Grid>
					<Grid item container xs={12}>
						<InputLabel id='returnTypeLabel'>Return type</InputLabel>
						<TextField
							className={classes.textField}
							variant='filled'
							color='primary'
							size='small'
							value={resultType}
							onChange={handleResultTypeChange}
							placeholder='type'
						/>
					</Grid>
					<Grid item container xs={12}>
						<InputLabel id='functionLabel'>Function body</InputLabel>
						<TextField
							className={classes.textField}
							variant="filled"
							multiline
							value={functions}
							onChange={handleFunctionChange}
							InputLabelProps={{
								shrink: true
							}}
							placeholder='begin ... end'
						/>
					</Grid>
				</Grid>
			</DialogContent>
			<DialogActions>
				<Button onClick={handleApply} color="primary">
					Done
				</Button>
			</DialogActions>
		</Dialog>
	)
}

export default CustomFunctionDialog
