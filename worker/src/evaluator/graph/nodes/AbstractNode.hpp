#pragma once
#include <abstraction/TemporariesHolder.h>
#include <ext/vector>
#include <json/value.h>
#include <memory>
#include <optional>
#include <stdexcept>
#include <string>
#include "core/type_details.hpp"
#include "graph/Edge.hpp"

class NodeException : public std::logic_error {
public:
    explicit NodeException(const std::string& msg)
        : std::logic_error(msg)
    {
    }
};

/**
 * Abstract class representing a single node in algorithm graph
 */
class AbstractNode {
public:
    /**
     * Constructs a node with id and given template parameters
     * @param id
     * @param templateParams
     */
    AbstractNode(const std::string& id, const ext::vector<std::string>& templateParams);
    AbstractNode(const std::string& id, const Json::Value& json);

    virtual ~AbstractNode() = default;

    /**
     * Creates an edge to a given node and parameter index
     * @param to Node to create an edge to
     * @param paramIndex index of the parameter in the destination node
     */
    void createEdge(const std::shared_ptr<AbstractNode>& to, int paramIndex);

    /**
     * Removes edge
     * @param edge
     * @return true if the edge was found and removed, false if no edge was removed
     */
    bool removeEdge(const Edge& edge);

    const std::string& getId() const;

    /**
     * @return Set of edges to child nodes
     */
    const std::set<Edge>& getEdges() const;

    /**
     * @return Vector of received parameters
     */
    const std::vector<std::shared_ptr<abstraction::Value>>& getParams() const;

    /**
     * Evaluates the node with given parameters and template parameters
     * @param environment Environment in which the node will be evaluated
     */
    virtual void evaluate(abstraction::TemporariesHolder& environment) = 0;

    /**
     * Returns the result of the node (result can be debug output)
     * @return std::optional result of evaluation
     */
    virtual std::optional<std::string> getResult() { return std::nullopt; }

    /**
     * Returns the result of the node printed with string::Compose
     * @return std::optional result of evaluation
     */
    virtual std::optional<std::string> getResultStringCompose() { return std::nullopt; }

    /**
     * Returns the result of the node printed with convert::DotConverter
     * @return std::optional result of evaluation
     */
    virtual std::optional<std::string> getResultDot() { return std::nullopt; }

    /**
     * Returns the result of the node printed with convert::LatexConverter
     * @return std::optional result of evaluation
     */
    virtual std::optional<std::string> getResultLatex() { return std::nullopt; }

    /**
     * Returns the type of the result of the algorithm node or std::nullopt for other nodes.
     * @see core::type_details
     */
    virtual std::optional<core::type_details> getResultType() { return std::nullopt; }

    virtual bool isOutput() const = 0;
    virtual bool isInput() const = 0;

protected:
    const std::string id;
    std::set<Edge> edges;
    std::vector<std::shared_ptr<abstraction::Value>> params;
    ext::vector<std::string> templateParams;

    void setParam(const std::shared_ptr<abstraction::Value>& value, unsigned int index)
    {
        if (index >= params.size())
            params.resize(index + 1);
        params[index] = value;
    }

    /**
     * Helper function that passes the result to all child nodes
     * @param result value to be passed to childs
     */
    void passResultToChilds(const std::shared_ptr<abstraction::Value>& result);
};
