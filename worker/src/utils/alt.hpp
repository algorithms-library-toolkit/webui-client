#pragma once
#include <abstraction/TemporariesHolder.h>
#include <ext/vector>
#include <memory>
#include <string>

namespace abstraction {
class Value;
}

std::string abstractValueToString(const std::shared_ptr<abstraction::Value>& val);
std::shared_ptr<abstraction::Value> evalAlgorithm(abstraction::TemporariesHolder& environment, const std::string& name, const ext::vector<std::string>& templateArgs, const ext::vector<std::shared_ptr<abstraction::Value>>& args);
