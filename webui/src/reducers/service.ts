import AlgorithmService, {
	IAlgorithmService
} from '../service/AlgorithmsService'
import { ActionsUnion } from '../utils/redux'


export interface ServiceState {
	algorithmsService: IAlgorithmService
}

export const initialServiceState: ServiceState = {
	algorithmsService: new AlgorithmService(),
}

export const serviceActions = {}

export type ServiceAction = ActionsUnion<typeof serviceActions>

export const serviceReducer = (
	state: ServiceState = initialServiceState,
	action: ServiceAction
) => {
	return state
}
