import React, { useCallback, useState } from 'react'
import {
    makeStyles,
    AppBar,
    Tab,
    Tabs,
} from '@material-ui/core'

import { a11yProps, TabPanel } from 'components/WebUI/Dialog/TabUtils'
import { InputAutomatonString } from 'components/WebUI/Dialog/InputDialog/Automaton/InputAutomatonString'
import { InputAutomatonTable } from 'components/WebUI/Dialog/InputDialog/Automaton/InputAutomatonTable'
import { parseAutomaton } from 'utils/alt/import/parsers'

const useStyles = makeStyles({
    root: {
        flexGrow: 1,
        padding: '0px 0px 0px 0px',
        width: '1'
    },
})

function checkForParseErrors(stringValue: string) {
	try {
		return { automaton: parseAutomaton(stringValue), parseError: undefined }
	} catch (error: any) {
		return { automaton: undefined, parseError: error.message }
	}
}

interface InputAutomatonFieldProps {
    stringValue: string
    onChange: (stringValue: string) => void
    setDoneButtonState: (enabled: boolean) => void
}
export const InputAutomatonField = (props: InputAutomatonFieldProps) => {
    const classes = useStyles()

    const [ tabIndex, setTabIndex ] = useState<number>(0)
    const { parseError, automaton } = checkForParseErrors(props.stringValue)

    const handleTabChange = useCallback((event: React.ChangeEvent<{}>, newValue: number) => {
		setTabIndex(newValue)
		props.setDoneButtonState(true)
    }, [])

    return (
        <div className={classes.root}>
            <AppBar position='static'>
                <Tabs
                    variant='fullWidth'
                    indicatorColor='secondary'
                    value={tabIndex}
                    onChange={handleTabChange}
                >
                    <Tab label='Table' {...a11yProps(1)} />
                    <Tab label='String' {...a11yProps(0)} />
                </Tabs>
            </AppBar>
            <TabPanel value={tabIndex} index={0}>
            	<InputAutomatonTable updateValue={props.onChange} automaton={automaton} parseError={parseError} setDoneButtonState={props.setDoneButtonState} />
            </TabPanel>
            <TabPanel value={tabIndex} index={1}>
            	<InputAutomatonString stringValue={props.stringValue} updateValue={props.onChange} parseError={parseError} />
            </TabPanel>
        </div>
    )
}

export default InputAutomatonField
