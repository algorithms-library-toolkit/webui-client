import React, { useCallback, useRef, memo } from 'react'
import {
	Divider,
	Button,
	ButtonGroup
} from '@material-ui/core'
import { makeStyles } from '@material-ui/styles'
import { useSnackbar } from 'notistack'
import { saveAs } from 'file-saver'
import 'katex/dist/katex.min.css';
import TeX from '@matejmazur/react-katex'

const useStyles = makeStyles({
	buttons: {
		display: 'flex',
		justifyContent: 'flex-end'
	}
})

const MemoizedLatexContent = memo(({ content } : { content: string}) => ( <TeX>{content}</TeX> ))

interface Props {
	content: string;
}

/** KaTeX does not know how to handle tabular and multicolumn. Let's replace tabular with array and multicolumn with empty column */
const processForKaTeX = (content: string) => {
	content = content.replaceAll("tabular", "array")
	content = content.replaceAll("$", "")

	const regex = /\\multicolumn\{2\}\{\|c\|\|\}\{([-\\\w]+)\}/g
	content = content.replaceAll(regex, (_, capture) => " & " + capture)
	return content
}

const OutputDot = (props: Props) => {
	const { content } = props
	const { enqueueSnackbar } = useSnackbar()
	const contentRef = useRef<HTMLDivElement>(null)
	const classes = useStyles()

	const handleSourceDownload = useCallback(() => {
		const blob = new Blob([content], {
			type: 'application/x-tex'
		})

		saveAs(blob, 'source.tex')
	}, [content])

	const handleSourceCopy = useCallback(async () => {
		try {
			await navigator.clipboard.writeText(content);
			enqueueSnackbar("LaTeX source copied to clipboard.", { variant: 'success' })
		} catch (err) {
			enqueueSnackbar("Could not copy source to clipboard.", { variant: 'error' })
		}
	}, [content])

    const onGraphvizRenderError = (errorMsg: string) => {
      console.error("Graphviz render error:", errorMsg)
      enqueueSnackbar("Graphviz image could not be rendered.", { variant: 'error' })
    }
    // const onGraphvizRenderErrorMemo = useCallback(onGraphvizRenderError, []); // https://stackoverflow.com/questions/54368822/react-memo-components-and-re-render-when-passing-function-as-props

	return (
		<div>
			<div ref={contentRef}>
				<MemoizedLatexContent content={processForKaTeX(content)} />
			</div>
			<Divider />
			<ButtonGroup className={classes.buttons} variant="text" color="primary" aria-label="text primary button group">
				<Button color="primary" aria-controls="saveMenu" aria-haspopup="true" onClick={handleSourceDownload}>Download source</Button>
				<Button color="primary" aria-controls="saveMenu" aria-haspopup="true" onClick={handleSourceCopy}>Copy source</Button>
			</ButtonGroup>
		</div>
	)
}

export default OutputDot
