#pragma once
#include <abstraction/TemporariesHolder.h>
#include <json/value.h>
#include <string>
#include "graph/nodes/AbstractNode.hpp"

class AlgorithmNode : public AbstractNode {
public:
    AlgorithmNode(const std::string& id, std::string algorithmName, const ext::vector<std::string>& templateParams);
    AlgorithmNode(const std::string& id, const Json::Value& jsonObject);

    bool isOutput() const override;
    bool isInput() const override;

    /**
     * Evaluates the alib algorithm of given name with given parameters and template parameters
     * @param environment Environment to evaluate the algorithm in
     */
    void evaluate(abstraction::TemporariesHolder& environment) override;

    /**
     * @brief Returns result of an algorithm name, which is, verbose output (contents of common::Streams::log)
     */
    std::optional<std::string> getResult() override;

    /**
     * @brief Returns the type of the actual result of the algorithm.
     *
     * This is needed for algorithms like string::Parse which return abstraction::UnspecifiedType but we can query what actual type is there.
     */
    std::optional<core::type_details> getResultType() override;

private:
    const std::string algorithmName;

    /**
     * Converts the parameter map to a vector used to pass the parameters to alib
     * @return vector of parameters
     */
    ext::vector<std::shared_ptr<abstraction::Value>> getParamsVec();

    std::optional<std::string> m_log;

    std::optional<core::type_details> m_resultType; // FIXME: core::type_details doesn't have an 0-param constructor so it is uninitialized until the actual evaluation takes place
};
