import validate from '../txt'

const data: any[][] = [
	['a', 'b', 'c', {}],
	['a', {}],
	['b', {}],
	['a', '1', 'b'],
	['a', 'b'],
	['a', 'b', 1]
]

test('expect error with file < 3 lines', () => {
	expect(validate([[], []])).toEqual([
		'Missing data lines (1 - states, 2 - initial, 3 - final)'
	])
})

test('expect get correct errors', () => {
	expect(validate(data)).toEqual([
		'[object Object] is not string or number',
		'[object Object] is not string or number',
		'[object Object] is not string or number',
		'Transition 1: length error (not 3)',
		'Transition 2: type error (not string)'
	])
})
