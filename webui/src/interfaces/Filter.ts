import { Overload } from 'interfaces/Algorithms'

export interface Filter {
    filter(overload: Overload, value: string) : boolean

    isNameFilter(): boolean
}

export class NameFilter implements Filter {
    filter(overload: Overload, value: string) {
        return true
    }

    isNameFilter(): boolean {
        return true
    }
}

export class ParamFilter implements Filter {
    filter(overload: Overload, value: string) {
        return overload.params.some((param) =>
            param.type.toLocaleUpperCase().includes(value.toLocaleUpperCase())
            || param.name.toLocaleUpperCase().includes(value.toLocaleUpperCase()))
    }

    isNameFilter(): boolean {
        return false
    }
}

export class ReturnValueFilter implements Filter {
    filter(overload: Overload, value: string) {
        return overload.resultType.toLocaleUpperCase().includes(value.toLocaleUpperCase())
    }

    isNameFilter(): boolean {
        return false
    }
}

export class DocsFilter implements Filter {
    filter(overload: Overload, value: string) {
        if(overload.docs !== undefined)
            return overload.docs.toLocaleUpperCase().includes(value.toLocaleUpperCase())
        return false
    }

    isNameFilter(): boolean {
        return false
    }
}