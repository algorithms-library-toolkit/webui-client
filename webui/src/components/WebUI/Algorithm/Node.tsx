import * as React from 'react'

import { Theme } from '@material-ui/core/styles'
import { makeStyles } from '@material-ui/styles'
import { Visibility as VisibilityIcon, Edit as EditIcon, Info as InfoIcon, LibraryAdd as AddIcon } from '@material-ui/icons'
import AlgorithmNode, { isCustomFunction, isOutputNode } from 'interfaces/AlgorithmNode'
import ParamNode from './ParamNodeContainer'
import {
	inputParamNodePosition,
	outputParamNodePosition,
	lineOffset
} from 'utils/positioning'
import { SVGButton } from '../SVGButton'
import { getNodeLabel } from 'utils/textSize'
import { DocsPopover } from 'components/WebUI/DocsPopover'

interface Props {
	/** State element data */
	data: AlgorithmNode
	isSelected?: boolean
	isGhost?: boolean
	hasError?: boolean
	hasValue?: boolean
	/** Some transition from or to this state is selected */
	hasTransitionSelected?: boolean
	showEditButton?: boolean
	showCopyButton?: boolean
	hasOutput?: boolean
	onClick?: (e: React.MouseEvent<SVGRectElement, MouseEvent>) => void
	onMouseUp?: (e: React.MouseEvent<SVGRectElement, MouseEvent>) => void
	onMouseDown?: (e: React.MouseEvent<SVGRectElement, MouseEvent>) => void
	onMouseMove?: (e: React.MouseEvent<SVGRectElement, MouseEvent>) => void
	onEdit?: (e: React.MouseEvent<SVGGElement, MouseEvent>) => void
	onCopy?: (e: React.MouseEvent<SVGGElement, MouseEvent>) => void
	onView?: (e: React.MouseEvent<SVGGElement, MouseEvent>) => void
}

const noneString: 'none' = 'none'
const highlightColor = '#0e0'
const verboseHighlightColor = '#118811'
const useStyles = makeStyles((theme: Theme) => ({
	root: {
		cursor: 'pointer',
		'&[data-ghost="true"]': {
			opacity: 0.4
		},
		'&:hover > rect[data-highlight="false"]': {
			stroke: theme.palette.primary.light
		},
		'&:hover > g': {
			display: 'block'
		}
	},
	node: {
		strokeWidth: 2,
		stroke: theme.palette.grey[400],
		fill: theme.palette.common.white,
		rx: 2,
		ry: 2,
		'&[data-error="true"]': {
			stroke: theme.palette.error.main
		},
		'&[data-selected="true"]': {
			stroke: theme.palette.primary.main
		},
		'&[data-transition-selected="true"]': {
			stroke: theme.palette.secondary.main
		},
		'&[data-highlight="true"]': {
			stroke: highlightColor
		},
		'&[data-verbose="true"]': {
			stroke: verboseHighlightColor
		}
	},
	text: {
		fill: theme.palette.text.primary,
		fontFamily: 'Roboto',
		fontSize: 20,
		pointerEvents: noneString
	},
	editButton: {
		display: 'none',
		'&:hover rect': {
			stroke: theme.palette.primary.light
		}
	},
	editButtonRect: {
		fill: theme.palette.common.white,
		stroke: theme.palette.grey[400],
		strokeWidth: 1
	},
	viewButton: {
		'& > rect': { stroke: highlightColor },
		'&:hover > rect': { stroke: highlightColor }
	}
}))

/**
 * Represents the state element in state machine
 *
 * @param {Props} props
 * @param {React.Ref<SVGTextElement>} textRef Reference to the name of state used to calculate the state size
 * @returns
 */
function Node(props: Props, textRef: React.Ref<SVGTextElement>) {
	const {
		data,
		hasError,
		hasValue,
		hasTransitionSelected,
		isGhost,
		isSelected,
		showEditButton,
		showCopyButton,
		hasOutput,
		onClick,
		onMouseDown,
		onMouseMove,
		onMouseUp,
		onEdit,
		onCopy,
		onView
	} = props
	const classes = useStyles()

	const id = 'node_' + data.id

	const { height, width } = data
	const x = data.position.x - data.width / 2
	const y = data.position.y - data.height / 2

	const text = getNodeLabel(data.name, data.templateParams)

	const textLines = React.useCallback(
		(line: string, row: number) => (
			<tspan
				key={row}
				x={0}
				y={lineOffset(row, data.templateParams.length + 1)}>
				{line}
			</tspan>
		),
		[]
	)

	const [anchorEl, setAnchorEl] = React.useState<SVGGElement | null>(null)

	const handleClick = (event: React.MouseEvent<SVGGElement>) => {
		setAnchorEl(event.currentTarget)
	}

	const handleClose = (event: React.MouseEvent<HTMLButtonElement>) => {
		setAnchorEl(null)
	}

	return (
		<g id={id} className={classes.root} data-ghost={isGhost}>
			<rect
				className={classes.node}
				height={height}
				width={width}
				x={x}
				y={y}
				onClick={onClick}
				onMouseUp={onMouseUp}
				onMouseDown={onMouseDown}
				onMouseMove={onMouseMove}
				data-error={hasError}
				data-selected={isSelected}
				data-transition-selected={hasTransitionSelected}
				data-highlight={hasValue && isOutputNode(data.nodeType)}
				data-verbose={hasValue && data.nodeType === 'algorithm'}
			/>
			<text
				className={classes.text}
				ref={textRef}
				alignmentBaseline="middle"
				textAnchor="middle"
				transform={`translate(${data.position.x},${data.position.y})`}
			>
				{text.map(textLines)}
			</text>
			{data.nodeType === 'algorithm' && (
				<React.Fragment>
					<SVGButton
						x={x + width}
						y={y + height}
						onClick={handleClick}
						icon={InfoIcon}
					/>
					<DocsPopover
						dataId={data.id}
						anchorEl={anchorEl}
						name={data.name}
						templateParams={data.templateParams}
						overloads={data.overloads!}
						docs={data.docs!}
						onClose={handleClose}
					/>
				</React.Fragment>
			)}
			{showEditButton && (
				<SVGButton
					x={data.position.x}
					y={y}
					onClick={onEdit}
					icon={EditIcon}
				/>
			)}
			{showCopyButton && (
				<SVGButton
					x={data.position.x}
					y={y + height}
					onClick={onCopy}
					icon={AddIcon}
				/>
			)}
			{hasValue
				&& !isCustomFunction(data.nodeType) && (
					<SVGButton
					className={classes.viewButton}
					x={data.position.x}
					y={y}
					onClick={onView}
					icon={VisibilityIcon}
				/>
			)}
			{hasOutput && (
				<ParamNode
					nodeId={data.id}
					type={data.resultType}
					{...outputParamNodePosition(data)}
				/>
			)}
			{data.params.map((param, index) => (
				<ParamNode
					key={index}
					paramIndex={index}
					nodeId={data.id}
					{...param}
					{...inputParamNodePosition(data, index)}
				/>
			))}
		</g>
	)
}

export default React.memo(React.forwardRef(Node), (prevProps, nextProps) => {
	if (prevProps.data.name !== nextProps.data.name) {
		return false
	}
	if (prevProps.data.position.x !== nextProps.data.position.x) return false
	if (prevProps.data.position.y !== nextProps.data.position.y) return false
	if (prevProps.data.width !== nextProps.data.width) return false
	if (prevProps.isSelected !== nextProps.isSelected) return false
	if (prevProps.isGhost !== nextProps.isGhost) return false
	if (prevProps.hasError !== nextProps.hasError) return false
	if (prevProps.onClick !== nextProps.onClick) return false
	if (prevProps.onMouseDown !== nextProps.onMouseDown) return false
	if (prevProps.onMouseMove !== nextProps.onMouseMove) return false
	if (prevProps.onMouseUp !== nextProps.onMouseUp) return false
	if (prevProps.hasTransitionSelected !== nextProps.hasTransitionSelected)
		return false
	if (prevProps.onEdit !== nextProps.onEdit) return false
	if (prevProps.onCopy !== nextProps.onCopy) return false
	if (prevProps.onView !== nextProps.onView) return false
	if (prevProps.showEditButton !== nextProps.showEditButton) return false
	if (prevProps.showCopyButton !== nextProps.showCopyButton) return false
	if (prevProps.hasValue !== nextProps.hasValue) return false
	if (prevProps.hasOutput !== nextProps.hasOutput) return false
	return true
})
