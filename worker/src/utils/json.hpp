#pragma once
#include <string>

namespace Json {
struct Value;
};

Json::Value parseJson(const std::string& jsonText);
void writeJson(const Json::Value& json, std::ostream& os);
std::string writeJson(const Json::Value& json);
