import * as React from 'react'

import { Theme } from '@material-ui/core/styles'
import { makeStyles } from '@material-ui/styles'

import Position from '../../../interfaces/Position'
import AlgorithmEdge from '../../../interfaces/AlgorithmEdge'
import { bezierSCurve, bezierZCurve } from '../../../utils/svg'

interface Props {
	/** Transition element data */
	data: AlgorithmEdge
	/** Line start at */
	fromPoint: Position
	/** Line ends at */
	toPoint: Position
	isSelected?: boolean
	isGhost?: boolean
	hasError?: boolean
	onClick?: (e: React.MouseEvent<SVGGElement, MouseEvent>) => void
}

const noneString: 'none' = 'none' // Fixes linting errpr
const useStyles = makeStyles((theme: Theme) => ({
	root: {
		cursor: 'pointer',
		'&[data-ghost="true"]': {
			pointerEvents: noneString,
		},
		'&:hover > path': {
			stroke: theme.palette.primary.light,
		},
		'&:hover > marker': {
			fill: theme.palette.primary.light,
		},
	},
	line: {
		strokeWidth: 2,
		stroke: theme.palette.grey[400],
		fill: 'none',
		'&[data-error="true"]': {
			stroke: theme.palette.error.main,
		},
		'&[data-selected="true"]': {
			stroke: theme.palette.primary.main,
		},
	},
	arrowHead: {
		fill: theme.palette.grey[400],
		'&[data-error="true"]': {
			fill: theme.palette.error.main,
		},
		'&[data-selected="true"]': {
			fill: theme.palette.primary.main,
		},
	}
}))

const Edge = (props: Props) => {
	const {
		data,
		fromPoint,
		toPoint,
		hasError,
		isGhost,
		isSelected,
		onClick,
	} = props
	const classes = useStyles()

	const id = 'edge_' + data.id

	const path =
		fromPoint.x <= toPoint.x
			? bezierSCurve(fromPoint, toPoint)
			: bezierZCurve(fromPoint, toPoint)

	return (
		<g
			id={id}
			className={classes.root}
			onClick={onClick}
			data-ghost={isGhost}
		>
			<marker
				id={'arrowhead_' + data.id}
				orient="auto-start-reverse"
				markerWidth="3"
				markerHeight="6"
				refX="3"
				refY="3"
				data-error={hasError}
				data-selected={isSelected}
				className={classes.arrowHead}
			>
				<path d="M0,0 V6 L3,3 Z" />
			</marker>
			<path
				id={'path_' + data.id}
				className={classes.line}
				d={path}
				data-error={hasError}
				data-selected={isSelected}
				markerEnd={`url(#arrowhead_${data.id})`}
			/>
		</g>
	)
}

export default React.memo(Edge, (prevProps, nextProps) => {
	if (prevProps.fromPoint.x !== nextProps.fromPoint.x) return false
	if (prevProps.fromPoint.y !== nextProps.fromPoint.y) return false
	if (prevProps.toPoint.x !== nextProps.toPoint.x) return false
	if (prevProps.toPoint.y !== nextProps.toPoint.y) return false
	if (prevProps.isSelected !== nextProps.isSelected) return false
	if (prevProps.hasError !== nextProps.hasError) return false
	if (prevProps.onClick !== nextProps.onClick) return false
	return true
})
