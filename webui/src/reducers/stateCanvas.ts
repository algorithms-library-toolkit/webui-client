import { canvasActionsFactory, canvasReducerFactory, CanvasState, initialState } from './canvas'

export type StateCanvasState = CanvasState

export const stateCanvasId = 'state'

export const initialStateCanvasState = initialState

export const stateCanvasActions = canvasActionsFactory(stateCanvasId)

export const stateCanvasReducer = canvasReducerFactory(stateCanvasId)
