import { getHeightFromRef, getSizeFromRef, getWidthFromRef } from 'utils/size'
import { AlibTemplateType } from 'interfaces/Algorithms'
import { removeTemplate } from 'utils/alt/removeTemplate'
import { lineOffset } from 'utils/positioning'
import { paramNodeRadius } from 'components/WebUI/Algorithm/ParamNode'

/**
 * Set text attributes to svg text element
 * @param lines
 * @param textEl
 * @param root
 */
function setTextAttributes(lines: string[], textEl: SVGTextElement, root: SVGSVGElement): SVGTextElement {
	lines.forEach((line, i) => {
		const lineEl = document.createElementNS(
			'http://www.w3.org/2000/svg',
			'tspan'
		)
		lineEl.innerHTML = line
		lineEl.setAttributeNS(null, 'alignment-baseline', 'middle')
		lineEl.setAttributeNS(null, 'text-anchor', 'middle')
		lineEl.setAttributeNS(null, 'font-family', 'Roboto')
		lineEl.setAttributeNS(null, 'font-size', '20')
		lineEl.setAttributeNS(null, 'visibility', 'none')
		lineEl.setAttributeNS(null, 'x', '0')
		lineEl.setAttributeNS(null, 'y', lineOffset(i, lines.length) + 'px')
		textEl.appendChild(lineEl)
	})

	root.appendChild(textEl)
	textEl.setAttributeNS(null, 'alignment-baseline', 'middle')
	textEl.setAttributeNS(null, 'text-anchor', 'middle')
	textEl.setAttributeNS(null, 'font-family', 'Roboto')
	textEl.setAttributeNS(null, 'font-size', '20')
	textEl.setAttributeNS(null, 'visibility', 'none')

	return textEl
}

/**
 * Creates temp element with text and gets its size
 *
 * @export
 * @param {string} text
 * @returns {number}
 */
export function getTextSize(text: string): number {
	const root = document.createElementNS('http://www.w3.org/2000/svg', 'svg')

	const textEl = document.createElementNS(
		'http://www.w3.org/2000/svg',
		'text'
	)

	// Split name to array of lines
	const lines = text.split('\n')
	setTextAttributes(lines, textEl, root)
	document.body.appendChild(root)
	const size = getSizeFromRef(textEl) / 2
	document.body.removeChild(root)

	return size
}

/**
 * Create temp svg text element from string and get its width with padding
 * @param text
 */
export function getNodeWidth(text: string): number {
	const root = document.createElementNS('http://www.w3.org/2000/svg', 'svg')

	const textEl = document.createElementNS(
		'http://www.w3.org/2000/svg',
		'text'
	)

	const lines = text.split('\n')

	setTextAttributes(lines, textEl, root)
	document.body.appendChild(root)
	const width = getWidthFromRef(textEl)
	document.body.removeChild(root)

	return width
}

/**
 * Create temp svg text element from string and get its height with padding from text height and number of input parameters
 * @param text
 * @param paramsCount
 */
export function getNodeHeight(text: string, paramsCount: number): number {
	const root = document.createElementNS('http://www.w3.org/2000/svg', 'svg')

	const textEl = document.createElementNS(
		'http://www.w3.org/2000/svg',
		'text'
	)

	const lines = text.split('\n')

	setTextAttributes(lines, textEl, root)
	document.body.appendChild(root)
	const size = getHeightFromRef(textEl)
	document.body.removeChild(root)

	return Math.max(paramsCount * (4 * paramNodeRadius), size)
}

/**
 * Compose array of name and template parameters
 * @param name
 * @param templateParams
 */
export function getNodeLabel(name: string, templateParams: AlibTemplateType[]): string[] {
	const shortName = name.split('::').reverse()[0]
	const params = templateParams.map((param) =>
		removeTemplate(param).split('::').reverse()[0])
	return [shortName, ...params]
}

/**
 * Compose string from name and template parameters separated by newline
 * @param name
 * @param templateParams
 */
export function getNodeLabelText(name: string, templateParams: AlibTemplateType[]): string {
	const textArr = getNodeLabel(name, templateParams)
	return textArr.join('\n')
}
