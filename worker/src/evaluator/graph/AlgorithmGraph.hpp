#pragma once
#include <map>
#include <memory>
#include <string>

class AbstractNode;

class AlgorithmGraph {
    std::vector<std::string> m_functions;
    std::map<std::string, std::shared_ptr<AbstractNode>> m_nodes;

public:
    struct EvaluatedGraph {
        std::map<std::string, std::string> outputs;
        std::map<std::string, std::string> prettyOutputs;
        std::map<std::string, std::string> dotOutputs;
        std::map<std::string, std::string> latexOutputs;
        std::map<std::string, std::string> types;
    };

    explicit AlgorithmGraph(const std::map<std::string, std::shared_ptr<AbstractNode>>& nodes);
    explicit AlgorithmGraph(const std::map<std::string, std::shared_ptr<AbstractNode>>& nodes, const std::vector<std::string>& functions);
    std::vector<std::string> getFunctions() const;
    std::map<std::string, std::shared_ptr<AbstractNode>> nodes() const;
    EvaluatedGraph evaluate();
};
