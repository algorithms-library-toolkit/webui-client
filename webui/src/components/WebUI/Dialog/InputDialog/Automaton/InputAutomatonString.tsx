import {
    makeStyles,
    TextField,
} from '@material-ui/core'

const useStyles = makeStyles({
    textField: {
        width: '100%'
    },
})

export function InputAutomatonString({ stringValue, parseError, updateValue} : { stringValue: string, parseError?: string, updateValue: (stringValue: string) => void }) {
    const classes = useStyles()

	return (
		<TextField
			className={classes.textField}
			variant='filled'
			multiline
			value={stringValue}
			onChange={(event: React.ChangeEvent<HTMLTextAreaElement | HTMLInputElement>) => updateValue(event.target.value)}
			error={parseError !== undefined}
			helperText={parseError}
		/>
	)
}
