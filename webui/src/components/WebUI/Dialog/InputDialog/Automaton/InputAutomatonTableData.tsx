import React, { useEffect, useRef, useState } from 'react'
import {
    FormControl,
    Grid,
    IconButton,
    makeStyles,
    MenuItem,
    Paper, Select,
    Table,
    TableBody,
    TableCell,
    TableContainer,
    TableHead,
    TableRow,
    TextField,
    Typography
} from '@material-ui/core'
import {
	Add as AddRowIcon,
	Remove as RemoveRowIcon,
	AddCircle as AddSymbolIcon,
	RemoveCircle as RemoveSymbolIcon,
    SyncAlt,
    ArrowRightAlt,
} from '@material-ui/icons'

import AutomatonFormHint from 'components/WebUI/Dialog/OutputDialog/AutomatonFormHint'
import InfoButton from 'components/WebUI/Dialog/InfoButton'
import { Row, StateType, stateTypes, TableError } from 'interfaces/TableRow'
import { get1DIndex } from 'utils/automatonTable'

const DELTA = '\u03B4'

const useStyles = makeStyles({
    cellTextField: {
        width: '50px'
    },
    table: {
        width: '100%'
    },
    icon: {
        verticalAlign: 'middle',
        display: 'inline-flex'
    },
    out: {
        transform: 'rotate(-180deg)',
        verticalAlign: 'middle',
        display: 'inline-flex'
    },
})

interface InputAutomatonTableDataProps {
	table: () => Row[]
	symbols: () => string[]
	tableErrors: TableError[]
	onAddRow: () => void
	onSymbolChange: (index: number, value: string) => void
	onStateTypeChange: (index: number, value: StateType) => void
	onAddColumn: () => void
	onTransitionChange: (index: number, index2: number, value: string) => void
	onStateNameChange: (index: number, value: string) => void
	onDeleteRow: (index: number) => void
	onDeleteColumn: (index: number) => void
}

export function InputAutomatonTableData(props: InputAutomatonTableDataProps) {
	const classes = useStyles()
	const { table, symbols, tableErrors } = props

	const stateTypeIcons = [
		<em>None</em>,
		<ArrowRightAlt className={classes.icon}/>,
		<ArrowRightAlt className={classes.out}/>,
		<SyncAlt className={classes.icon}/>
	]

    const headerRefs = useRef<HTMLInputElement[]>([])
    const contentRefs = useRef<HTMLInputElement[]>([])

    const width = table().length > 0
        ? table()[0].transitions.length + 1 //for the stateName column
        : 0
    const size = width * table().length

    //move focus to the added line
    useEffect(() =>{
        if (table.length > 0 && contentRefs.current[size - width])
            contentRefs.current[size - width].focus()
    }, [table.length])

    //set focus when columns are modified
    useEffect(() =>{
        if (headerRefs.current[0])
            headerRefs.current[0].focus()
    }, [symbols.length])


    const handleEnter = (e: React.KeyboardEvent<HTMLDivElement>, targetIndex: number) => {
        if (e.key !== "Enter")
            return

        //add new line when reaching end of the last line
        if (targetIndex === size) {
             props.onAddRow()
             // return
         }

        //move focus to the next field
        const targetElem = contentRefs.current[targetIndex]
        if (targetElem) {
            e.stopPropagation()
            targetElem.focus();
        }
    }

    const handleHeaderKeys = (e: React.KeyboardEvent<HTMLDivElement>, targetIndex: number) => {
        if (e.key === "Delete") {
            props.onDeleteColumn(targetIndex - 1)
            return
        }

        if (e.key !== "Enter")
            return

        //add first line on enter in the last symbol's field
        if (targetIndex === symbols.length && table.length < 1) {
            props.onAddRow()
            return
        }

        //move to the first row on enter in the last symbol's field
        if (targetIndex > symbols.length - 1) {
            contentRefs.current[0].focus()
            return
        }

        //move to the next symbol field
        const targetElem = headerRefs.current[targetIndex]
        if (targetElem) {
            e.stopPropagation()
            targetElem.focus();
        }
    }

	const handleAddRow = () => props.onAddRow()
	const handleDeleteColumn = (idx: number) => props.onDeleteColumn(idx)

    const handleSymbolChange = (index: number) => (e: React.ChangeEvent<HTMLInputElement>) => {
       props.onSymbolChange(index, e.currentTarget.value)
    }

    const handleTypeChange = (index: number) => (e: React.ChangeEvent<{ value: unknown }>) => {
        props.onStateTypeChange(index, e.target.value as StateType)
    }

    const handleAddColumn = () => {
        props.onAddColumn()
    }

    const handleTextChange = (index: number, index2: number) => (e: React.ChangeEvent<HTMLInputElement>) => {
        props.onTransitionChange(index, index2, e.currentTarget.value)
    }

    const handleNameChange = (index: number) => (e: React.ChangeEvent<HTMLInputElement>) => {
        props.onStateNameChange(index, e.currentTarget.value)
    }

    const handleDelete = (index: number) => () => {
        props.onDeleteRow(index)
    }

	return (
		<React.Fragment>
			<Grid container justifyContent='flex-end'>
				<InfoButton
					content={<AutomatonFormHint/>}
				/>
			</Grid>
			<TableContainer component={Paper}>
				<Table className={classes.table} size="small">
					<TableHead>
						<TableRow>
							<TableCell colSpan={2} />
							{symbols().map((symbol, index) => (
								<TableCell key={"symbol-ctrl-" + index.toString()} align="center">
									<IconButton color='default' onClick={() => handleDeleteColumn(index)}>
										<RemoveSymbolIcon />
									</IconButton>
								</TableCell>
							))}
							<TableCell align="center">
								<IconButton color='default' onClick={handleAddColumn}>
									<AddSymbolIcon />
								</IconButton>
							</TableCell>
						</TableRow>
						<TableRow>
							<TableCell/>
							<TableCell align="left">
								<Typography component="span" variant="h6">{DELTA}</Typography>
							</TableCell>
							{symbols().map((symbol, index) => (
								<TableCell key={index.toString()} align="center">
									<TextField
										className={classes.cellTextField}
										onKeyDown={(e) =>
											handleHeaderKeys(e, index + 1)
										}
										inputRef={(el) => (headerRefs.current[index] = el)}
										value={symbol}
										onChange={handleSymbolChange(index)}
										inputProps={{style: {fontSize: 20}}}
										error={tableErrors.some(e => e.cellType === 'symbol' && e.cell1DIndex === index)}
									/>
								</TableCell>
							))}
							<TableCell />
						</TableRow>
					</TableHead>
					<TableBody>
						{table().map((row, rowIndex, tableArray) => (
							<TableRow key={`row-${rowIndex}`}>
								<TableCell key={row.name+ "stateTypeCell"} align="center">
									<FormControl key={row.name + "formControl"} fullWidth>
										<Select
											key={row.name + "stateTypeSelect"}
											value={row.type}
											onChange={handleTypeChange(rowIndex)}
										>
											{stateTypes.map((type, index) =>
												<MenuItem key={row.name + type}
													value={type}>{stateTypeIcons[index]}</MenuItem>
											)}
										</Select>
									</FormControl>
								</TableCell>
								<TableCell key={`row-${rowIndex}-name`} align="left">
									<TextField
										className={classes.cellTextField}
										onKeyDown={(e) =>
											handleEnter(e, get1DIndex(rowIndex, width, 0) + 1)
										}
										inputRef={(el) => (contentRefs.current[get1DIndex(rowIndex, width, 0)] = el)}
										value={row.name}
										onChange={handleNameChange(rowIndex)}
										inputProps={{style: {fontSize: 20}}}
										error={tableErrors.some(e => e.cellType === 'table' && e.cell1DIndex === get1DIndex(rowIndex, width, 0))}
									/>
								</TableCell>
								{row.transitions.map((transition, transitionIndex) =>
									<TableCell key={`row-${rowIndex}-tr-${transitionIndex}`} align="center">
										<TextField
											className={classes.cellTextField}
											onKeyDown={(e) =>
												handleEnter(e, get1DIndex(rowIndex, width, transitionIndex + 1) + 1)
											}
											inputRef={(el) => (contentRefs.current[get1DIndex(rowIndex, width, transitionIndex + 1)] = el)}
											value={transition}
											onChange={handleTextChange(rowIndex, transitionIndex)}
											error={tableErrors.some(e => e.cellType === 'table' && e.cell1DIndex === get1DIndex(rowIndex, width, transitionIndex + 1))}
										/>
									</TableCell>
								)}
								<TableCell align="center">
									<IconButton
										color='default'
										onClick={handleDelete(rowIndex)}
										disabled={tableArray.length === 1}
										aria-label="Delete row"
									>
										<RemoveRowIcon/>
									</IconButton>
								</TableCell>
							</TableRow>
						))}
							<TableRow>
								<TableCell colSpan={symbols().length + 2} />
								<TableCell align="center">
									<IconButton
										color='default'
										onClick={() => handleAddRow()}
										aria-label="Add row"
									>
										<AddRowIcon />
									</IconButton>
								</TableCell>
							</TableRow>
					</TableBody>
				</Table>
			</TableContainer>
		</React.Fragment>
	)
}
