import { Overload } from 'interfaces/Algorithms'
import { Map } from 'immutable'

export function categorizeOverloads(overloads: Overload[]): Map<number, Overload[]> {
    return overloads.reduce((acc: Map<number, Overload[]>, overload: Overload) => {
        let value: Overload[] | undefined = acc.get(overload.params.length)

        return acc.set(overload.params.length,
            value === undefined
                ? [overload]
                : [...value, overload])
    }, Map<number, Overload[]>())
}
