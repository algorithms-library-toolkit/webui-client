#pragma once
#include <chrono>
#include <optional>
#include <stdexcept>
#include <string>


namespace utils {

class ExecTimeoutError : public std::runtime_error {
    using std::runtime_error::runtime_error;
};

class ExecReturnCodeError : public std::runtime_error {
    using std::runtime_error::runtime_error;
};

std::string execAndWait(const std::string& absolutePath, std::initializer_list<std::string> args, std::optional<std::string_view> std_in, const std::chrono::microseconds& timeoutUs);

template <class Duration>
std::string execAndWait(const std::string& absolutePath, std::initializer_list<std::string> args, std::optional<std::string_view> std_in, const Duration& d)
{
    return execAndWait(absolutePath, args, std_in, std::chrono::duration_cast<std::chrono::microseconds>(d));
}

}
