import { Map } from 'immutable'
import AlgorithmNode, {
	InputNodeTypesToParse,
	inputNodeTypesToParse,
	isAlgorithmNode,
	isOutputNode,
	parsedInputType
} from 'interfaces/AlgorithmNode'
import AlgorithmEdge, { isAlgorithmEdge } from './AlgorithmEdge'
import { isObject } from '../utils/typeGuards'
import { emptyAutomaton } from 'interfaces/Automaton'
import Data, { toAutomaton } from 'interfaces/StateData'
import { v4 as uuidv4 } from 'uuid'
import { NodeOutputs } from 'interfaces/NodeOutputs'
import GrammarData, { grammarToString } from 'interfaces/GrammarData'
import { rowToAutomaton, TableAutomaton } from 'interfaces/TableRow'
import { CustomFunction, customFunctionToString } from 'interfaces/CustomFunction'
import { Set } from 'immutable'
import { serializeAutomaton } from 'utils/alt/export/serializers'
import applyMigrations from 'utils/migrations/migrations'

export interface AlgorithmGraph {
	nodes: Map<string, AlgorithmNode>
	edges: Map<string, AlgorithmEdge>
	outputValues: Map<string, NodeOutputs>
}

const CURRENT_JSON_VERSION = 1

export const algorithmGraphFromJSON = (jsonString: string): AlgorithmGraph => {
	const rawJson = JSON.parse(jsonString)
	const json = applyMigrations(rawJson, CURRENT_JSON_VERSION)

	if (
		!json.nodes ||
		!json.edges ||
		!isObject(json.nodes) ||
		!isObject(json.edges)
	)
		throw new Error('Invalid JSON structure.')

	Object.values(json.nodes).forEach((node: any) => {
		if (!isAlgorithmNode(node)) {
			console.error(node)
			throw new Error('Invalid node structure')
		}
	})
	Object.values(json.edges).forEach((edge: any) => {
		if (!isAlgorithmEdge(edge)) {
			console.error(edge)
			throw new Error('Invalid edge structure')
		}
	})

	return {
		nodes: Map<string, AlgorithmNode>(json.nodes),
		edges: Map<string, AlgorithmEdge>(json.edges),
		outputValues: Map<string, NodeOutputs>()
	}
}

const createEdge = (
	from: string,
	to: string,
	edges: Map<string, AlgorithmEdge>
) => {
	const edgeId = uuidv4()

	return edges.set(edgeId, {
		id: edgeId,
		startNodeId: from,
		endNodeId: to,
		endParamIndex: 0
	})
}

const inputMaps = (nodes: AlgorithmGraph['nodes']) => ({
	strings: nodes
		.filter(
			(node) =>
				node.nodeType === 'inputString' &&
				typeof node.value === 'string'
		)
		.map((node) => ({ value: node.value })),

	ints: nodes
		.filter(
			(node) =>
				node.nodeType === 'inputInt' &&
				typeof node.value === 'number' &&
				Number.isInteger(node.value)
		)
		.map((node) => ({ value: node.value })),

	doubles: nodes
		.filter(
			(node) =>
				node.nodeType === 'inputDouble' &&
				typeof node.value === 'number'
		)
		.map((node) => ({ value: node.value })),

	bools: nodes
		.filter(
			(node) =>
				node.nodeType === 'inputBoolean' &&
				typeof node.value === 'boolean'
		)
		.map((node) => ({ value: node.value }))
})

const reassignEdgeEnds = (
	edges: AlgorithmGraph['edges'],
	from: string,
	to: string
) =>
	edges.map((edge) =>
		edge.endNodeId === from ? { ...edge, endNodeId: to } : edge
	)

/**
 * Return type of content in the output node
 * @param edges
 * @param nodes
 * @param outputNodeId
 */
const getContentType = (edges: Map<string, AlgorithmEdge>, nodes: Map<string, AlgorithmNode>, outputNodeId: string) => {
	//find output nodes's edge
	const outputEdge = edges.find((edge) => edge.endNodeId === outputNodeId)
	//find result type of connected node
	const startNodeId = outputEdge?.startNodeId
	const startNode = nodes.find((node) => node.id === startNodeId)

	return startNode?.resultType
}

export const serialize = (graph: AlgorithmGraph) => {
	let edges = graph.edges

	const nodesSeq = graph.nodes.toSeq()

	let nodes = graph.nodes
	const inputs = inputMaps(graph.nodes)
	let algorithmNodes = nodes.filter((node) => node.nodeType === 'algorithm' || node.nodeType === 'customFunction')
	const outputs: string[] = []
	let customFunctions: Set<string> = Set<string>()

	nodesSeq
		.filter((node) => node.nodeType === 'customFunction')
		.forEach((node) => {
			if (node.value) {
				customFunctions = customFunctions.add(customFunctionToString(node.value as CustomFunction))
			}
	})

	nodesSeq
		.filter((node) => node.nodeType === 'inputAutomaton')
		.forEach((node) => {
			algorithmNodes = algorithmNodes.set(node.id, {
				...node,
				name: 'string::Parse',
				templateParams: ['automaton::Automaton']
			})

			let data = emptyAutomaton
			if (typeof node.value === 'object') {
				data = toAutomaton(node.value as Data)
				console.log(data)
			}

			const inputId = uuidv4()
			inputs.strings = inputs.strings.set(inputId, {
				value: serializeAutomaton(data)
			})

			edges = createEdge(inputId, node.id, edges)
		})

	nodesSeq
		.filter((node) => node.nodeType === 'inputGrammar')
		.forEach((node) => {
			algorithmNodes = algorithmNodes.set(node.id, {
				...node,
				name: 'string::Parse',
				templateParams: ['grammar::Grammar']
			})

			const inputId = uuidv4()
			inputs.strings = inputs.strings.set(inputId, {
				value: grammarToString(node.value as GrammarData)
			})

			edges = createEdge(inputId, node.id, edges)
		})

	nodesSeq
		.filter((node) => inputNodeTypesToParse.includes(node.nodeType as InputNodeTypesToParse))
		.forEach((node) => {
			algorithmNodes = algorithmNodes.set(node.id, {
				...node,
				name: 'string::Parse',
				templateParams: parsedInputType[node.nodeType as InputNodeTypesToParse].templateParams
			})

			const inputId = uuidv4()
			inputs.strings = inputs.strings.set(inputId, {
					value: node.value
			})

			edges = createEdge(inputId, node.id, edges)
		})

	nodesSeq
		.filter((node) => isOutputNode(node.nodeType))
		.forEach((node) => {
			outputs.push(node.id)
		})

	return {
		version: CURRENT_JSON_VERSION,
		customFunctions: Array.from(customFunctions),
		graph: {
			inputs: {
				bools: inputs.bools.toJS(),
				ints: inputs.ints.toJS(),
				doubles: inputs.doubles.toJS(),
				strings: inputs.strings.toJS()
			},
			algorithms: algorithmNodes
				.map(({ name, templateParams }) => ({ name, templateParams }))
				.toJS(),
			outputs,
			pipes: edges
				.valueSeq()
				.map((edge) => ({
					from: edge.startNodeId,
					to: edge.endNodeId,
					paramIndex: edge.endParamIndex
				}))
				.toArray()
		}
	}
}
