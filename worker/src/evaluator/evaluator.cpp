#include <ext/exception>
#include "evaluator.hpp"
#include "graph/AlgorithmGraph.hpp"
#include "graph/NodesFromJSONBuilder.hpp"
#include "utils/json.hpp"

namespace {

Json::Value replyError(const std::exception& exception)
{
    alib::ExceptionHandler::NestedExceptionContainer exceptions;
    alib::ExceptionHandler::handle(exceptions);
    Json::Value errorJson;
    for (size_t i = 0; i < exceptions.getExceptions().size(); i++) {
        errorJson["error"][static_cast<int>(i)] = exceptions.getExceptions()[i].desc.value();
    }

    return errorJson;
}


Json::Value replyOutputs(const AlgorithmGraph::EvaluatedGraph& eval)
{
    Json::Value outputsJson(Json::objectValue);

    for (const auto& [nodeId, output] : eval.outputs) {
        outputsJson[nodeId]["result"]["plain"] = output;
    }

    for (const auto& [nodeId, output] : eval.prettyOutputs) {
        outputsJson[nodeId]["result"]["pretty"] = output;
    }

    for (const auto& [nodeId, output] : eval.dotOutputs) {
        outputsJson[nodeId]["result"]["dot"] = output;
    }

    for (const auto& [nodeId, output] : eval.latexOutputs) {
        outputsJson[nodeId]["result"]["latex"] = output;
    }

    for (const auto& [nodeId, outputType] : eval.types) {
        outputsJson[nodeId]["type"] = outputType;
    }

    Json::Value responseJson;
    responseJson["outputs"] = outputsJson;

    return responseJson;
}

}

std::string evaluate(const std::string& jsonString)
{
    auto json = parseJson(jsonString);

    auto graph = NodesFromJSONBuilder {}.fromJson(json).build();

    try {
        return writeJson(replyOutputs(graph.evaluate()));
    } catch (std::exception& exception) {
        return writeJson(replyError(exception));
    }
}
