#include "testing.hpp"
#include <abstraction/ValueHolder.hpp>
#include <forward_list>
#include <memory>
#include <utility>
#include "graph/nodes/AbstractNode.hpp"
#include "graph/topSort.hpp"
#include "utils/alt.hpp"

#define NONE false, false
#define INPUT true, false
#define OUTPUT false, true
#define INPUT_OUTPUT true, true

#define NODE(ID, SPECIFIERS) std::make_shared<TopSortDummyNode>(ID, SPECIFIERS)
#define EDGE(FROM, TO, TO_SLOT) nodes[FROM]->createEdge(nodes[TO], TO_SLOT);

#define BEFORE(I, J) order[std::to_string(I)] < order[std::to_string(J)]
#define FILTERED(I) order.find(std::to_string(I)) == order.end()

std::map<std::string, std::shared_ptr<AbstractNode>> nodesToMap(const std::vector<std::shared_ptr<AbstractNode>>& nodes)
{
    std::map<std::string, std::shared_ptr<AbstractNode>> nodeMap;

    for (const auto& node : nodes)
        nodeMap[node->getId()] = node;

    return nodeMap;
}

std::map<std::string, size_t> orderById(const std::forward_list<std::shared_ptr<AbstractNode>>& sorted)
{
    std::map<std::string, size_t> res;
    size_t idx = 0;

    for (const auto& node : sorted) {
        res[node->getId()] = ++idx;
    }

    return res;
}

class TopSortDummyNode : public AbstractNode {
public:
    explicit TopSortDummyNode(const int& id, bool isInput, bool isOutput)
        : AbstractNode(std::to_string(id), ext::vector<std::string>())
        , input(isInput)
        , output(isOutput)
    {
    }

    bool isOutput() const override
    {
        return output;
    }

    bool isInput() const override
    {
        return input;
    }

    void evaluate(abstraction::TemporariesHolder& environment) override
    {
        throw std::runtime_error("Not implemented");
    }

    std::optional<std::string> getResult() override
    {
        return result;
    }

private:
    bool input, output;
    std::string result;
};

TEST_CASE("TopSort")
{
    std::vector<std::shared_ptr<AbstractNode>> nodes;
    std::vector<std::pair<size_t, size_t>> expectations;
    std::vector<size_t> filtered;

    SECTION("Graph is a path")
    {
        nodes = {
            NODE(0, INPUT),
            NODE(1, NONE),
            NODE(2, OUTPUT),
        };

        EDGE(0, 1, 0);
        EDGE(1, 2, 0);

        expectations = {{0, 1}, {1, 2}, {0, 2}};
    }

    SECTION("Graph contains a join node")
    {
        nodes = {
            NODE(0, INPUT),
            NODE(1, OUTPUT),
            NODE(2, INPUT),
        };

        EDGE(0, 1, 0);
        EDGE(2, 1, 0);

        expectations = {{0, 1}, {2, 1}};
    }

    SECTION("Graph contains a fork node")
    {
        nodes = {
            NODE(0, INPUT),
            NODE(1, OUTPUT),
            NODE(2, OUTPUT),
        };

        EDGE(0, 2, 0);
        EDGE(0, 1, 1);

        expectations = {{0, 1}, {0, 2}};
    }

    SECTION("Graph contains multiple components")
    {
        nodes = {
            NODE(0, INPUT),
            NODE(1, NONE),
            NODE(2, OUTPUT),

            NODE(3, INPUT),
            NODE(4, NONE),
            NODE(5, OUTPUT),
        };

        EDGE(0, 1, 0);
        EDGE(1, 2, 0);
        EDGE(3, 4, 0);
        EDGE(4, 5, 0);

        expectations = {{0, 1}, {1, 2}, {3, 4}, {4, 5}};
    }

    SECTION("Component without an input node is removed")
    {
        nodes = {
            NODE(0, INPUT),
            NODE(1, OUTPUT),
            NODE(2, OUTPUT),
        };

        EDGE(0, 1, 0);

        expectations = {{0, 1}};
        filtered = {2};
    }

    SECTION("Component without an output node is removed")
    {
        nodes = {
            NODE(0, INPUT),
            NODE(1, OUTPUT),
            NODE(2, INPUT),
        };

        EDGE(0, 1, 0);

        expectations = {{0, 1}};
        filtered = {2};
    }

    SECTION("Component with non-terminated branch is removed")
    {
        nodes = {
            NODE(0, INPUT),
            NODE(1, OUTPUT),
            NODE(2, NONE),
        };

        EDGE(0, 1, 0);
        EDGE(0, 2, 0);

        expectations = {{0, 1}};
        filtered = {2};
    }

    SECTION("Empty graph")
    {
    }

    SECTION("Single node")
    {
        SECTION("Single node")
        {
            nodes = {NODE(0, NONE)};
            filtered = {0};
        }

        SECTION("Single input node")
        {
            nodes = {NODE(0, INPUT)};
            filtered = {0};
        }

        SECTION("Single output node")
        {
            nodes = {NODE(0, OUTPUT)};
            filtered = {0};
        }

        SECTION("Single input/output node")
        {
            nodes = {NODE(0, INPUT_OUTPUT)};
        }
    }

    auto sorted = topSort(nodesToMap(nodes));
    auto order = orderById(sorted);

    REQUIRE(order.size() == nodes.size() - filtered.size());

    for (const auto& [source, sink] : expectations) {
        CAPTURE(source, sink);
        REQUIRE(BEFORE(source, sink));
    }
}
