import { AlibType } from 'interfaces/Algorithms'
import { removeTemplate } from 'utils/alt/removeTemplate'

export const isFiniteAutomaton = (type: AlibType): boolean =>
	['automaton::DFA', 'automaton::NFA', 'automaton::EpsilonNFA', 'automaton::MultiInitialStateNFA', 'automaton::MultiInitialStateEpsilonNFA'].includes(removeTemplate(type))

export const isGrammar = (type: AlibType): boolean =>
	removeTemplate(type).startsWith('grammar::')
