import { parseAutomaton } from 'utils/alt/import/parsers'
import Automaton from 'interfaces/Automaton'


const data: Automaton = {
	type: 'DFA',
	states: ['s1', 's2', 's3', 's4'],
	alphabet: ['t1', 't2', 't3', 't4'],
	initialStates: ['s1'],
	finalStates: ['s1', 's2'],
	transitions: [{symbol: 't1', from: 's1', to: 's2'}, {symbol: 't2', from: 's1', to: 's1'}, {symbol: 't3', from: 's2', to: 's3'}, {symbol: 't3', from: 's4', to: 's5'}, {symbol: 't4', from: 's4', to: 's1'}]
}

const file: string = `DFA	t1	t2	t3	t4
><s1	s2	s1	-	-
<s2	-	-	s3	-
s3	-	-	-	-
s4	-	-	s5	s1`

const NFA: Automaton = {
	type: 'NFA',
	states: ['a', 'b'],
	alphabet: ['1', '0'],
	initialStates: ['a'],
	finalStates: ['b'],
	transitions: [{symbol: '1', from: 'a', to: 'b'}, {symbol: '1', from: 'a', to: 'a'}, {symbol: '0', from: 'a', to: 'a'}]
}

const NFAFile = `NFA	1	0
>a	b|a	a
<b	-	-`

const setDFA: Automaton = {
	type: 'DFA',
	states: ['{a}', '{a, b}'],
	alphabet: ['0', '1'],
	initialStates: ['{a}'],
	finalStates: ['{a, b}'],
	transitions: [{symbol: '0', from: '{a}', to: '{a}'}, {symbol: '1', from: '{a}', to: '{a, b}'}, {symbol: '0', from: '{a, b}', to: '{a}'}, {symbol: '1', from: '{a, b}', to: '{a, b}'}]
}

const DFASetFile = `DFA 0 1
>{a} {a} {a, b}
<{a, b} {a} {a, b}\n\n`


const GlushkovNFA: Automaton = {
	type: 'NFA',
	states: ['((InitialStateLabel), 0)', '(a, 1)', '(b, 2)', '(c, 3)', '(b, 4)'],
	alphabet: ['a', 'b', 'c'],
	initialStates: ['((InitialStateLabel), 0)'],
	finalStates: ['(b, 2)', '(c, 3)', '(b, 4)'],
	transitions: [
		{symbol: 'a', from: '((InitialStateLabel), 0)', to: '(a, 1)'},
		{symbol: 'b', from: '((InitialStateLabel), 0)', to: '(b, 2)'},
		{symbol: 'c', from: '((InitialStateLabel), 0)', to: '(c, 3)'},
		{symbol: 'b', from: '(a, 1)', to: '(b, 2)'},
		{symbol: 'b', from: '(a, 1)', to: '(b, 4)'},
		{symbol: 'c', from: '(c, 3)', to: '(c, 3)'},
	]
}

const GlushkovNFAFile = `NFA a b c
>((InitialStateLabel), 0) (a, 1) (b, 2) (c, 3)
(a, 1) - (b, 2)|(b, 4) -
<(b, 2) - - -
<(c, 3) - - (c, 3)
<(b, 4) - - -`

const MultipleDigitStates: Automaton = {
	type: 'MISNFA',
	states: ['111', '777', '3', '3150'],
	alphabet: ['a', 'b', 'c'],
	initialStates: ['111', '777'],
	finalStates: ['3150'],
	transitions: [
		{symbol: 'a', from: '111', to: '777'},
		{symbol: 'a', from: '111', to: '3'},
		{symbol: 'c', from: '111', to: '3150'},
		{symbol: 'c', from: '111', to: '3'},
		{symbol: 'c', from: '777', to: '3'},
		{symbol: 'a', from: '3150', to: '777'},
		{symbol: 'a', from: '3150', to: '3150'},
		{symbol: 'b', from: '3150', to: '777'},
		{symbol: 'b', from: '3150', to: '3150'},
		{symbol: 'c', from: '3150', to: '3'},
	]
}

const MultipleDigitStatesFile = `MISNFA a b c
>111 777|3 - 3150|3
>777 - - 3
3 - - -
<3150 777|3150 777|3150 3`

const EmptySetStateAutomaton: Automaton = {
	type: 'DFA',
	states: ['{}', '{(InitialStateLabel)}', '{D}'],
	alphabet: ['a', 'b', 'c'],
	initialStates: ['{(InitialStateLabel)}'],
	finalStates: ['{(InitialStateLabel)}', '{D}'],
	transitions: [
		{symbol: 'a', from: '{}', to: '{}'},
		{symbol: 'b', from: '{}', to: '{}'},
		{symbol: 'c', from: '{}', to: '{}'},
		{symbol: 'a', from: '{(InitialStateLabel)}', to: '{C}'},
		{symbol: 'b', from: '{(InitialStateLabel)}', to: '{}'},
		{symbol: 'a', from: '{D}', to: '{D}'},
		{symbol: 'b', from: '{D}', to: '{D}'},
		{symbol: 'c', from: '{D}', to: '{}'},
	]
}

const EmptySetState = `DFA a b c
{} {} {} {}
><{(InitialStateLabel)} {C} {} -
<{D} {D} {D} {}`

describe('import txt-fit', () => {
	it('converts DFA', () => {
		expect(parseAutomaton(file)).toEqual(data)
	})

	it('converts NFA', () => {
		expect(parseAutomaton(NFAFile)).toEqual(NFA)
	})

	it('converts DFA with set syntax', () => {
		expect(parseAutomaton(DFASetFile)).toEqual(setDFA)
	})

	it('converts GlushkovNFA', () => {
		expect(parseAutomaton(GlushkovNFAFile)).toEqual(GlushkovNFA)
	})

	it('converts an NFA with states identified by a sequence of numbers', () => {
		expect(parseAutomaton(MultipleDigitStatesFile)).toEqual(MultipleDigitStates)
	})

	it('parses a state with empty set', () => {
		expect(parseAutomaton(EmptySetState)).toEqual(EmptySetStateAutomaton)
	})

	afterAll(() => jest.resetModules())
})
