#include <csignal>
#include <stdexcept>
#include <string>
#include <unistd.h>

int main(int argc, char* argv[])
{
    int signo = SIGABRT;

    try {
        if (argc == 2) {
            signo = std::stoi(argv[1]);
        }
    } catch (const std::invalid_argument&) {
    }

    kill(getpid(), signo);

    return 0;
}
