import * as uuid from 'uuid'
import { Map, Set } from 'immutable'
import { ActionsUnion, createAction } from '../utils/redux'
import AlgorithmNode, {
	emptyNode,
	InputNodeTypesToParse,
	parsedInputType
} from 'interfaces/AlgorithmNode'
import AlgorithmEdge from '../interfaces/AlgorithmEdge'
import StateData, { toAutomaton } from '../interfaces/StateData'
import {
	automanAlibTypeMap,
	classifyAutomaton
} from 'interfaces/AutomatonTypes'
import { PartialProperties } from '../interfaces/types'
import { AlgorithmGraph } from '../interfaces/AlgorithmGraph'
import { detectCycles } from '../utils/detectCycle'
import Position from '../interfaces/Position'
import { Casts } from 'interfaces/Casts'
import { areCastable, fromTypes, toTypes } from 'utils/alt/casts'
import { AlibType } from '../interfaces/Algorithms'
import { updateIncompatibleEdges } from 'utils/errors'
import { NodeOutputs } from 'interfaces/NodeOutputs'
import GrammarData, { grammarTypeMap } from 'interfaces/GrammarData'
import { TableAutomaton } from 'interfaces/TableRow'
import { CustomFunction } from 'interfaces/CustomFunction'

export const algorithmDataActions = {
	addNode: (data: PartialProperties<AlgorithmNode, 'id'>) =>
		createAction('addNode', data),

	addEdge: (data: Omit<AlgorithmEdge, 'id'>, castData: Casts) => createAction('addEdge', { data, castData } ),

	positionNode: (id: string, position: Position) =>
		createAction('positionNode', { id, position }),

	resizeNode: (id: string, width: number, height: number) =>
		createAction('resizeNode', { id, width, height }),

	setInputAutomaton: (id: string, automaton: StateData) =>
		createAction('setInputAutomaton', { id, automaton }),

	setInputGrammar: (id: string, grammar: GrammarData, castData: Casts) =>
		createAction('setInputGrammar', { id, grammar, castData }),

	setInputString: (id: string, value: string, castData: Casts) =>
		createAction('setInputString', { id, value, castData }),

	setInputNumber: (id: string, value: number, castData: Casts) =>
		createAction('setInputNumber', { id, value, castData }),

	setInputBoolean: (id: string, value: boolean, castData: Casts) =>
		createAction('setInputBoolean', { id, value, castData }),

	setInputStringToParse: (id: string, value: string, nodeType: InputNodeTypesToParse, castData: Casts) =>
		createAction('setInputStringToParse', { id, value, nodeType, castData }),

	setCustomFunctionNode: (id: string, value: CustomFunction) =>
		createAction('setCustomFunctionNode', { id, value }),

	setOutputValues: (values: Map<string, NodeOutputs>) =>
		createAction('setOutputValues', values),

	deleteNode: (id: string) => createAction('deleteNode', id),
	deleteEdge: (id: string) => createAction('deleteEdge', id),

	importGraph: (data: Omit<AlgorithmGraph, 'outputValues'>, castData: Casts) =>
		createAction('importGraph', { data, castData }),

	clear: () => createAction('clear')
}

export type AlgorithmDataActions = ActionsUnion<typeof algorithmDataActions>

export interface AlgorithmGraphState extends AlgorithmGraph {
	cycleEdges: Set<string>
	incompatibleEdges: Set<AlgorithmEdge>
	errors: string[]
}

export const initialAlgorithmState: AlgorithmGraphState = {
	nodes: Map(),
	edges: Map(),
	outputValues: Map<string, NodeOutputs>(),
	cycleEdges: Set(),
	incompatibleEdges: Set(),
	errors: []
}

export const incompatibleTypeError = (algorithm: string, to: Set<AlibType>) =>
	`Incorrect parameter type for ${algorithm}, allowed types: ${to.toJS()}`

const localStorageKey = 'webui_graph'

export const localStorageAlgorithmReducer = (
	state?: AlgorithmGraphState,
	action?: any
) => {
	if (state === undefined) {
		const savedGraph = window.localStorage.getItem(localStorageKey)

		if (savedGraph === null) return algorithmReducer(state, action)

		const parsedGraph = JSON.parse(savedGraph)

		const loadedState: AlgorithmGraphState = {
			nodes: Map(parsedGraph.nodes),
			edges: Map(parsedGraph.edges),
			outputValues: Map(parsedGraph.outputValues),
			cycleEdges: Set(parsedGraph.cycleEdges),
			incompatibleEdges: Set(parsedGraph.incompatibleEdges),
			errors: parsedGraph.errors
		}

		return algorithmReducer(loadedState, action)
	}

	const newState = algorithmReducer(state, action)

	if (
		newState !== state ||
		['undoAlgorithm', 'redoAlgorithm'].includes(action.type)
	) {
		try {
			window.localStorage.setItem(
				localStorageKey,
				JSON.stringify(newState)
			)
		} catch (e) {
			console.error(e)
		}
	}

	return newState
}

export const algorithmReducer = (
	state = initialAlgorithmState,
	action: AlgorithmDataActions
): AlgorithmGraphState => {
	switch (action.type) {
		case 'addNode': {
			const id = action.payload.id ?? uuid.v4()
			return {
				...state,
				nodes: state.nodes.update(id, () => ({
					...action.payload,
					id
				})),
				outputValues: Map()
			}
		}
		case 'addEdge': {
			const { data, castData } = action.payload
			const id = uuid.v4()
			const newEdge: AlgorithmEdge = {
				...data,
				id
			}
			const newEdges = state.edges.set(id, newEdge)

			const errors = []
			const cycleEdges = detectCycles(newEdges.toSet())
			if (!cycleEdges.isEmpty()) errors.push('Cycle detected')

			const from = fromTypes(state.nodes, data.startNodeId)
			const to = toTypes(state.nodes, data.endNodeId, data.endParamIndex)
			let incompatibleEdges = state.incompatibleEdges

			if (!from.isEmpty() && !to.isEmpty() && !areCastable(castData, from, to)) {
				incompatibleEdges = incompatibleEdges.add(newEdge)
				errors.push(incompatibleTypeError(state.nodes.get(data.endNodeId)?.name ?? '', to))
			}

			return {
				...state,
				edges: newEdges,
				cycleEdges: cycleEdges,
				outputValues: Map(),
				errors: errors,
				incompatibleEdges: incompatibleEdges
			}
		}
		case 'positionNode': {
			const { id, position } = action.payload

			return {
				...state,
				nodes: state.nodes.update(id, emptyNode, (oldNode: AlgorithmNode) => ({
					...oldNode,
					position
				}))
			}
		}
		case 'resizeNode': {
			const { id, width, height } = action.payload

			return {
				...state,
				nodes: state.nodes.update(id, emptyNode, (oldNode: AlgorithmNode) => ({
					...oldNode,
					width,
					height
				}))
			}
		}
		case 'setInputAutomaton': {
			const { id, automaton } = action.payload
			return {
				...state,
				nodes: state.nodes.update(id, emptyNode, (node) => ({
					...node,
					resultType: automanAlibTypeMap[classifyAutomaton(toAutomaton(automaton))],
					value: automaton
				})),
				outputValues: Map()
			}
		}
		case 'setInputGrammar': {
			const { id, grammar, castData } = action.payload
			const nodeResultType = grammarTypeMap[grammar.type]
			//find all edges where this node is start node
			const edges = Array.from(state.edges.values()).filter((edge) => edge.startNodeId === id)
			const nodes = state.nodes
			let errorEdges = state.incompatibleEdges

			//check all node's edges after setting node state
			const { errors, incompatibleEdges } = updateIncompatibleEdges(edges, nodes, nodeResultType, castData, errorEdges)
			return {
				...state,
				nodes: state.nodes.update(id, emptyNode, (node) => ({
					...node,
					name: 'Grammar',
					resultType: nodeResultType,
					nodeType: 'inputGrammar',
					value: grammar
				})),
				outputValues: Map(),
				errors: errors,
				incompatibleEdges: incompatibleEdges
			}
		}
		case 'setInputString': {
			const { id, value, castData } = action.payload
			const nodeResultType = 'std::string'
			//find all edges where this node is start node
			const edges = Array.from(state.edges.values()).filter((edge) => edge.startNodeId === id)
			const nodes = state.nodes
			let errorEdges = state.incompatibleEdges

			//check all node's edges after setting node state
			const { errors, incompatibleEdges } = updateIncompatibleEdges(edges, nodes, nodeResultType, castData, errorEdges)

			return {
				...state,
				nodes: state.nodes.update(id, emptyNode, (node) => ({
					...node,
					name: 'string',
					resultType: nodeResultType,
					nodeType: 'inputString',
					value
				})),
				outputValues: Map(),
				errors: errors,
				incompatibleEdges: incompatibleEdges
			}
		}
		case 'setInputNumber': {
			const { id, value, castData } = action.payload

			const isInt = Number.isInteger(value)
			const nodeType = isInt ? 'inputInt' : 'inputDouble'
			const nodeResultType = isInt ? 'int' : 'double'

			//find all edges where this node is start node
			const edges = Array.from(state.edges.values()).filter((edge) => edge.startNodeId === id)
			const nodes = state.nodes
			let errorEdges = state.incompatibleEdges

			//check all node's edges after setting node state
			const { errors, incompatibleEdges } = updateIncompatibleEdges(edges, nodes, nodeResultType, castData, errorEdges)

			return {
				...state,
				nodes: state.nodes.update(id, emptyNode, (node) => ({
					...node,
					name: nodeResultType,
					resultType: nodeResultType,
					nodeType,
					value
				})),
				outputValues: Map(),
				errors,
				incompatibleEdges
			}
		}
		case 'setInputBoolean': {
			const { id, value, castData } = action.payload
			const nodeResultType = 'bool'
			//find all edges where this node is starnode
			const edges = Array.from(state.edges.values()).filter((edge) => edge.startNodeId === id)
			const nodes = state.nodes
			let errorEdges = state.incompatibleEdges

			//check all node's edges after setting node state
			const { errors, incompatibleEdges } = updateIncompatibleEdges(edges, nodes, nodeResultType, castData, errorEdges)

			return {
				...state,
				nodes: state.nodes.update(id, emptyNode, (node) => ({
					...node,
					name: nodeResultType,
					resultType: nodeResultType,
					nodeType: 'inputBoolean',
					value
				})),
				outputValues: Map(),
				errors,
				incompatibleEdges
			}
		}
		case 'setInputStringToParse': {
			const { id, value, nodeType, castData } = action.payload
			const nodeResultType = 'abstraction::UnspecifiedType'
			//find all edges where this node is start node
			const edges = Array.from(state.edges.values()).filter((edge) => edge.startNodeId === id)
			const nodes = state.nodes
			let errorEdges = state.incompatibleEdges

			//check all node's edges after setting node state
			const { errors, incompatibleEdges } = updateIncompatibleEdges(edges, nodes, nodeResultType, castData, errorEdges)

			return {
				...state,
				nodes: state.nodes.update(id, emptyNode, (node) => ({
					...node,
					name: parsedInputType[nodeType].name,
					resultType: nodeResultType,
					nodeType: nodeType,
					value: value
				})),
				outputValues: Map(),
				errors,
				incompatibleEdges
			}
		}
		case 'setCustomFunctionNode': {
			const { id, value } = action.payload
			const nodeResultType = value.resultType

			return {
				...state,
				nodes: state.nodes.update(id, emptyNode, (node) => ({
					...node,
					name: value.name,
					params: value.params,
					resultType: nodeResultType,
					nodeType: 'customFunction',
					value: value
				})),
				outputValues: Map()
			}
		}
		case 'setOutputValues': {
			return {
				...state,
				outputValues: action.payload
			}
		}
		case 'deleteNode': {
			const newEdges = state.edges.filter(
				({ startNodeId, endNodeId }) =>
					![startNodeId, endNodeId].includes(action.payload)
			)

			return {
				...state,
				nodes: state.nodes.delete(action.payload),
				edges: newEdges,
				cycleEdges: detectCycles(newEdges.toSet()),
				outputValues: Map(),
				incompatibleEdges: state.incompatibleEdges.filter(
					({ startNodeId, endNodeId }) =>
						![startNodeId, endNodeId].includes(action.payload)
				)
			}
		}
		case 'deleteEdge': {
			const newEdges = state.edges.delete(action.payload)
			return {
				...state,
				edges: newEdges,
				cycleEdges: detectCycles(newEdges.toSet()),
				outputValues: Map(),
				incompatibleEdges: state.incompatibleEdges.filter(
					({ id }) => id !== action.payload
				)
			}
		}
		case 'importGraph': {
			const { data, castData } = action.payload
			const { nodes, edges } = data
			const errors: string[] = []

			const cycleEdges = detectCycles(edges.toSet())
			if (!cycleEdges.isEmpty) errors.push('Cycle detected')

			let incompatibleEdges = Set<AlgorithmEdge>()
			const validatedEdges = edges.filter((edge) => {
				const startNode = nodes.get(edge.startNodeId)
				const endNode = nodes.get(edge.endNodeId)

				if (!startNode || !endNode || !endNode.params[edge.endParamIndex]) {
					errors.push(`Import error: edge ${edge.id} is invalid`)
					return false
				}

				const from = fromTypes(nodes, edge.startNodeId)
				const to = toTypes(nodes, edge.endNodeId, edge.endParamIndex)

				if (!areCastable(castData, from, to)) {
					incompatibleEdges = incompatibleEdges.add(edge)
					errors.push(incompatibleTypeError(state.nodes.get(edge.endNodeId)?.name ?? '', to))
				}

				return true
			})

			return {
				...initialAlgorithmState,
				nodes,
				edges: validatedEdges,
				cycleEdges,
				errors,
				incompatibleEdges
			}
		}
		case 'clear' : {
			return { ...initialAlgorithmState }
		}
		default: {
			return state
		}
	}
}
