import { Set } from 'immutable'
import { parseGrammar } from 'utils/alt/import/parsers'
import GrammarData from 'interfaces/GrammarData'

const data1 = `CFG (
{S, A, B, C},
{a, b, c, d, e},
{S -> C d e S e | d d B | c A,
A -> C d | b b,
B -> b a C d |,
C -> A c a | a
},
S)`;

const expected1: GrammarData = {
	initialSymbol: 'S',
	type: 'CFG',
	nonterminals: Set.of('S', 'A', 'B', 'C'),
	terminals: Set.of('a', 'b', 'c', 'd', 'e'),
	rules: [
		{ left: 'S', right: 'C d e S e' },
		{ left: 'S', right: 'd d B' },
		{ left: 'S', right: 'c A' },
		{ left: 'A', right: 'C d' },
		{ left: 'A', right: 'b b' },
		{ left: 'B', right: 'b a C d' },
		{ left: 'B', right: '' },
		{ left: 'C', right: 'A c a' },
		{ left: 'C', right: 'a' },
	]
};

const data2 = `RIGHT_RG (
{S', S, A},
{a, b},
{
	S' -> a S | | b S,
	S -> a,
	A -> a
},
S')`;

const expected2: GrammarData = {
	initialSymbol: 'S\'',
	type: 'RIGHT_RG',
	nonterminals: Set.of('S\'', 'S', 'A'),
	terminals: Set.of('a', 'b'),
	rules: [
		{ left: 'S\'', right: 'a S' },
		{ left: 'S\'', right: '' },
		{ left: 'S\'', right: 'b S' },
		{ left: 'S', right: 'a' },
		{ left: 'A', right: 'a' }
	]
};

const data3 = `RIGHT_RG (
{
	((InitialStateLabel), 0),
	(a, 1),
	(b, 2)
},
{a, b},
{
	((InitialStateLabel), 0) -> a | a (a, 1) | b | b (b, 2)
},
((InitialStateLabel), 0)
)`

const expected3: GrammarData = {
	initialSymbol: '((InitialStateLabel), 0)',
	type: 'RIGHT_RG',
	nonterminals: Set.of('((InitialStateLabel), 0)', '(a, 1)', '(b, 2)'),
	terminals: Set.of('a', 'b'),
	rules: [
		{ left: '((InitialStateLabel), 0)', right: 'a' },
		{ left: '((InitialStateLabel), 0)', right: 'a (a, 1)' },
		{ left: '((InitialStateLabel), 0)', right: 'b' },
		{ left: '((InitialStateLabel), 0)', right: 'b (b, 2)' }
	]
};

const data4 = "RIGHT_RG ( {A}, {}, {}, A)"

const expected4: GrammarData = {
	initialSymbol: 'A',
	type: 'RIGHT_RG',
	nonterminals: Set.of('A'),
	terminals: Set.of(),
	rules: []
};

const data5 = "RIGHT_RG ( {A}, {a}, {}, A) ooops"

const data6 = "RIGHT_RGFFFFFFF ( {A}, {a}, {}, A)"

const data7 = `CFG( {[S]', [S'], S'}, {a}, {}, [S]')`
const expected7: GrammarData = {
	initialSymbol: "[S]'",
	type: 'CFG',
	nonterminals: Set.of("[S]'", "[S']", "S'"),
	terminals: Set.of("a"),
	rules: []
};

describe('import alt-grammar', () => {
	it('converts CFG', () => {
		expect(parseGrammar(data1)).toEqual(expected1)
	})
	it('converts RightRG', () => {
		expect(parseGrammar(data2)).toEqual(expected2)
	})
	it('converts RightRG with composed names', () => {
		expect(parseGrammar(data3)).toEqual(expected3)
	})
	it('parses empty terminal set and rule set', () => {
		expect(parseGrammar(data4)).toEqual(expected4)
	})
	it('throws when not an EOF after parsing', () => {
		expect(() => parseGrammar(data5)).toThrow("but \"o\" found")
	})
	it('throws on invalid grammar type', () => {
		expect(() => parseGrammar(data6)).toThrow("but \"F\" found")
	})
	it('parses [S]\' symbol', () => {
		expect(parseGrammar(data7)).toEqual(expected7)
	})

	afterAll(() => jest.resetModules())
})
