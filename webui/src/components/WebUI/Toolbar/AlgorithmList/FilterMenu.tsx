import {
    Checkbox,
    FormControlLabel,
    FormGroup, FormLabel,
    Grid,
    IconButton,
    makeStyles
} from '@material-ui/core'
import React, { useCallback } from 'react'
import { ExpandMore } from '@material-ui/icons'
import { useSyncState } from 'hooks/useSyncState'

export interface FilterMenuProps {
    values: () => {name: boolean, params: boolean, returnValue: boolean, docs: boolean}
    enable: () => void
    disable: () => void
    onChange: (e: React.ChangeEvent<HTMLInputElement>) => void
}

const useStyles = makeStyles({
    open: {
        transition: 'transform 300ms cubic-bezier(0.4, 0, 0.2, 1)',
        transform: 'rotate(-180deg)'
    },
    close: {
        transition: 'transform 300ms cubic-bezier(0.4, 0, 0.2, 1)',
        transform: 'rotate(0)'
    }
})

export const FilterMenu = (props: FilterMenuProps) => {
    const { values, enable, disable, onChange } = props
    const classes = useStyles()
    const [show, setShow] = useSyncState<boolean>(false)

    const handleClick = useCallback(() => {
        setShow(!show())

        if (show()) {
            enable()
        } else {
            disable()
        }
    }, [show] )

    return (
        <React.Fragment>
            <Grid container direction={'column'}>
            <Grid container direction={'row'} justifyContent="flex-end" alignItems="center">
                <FormLabel component="legend">filtering options</FormLabel>
                <IconButton onClick={handleClick}>
                    <ExpandMore className={show() ? classes.open : classes.close}/>
                </IconButton>
            </Grid>
            {show() &&
                <FormGroup row>
                    <FormControlLabel
                        control={
                            <Checkbox
                                checked={values().name}
                                onChange={onChange}
                                name="name"
                                color="primary"
                            />
                        }
                        label="name"
                    />
                    <FormControlLabel
                        control={
                            <Checkbox
                                checked={values().params}
                                onChange={onChange}
                                name="params"
                                color="primary"
                            />
                        }
                        label="parameter"
                    />
                    <FormControlLabel
                        control={
                            <Checkbox
                                checked={values().returnValue}
                                onChange={onChange}
                                name="returnValue"
                                color="primary"
                            />
                        }
                        label="return value"
                    />
                    <FormControlLabel
                        control={
                            <Checkbox
                                checked={values().docs}
                                onChange={onChange}
                                name="docs"
                                color="primary"
                            />
                        }
                        label="docs"
                    />
                </FormGroup>
            }
            </Grid>
        </React.Fragment>
    )
}
