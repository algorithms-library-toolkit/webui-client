import Automaton from 'interfaces/Automaton'

export const serializeAutomaton = (automaton: Automaton): string => {
	let res: string = ""

	res = res.concat(`${automaton.type} `).concat(automaton.alphabet.join(" ")).concat("\n")
	res = res.concat(automaton.states.reduce((acc, state) => {
		let row: string = ""

		if (automaton.initialStates.indexOf(state) >= 0) {
			row = row.concat(">")
		}

		if (automaton.finalStates.indexOf(state) >= 0) {
			row = row.concat("<")
		}

		row = row.concat(state).concat(" ")

		const cells = automaton.alphabet.reduce((acc, symb) => {
			const outgoing = automaton.transitions.filter(tr => tr.symbol === symb && tr.from === state).map(tr => tr.to)
			return [
				...acc,
				outgoing.length > 0 ? outgoing.join("|") : "-"
			]
		}, [] as string[])

		return acc.concat(row.concat(cells.join(" ")).concat("\n"))
	}, ""))

	return res
}
