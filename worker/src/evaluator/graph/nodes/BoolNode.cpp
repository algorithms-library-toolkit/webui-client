#include <string>
#include "BoolNode.hpp"

BoolNode::BoolNode(const std::string& id, bool value, const ext::vector<std::string>& templateParams)
    : InputNode(id, value, templateParams)
{
}

BoolNode::BoolNode(const std::string& id, const Json::Value& json)
    : InputNode(id, json["value"].asBool(), {})
{
}
