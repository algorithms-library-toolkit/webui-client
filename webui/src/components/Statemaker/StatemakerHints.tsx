import * as React from 'react'
import { useSelector } from 'react-redux'

import { makeStyles } from '@material-ui/styles'

import { ReduxState } from '../../reducers'
import { Hints } from '../Hints'

/** All text showed in main hint */
const text: string[][] = [
	['Build state', 'S'],
	['Build initial state', 'I'],
	['Build final state', 'F'],
	['Build transition', 'T'],
	['Toggle hint', 'H'],
	['Toggle grid', 'G'],
	['Select mode', 'L'],
	['Move mode', 'M'],
	['Position machine', 'P'],
	['Horizontal positioning', 'Hold Shift'],
	['Vertical positioning', 'Hold Alt'],
	['Undo', 'Ctrl + Z'],
	['Redo', 'Ctrl + Y'],
	['Save', 'Ctrl + S'],
	['Load', 'Ctrl + O']
]

/** Text showed when state is selected */
const stateText: string[][] = [
	['Set state as initial', 'Ctrl + I'],
	['Set state as final', 'Ctrl + F'],
	['Delete state', 'Ctrl + Del'],
	['New line', 'Shift + Enter'],
	['Deselect', 'Enter / Esc']
]

/** Text showed when transition is selected */
const transitionText: string[][] = [
	['Delete transition', 'Ctrl + Del'],
	['New line', 'Shift + Enter'],
	['Deselect', 'Enter / Esc']
]

/** Text showed when state is selected */
const stateBuildText: string[][] = [['Drop or click on canvas to place state']]

/** Text showed when transition is selected */
const transitionBuildText: string[][] = [['Drag or click onto state to place']]

const useStyles = makeStyles({
	root: {
		position: 'absolute',
		bottom: '60px',
		right: 0,
		zIndex: 8,
		pointerEvents: 'none',
		marginRight: 8,
		marginBottom: 8,
		textAlign: 'right'
	},
	action: {
		position: 'absolute',
		top: '60px',
		right: 0,
		zIndex: 8,
		pointerEvents: 'none',
		marginTop: 8,
		marginRight: 8,
		textAlign: 'right'
	}
})

/**
 * Shows table with keyboard shortcuts
 *
 * @returns
 */
function Hint() {
	const classes = useStyles()
	const {
		showHint,
		buildState,
		buildTransition,
		selectedState,
		selectedTransition
	} = useSelector(
		(state: ReduxState) => ({
			showHint: state.stateUI.showHint,
			buildState: state.stateUI.buildState,
			buildTransition: state.stateUI.buildTransition,
			selectedState: state.stateCanvas.selectedNode,
			selectedTransition: state.stateCanvas.selectedEdge
		})
	)

	if (!showHint) return null

	let actionText: string[][] = []
	if (selectedState) {
		actionText = stateText
	} else if (selectedTransition) {
		actionText = transitionText
	} else if (buildState) {
		actionText = stateBuildText
	} else if (buildTransition) {
		actionText = transitionBuildText
	}

	return (
		<React.Fragment>
			{actionText.length > 0 && (
				<div className={classes.action}>
					<Hints hints={actionText} />
				</div>
			)}
			<div className={classes.root}>
				<Hints hints={text} />
			</div>
		</React.Fragment>
	)
}

export default Hint
