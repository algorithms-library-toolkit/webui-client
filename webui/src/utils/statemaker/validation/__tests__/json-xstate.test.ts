import validate from '../json-xstate'

const data: any = {
	id: 'machine',
	initial: 'a',
	states: {
		a: {},
		b: {
			on: { '1': 'a' }
		},
		d: {
			on: { '1': 'a' },
			// @ts-ignore
			on: { '2': {} },
			off: {}
		},
		c: {
			'1': {}
		},
		x: 'k'
	},
	test: {}
}

test('expect error with file wrong props', () => {
	expect(validate({})).toEqual([
		'Missing property: id',
		'Missing property: initial',
		'Missing property: states'
	])
})

test('expect error with wrong id and initial type', () => {
	expect(validate({ id: 1, initial: [], states: {} })).toEqual([
		'Id name error: 1 is not string',
		'Initial state name error:  is not string'
	])
})

test('expect get correct errors', () => {
	expect(validate(data)).toEqual([
		'Unidentified key: test',
		'State error: k is not object',
		'State error: Missing property: on',
		'State error: Missing property: on',
		'State error: Missing property: on',
		'State error: Unidentified key: off',
		'State error: Unidentified key: 1',
		'State error: Unidentified key: 0',
		'Transitions error: k is not object',
		'Transition error: [object Object] is not string or number'
	])
})
