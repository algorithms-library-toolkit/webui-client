#include <common/EvalHelper.h>
#include "OutputNode.hpp"
#include "utils/string.hpp"

OutputNode::OutputNode(const std::string& id, const ext::vector<std::string>& templateParams)
    : AbstractNode(id, templateParams)
{
}

OutputNode::OutputNode(const std::string& id, const Json::Value& jsonObject)
    : AbstractNode(id, jsonObject)
{
}

bool OutputNode::isOutput() const
{
    return true;
}

bool OutputNode::isInput() const
{
    return false;
}

std::optional<std::string> OutputNode::getResult()
{
    return result;
}

std::optional<std::string> OutputNode::getResultStringCompose()
{
    return resultCompose;
}

std::optional<std::string> OutputNode::getResultDot()
{
    return resultDot;
}

std::optional<std::string> OutputNode::getResultLatex()
{
    return resultLatex;
}

std::optional<core::type_details> OutputNode::getResultType()
{
    return resultType;
}

void OutputNode::evaluate(abstraction::TemporariesHolder& environment)
{
    result = utils::trim(abstractValueToString(params[0]));
    resultType = params[0]->getActualType();

    try {
        auto composeValue = abstraction::EvalHelper::evalAlgorithm(environment, "string::Compose", {}, {params[0]}, abstraction::AlgorithmCategories::AlgorithmCategory::NONE);
        resultCompose = utils::trim(abstractValueToString(composeValue));
    } catch (...) {
    }

    try {
        auto composeValue = abstraction::EvalHelper::evalAlgorithm(environment, "convert::DotConverter", {}, {params[0]}, abstraction::AlgorithmCategories::AlgorithmCategory::NONE);
        resultDot = utils::trim(abstractValueToString(composeValue));
    } catch (...) {
    }

    try {
        auto composeValue = abstraction::EvalHelper::evalAlgorithm(environment, "convert::LatexConverter", {}, {params[0]}, abstraction::AlgorithmCategories::AlgorithmCategory::NONE);
        resultLatex = utils::trim(abstractValueToString(composeValue));
    } catch (...) {
    }
}
