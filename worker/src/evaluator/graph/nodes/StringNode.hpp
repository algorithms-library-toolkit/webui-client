#pragma once
#include <ext/vector>
#include <json/json.h>
#include <string>
#include "graph/nodes/InputNode.hpp"

/**
 * InputNode representing a string value
 */
class StringNode : public InputNode<std::string> {
public:
    StringNode(const std::string& id, const std::string& value, const ext::vector<std::string>& templateParams);
    StringNode(const std::string& id, const Json::Value& jsonObject);
};
