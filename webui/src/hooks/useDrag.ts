import * as React from 'react'

/**
 * Listens on global mouse up and disables drag when triggered
 *
 * @returns {[ boolean, React.Dispatch<React.SetStateAction<boolean>> ]}
 */
const useDrag = (canvasRef: React.RefObject<SVGSVGElement>): [
	boolean,
	React.Dispatch<React.SetStateAction<boolean>>
] => {
	const [selected, setSelected] = React.useState(false)

	const handleDeselect = React.useCallback(() => {
		setSelected(false)
	}, [])

	React.useEffect(() => {
		// Do nothing if no state selected
		if (!selected) return

		const canvas = canvasRef.current!

		canvas.addEventListener('mouseup', handleDeselect)
		return () => {
			canvas.removeEventListener('mouseup', handleDeselect)
		}
	}, [canvasRef, handleDeselect, selected])

	return [selected, setSelected]
}

export default useDrag
