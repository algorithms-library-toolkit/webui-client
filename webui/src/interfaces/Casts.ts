import { AlibType } from 'interfaces/Algorithms'

export type Casts = Record<AlibType, AlibType[]>
