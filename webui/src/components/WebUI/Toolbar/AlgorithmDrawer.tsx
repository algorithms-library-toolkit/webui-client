import React, { useMemo, useCallback } from 'react'
import {
	Drawer,
	makeStyles,
	Divider,
	Grid,
	Card,
	CardActionArea,
	CardContent,
	Typography,
} from '@material-ui/core'
import { WebUITheme } from '../../../interfaces/Theme'
import {
	automatonTypes,
	automanAlibTypeMap,
} from '../../../interfaces/AutomatonTypes'
import { batch, useDispatch, useSelector } from 'react-redux'
import AlgorithmList from './AlgorithmList/AlgorithmList'
import { algorithmCanvasActions } from '../../../reducers/algorithmCanvas'
import { algorithmUIActions } from '../../../reducers/algorithmUI'
import { ReduxState } from '../../../reducers'
import { AlgorithmNodeType, AlgorithmNodeValue } from '../../../interfaces/AlgorithmNode'
import { AlibType, AlgorithmParam } from '../../../interfaces/Algorithms'

const { setBuildNode } = algorithmCanvasActions
const { updateGhostNode, setAlgorithmToolbarOpen } = algorithmUIActions

const useStyles = makeStyles<WebUITheme>((theme) => ({
	drawer: {
		width: theme.drawerWidth,
	},
	drawerPaper: {
		width: theme.drawerWidth,
	},
	gridRow: {
		padding:".3em",
		width: theme.drawerWidth,
	},
	inputButtonCard: {
		minHeight: "100%",
	},
}))

export const AlgorithmDrawer = () => {
	const dispatch = useDispatch()
	const classes = useStyles()

	const { toolbarOpen } = useSelector((state: ReduxState) => ({
		algorithmService: state.service.algorithmsService,
		toolbarOpen: state.algorithmUI.toolbarOpen,
	}))

	const drawerClassNames = useMemo(() => ({ paper: classes.drawerPaper }), [
		classes.drawerPaper,
	])

	const handleButtonClick = useCallback(
		(
			nodeType: AlgorithmNodeType,
			name: string,
			resultType: AlibType,
			params: AlgorithmParam[],
			value?: AlgorithmNodeValue
		) =>
			batch(() => {
				dispatch(
					updateGhostNode({
						nodeType,
						name,
						params,
						resultType,
						height: 75,
						value,
						templateParams: [],
					})
				)
				dispatch(setBuildNode(true))
				dispatch(setAlgorithmToolbarOpen(false))
			}),
		[dispatch]
	)

	const handleInputClick = useCallback(
		() =>
			handleButtonClick(
				'inputStringAutomaton',
				'Automaton',
				automanAlibTypeMap['DFA'],
				[],
				''
			),
		[handleButtonClick]
	)

	const handleAutomatonInputClick = useCallback(
		() =>
			handleButtonClick(
				'inputAutomaton',
				'Automaton',
				automanAlibTypeMap['DFA'],
				[]
			),
		[handleButtonClick]
	)

	const handleOutputButtonClick = useCallback(
		() =>
			handleButtonClick('output', 'Output', '', [
				{ name: 'input', type: 'abstraction::UnspecifiedType' },
			]),
		[handleButtonClick]
	)

	const handleAutomatonOutputClick = useCallback(
		() =>
			handleButtonClick('outputAutomaton', 'Automaton output', '', [
				{
					name: 'automaton',
					type: automatonTypes.reduce(
						(acc, type, index, arr) =>
							index === arr.length - 1
								? automanAlibTypeMap[type]
								: `${automanAlibTypeMap[type]} | ${acc}`,
						''
					),
				},
			]),
		[handleButtonClick]
	)

	const handleCustomFunction = useCallback(
		() =>
			handleButtonClick('customFunction', 'Custom Function', '', []),
		[handleButtonClick]
	)

	const buttons = [{
		fields: [
			{ name: "Input node", desc: "Input a (primitive or complex) value for further processing", handler: handleInputClick },
			{ name: "Automaton Interactive Input", desc: "Draw a finite automaton in interactive application and use as input", handler: handleAutomatonInputClick },
		],
	}, {
		fields: [
			{ name: "Output node", desc: "Show output as a text, image, table, LaTeX, ...", handler: handleOutputButtonClick },
			{ name: "Automaton Interactive Output", desc: "Visualize finite automata in interactive application.", handler: handleAutomatonOutputClick },
		],
	}, {
		fields: [
			{ name: "Custom function", desc: "Create a custom reusable function node using aql syntax", handler: handleCustomFunction }
		],
	}]

	return (
		<Drawer className={classes.drawer} classes={drawerClassNames} variant={'persistent'} open={toolbarOpen}>

			{buttons.map((category) => (
				<Grid container spacing={1} className={classes.gridRow}>
				{category.fields.map((field) => (
					<Grid item xs>
						<Card variant="outlined">
							<CardActionArea onClick={field.handler}>
								<CardContent className={classes.inputButtonCard}>
									<Typography variant="subtitle1">{field.name}</Typography>
									<Typography variant="caption" color="textSecondary">{field.desc}</Typography>
								</CardContent>
							</CardActionArea>
						</Card>
					</Grid>
				))}
				</Grid>
			))}

			<Divider />
			<AlgorithmList />
		</Drawer>
	)
}
