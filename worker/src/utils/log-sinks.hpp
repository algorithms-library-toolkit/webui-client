#pragma once
#include <map>
#include <spdlog/sinks/base_sink.h>
#include <spdlog/sinks/stdout_sinks.h>
#include <spdlog/spdlog.h>

/** @brief Distribution sink (mux). Stores a vector of sinks. It chooses the sink according to log message severity. */
template <class Mutex>
class loglevel_dist_sink : public spdlog::sinks::base_sink<Mutex> {
    std::vector<std::shared_ptr<spdlog::sinks::sink>> m_sinks;
    std::map<std::shared_ptr<spdlog::sinks::sink>, std::pair<spdlog::level::level_enum, spdlog::level::level_enum>> m_sinks_levels;

    void sink_it_(const spdlog::details::log_msg& msg) override
    {
        for (auto& sink : m_sinks) {
            auto [min, max] = m_sinks_levels[sink];

            if (sink->should_log(msg.level) && msg.level >= min && msg.level <= max) {
                sink->log(msg);
            }
        }
    }

    void flush_() override
    {
        for (auto& sink : m_sinks) {
            sink->flush();
        }
    }

public:
    explicit loglevel_dist_sink()
        : m_sinks()
    {
    }

    loglevel_dist_sink(const loglevel_dist_sink&) = delete;
    loglevel_dist_sink& operator=(const loglevel_dist_sink&) = delete;

    /** @brief Add sink and log levels when the sink should be used */
    void add_sink(std::shared_ptr<spdlog::sinks::sink> sink, const spdlog::level::level_enum& lo, const spdlog::level::level_enum& hi)
    {
        std::lock_guard<Mutex> lock(spdlog::sinks::base_sink<Mutex>::mutex_);
        m_sinks.push_back(sink);
        m_sinks_levels.insert(std::make_pair(sink, std::make_pair(lo, hi)));
    }
};

using loglevel_dist_sink_mt = loglevel_dist_sink<std::mutex>;
using loglevel_dist_sink_st = loglevel_dist_sink<spdlog::details::null_mutex>;
