import React, { useState, useCallback } from 'react'
import { useSelector } from 'react-redux'
import { ReduxState } from 'reducers'
import {
	Tooltip,
	List,
	ListItem,
	TextField,
	IconButton,
	makeStyles
} from '@material-ui/core'
import { AlgorithmCategory } from 'interfaces/Algorithms'
import { AlgorithmCategoryList } from 'components/WebUI/Toolbar/AlgorithmList/AlgorithmCategoryList'
import { ClearOutlined } from '@material-ui/icons';
import { theme } from 'interfaces/Theme'
import { useSyncState } from 'hooks/useSyncState'
import {
	DocsFilter,
	Filter,
	NameFilter,
	ParamFilter,
	ReturnValueFilter
} from 'interfaces/Filter'
import { FilterMenu } from 'components/WebUI/Toolbar/AlgorithmList/FilterMenu'

const useStyles = makeStyles({
	textField: {
		width: '100%'
	},
	root: {
		'& > div': {
			border: 'unset'
		}
	},
	filterItem: {
		paddingTop: theme.spacing(0),
		paddingBottom: theme.spacing(0)
	}
})

const AlgorithmList = () => {
	const classes = useStyles()
	const [filterParams, setFilterParams] = useSyncState({
		name: false,
		params: false,
		returnValue: false,
		docs: false
	})
	const [value, setValue] = useState<string>('')
	const [filter, setFilter] = useSyncState<Filter[]>([new NameFilter(), new ParamFilter(),
		new ReturnValueFilter(), new DocsFilter()])

	const { algorithms } = useSelector((state: ReduxState) => ({
		algorithms: state.workerDefinitions.algorithms
	}))

	const getFilters = (): Filter[] => {
		const filters: Filter[] = []

		if (filterParams().name)
			filters.push(new NameFilter())
		if (filterParams().params)
			filters.push(new ParamFilter())
		if (filterParams().returnValue)
			filters.push(new ReturnValueFilter())
		if (filterParams().docs)
			filters.push(new DocsFilter())

		if (filters.length === 0)
			filters.push(new NameFilter(), new ParamFilter(), new ReturnValueFilter(), new DocsFilter())

		return filters
	}

	const enableFilter = () => {
		setFilter(getFilters())
	}

	const disableFilter = () => {
		const filters: Filter[] = [new NameFilter(), new ParamFilter(), new ReturnValueFilter(), new DocsFilter()]
		setFilter(filters)
	}

	const handleChange = useCallback(
		(e: React.ChangeEvent<HTMLInputElement>) => {
			setFilterParams({...filterParams(), [e.target.name]: e.target.checked})
			setFilter(getFilters())
		},
		[filterParams]
	)

	const handleFilterChange = useCallback(
		(e: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>) =>
			setValue(e.target.value),
		[]
	)

	const handleKeyDown = useCallback(
		(e: React.KeyboardEvent) => e.stopPropagation(),
		[]
	)

	return (
		<List className={classes.root}>
			<ListItem>
				<TextField
					className={classes.textField}
					label="Filter algorithms"
					value={value}
					onChange={handleFilterChange}
					onKeyDown={handleKeyDown}
				/>
				<Tooltip title="Clear filter" aria-label="delete" placement="left">
					<IconButton aria-label="delete" color="primary" onClick={() => {setValue('')}}>
						<ClearOutlined/>
					</IconButton>
				</Tooltip>
			</ListItem>
			<ListItem className={classes.filterItem}>
				<FilterMenu
					values={filterParams}
					enable={enableFilter}
					disable={disableFilter}
					onChange={handleChange}
				/>
			</ListItem>
			{algorithms
				.valueSeq()
				.map((category: AlgorithmCategory, index: number) => (
					<AlgorithmCategoryList
						key={index}
						algorithmCategory={category}
						filters={filter()}
						filterValue={value}
					/>
				))
				.toArray()}
		</List>
	)
}

export default AlgorithmList
