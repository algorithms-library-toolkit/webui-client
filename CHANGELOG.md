# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]
## [0.10.0] - 2024-10-22
- webui: Add an option to copy the LaTeX output to the clipboard (https://gitlab.fit.cvut.cz/algorithms-library-toolkit/webui-client/-/merge_requests/68)
## [0.9.3] - 2022-12-08
## Fixes
- webui: Fix correct setting of final states in automata from Statemaker inputs.

## [0.9.2] - 2022-12-04
## Fixes
- webui: Allow digit terminals in the grammar form (https://gitlab.fit.cvut.cz/algorithms-library-toolkit/webui-client/-/issues/92)

## [0.9.1] - 2022-11-20
## Fixes
- webui: Fix nostats flag detection

## [0.9.0] - 2022-11-02
## Changed
- Bump [core] version to ref [d3684dd430e746dcc8fa8ea7d0795a048f49620a](https://gitlab.fit.cvut.cz/algorithms-library-toolkit/automata-library/commit/d3684dd430e746dcc8fa8ea7d0795a048f49620a). This brings only a fix of AutomataUnionCartesianProduct which produced invalid results for automata with different alphabets.

## [0.8.0] - 2022-10-27
## Changed
- Bump [core] version to ref [bd3eddd7cf1645d2d54113c5ffa2ecedffd4f0fe](https://gitlab.fit.cvut.cz/algorithms-library-toolkit/automata-library/commit/bd3eddd7cf1645d2d54113c5ffa2ecedffd4f0fe). This brings mainly change in the automaton::determinize::Determinize algorithms which now doesn't create total DFA. There are also some new algorithms and overloads over unranked trees.

## [0.7.3] - 2022-10-21
### Fixed
- webui: Shortener's nostats flag should be passed to shortener

## [0.7.2] - 2022-10-19
### Fixed
 - worker: Try to fix Internal error caused by wrong move semantics on ALT objects (https://gitlab.fit.cvut.cz/algorithms-library-toolkit/webui-client/-/issues/91)

## [0.7.1] - 2022-10-18
### Fixed
 - webui: Fix graphviz output error not showing

## [0.7.0] - 2022-10-18
### Added
 - webui: Basic support for JSON migrations. Old JSONs should now be loadable and they should be converted.
 - webui: Canvas is now movable with mouse secondary button drag (right button on standard layout).

### Changed
 - webui: Automaton table input and automaton string input are now in sync.
 - webui: Improved automaton table input user experience.
 - webui: Table input is now shown first when Automaton input selected (formerly it was the string input).
 - webui: Try to clarify string vs String input in the app (https://gitlab.fit.cvut.cz/algorithms-library-toolkit/webui-client/-/issues/89)
 - webui: Remember last type of input box. Don't always reset to string. Also, default is now Automaton instead of string.
 - webui: Version info of the WebUI in the top right corner now points to CHANGELOG.md of the current version.
 - webui: Algorithm list is now a bit more compact.

## [0.6.0] - 2022-10-16
### Changed
 - webui: Share button now also generates short links using our new link shortener service. The JSON is stored on our side.

## [0.5.3] - 2022-10-11
### Fixed
- webui: Fix a bug where Output node failed for automata with state labelled '{}'

## [0.5.2] - 2022-10-04
### Fixed
- webui: Automaton table input did not allow symbols consisting from digits (https://gitlab.fit.cvut.cz/algorithms-library-toolkit/webui-client/-/issues/88)
- webui: Automaton table input should now correctly change fields on TAB and ENTER keystrokes (https://gitlab.fit.cvut.cz/algorithms-library-toolkit/webui-client/-/issues/87)

## [0.5.1] - 2022-10-03
### Added
- webui: Add bug report button

## [0.5.0] - 2022-10-02
- webui: Fix a bug where clear canvas button was not working after graph import
- webui: Fix deprecated MouseEvent code
- webui: Fix overflowing docs popover

### Changed
- webui: Restyle basic input boxes in the left drawer

### Added
- worker: Show current log level when starting worker

## [0.4.1] - 2022-05-15
### Changed
- Bump [core] version to ref [e38a6578eca11b18a0fbe5802e5d9f110d2c38d8](https://gitlab.fit.cvut.cz/algorithms-library-toolkit/automata-library/commit/e38a6578eca11b18a0fbe5802e5d9f110d2c38d8).
- webui: output dialog now offers all output types available (text, graphviz, latex, and specialized formats for grammars and finite automata)

### Removed
- webui: removed dot output dialog in favour of new output dialog

## [0.4.0] - 2022-05-14
### Added
- webui: A custom functions experimental support

### Changed
- Bump [core] version to ref [1b02620e2078987347e022f8dd8155db7d39c40f](https://gitlab.fit.cvut.cz/algorithms-library-toolkit/automata-library/commit/1b02620e2078987347e022f8dd8155db7d39c40f).
- Hide some algorithms from the webui (example, raw, cli, xml, and sax namespaces).
- worker: Simplified argument passing. All args are now optional, defaults equal to standard local setups (i.e. 15s job timeout, tcp://localhost:61616).
- worker: Now uses boost::asio and boost::process for subprocess timeout handling (Boost header only dependency).
- webui: Rewritten ALT format parsers using peggy.js (new npm dependency).
- webui: We now group algorithm overloads with same number of parameters.
- webui: We now show the Algorithm docs (not only the overload specific one)

## [0.3.0] - 2022-04-17
### Added
- webui: Add unified input dialog that can be used instead of (raw) Input connected to string::Parse<T> algorithm
- webui: Add input and output forms for grammars and finite automata
- webui: Add advanced filters for the algorithm list
- webui: Added documentation button to algorithm boxes on canvas
- webui: string::Parse<T> algorithms now show the T type on its box on canvas
- webui: A custom functions support

### Changed
- Bump [core] version to ref [6fa14a8d87b625cf90204a79a415e85c87acc465](https://gitlab.fit.cvut.cz/algorithms-library-toolkit/automata-library/commit/6fa14a8d87b625cf90204a79a415e85c87acc465)
- webui: Nodes with verbose output from core are now visualized in another color than output boxes and their content is now printed in monospace font
- worker: Simplified argument passing. All args are now optional, defaults equal to standard local setups (i.e. 15s job timeout, tcp://localhost:61616).
- worker: Now uses boost::asio and boost::process for subprocess timeout handling (Boost header only dependency).
- webui: Rewritten ALT format parsers using peggy.js (new npm dependency).
- webui: We now group algorithm overloads with same number of parameters.
- webui: Some algorithms (like the cli:: builtins, xml-handling algorithms, or example::) are now hidden from webui.
- webui: We now show the Algorithm docs (not only the overload specific one)

### Fixed
- webui: Fixed a bug where AutomatonOutput failed to open [#48](https://gitlab.fit.cvut.cz/algorithms-library-toolkit/webui-client/issues/48)

## [0.2.1] - 2022-02-25
### Added
- server: Enable HTTP responses compression

### Fixed
- worker: Fix worker crash when processing empty request

## [0.2.0] - 2022-02-19
### Added
- Reworked to dynamically load definitions from server. This decouples WebUI from [core].
- WebUI: Show running [core] version as well as WebUI.
- WebUI: Introduce production and development environment files.

### Changed
- WebUI: Bump all dependencies.
- WebUI: Remove react-graphviz dependency in favour of directly using d3-graphviz.
- WebUI: Graphviz dialog now uses two buttons for download instead of the menu and default dimensions are a bit larger.
- deployment: Deploy on push to staging or tags.

## [0.1.0] - 2021-11-25
### Added
- Initial release.
- Bump supported [core] version to ref [9808385fd97d465959275fe007acce3c9b11e29d](https://gitlab.fit.cvut.cz/algorithms-library-toolkit/automata-library/commit/9808385fd97d465959275fe007acce3c9b11e29d)

[core]: https://gitlab.fit.cvut.cz/algorithms-library-toolkit/automata-library
[Unreleased]: https://gitlab.fit.cvut.cz/algorithms-library-toolkit/webui-client/compare/v0.10.0...master
[0.10.0]: https://gitlab.fit.cvut.cz/algorithms-library-toolkit/webui-client/compare/v0.9.3...v0.10.0
[0.9.3]: https://gitlab.fit.cvut.cz/algorithms-library-toolkit/webui-client/compare/v0.9.2...v0.9.3
[0.9.2]: https://gitlab.fit.cvut.cz/algorithms-library-toolkit/webui-client/compare/v0.9.1...v0.9.2
[0.9.1]: https://gitlab.fit.cvut.cz/algorithms-library-toolkit/webui-client/compare/v0.9.0...v0.9.1
[0.9.0]: https://gitlab.fit.cvut.cz/algorithms-library-toolkit/webui-client/compare/v0.8.0...v0.9.0
[0.8.0]: https://gitlab.fit.cvut.cz/algorithms-library-toolkit/webui-client/compare/v0.7.3...v0.8.0
[0.7.3]: https://gitlab.fit.cvut.cz/algorithms-library-toolkit/webui-client/compare/v0.7.2...v0.7.3
[0.7.2]: https://gitlab.fit.cvut.cz/algorithms-library-toolkit/webui-client/compare/v0.7.1...v0.7.2
[0.7.1]: https://gitlab.fit.cvut.cz/algorithms-library-toolkit/webui-client/compare/v0.7.0...v0.7.1
[0.7.0]: https://gitlab.fit.cvut.cz/algorithms-library-toolkit/webui-client/compare/v0.6.0...v0.7.0
[0.6.0]: https://gitlab.fit.cvut.cz/algorithms-library-toolkit/webui-client/compare/v0.5.3...v0.6.0
[0.5.3]: https://gitlab.fit.cvut.cz/algorithms-library-toolkit/webui-client/compare/v0.5.2...v0.5.3
[0.5.2]: https://gitlab.fit.cvut.cz/algorithms-library-toolkit/webui-client/compare/v0.5.1...v0.5.2
[0.5.1]: https://gitlab.fit.cvut.cz/algorithms-library-toolkit/webui-client/compare/v0.5.0...v0.5.1
[0.5.0]: https://gitlab.fit.cvut.cz/algorithms-library-toolkit/webui-client/compare/v0.4.1...v0.5.0
[0.4.1]: https://gitlab.fit.cvut.cz/algorithms-library-toolkit/webui-client/compare/v0.4.0...v0.4.1
[0.4.0]: https://gitlab.fit.cvut.cz/algorithms-library-toolkit/webui-client/compare/v0.3.0...v0.4.0
[0.3.0]: https://gitlab.fit.cvut.cz/algorithms-library-toolkit/webui-client/compare/v0.2.1...v0.3.0
[0.2.1]: https://gitlab.fit.cvut.cz/algorithms-library-toolkit/webui-client/compare/v0.2.0...v0.2.1
[0.2.0]: https://gitlab.fit.cvut.cz/algorithms-library-toolkit/webui-client/compare/v0.1.0...v0.2.0
[0.1.0]: https://gitlab.fit.cvut.cz/algorithms-library-toolkit/webui-client/tags/v0.1.0
