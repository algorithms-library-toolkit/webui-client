if [ -z "$ALT_WORKER_SERVER" ]; then
	echo "ALT_WORKER_SERVER env var unspecified" >&2
	exit 1
fi
if [ -z "$ALT_WORKER_TIMEOUT" ]; then
	ALT_WORKER_TIMEOUT=15000
fi
PARAMS=""
if [ ! -z "$ALT_WORKER_DEBUG" ]; then
  PARAMS="$PARAMS --log-level 5"
fi
if [ ! -z "$ALT_WORKER_LOG_STDERR" ]; then
  PARAMS="$PARAMS --log-stderr"
fi

/usr/local/bin/alt-webui-worker --broker tcp://"${ALT_WORKER_SERVER}" --evaluator /usr/local/bin/alt-webui-worker-evaluator --timeout "$ALT_WORKER_TIMEOUT" $PARAMS
