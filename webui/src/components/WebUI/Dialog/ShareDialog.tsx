import React, { useMemo } from 'react'
import { useSelector, useDispatch } from 'react-redux'
import { useSnackbar } from 'notistack'
import {
	makeStyles,
	Dialog,
	DialogContent,
	DialogTitle,
	CircularProgress,
	IconButton,
	FormControl,
	FormHelperText,
	FilledInput,
	InputLabel,
	InputAdornment,
	Typography,
} from '@material-ui/core'
import { Close as CloseIcon, FileCopy as CopyIcon, Share as ShareIcon } from '@material-ui/icons';
import { ReduxState } from 'reducers'
import { algorithmUIActions } from 'reducers/algorithmUI'

const { closeShareDialog } = algorithmUIActions

const useStyles = makeStyles({
	dialogContent: {
		minWidth: '600px',
		width: '100%',
		height: '100%',
	},
    closeButton: {},
    dialogTitle: {
		display: 'flex',
		justifyContent: 'space-between',
		alignItems: 'center',
		padding: '4px 4px 0px 24px'
	},
	dialogPaper: {
		width: '100%',
		overflow: 'visible'
	},
})

const CopyableField = ({ value, loading, error, label, id, helperText } : { value?: string, error?: string, loading: boolean, id: string, label: string, helperText: string }) => {
	const { enqueueSnackbar } = useSnackbar()

	const handleClickCopy = (text: string) => {
		enqueueSnackbar("Link copied to clipboard", { variant: 'info' })
		navigator.clipboard.writeText(text)
	}

	const displayedValue = error !== undefined ? error : ( loading ? "Creating link..." : value)
	const isCopyable = error === undefined && loading === false && value !== undefined

	return (
		<FormControl fullWidth margin="normal">
			<InputLabel htmlFor={id} error={error !== undefined}>
				{label}
			</InputLabel>
			<FilledInput
				id={id}
				type='text'
				error={error !== undefined}
				value={displayedValue}
				inputProps={{
					readOnly: true,
				}}
				endAdornment={
				  <InputAdornment position="end">
					{ isCopyable && (
						<IconButton aria-label="Copy link to clipboard" onClick={() => handleClickCopy(displayedValue!)}>
						  <CopyIcon />
						</IconButton>
					)}
					{ loading && (
						<CircularProgress />
					)}
				  </InputAdornment>
				}
			/>

			<FormHelperText error={error !== undefined}>{error}</FormHelperText>
			<FormHelperText>{helperText}</FormHelperText>
		</FormControl>
	)
}

export const ShareDialog = () => {
	const dispatch = useDispatch()
	const { open, urlHash, urlShort, urlShortError } = useSelector((state: ReduxState) => ({
		...state.algorithmUI.shareDialog,
	}))
	const urlShortLoading = urlShortError === undefined && urlShort === undefined

	const handleClose = () => dispatch(closeShareDialog())
	const classes = useStyles()
	const dialogClasses = useMemo(
		() => ({
			paper: classes.dialogPaper
		}),
		[classes.dialogPaper]
	)

	const urlPrefix: string = window.location.protocol + '//' +  window.location.host + window.location.pathname;
	const linkShort = `${urlPrefix}?s=${urlShort}`
	const linkHash = `${urlPrefix}?#${urlHash}`

	return (
		<Dialog
			open={open}
			classes={dialogClasses}
			fullWidth
			scroll="body"
			onClose={handleClose}
			maxWidth="md"
		>
			<DialogTitle disableTypography className={classes.dialogTitle}>
				<Typography variant="h6">
					<ShareIcon />
					Share link
				</Typography>
				<IconButton onClick={handleClose}>
					<CloseIcon />
				</IconButton>
			</DialogTitle>
			<DialogContent className={classes.dialogContent}>
				<CopyableField id="share1" label="Short link" value={linkShort} loading={urlShortLoading} error={urlShortError} helperText="The algorithm graph is stored on our servers" />

				<CopyableField id="share2" label="Long link"  value={linkHash} loading={false} helperText="The algorithm graph is encoded in the URL" />
        	</DialogContent>
		</Dialog>
	)
}
