import { ExportFn } from 'interfaces/Transformations'

/**
 * Exports data in txt format
 *
 * @param {*} data
 * @returns
 */
const exportData: ExportFn = data => {
	const outputArr: string[][] = [
		// State names as first row
		Object.values(data.states).map(s => s.name),
		// Initial state names as second row
		data.initialStates.map(s => {
			const state = data.states[s]
			if (!state) return ''

			return state.name
		}),
		// Final state names as third row
		data.finalStates.map(s => {
			const state = data.states[s]
			if (!state) return ''

			return state.name
		}),
		// Transitions as rows
		...Object.values(data.transitions).map(t => {
			const startState = data.states[t.startState]
			const endState = data.states[t.endState]
			if (!startState || !endState) return []

			return [startState.name, t.name, endState.name]
		})
	]

	return outputArr.map(arr => arr.join('\t')).join('\n')
}

export default exportData
