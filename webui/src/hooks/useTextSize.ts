import * as React from 'react'

import { getSizeFromRef } from 'utils/size'

/**
 * Updates component's size to fit current text content
 * - updates width and height for node
 * @param text
 * @param onUpdate
 * @param runOnInit
 */
const useTextSize = (
	text: string,
	onUpdate: (size: number) => void,
	runOnInit = true
): React.RefObject<SVGTextElement> => {
	const { current: ref } = React.useRef(React.createRef<SVGTextElement>())
	const [prevText, setPrevText] = React.useState(text)

	const firstUpdate = React.useRef(true) // Checks for 1st render
	const refContent = ref ? ref.current : ''
	React.useLayoutEffect(() => {
		// Don't update on first render
		if (firstUpdate.current && !runOnInit) {
			
			firstUpdate.current = false
			return
		}
		// Don't update if no text or text hasn't changed
		if (!refContent || (prevText === text && firstUpdate.current !== true))
			return

		setPrevText(text)
		const size = getSizeFromRef(refContent)

		onUpdate(size)
	}, [runOnInit, refContent, text, prevText, onUpdate])

	return ref
}

export default useTextSize
