import React from 'react'
import { useSelector } from 'react-redux'

import usePosition from './usePosition'
import { ReduxState } from '../reducers'


const useAlgorithmPosition = (
	canvas: React.RefObject<SVGSVGElement>,
	allowChange: boolean,
	useGrid: boolean = true
) => {
	const { grid, scale, offset, mouseDownOffset } = useSelector(
		(state: ReduxState) => ({
			grid: state.algorithmCanvas.gridOn,
			scale: state.algorithmCanvas.scale,
			offset: state.algorithmCanvas.offset,
			mouseDownOffset: state.algorithmCanvas.mouseDownOffset
		})
	)

	return usePosition(canvas, allowChange, 'default', grid && useGrid, scale, offset, mouseDownOffset)
}

export default useAlgorithmPosition
