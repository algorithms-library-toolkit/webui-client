#pragma once
#include <string>

namespace utils {
std::string trim(std::string str);

}
