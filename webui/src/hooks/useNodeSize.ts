import * as React from 'react'

import { getNodeHeight, getNodeWidth } from 'utils/textSize'

/**
 * Updates node's width and height based on text size and number of input parameters
 * @param text
 * @param onUpdate
 * @param inputParamsCount
 * @param runOnInit
 */
const useNodeSize = (
    text: string,
    onUpdate: (width: number, height: number) => void,
    inputParamsCount: number,
    runOnInit = true
): React.RefObject<SVGTextElement> => {
    const { current: ref } = React.useRef(React.createRef<SVGTextElement>())
    const [prevText, setPrevText] = React.useState(text)

    const firstUpdate = React.useRef(true) // Checks for 1st render
    const refContent = ref ? ref.current : ''
    React.useLayoutEffect(() => {
        // Don't update on first render
        if (firstUpdate.current && !runOnInit) {

            firstUpdate.current = false
            return
        }
        // Don't update if no text or text hasn't changed
        if (!refContent || (prevText === text && firstUpdate.current !== true))
            return

        setPrevText(text)

        const height = getNodeHeight(text, inputParamsCount)
        const width = getNodeWidth(text)
        onUpdate(width, height)

    }, [runOnInit, refContent, text, prevText, onUpdate])

    return ref
}

export default useNodeSize
