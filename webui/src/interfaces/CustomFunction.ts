import { AlgorithmParam, AlibType } from 'interfaces/Algorithms'

export interface CustomFunction {
    name: string
    params: AlgorithmParam[]
    resultType: AlibType
    body: string
}

export const emptyCustomFunction: CustomFunction = {
    name: '',
    params: [],
    resultType: '',
    body: ''
}

export function dataToCustomFunction(name: string, params: string, resultType: string, body: string): CustomFunction {
    const parameters = params.split(',')
    const parsedParameters: AlgorithmParam[] = parameters.reduce((acc, param) => {
        const split = param.trim().split(' ')
        if (split.length === 2) {
            return [...acc, { name: split[1], type: split[0] }]
        } else {
            throw new Error('Missing type or name of parameter')
        }
    }, [] as AlgorithmParam[])


    return {
        name: name === '' ? 'Custom Function' : name,
        params: parsedParameters,
        resultType: resultType,
        body: body
    }
}

export function functionParamsToString(params: AlgorithmParam[]): string {
    if (params === undefined)
        return ''
    const paramArray: string[] = params.reduce((acc: string[], param: AlgorithmParam) => {
        return [...acc, param.type + ' ' + param.name]
    }, [] as string[])

    return paramArray.join(', ')
}

export function customFunctionToString(func: CustomFunction): string {
    const params = func.params.map((param) => param.type + ' ' + param.name)
    return 'function ' + func.name + ' ( ' + params.join(', ') + ' ) '
    + 'returning ' + func.resultType + ' ' + func.body
}

export const isEmptyFunction = (name: string, parameters: string, resultType: string, functions: string) => {
    return name === '' || name === undefined || parameters === '' || parameters === undefined || resultType === ''
        || resultType === undefined || functions === '' || functions === undefined
}