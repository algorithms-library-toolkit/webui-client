# WebUI

Web interface for [Algorithms Library Toolkit](https://alt.fit.cvut.cz/). 

This project is split into 3 subprojects:

1. [WebUI](webui/README.md) - [React](https://reactjs.org/) frontend
2. [API Server](server/README.md) - [Ktor](https://ktor.io/) API Server with embedded [ActiveMQ](https://activemq.apache.org/) broker
3. [Worker](worker/README.md) - Evaluates requests from the broker

## Developing
When developing, please follow the rules:
 - The code must be formatted with respect to `.clang-format`.
 - Please provide the reasoning for your changes in commit messages. Link to ALT commits, if you need to.
 - The `master` branch must be always buildable.
 - Any changes must go through code review into the `staging` branch.
 - `staging` branches are then merged into `master` branch and auto-deployed to production.

### Developing with pre-built docker images
If you don't want to bloat your system with our development dependencies for server and client it might be useful to use out pre-built docker images (or create your own from our Dockerfiles) for them.

1. First, create custom network for containers to communicate: `docker network create webui`
2. Pull images from our docker registry. Bear in mind that `latest` tag refers to last stable revision. See below for locally building images from current repository state.
 * `docker pull gitlab.fit.cvut.cz:5050/algorithms-library-toolkit/webui-client/worker:latest`
 * `docker pull gitlab.fit.cvut.cz:5050/algorithms-library-toolkit/webui-client/server:latest`
3. Start server container, publish necessary communication ports and connect to network. `docker run -it --rm --name server -p3001:3001 --network webui gitlab.fit.cvut.cz:5050/algorithms-library-toolkit/webui-client/server:latest`
4. Start worker container and connect it to server. `docker run -it --rm --network webui --name worker --env ALT_WORKER_SERVER="server:61616" gitlab.fit.cvut.cz:5050/algorithms-library-toolkit/webui-client/worker:latest`
5. Use `npm install` and `npm start` in `webui` directory

NOTE: All the docker containers are started with `-it --rm` flags meaning that they will stay atop of TTY so they capture your input and the container gets deleted after they are terminated. Consult docker reference for other types of usage.

#### Building images locally:
In root of the repository:

1. `docker build -f server/Dockerfile . -t server-dev`
2. `docker build -f worker/Dockerfile . -t worker-dev`

Now you can swap the `gitlab.fit.cvut.cz:5050/algorithms-library-toolkit/webui-client/server:latest` for `server-dev`. Similarly for worker.

## Authors
- Michael Vrána: vranami8@fit.cvut.cz
- Tomáš Pecka: peckato1@fit.cvut.cz
