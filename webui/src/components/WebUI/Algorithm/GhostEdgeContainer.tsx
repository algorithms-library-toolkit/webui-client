import * as React from 'react'
import { useSelector } from 'react-redux'

import useAlgorithmPosition from '../../../hooks/useAlgorithmPosition'
import { ReduxState } from '../../../reducers'
import {
	outputParamNodePointsCursor,
	inputParamNodePointsCursor
} from '../../../utils/positioning'
import Edge from './Edge'

interface GhostEdgeContainerProps {
	canvasRef: React.RefObject<SVGSVGElement>
}

/**
 * Ghost edge shown when drawing new edge on canvas
 * Uses mouse cursor for position
 *
 * @returns
 */
const GhostEdgeContainer = (props: GhostEdgeContainerProps) => {
	const { canvasRef } = props
	const { data, startNode, endNode, buildEdge } = useSelector(
		(state: ReduxState) => {
			const ghostEdge = state.algorithmUI.ghostEdge
			const nodes = state.algorithmData.present.nodes

			return {
				data: {
					...ghostEdge
				},
				startNode: nodes.get(ghostEdge.startNodeId),
				endNode: nodes.get(ghostEdge.endNodeId),
				buildEdge: state.algorithmCanvas.buildEdge
			}
		}
	)

	const position = useAlgorithmPosition(canvasRef, buildEdge, false)

	if ((startNode === undefined && endNode === undefined) || !buildEdge) return null

	// Calculate the start and end points of the transition
	const points =
		startNode !== undefined
			? outputParamNodePointsCursor(startNode, position)
			: inputParamNodePointsCursor(data.endParamIndex, endNode!, position)

	return <Edge data={data} {...points} isGhost={true} />
}

export default GhostEdgeContainer
