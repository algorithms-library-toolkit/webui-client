#include "utils/string.hpp"

namespace utils {
std::string trim(std::string str)
{
    static const char* WS = " \t\n\r";
    str.erase(str.find_last_not_of(WS) + 1);
    str.erase(0, str.find_first_not_of(WS));
    return str;
}

}
