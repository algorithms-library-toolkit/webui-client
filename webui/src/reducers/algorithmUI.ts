import { createAction, ActionsUnion } from '../utils/redux'
import AlgorithmNode from 'interfaces/AlgorithmNode'
import AlgorithmEdge from '../interfaces/AlgorithmEdge'
import { NodeOutputs } from 'interfaces/NodeOutputs'
import { AlgorithmDataActions } from './algorithmData'
import { AlgorithmNodeType } from 'interfaces/AlgorithmNode'
import { CustomFunction, emptyCustomFunction } from 'interfaces/CustomFunction'

export interface AlgorithmUIState {
	/** Open/close Statemaker dialog */
	statemakerOpen: boolean
	/** Show/hide Statemaker apply and cancel buttons */
	showStatemakerApplyButton: boolean
	/** Open/close output dialog */
	outputDialog: {
		open: boolean,
		header: string,
		content: NodeOutputs,
		nodeType: AlgorithmNodeType,
	},
	/** Open/close evaluating dialog */
	evaluatingDialogOpen: boolean
	ghostNode: AlgorithmNode
	ghostEdge: AlgorithmEdge
	toolbarOpen: boolean
	inputDialog: {
		open: boolean
		dataType: string
		initialValue: any
		editedNodeId: string
	}
	customFunctionDialog: {
		open: boolean
		initialValue: CustomFunction
		editedNodeId: string
	}
	showHint: boolean
	shareDialog: {
		open: boolean
		urlHash: string
		urlShort?: string
		urlShortError?: string
	}
}

export const initialAlgorithmUIState: AlgorithmUIState = {
	statemakerOpen: false,
	showStatemakerApplyButton: false,
	outputDialog: {
		open: false,
		header: '',
		content: {},
		nodeType: 'output' as AlgorithmNodeType
	},
	evaluatingDialogOpen: false,
	ghostNode: {
		id: 'ghostNode',
		height: 0,
		width: 0,
		name: '',
		params: [],
		templateParams: [],
		position: { x: 0, y: 0 },
		resultType: '',
		nodeType: 'algorithm'
	},
	ghostEdge: {
		id: 'ghostEdge',
		startNodeId: '',
		endNodeId: '',
		endParamIndex: -1
	},
	toolbarOpen: true,
	inputDialog: {
		open: false,
		dataType: '',
		initialValue: '',
		editedNodeId: ''
	},
	customFunctionDialog: {
		open: false,
		initialValue: emptyCustomFunction,
		editedNodeId: ''
	},
	showHint: true,
	shareDialog: {
		open: false,
		urlHash: '',
		urlShort: undefined,
		urlShortError: undefined,
	}
}

export const algorithmUIActions = {
	/** Opens/closes statemaker */
	setStatemakerOpen: (value: boolean, showButtons: boolean = false) =>
		createAction('setStatemakerOpen', { value, showButtons }),
	/** Opens/closes output edit dialog */
	setOutputDialogOpen: (value: boolean) =>
		createAction('setOutputDialogOpen', value),

	/** Opens/closes evaluating dialog */
	setEvaluatingDialogOpen: (value: boolean) =>
		createAction('setEvaluatingDialogOpen', value),

	updateGhostNode: (node: Partial<AlgorithmNode>) =>
		createAction('updateGhostNode', node),

	updateGhostEdge: (edge: Partial<AlgorithmEdge>) =>
		createAction('updateGhostEdge', edge),

	setAlgorithmToolbarOpen: (value: boolean) =>
		createAction('setAlgorithmToolbarOpen', value),

	openOutputDialog: (header: string, content: NodeOutputs, nodeType: AlgorithmNodeType) =>
		createAction('openOutputDialog', { header, content, nodeType }),

	openInputDialog: (editedNodeId: string, dataType: string, initialValue: any = '') =>
		createAction('openInputDialog', { editedNodeId, dataType, initialValue }),

	closeInputDialog: () => createAction('closeInputDialog'),

	openCustomFunctionDialog: (editedNodeId: string, initialValue: any = '') =>
		createAction('openCustomFunctionDialog', { editedNodeId, initialValue }),

	closeCustomFunctionDialog: () => createAction('closeCustomFunctionDialog'),

	toggleHint: () => createAction('toggleHint'),

	openShareDialog: (urlHash: string) => createAction('openShareDialog', { urlHash }),

	closeShareDialog: () => createAction('closeShareDialog'),

	setShareDialogContent: ({ key, error } : { key?: string, error?: string }) => createAction('setShareDialogContent', { key, error }),
}

export type AlgorithmUIActions = ActionsUnion<
	typeof algorithmUIActions
>

export const algorithmUIReducer = (
	state: AlgorithmUIState = initialAlgorithmUIState,
	action: AlgorithmUIActions | AlgorithmDataActions
) => {
	switch (action.type) {
		case 'setStatemakerOpen': {
			return {
				...state,
				statemakerOpen: action.payload.value,
				showStatemakerApplyButton: action.payload.showButtons
			}
		}
		case 'setOutputDialogOpen': {
			return {
				...state,
				outputDialog: {
					...state.outputDialog,
					open: action.payload,
				}
			}
		}
		case 'setEvaluatingDialogOpen': {
			return {
				...state,
				evaluatingDialogOpen: action.payload
			}
		}
		case 'updateGhostNode': {
			return {
				...state,
				ghostNode: {
					...state.ghostNode,
					...action.payload
				}
			}
		}
		case 'updateGhostEdge': {
			return {
				...state,
				ghostEdge: {
					...state.ghostEdge,
					...action.payload
				}
			}
		}
		case 'setAlgorithmToolbarOpen': {
			return {
				...state,
				toolbarOpen: action.payload
			}
		}
		case 'openOutputDialog': {
			const { header, content, nodeType } = action.payload
			return {
				...state,
				outputDialog: {
					open: true,
					header: header,
					content: content,
					nodeType: nodeType
				},
			}
		}
		case 'openInputDialog': {
			const { editedNodeId, dataType, initialValue } = action.payload
			return {
				...state,
				inputDialog: {
					open: true,
					dataType,
					editedNodeId,
					initialValue
				}
			}
		}
		case 'closeInputDialog': {
			return {
				...state,
				inputDialog: {
					...state.inputDialog,
					open: false
				}
			}
		}
		case 'openCustomFunctionDialog': {
			const { editedNodeId, initialValue } = action.payload
			return {
				...state,
				customFunctionDialog: {
					open: true,
					editedNodeId,
					initialValue
				}
			}
		}
		case 'closeCustomFunctionDialog': {
			return {
				...state,
				customFunctionDialog: {
					...state.customFunctionDialog,
					open: false
				}
			}
		}
		case 'addNode': {
			return {
				...state,
				toolbarOpen: true
			}
		}
		case 'toggleHint': {
			return {
				...state,
				showHint: !state.showHint
			}
		}
		case 'openShareDialog': {
			const { urlHash } = action.payload
			return {
				...state,
				shareDialog: {
					...state.shareDialog,
					open: true,
					urlHash: urlHash,
					urlShort: undefined,
				}
			}
		}
		case 'closeShareDialog': {
			return {
				...state,
				shareDialog: {
					...state.shareDialog,
					open: false
				}
			}
		}
		case 'setShareDialogContent': {
			const { key, error } = action.payload
			return {
				...state,
				shareDialog: {
					...state.shareDialog,
					urlShort: key,
					urlShortError: error,
				}
			}
		}
		default:
			return state
	}
}
