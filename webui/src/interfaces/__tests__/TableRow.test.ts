import { Row, rowToAutomaton } from 'interfaces/TableRow'

const alphabetDKA = ['0', '1']

const rowsDKA: Row[] = [
    { type: 'inout', name: '0', transitions: ['0', '1']},
    { type: '', name: '1', transitions: ['2', '0']},
    { type: '', name: '2', transitions: ['1', '2']}
]

const alphabetNKA = ['a', 'b']

const rowsNKA: Row[] = [
    { type: 'inout', name: 'A', transitions: ['A,C', 'B']},
    { type: '', name: 'B', transitions: ['B ,D', '']},
    { type: 'out', name: 'C', transitions: ['', '']},
    { type: 'in', name: 'D', transitions: ['A', 'C, D']}
]

test('Convert row to state data DKA', () => {
    const automaton = rowToAutomaton({ symbols: alphabetDKA, table: rowsDKA })

    expect(automaton.states).toEqual(['0', '1', '2'])
    expect(automaton.initialStates).toEqual(['0'])
    expect(automaton.finalStates).toEqual(['0'])
    expect(automaton.transitions).toEqual([
		{symbol: '0', from: '0', to: '0'},
		{symbol: '1', from: '0', to: '1'},
		{symbol: '0', from: '1', to: '2'},
		{symbol: '1', from: '1', to: '0'},
		{symbol: '0', from: '2', to: '1'},
		{symbol: '1', from: '2', to: '2'},
    ])
})

test('Convert row to state data NKA', () => {
    const automaton = rowToAutomaton({ symbols: alphabetNKA, table: rowsNKA })

    expect(automaton.states).toEqual(['A', 'B', 'C', 'D'])
    expect(automaton.initialStates).toEqual(['A', 'D'])
    expect(automaton.finalStates).toEqual(['A', 'C'])
    expect(automaton.transitions).toEqual([
		{symbol: 'a', from: 'A', to: 'A'},
		{symbol: 'a', from: 'A', to: 'C'},
		{symbol: 'b', from: 'A', to: 'B'},
		{symbol: 'a', from: 'B', to: 'B'},
		{symbol: 'a', from: 'B', to: 'D'},
		{symbol: 'a', from: 'D', to: 'A'},
		{symbol: 'b', from: 'D', to: 'C'},
		{symbol: 'b', from: 'D', to: 'D'},
    ])
})
