import { AlibType } from 'interfaces/Algorithms'
import { Set } from 'immutable'
import { isFirstUpper } from 'utils/validation'

export const grammarTypes = [
	'RIGHT_RG',
	'LEFT_RG',
	'RIGHT_LG',
	'LEFT_LG',
	'LG',
	'CFG',
	'EPSILON_FREE_CFG',
	'GNF',
	'CNF',
	'CSG',
	'NON_CONTRACTING_GRAMMAR',
	'CONTEXT_PRESERVING_UNRESTRICTED_GRAMMAR',
	'UNRESTRICTED_GRAMMAR'
] as const

export type GrammarType = typeof grammarTypes[number]

type GrammarAlibTypeMap = {
	[key: string]: AlibType
}

export const grammarTypeMap: GrammarAlibTypeMap = {
	RIGHT_RG: 'grammar::RightRG',
	LEFT_RG: 'grammar::LeftRG',
	RIGHT_LG: 'grammar::RightLG',
	LEFT_LG: 'grammar::LeftLG',
	LG: 'grammar::LG',
	CFG: 'grammar::CFG',
	EPSILON_FREE_CFG: 'grammar::EpsilonFreeCFG',
	GNF: 'grammar::GNF',
	CNF: 'grammar::CNF',
	CSG: 'grammar::CSG',
	NON_CONTRACTING_GRAMMAR: 'grammar::NonContractingGrammar',
	CONTEXT_PRESERVING_UNRESTRICTED_GRAMMAR: 'grammar::ContextPreservingUnrestrictedGrammar',
	UNRESTRICTED_GRAMMAR: 'grammar::UnrestrictedGrammar'
} as const

export interface Rule {
	left: string
	right: string
}

export default interface GrammarData {
	initialSymbol: string
	type: GrammarType
	terminals: Set<string>
	nonterminals: Set<string>
	rules: Rule[]
}

export const emptyGrammar: GrammarData = {
	initialSymbol: '',
	type: 'RIGHT_RG',
	terminals: Set(),
	nonterminals: Set(),
	rules: []
}

/**
 * Create GrammarData object from initial symbol, grammar type and rules
 * @param initialSymbol
 * @param type
 * @param rules
 */
export function dataToGrammar(initialSymbol: string, type: GrammarType, rules: Rule[]) {
	//split symbols to string array by space and pipe delimiter
	let tokens = rules.reduce((acc, current) => {
		const left = current.left.split(/[ |\t]+/)
		const right = current.right.split(/[ |\t]+/)
		return acc.concat(left).concat(right)
	}, Set<string>())

	tokens = tokens.add(initialSymbol)

	//delete epsilon
	tokens = tokens.delete('')

	const terminals = tokens.filter(token => !isFirstUpper(token))
	const nonterminals = tokens.filter(token => isFirstUpper(token))

	return {
		initialSymbol: initialSymbol,
		type: type,
		rules: rules,
		terminals: terminals,
		nonterminals: nonterminals
	}
}

function setOfSymbolsToString(data: Set<string>) {
	return '{' + data.join(',') + '}'
}

/**
 * Return rules as string, for context grammars add pipes for context on the left side
 * e.g. S -> a T b will be | S | -> a T b
 * @param data
 */
function rulesToString(data: Rule[]) {
	let stringRules = data.reduce((acc, current) => {
		return acc.concat(current.left.concat(' -> ').concat(current.right)).concat(',')
	}, '')

	stringRules = stringRules.slice(0, -1)

	return '{' + stringRules + '}'
}

/**
 * Transform GrammarData object to language of string::Parse algorithm
 * @param data
 */
export function grammarToString(data: GrammarData) {
	return data.type + '('
		+ setOfSymbolsToString(data.nonterminals) + ','
		+ setOfSymbolsToString(data.terminals) + ','
		+ rulesToString(data.rules) + ','
		+ data.initialSymbol + ')'
}

export function isGrammar(type: string) {
	return Object.values(grammarTypeMap).some((grammarType) => type.includes(grammarType))
}