import Position from './Position'

export type StateType = 'default' | 'initial' | 'final'

/**
 * Represents state machine element state
 *
 * @export
 * @interface IState
 */
export default interface IState {
	id: string
	name: string
	/** Position on canvas */
	position: Position
	/** Size on canvas */
	size: number
}
