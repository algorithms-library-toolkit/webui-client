#!/bin/bash

FILE_CHANGELOG="CHANGELOG.md"
FILE_WEBUI_PACKAGE_JSON="webui/package.json"
FILE_WEBUI_PACKAGE_LOCK_JSON="webui/package-lock.json"
FILE_WORKER_CMAKELISTS="worker/CMakeLists.txt"
FILE_SERVER_BUILD_GRADLE="server/build.gradle.kts"


function prompt_y_n {
	while true; do
		read -r -p "$1 [y/n] " input

		case $input in
			[yY][eE][sS]|[yY])
				echo "Yes"
				break
				;;
			[nN][oO]|[nN])
				echo "No"
				break
				;;
			*)
				echo "Invalid input..."
				;;
		esac
	done
}

function change_versions {
	for file in "$@"; do
		sed -i "s@${OLD_VERSION}@${NEW_VERSION}@g" $file
	done
}

function change_versions_npm {
	for file in "$@"; do
		local TMP=$(mktemp)
		jq --tab ".version=\"${NEW_VERSION}\"" < "${file}" > ${TMP}
		mv ${TMP} ${file}
	done
}

function check_branch {
	local CURRENT_BRANCH=$(git rev-parse --abbrev-ref HEAD)
	if [[ ${CURRENT_BRANCH} != "master" ]]; then
		echo "You must be on master branch"
		exit 1
	fi
}

function change_changelog {
	sed -i "/\[Unreleased\]$/a ## [${NEW_VERSION}] - $(date +%Y-%m-%d)" ${FILE_CHANGELOG}
	sed -i "/^\[Unreleased\]:/c [Unreleased]: https://gitlab.fit.cvut.cz/algorithms-library-toolkit/webui-client/compare/v${NEW_VERSION}...master" ${FILE_CHANGELOG}
	sed -i "/^\[Unreleased\]/a [${NEW_VERSION}]: https://gitlab.fit.cvut.cz/algorithms-library-toolkit/webui-client/compare/v${OLD_VERSION}...v${NEW_VERSION}" ${FILE_CHANGELOG}
}

if [ "$#" -ne 1 ]; then
	echo "Usage: $0 NEW_VERSION" >&2
	exit 1
fi

NEW_VERSION="${1}"
OLD_VERSION=$(jq < webui/package.json .version | tr -d "\"")

check_branch
change_versions_npm ${FILE_WEBUI_PACKAGE_JSON} ${FILE_WEBUI_PACKAGE_LOCK_JSON}
change_versions ${FILE_WORKER_CMAKELISTS} ${FILE_SERVER_BUILD_GRADLE}
change_changelog ${FILE_CHANGELOG}

echo "Previewing changes..."
git diff

if [[ $(prompt_y_n "Commit and push?") == "Yes" ]]; then
	git add ${FILE_WEBUI_PACKAGE_JSON} ${FILE_WEBUI_PACKAGE_LOCK_JSON} ${FILE_WORKER_CMAKELISTS} ${FILE_SERVER_BUILD_GRADLE} ${FILE_CHANGELOG}
	git commit -m "release version ${NEW_VERSION}"
	git push
	git tag "v${NEW_VERSION}"
	git push --tags
else
	echo "Discarding changes"
	git restore webui/package.json worker/CMakeLists.txt server/build.gradle.kts ${FILE_CHANGELOG}
fi
