import React, { useState } from 'react'
import { Typography } from '@material-ui/core'

import { InputAutomatonTableData } from 'components/WebUI/Dialog/InputDialog/Automaton/InputAutomatonTableData'
import { Row, StateType, createTableRows, rowToAutomaton, splitTransitions, TableError } from 'interfaces/TableRow'
import Automaton, { emptyAutomaton, EPSILON_CHAR } from 'interfaces/Automaton'
import { useSyncState } from 'hooks/useSyncState'
import { get1DIndex } from 'utils/automatonTable'
import { serializeAutomaton } from 'utils/alt/export/serializers'

const validateAutomatonTable = (symbols: string[], table: Row[]) => {
	const transitionRegex = /[\\/\-()=*`!@#$%^&+?'"<>]/
	const regex = /[|\\/\-()=*`!@#$%^&+?'"<>\s]/
	const digits = /^[0-9]/
	const allDigits = /^[0-9]+$/

	let errors: TableError[] = []

	const width = table.length > 0
		? table[0].transitions.length + 1 //for the stateName column
		: 0


	//check symbols
	symbols.forEach((symbol, index) => {
		const name = symbol.trim()

		if (name === '') {
			errors.push({cellType: 'symbol', cell1DIndex: index, text: `Symbol "${name}" is empty string`})
		}

		if (regex.test(name) && name !== EPSILON_CHAR) {
			errors.push({cellType: 'symbol', cell1DIndex: index, text: `Symbol "${name}" contains a forbidden character`})
		}

		if (digits.test(name) && !allDigits.test(name)) {
			errors.push({cellType: 'symbol', cell1DIndex: index, text: `Symbol "${name}" can not start with a digit unless the symbol is all digits`})
		}

		if (symbols.some((e, idx) => index != idx && e === symbol)) {
			errors.push({cellType: 'symbol', cell1DIndex: index, text: `Symbol "${name}" is specified multiple times`})
		}
	})

	table.forEach((row, index) => {
		const name = row.name.trim()

		//check state name
		if (regex.test(name)) {
			errors.push({cellType: 'table', cell1DIndex: get1DIndex(index, width, 0), text: `State name "${name}" contains invalid character`})
		}
		if (digits.test(name) && !allDigits.test(name)) {
			errors.push({cellType: 'table', cell1DIndex: get1DIndex(index, width, 0), text: `State name "${name}" can not start with a digit unless the symbol is all digits`})
		}

		//check transitions
		row.transitions.forEach((field, fieldIndex) => {
			const states = splitTransitions(field)

			states.forEach((target) => {
				if (/\s/g.test(target)) {
					errors.push({cellType: 'table', cell1DIndex: get1DIndex(index, width, fieldIndex + 1), text: `State name "${target}" in transitions contains a whitespace`})
				}
				if (transitionRegex.test(target)) {
					errors.push({cellType: 'table', cell1DIndex: get1DIndex(index, width, fieldIndex + 1), text: `State name "${target}" in transitions contains a forbidden character`})
				}
				if (digits.test(target) && !allDigits.test(target)) {
					errors.push({cellType: 'table', cell1DIndex: get1DIndex(index, width, fieldIndex + 1), text: `State name "${target}" can not start with a digit unless the symbol is all digits`})
				}
			})

			if (table.some((r, idx) => index != idx && r.name === row.name)) {
				errors.push({cellType: 'table', cell1DIndex: get1DIndex(index, width, 0), text: `State name "${name}" is specified multiple times`})
			}
		})
	})

	if (!table.some(row => row.type === 'in' || row.type === 'inout')) {
		errors.push({cellType: 'table', cell1DIndex: get1DIndex(0, width, 0), text: `No initial state`})
	}

	return errors
}

interface InputAutomatonTableProps {
	automaton?: Automaton
	parseError?: string
	updateValue: (stringValue: string) => void
	setDoneButtonState: (enabled: boolean) => void
}
export function InputAutomatonTable(props: InputAutomatonTableProps) {
	let { automaton } = props

	if (!automaton) {
		automaton = emptyAutomaton
	}

	const tableRows = createTableRows(automaton)
	const [symbols, setSymbols] = useSyncState<string[]>(automaton.alphabet)
    const [table, setTable] = useSyncState<Row[]>(tableRows.length > 0 ? tableRows : [{type: '', name: '', transitions: symbols().map(() => '')}])

	const changed = () => {
		if (validateAutomatonTable(symbols(), table()).length === 0) {
			const automatonFromTable = rowToAutomaton({ symbols: symbols(), table: table() })
			const stringAutomaton = serializeAutomaton(automatonFromTable)
			props.updateValue(stringAutomaton)
			props.setDoneButtonState(true)
		} else {
			props.setDoneButtonState(false)
		}
	}

	const tableErrors = validateAutomatonTable(symbols(), table())
	changed()

	const onAddRow = () => {
		const transitions: string[] = symbols().map((symbols) => '')
		setTable([...table(), {
			type: '',
			name: '',
			transitions: transitions
		}])
	}

	const onSymbolChange = (index: number, value: string) => {
		const data = [...symbols()]
		data[index] = value
		setSymbols(data)
		changed()
	}

	const onStateTypeChange = (index: number, value: StateType) => {
		const data = [...table()]
		data[index].type = value
		setTable(data)
		changed()
	}

	const onAddColumn = () => {
		setSymbols([...symbols(), ''])
		let data = [...table()]
		data = data.map((row) => {
			let newRow = row
			newRow.transitions = [...newRow.transitions, '']
			return newRow
		})
		setTable(data)
		changed()
	}

	const onTransitionChange = (index: number, index2: number, value: string) => {
		const data = [...table()]
		data[index].transitions[index2] = value
		setTable(data)
		changed()
	}

	const onStateNameChange = (index: number, value: string) => {
		const data = [...table()]
		data[index].name= value
		setTable(data)
		changed()
	}

	const onDeleteRow = (index: number) => {
		const values = [...table()]
		values.splice(index, 1)
		setTable(values)
		changed()
	}

	const onDeleteColumn = (index: number) => {
		const alphabet = [...symbols()]
		alphabet.splice(index, 1)
		setSymbols(alphabet)

		const data = table().map((row) =>  {
			let newRow = row
			newRow.transitions.splice(index, 1)
			return newRow
		})
		setTable(data)
		changed()
	}

	const deduplicatedErrorTexts = [...new Set(tableErrors.map(e => e.text))]

	return (
		<React.Fragment>
			{ props.parseError && tableErrors.length === 0 && (
				<>
				<Typography color='error' variant='caption'>
					We were trying to fill the table from the string input but it parsed with errors.
				</Typography>
				<Typography color='error' paragraph variant='caption'>
					You can either go back to the string input and try to fix that or you can start fresh here.
				</Typography>
				<Typography color='error' display="block" variant='caption'>The error was: {props.parseError}</Typography>
				</>
			)}
			{ tableErrors.length > 0 && (
				<>
				<Typography color='error' variant='caption'>The automaton table definitions contains errors. You must fix them before moving elsewhere.</Typography>
				<ul>
					{ deduplicatedErrorTexts.map(errorText => (
						<li><Typography color='error' display="block" variant='caption'>{errorText}</Typography></li>
					))}
				</ul>
				</>
			)}
			<InputAutomatonTableData
				table={table}
				symbols={symbols}
				tableErrors={tableErrors}
				onAddRow={onAddRow}
				onSymbolChange={onSymbolChange}
				onStateTypeChange={onStateTypeChange}
				onAddColumn={onAddColumn}
				onTransitionChange={onTransitionChange}
				onStateNameChange={onStateNameChange}
				onDeleteRow={onDeleteRow}
				onDeleteColumn={onDeleteColumn}
			/>
		</React.Fragment>
	)
}
