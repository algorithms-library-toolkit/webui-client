import { Typography } from '@material-ui/core'
import React from 'react'

const GrammarFormHint = () => {

    return(<>
        <Typography color='inherit'>Input is case sensitive</Typography>
        <Typography color='inherit'>- symbol starting with [A-Z] is considered to be nonterminal</Typography>
        <Typography color='inherit'>- symbol starting with anything else is terminal</Typography>
        <Typography color='inherit'>- do not use a pipe | character in the symbol</Typography>
        <Typography color='inherit'>If your input doesn't conform to this format, please use the string input</Typography>
        <Typography color='inherit'>You can specify multiple right hand sides in one row by
            delimiting them with a pipe symbol | or write each rule on a new row</Typography>
        <Typography color='inherit' align='right'>A -&gt; a | b | C</Typography>
        <Typography color='inherit'>Individual symbols are delimited by space</Typography>
        <Typography color='inherit' align='right'>A -&gt; a b abc</Typography>
        <Typography color='inherit'>For the rule A -&gt; eps leave the right hand side
            empty</Typography>
        <Typography color='inherit' align='right'>A -&gt; </Typography>
        <Typography color='inherit'>Epsilon in the rules with multiple right hand sides (e.g. A
            -&gt; a | eps or A -&gt; eps | a) is represented by
            empty sequence delimited by a pipe symbol |</Typography>
        <Typography color='inherit' align='right'>A -&gt; a |</Typography>
        <Typography color='inherit' align='right'>A -&gt; | a</Typography>
        <Typography color='inherit'>Context on the left hand side in CSG or CONTEXT_PRESERVING_UNRESTRICTED_GRAMMAR
            types must be separated by pipe symbol |</Typography>
        <Typography color='inherit' align='right'>a | B | c -&gt; a C C c</Typography>
    </>)
}

export default GrammarFormHint