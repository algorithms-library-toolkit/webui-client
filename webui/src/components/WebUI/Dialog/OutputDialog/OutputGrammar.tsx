import * as React from 'react'
import {
    Card,
    CardContent,
    Grid,
    makeStyles,
    Typography
} from '@material-ui/core'
import { ArrowRightAlt } from '@material-ui/icons'
import GrammarData from 'interfaces/GrammarData'
import { parseGrammar } from 'utils/alt/import/parsers'
import { EPSILON } from 'components/WebUI/Dialog/OutputDialog/OutputTable'

interface OutputGrammarContentProps {
    value: string
}

const useStyles = makeStyles({
    root: {
        minWidth: 275,
    },
    rules: {
        fontSize: 18,
    }
});

export const OutputGrammar = (props: OutputGrammarContentProps) => {
    const { value } = props
    const classes = useStyles()

	try {
	    const grammar: GrammarData = parseGrammar(value)
		return (
			<Card className={classes.root}>
				<CardContent>
					<Typography component="span" color="textSecondary" gutterBottom>
						{grammar.type} <br/>
					</Typography>
					<Typography component="span" color="textSecondary" gutterBottom>
						Initial symbol: {grammar.initialSymbol}
						<br/>
						Nonterminals: {grammar.nonterminals.join(", ")}
						<br/>
						Terminals: {grammar.terminals.join(", ")}
					</Typography>
					{grammar.rules.map((rule) =>
						<Grid container direction="row" alignItems="center">
							<Typography component="span" className={classes.rules}>{rule.left}</Typography>
							<ArrowRightAlt />
							<Typography component="span" className={classes.rules}>
								{rule.right === '' ? EPSILON : rule.right}
							</Typography>
						</Grid>)
					}
				</CardContent>
			</Card>
		)
    } catch(e) {
		console.log(value)
		console.error(e)
    	return (
    		<Card>
    			<CardContent>
					<Typography component="span" color="error">
						We are unable to show nice grammar output due to parse error.
						You can try string output view.
					</Typography>
    			</CardContent>
    		</Card>
    	)
    }

}

export default OutputGrammar
