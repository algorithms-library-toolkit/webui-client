#include "graph/nodes/AbstractNode.hpp"
#include "topSort.hpp"

bool traverseNodes(
    std::shared_ptr<AbstractNode>& node,
    std::set<std::shared_ptr<AbstractNode>>& visitedNodes,
    std::set<std::shared_ptr<AbstractNode>>& validNodes,
    std::forward_list<std::shared_ptr<AbstractNode>>& topSort)
{
    if (!visitedNodes.insert(node).second)
        // has been visited in current traversal, this means a cycle exists
        throw std::logic_error("Cycle detected");

    if (validNodes.find(node) != validNodes.end()) {
        // already has been visited in previous traversals and is valid
        visitedNodes.erase(node);
        return true;
    }
    // output can't have any child nodes
    bool res = node->isOutput();
    // list of edges to remove is needed because you can't delete edge during iteration of edges
    std::forward_list<Edge> edgesToRemove;
    for (const auto& edge : node->getEdges()) {
        auto child = edge.getNode();

        if (traverseNodes(child, visitedNodes, validNodes, topSort)) {
            // this node is valid
            res = true;
            continue;
        }
        // child node is invalid, remove the edge to the child
        edgesToRemove.push_front(edge);
    }
    // remove all edges to invalid nodes
    for (auto& edge : edgesToRemove)
        node->removeEdge(edge);

    if (res) {
        // node is valid
        validNodes.insert(node);
        topSort.push_front(node);
    }

    visitedNodes.erase(node);
    return res;
}

std::forward_list<std::shared_ptr<AbstractNode>> topSort(const std::map<std::string, std::shared_ptr<AbstractNode>>& nodes)
{
    std::set<std::shared_ptr<AbstractNode>> validNodes;
    std::set<std::shared_ptr<AbstractNode>> visitedEdges;
    std::forward_list<std::shared_ptr<AbstractNode>> topSortedNodes;

    for (const auto& nodeIt : nodes) {
        auto node = nodeIt.second;
        if (!node->isInput())
            continue;

        traverseNodes(node, visitedEdges, validNodes, topSortedNodes);
    }

    return topSortedNodes;
}
