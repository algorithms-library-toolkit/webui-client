#include "NodesFromJSONBuilder.hpp"
#include "graph/AlgorithmGraph.hpp"
#include "graph/nodes/AlgorithmNode.hpp"
#include "graph/nodes/BoolNode.hpp"
#include "graph/nodes/DoubleNode.hpp"
#include "graph/nodes/IntNode.hpp"
#include "graph/nodes/OutputNode.hpp"
#include "graph/nodes/StringNode.hpp"

using namespace std::string_literals;

NodesFromJSONBuilder& NodesFromJSONBuilder::fromJson(const Json::Value& root)
{
    const auto& newFunctions = root["customFunctions"];
    const auto& graphRoot = root["graph"];
    const auto& inputs = graphRoot["inputs"];
    const auto& algorithms = graphRoot["algorithms"];
    const auto& outputs = graphRoot["outputs"];
    const auto& strings = inputs["strings"];
    const auto& ints = inputs["ints"];
    const auto& doubles = inputs["doubles"];
    const auto& bools = inputs["bools"];
    const auto& edges = graphRoot["pipes"];

    for (const auto& function : newFunctions) {
        functions.emplace_back(function.asString());
    }

    if (inputs.isNull())
        throw std::invalid_argument("Missing inputs");
    if (outputs.isNull())
        throw std::invalid_argument("Missing outputs");

    if (!algorithms.isNull())
        withAlgorithmNodes(algorithms);
    if (!strings.isNull())
        withStrings(strings);
    if (!ints.isNull())
        withInts(ints);
    if (!doubles.isNull())
        withDoubles(doubles);
    if (!bools.isNull())
        withBools(bools);
    if (!outputs.isNull())
        withOutputs(outputs);
    if (!edges.isNull())
        withEdges(edges);

    return *this;
}

void NodesFromJSONBuilder::withInts(const Json::Value& intNodesJson)
{
    createNodes<IntNode>(intNodesJson);
}

void NodesFromJSONBuilder::withDoubles(const Json::Value& doubleNodesJson)
{
    createNodes<DoubleNode>(doubleNodesJson);
}

void NodesFromJSONBuilder::withBools(const Json::Value& boolNodesJson)
{
    createNodes<BoolNode>(boolNodesJson);
}

void NodesFromJSONBuilder::withStrings(const Json::Value& stringNodesJson)
{
    createNodes<StringNode>(stringNodesJson);
}

void NodesFromJSONBuilder::withAlgorithmNodes(const Json::Value& algorithmNodesJson)
{
    createNodes<AlgorithmNode>(algorithmNodesJson);
}

void NodesFromJSONBuilder::withOutputs(const Json::Value& outputsJson)
{
    createOutputNodes<OutputNode>(outputsJson);
}

void NodesFromJSONBuilder::withEdges(const Json::Value& edgesJson)
{
    linkNodes(edgesJson);
}

AlgorithmGraph NodesFromJSONBuilder::build()
{
    return AlgorithmGraph(nodes, functions);
}

template <class NodeType>
void NodesFromJSONBuilder::createNodes(const Json::Value& nodesJson)
{
    try {
        for (const auto& nodeId : nodesJson.getMemberNames()) {
            const auto& nodeObject = nodesJson[nodeId];

            if (!nodeObject.isObject())
                throw std::invalid_argument("Node " + nodeId + " is not a JSON object");

            nodes.insert_or_assign(nodeId, std::make_shared<NodeType>(nodeId, nodeObject));
        }
    } catch (Json::Exception::exception& e) {
        throw std::invalid_argument("JSON error when creating nodes: "s + e.what());
    }
}

template <class OutputNodeType>
void NodesFromJSONBuilder::createOutputNodes(const Json::Value& nodesJson)
{
    if (!nodesJson.isArray())
        throw std::invalid_argument("Invalid output nodes JSON");

    try {
        for (const auto& jsonValue : nodesJson) {
            if (!jsonValue.isString())
                throw std::invalid_argument("Output node ID must be a string");
            const auto& nodeId = jsonValue.asString();

            nodes.insert_or_assign(nodeId, std::make_shared<OutputNodeType>(nodeId));
        }
    } catch (Json::Exception::exception& e) {
        throw std::invalid_argument("JSON error when creating output nodes: "s + e.what());
    }
}

void NodesFromJSONBuilder::linkNodes(const Json::Value& edgesJson)
{
    if (!edgesJson.isArray())
        throw std::invalid_argument("Pipes must be of JSON array type");
    try {
        for (auto& edgeJson : edgesJson) {
            const auto& startNodeId = edgeJson["from"].asString();
            const auto& endNodeId = edgeJson["to"].asString();
            const auto& paramIndex = edgeJson["paramIndex"].asInt();

            const auto& startNodeIterator = nodes.find(startNodeId);
            const auto& endNodeIterator = nodes.find(endNodeId);

            if (startNodeIterator == nodes.end())
                throw std::invalid_argument("Node '" + startNodeId + "' doesn't exist");
            if (endNodeIterator == nodes.end())
                throw std::invalid_argument("Node '" + endNodeId + "' doesn't exist");

            startNodeIterator->second->createEdge(endNodeIterator->second, paramIndex);
        }
    } catch (Json::Exception::exception& e) {
        throw std::invalid_argument("JSON error when creating edges: "s + e.what());
    }
}
