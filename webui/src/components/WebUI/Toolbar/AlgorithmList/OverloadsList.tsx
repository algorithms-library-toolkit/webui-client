import React, { useCallback } from 'react'
import { Algorithm, Overload } from 'interfaces/Algorithms'
import { OverloadListItem } from './OverloadListItem'
import { Actions } from '../../../../reducers'
import { useDispatch, batch } from 'react-redux'
import { makeStyles, List } from '@material-ui/core'
import { WebUITheme } from '../../../../interfaces/Theme'
import { filterAll } from 'utils/filtering'
import { Filter } from 'interfaces/Filter'

interface AlgorithmListItemProps {
	algorithm: Algorithm
	filters: Filter[]
	filterValue: string
}
const { setBuildNode, updateGhostNode, setAlgorithmToolbarOpen } = Actions

const useStyles = makeStyles<WebUITheme>((theme) => ({
	root: {
		paddingBottom: 'unset',
		paddingLeft: theme.spacing(1),
		paddingRight: theme.spacing(1),
		paddingTop: theme.spacing(1),
		borderLeft: `2px solid ${theme.palette.primary.light}`,
		'& > *': {
			marginBottom: theme.spacing(1)
		}
	}
}))

export const AlgorithmListItem = (props: AlgorithmListItemProps) => {
	const { algorithm, filters, filterValue } = props
	const dispatch = useDispatch()
	const classes = useStyles()

	const handleClick = useCallback(
		(overloads: Overload[], docs?: string) =>
			batch(() => {
				dispatch(setBuildNode(true))
				dispatch(
					updateGhostNode({
						name: algorithm.name,
						templateParams: algorithm.templateParams,
						nodeType: 'algorithm',
						params: overloads[0].params,
						resultType: overloads[0].resultType,
						overloads: overloads,
						docs: docs
					})
				)
				dispatch(setAlgorithmToolbarOpen(false))
			}),
		[dispatch, algorithm]
	)

	return (
		<List className={classes.root} dense>
			{algorithm.overloads.keySeq().map((overload, index) => (
				filterAll(algorithm.overloads.get(overload) as Overload[], filters, filterValue) &&
				<OverloadListItem
					key={index}
					name={algorithm.name}
					templateParams={algorithm.templateParams}
					overloads={algorithm.overloads.get(overload) as Overload[]}
					docs={algorithm.docs}
					onClick={handleClick}
				/>
			))}
		</List>
	)
}
