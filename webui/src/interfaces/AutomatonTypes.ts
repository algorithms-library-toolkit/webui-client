import { AlibType } from './Algorithms'
import Automaton, { EPSILON_CHAR } from 'interfaces/Automaton'

export const automatonTypes = [
	'DFA',
	'NFA',
	'ENFA',
	'MISNFA',
	'MISENFA'
] as const
export type AutomatonType = typeof automatonTypes[number]

type AutomatonAlibTypeMap = {
	[key: string]: AlibType
}

export const automanAlibTypeMap: AutomatonAlibTypeMap = {
	DFA: 'automaton::DFA<object::Object, object::Object>',
	NFA: 'automaton::NFA<object::Object, object::Object>',
	ENFA: 'automaton::EpsilonNFA<object::Object, object::Object>',
	MISNFA: 'automaton::MultiInitialStateNFA<object::Object, object::Object>',
	MISENFA: 'automaton::MultiInitialStateEpsilonNFA<object::Object, object::Object>'
} as const

export const classifyAutomaton = (automaton: Omit<Automaton, 'type'>): AutomatonType => {
	const hasMultipleInitialStates = automaton.initialStates.length > 1
	const hasEpsilonSymbol = automaton.transitions.filter(tr => tr.symbol === EPSILON_CHAR).length > 0
	const hasMultipleTransitionsForStateAndSymbol = automaton.states.some(state => {
		return automaton.alphabet.some(symb => {
			return automaton.transitions.filter(tr => tr.symbol === symb && tr.from === state).length > 1
		})
	})

	if (hasMultipleInitialStates) {
		return hasEpsilonSymbol ? 'MISENFA' : 'MISNFA'
	}

	if (hasEpsilonSymbol) {
		return 'ENFA'
	}

	return hasMultipleTransitionsForStateAndSymbol ? 'NFA' : 'DFA'
}
