import * as React from 'react'
import {
    makeStyles,
    TextField,
} from '@material-ui/core'

interface Props {
    verbose?: boolean,
    plain?: string,
    pretty?: string,
}

const useStyles = makeStyles({
	root: {
		minWidth: '550px',
		width: '100%',
		height: '100%',
		marginBottom: '1em'
	},
	monospace: {
		fontFamily: 'Roboto Mono'
	}
})

const OutputString = (props: {header: string, content: string | undefined}) => {
	const classes = useStyles()
	const { header, content } = props

	if (!content) {
		return ( <TextField classes={{root: classes.root}} label={header} variant="filled" disabled error value="Not available" /> )
	}

	return (
		<TextField
			InputProps={{ className: classes.monospace }}
			classes={{ root: classes.root }}
			label={header}
			variant="filled"
			multiline
			value={content}  /> )
}

export const OutputStrings = (props: Props) => {
    return (
    	<div>
			{ (props.verbose === undefined || props.verbose === false) && props.pretty !== props.plain /* in case both outputs are same (primitives) */ && ( <OutputString header="ALT text format" content={props.pretty} /> ) }
			<OutputString header="Informative printout" content={props.plain} />
		</div>
    )
}

export default OutputStrings
