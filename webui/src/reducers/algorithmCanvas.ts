import {
	canvasActionsFactory,
	canvasReducerFactory,
	CanvasState,
	initialState
} from './canvas'
import { undoAlgorithmActionType, redoAlgorithmActionType } from '.'

export type AlgorithmCanvasState = CanvasState

export const algorithmCanvasId = 'algorithm'

export const initialAlgorithmCanvasState = initialState

export const algorithmCanvasActions = canvasActionsFactory(algorithmCanvasId)

const canvasReducer = canvasReducerFactory(algorithmCanvasId)

export const algorithmCanvasReducer = (
	state: AlgorithmCanvasState = initialState,
	action: any
) => {
	let newState = canvasReducer(state, action)

	if (
		[
			undoAlgorithmActionType,
			redoAlgorithmActionType,
			'deleteNode',
			'deleteEdge'
		].includes(action.type)
	)
		newState = { ...newState, selectedEdge: '', selectedNode: '' }

	return newState
}
