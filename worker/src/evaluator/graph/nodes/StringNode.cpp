#include <string>
#include "StringNode.hpp"

StringNode::StringNode(const std::string& id, const std::string& value, const ext::vector<std::string>& templateParams)
    : InputNode(id, value, templateParams)
{
}

StringNode::StringNode(const std::string& id, const Json::Value& jsonObject)
    : InputNode(id, jsonObject["value"].asString(), {})
{
}