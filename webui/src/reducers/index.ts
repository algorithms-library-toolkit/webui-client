import { combineReducers } from 'redux'
import undoable, { StateWithHistory } from 'redux-undo'

import { ActionsUnion, createAction } from '../utils/redux'

import {
	initialStateCanvasState,
	StateCanvasState,
	stateCanvasReducer,
	stateCanvasActions
} from './stateCanvas'
import {
	AlgorithmCanvasState,
	algorithmCanvasActions,
	algorithmCanvasReducer,
	initialAlgorithmCanvasState
} from './algorithmCanvas'
import dataReducer, {
	Actions as stateDataActions,
	State as DataState,
	initialState as initialDataState
} from './stateData'
import stateUIReducer, {
	stateUIActions as ToolbarActions,
	State as ToolbarState,
	initialStateUIState as initialToolbarState
} from './stateUI'
import {
	ServiceState,
	initialServiceState,
	serviceReducer,
	serviceActions
} from './service'
import {
	initialAlgorithmState,
	algorithmDataActions,
	localStorageAlgorithmReducer as algorithmReducer,
	AlgorithmGraphState
} from './algorithmData'
import {
	AlgorithmUIState,
	algorithmUIActions,
	algorithmUIReducer,
	initialAlgorithmUIState
} from './algorithmUI'
import {
	initialWorkerDefinitionsState,
	WorkerDefinitionsState,
	workerDefinitionsReducer,
	workerDefinitionsActions,
} from './workerDefinitions'

export const undoStatemakerActionType = 'undoStatemaker'
export const redoStatemakerActionType = 'redoStatemaker'
export const undoAlgorithmActionType = 'undoAlgorithm'
export const redoAlgorithmActionType = 'redoAlgorithm'

export const Actions = {
	...stateCanvasActions,
	...algorithmCanvasActions,
	...stateDataActions,
	...ToolbarActions,
	...serviceActions,
	...algorithmDataActions,
	...algorithmUIActions,
	...workerDefinitionsActions,
	undoStatemaker: () => createAction(undoStatemakerActionType),
	redoStatemaker: () => createAction(redoStatemakerActionType),
	undoAlgorithm: () => createAction(undoAlgorithmActionType),
	redoAlgorithm: () => createAction(redoAlgorithmActionType)
}

export type Actions = ActionsUnion<typeof Actions>

export interface ReduxState {
	stateCanvas: StateCanvasState
	algorithmCanvas: AlgorithmCanvasState
	stateData: StateWithHistory<DataState>
	algorithmData: StateWithHistory<AlgorithmGraphState>
	stateUI: ToolbarState
	service: ServiceState
	algorithmUI: AlgorithmUIState
	workerDefinitions: WorkerDefinitionsState
}

export const initialState: ReduxState = {
	stateCanvas: initialStateCanvasState,
	algorithmCanvas: initialAlgorithmCanvasState,
	stateData: {
		past: [],
		present: initialDataState,
		future: [],
		_latestUnfiltered: initialDataState,
		group: {},
		index: 1,
		limit: 10
	},
	algorithmData: {
		past: [],
		present: initialAlgorithmState,
		future: [],
		_latestUnfiltered: initialAlgorithmState,
		group: {},
		index: 1,
		limit: 10
	},
	stateUI: initialToolbarState,
	service: initialServiceState,
	algorithmUI: initialAlgorithmUIState,
	workerDefinitions: initialWorkerDefinitionsState,
}

/** Main app reducer */
const reducer = combineReducers<ReduxState>({
	stateCanvas: stateCanvasReducer,
	algorithmCanvas: algorithmCanvasReducer,
	stateData: undoable(dataReducer, {
		undoType: undoStatemakerActionType,
		redoType: redoStatemakerActionType
	}),
	algorithmData: undoable(algorithmReducer, {
		undoType: undoAlgorithmActionType,
		redoType: redoAlgorithmActionType,
		neverSkipReducer: true,
		filter: (action) => [
			'addNode',
			'addEdge',
			'positionNode',
			'deleteNode',
			'deleteEdge',
			'clear'
		].includes(action.type)
	}),
	stateUI: stateUIReducer,
	service: serviceReducer as any,
	algorithmUI: algorithmUIReducer,
	workerDefinitions: workerDefinitionsReducer,
})

export default reducer
