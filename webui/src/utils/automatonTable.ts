export function get1DIndex(rowIndex: number, length: number, columnIndex: number) {
    return (rowIndex * length) + columnIndex
}