import { useCallback } from 'react'
import { Map } from 'immutable'
import { batch, useDispatch, useSelector } from 'react-redux'
import { algorithmUIActions } from '../reducers/algorithmUI'
import { algorithmDataActions } from '../reducers/algorithmData'
import { ReduxState } from '../reducers'
import { useSnackbar } from 'notistack'
import { EvaluateResponseError } from 'interfaces/EvaluateResponseError'
import { NodeOutputs } from 'interfaces/NodeOutputs'
import { createEvalErrorMessage } from '../components/EvalErrorSnackbar'

const { setOutputValues } = algorithmDataActions
const { setEvaluatingDialogOpen } = algorithmUIActions

export const useEvaluate = () => {
	const dispatch = useDispatch()
	const { algorithmData, algorithmService } = useSelector(
		(state: ReduxState) => ({
			algorithmData: state.algorithmData.present,
			algorithmService: state.service.algorithmsService
		})
	)

	const { enqueueSnackbar } = useSnackbar()

	return useCallback(async (): Promise<void> => {
		if (!algorithmData.cycleEdges.isEmpty()) {
			enqueueSnackbar('Cycle detected', { variant: 'error' })
			return
		}

		dispatch(setEvaluatingDialogOpen(true))
		let result: Map<string, NodeOutputs>

		try {
			result = await algorithmService.evaluate(algorithmData)
		} catch (e: any) {
			if (e instanceof EvaluateResponseError) {
				enqueueSnackbar(createEvalErrorMessage(e), { variant: 'error' });
			} else {
				enqueueSnackbar(
					e.message ?? (typeof e === 'string' ? e : 'Evaluation failed'),
					{ variant: 'error' }
				)
			}

			dispatch(setEvaluatingDialogOpen(false))
			return
		}

		if (!result) return

		batch(() => {
			dispatch(setOutputValues(result))
			dispatch(setEvaluatingDialogOpen(false))
		})

		enqueueSnackbar('Evaluation succesful', {
			variant: 'success'
		})
	}, [dispatch, algorithmData, algorithmService, enqueueSnackbar])
}
