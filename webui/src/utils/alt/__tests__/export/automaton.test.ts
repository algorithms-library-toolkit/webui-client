import { serializeAutomaton } from 'utils/alt/export/serializers'
import Automaton from 'interfaces/Automaton'


const data: Automaton = {
	type: 'DFA',
	states: ['s1', 's2', 's3', 's4', 's5'],
	alphabet: ['t1', 't2', 't3', 't4'],
	initialStates: ['s1'],
	finalStates: ['s1', 's2'],
	transitions: [{symbol: 't1', from: 's1', to: 's2'}, {symbol: 't2', from: 's1', to: 's1'}, {symbol: 't3', from: 's2', to: 's3'}, {symbol: 't3', from: 's4', to: 's5'}, {symbol: 't4', from: 's4', to: 's1'}]
}

const file: string = `DFA t1 t2 t3 t4
><s1 s2 s1 - -
<s2 - - s3 -
s3 - - - -
s4 - - s5 s1
s5 - - - -\n`

const NFA: Automaton = {
	type: 'NFA',
	states: ['a', 'b'],
	alphabet: ['1', '0'],
	initialStates: ['a'],
	finalStates: ['b'],
	transitions: [{symbol: '1', from: 'a', to: 'b'}, {symbol: '1', from: 'a', to: 'a'}, {symbol: '0', from: 'a', to: 'a'}]
}

const NFAFile = `NFA 1 0
>a b|a a
<b - -\n`

const setDFA: Automaton = {
	type: 'DFA',
	states: ['{a}', '{a, b}'],
	alphabet: ['0', '1'],
	initialStates: ['{a}'],
	finalStates: ['{a, b}'],
	transitions: [{symbol: '0', from: '{a}', to: '{a}'}, {symbol: '1', from: '{a}', to: '{a, b}'}, {symbol: '0', from: '{a, b}', to: '{a}'}, {symbol: '1', from: '{a, b}', to: '{a, b}'}]
}

const DFASetFile = `DFA 0 1
>{a} {a} {a, b}
<{a, b} {a} {a, b}\n`


const GlushkovNFA: Automaton = {
	type: 'NFA',
	states: ['((InitialStateLabel), 0)', '(a, 1)', '(b, 2)', '(c, 3)', '(b, 4)'],
	alphabet: ['a', 'b', 'c'],
	initialStates: ['((InitialStateLabel), 0)'],
	finalStates: ['(b, 2)', '(c, 3)', '(b, 4)'],
	transitions: [
		{symbol: 'a', from: '((InitialStateLabel), 0)', to: '(a, 1)'},
		{symbol: 'b', from: '((InitialStateLabel), 0)', to: '(b, 2)'},
		{symbol: 'c', from: '((InitialStateLabel), 0)', to: '(c, 3)'},
		{symbol: 'b', from: '(a, 1)', to: '(b, 2)'},
		{symbol: 'b', from: '(a, 1)', to: '(b, 4)'},
		{symbol: 'c', from: '(c, 3)', to: '(c, 3)'},
	]
}

const GlushkovNFAFile = `NFA a b c
>((InitialStateLabel), 0) (a, 1) (b, 2) (c, 3)
(a, 1) - (b, 2)|(b, 4) -
<(b, 2) - - -
<(c, 3) - - (c, 3)
<(b, 4) - - -\n`

const MultipleDigitStates: Automaton = {
	type: 'MISNFA',
	states: ['111', '777', '3', '3150'],
	alphabet: ['a', 'b', 'c'],
	initialStates: ['111', '777'],
	finalStates: ['3150'],
	transitions: [
		{symbol: 'a', from: '111', to: '777'},
		{symbol: 'a', from: '111', to: '3'},
		{symbol: 'c', from: '111', to: '3150'},
		{symbol: 'c', from: '111', to: '3'},
		{symbol: 'c', from: '777', to: '3'},
		{symbol: 'a', from: '3150', to: '777'},
		{symbol: 'a', from: '3150', to: '3150'},
		{symbol: 'b', from: '3150', to: '777'},
		{symbol: 'b', from: '3150', to: '3150'},
		{symbol: 'c', from: '3150', to: '3'},
	]
}

const MultipleDigitStatesFile = `MISNFA a b c
>111 777|3 - 3150|3
>777 - - 3
3 - - -
<3150 777|3150 777|3150 3\n`

describe('import txt-fit', () => {
	it('converts DFA', () => {
		expect(serializeAutomaton(data)).toEqual(file)
	})

	it('converts NFA', () => {
		expect(serializeAutomaton(NFA)).toEqual(NFAFile)
	})

	it('converts DFA with set syntax', () => {
		expect(serializeAutomaton(setDFA)).toEqual(DFASetFile)
	})

	it('converts GlushkovNFA', () => {
		expect(serializeAutomaton(GlushkovNFA)).toEqual(GlushkovNFAFile)
	})

	it('converts an NFA with states identified by a sequence of numbers', () => {
		expect(serializeAutomaton(MultipleDigitStates)).toEqual(MultipleDigitStatesFile)
	})

	afterAll(() => jest.resetModules())
})
