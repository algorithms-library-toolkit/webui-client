import { AlgorithmParam, AlibType, AlibTemplateType, Overload } from 'interfaces/Algorithms'
import Position from './Position'
import StateData from './StateData'
import { isString, isNumber } from '../utils/typeGuards'
import GrammarData from 'interfaces/GrammarData'
import { TableAutomaton } from 'interfaces/TableRow'
import { CustomFunction } from 'interfaces/CustomFunction'

interface ParsedInputParameters {
	name: string
	templateParams: string[]
}

export const parsedInputType: Record<InputNodeTypesToParse, ParsedInputParameters> = {
	inputStringAutomaton: { name: 'Automaton', templateParams: ['automaton::Automaton'] },
	inputStringGrammar: { name: 'Grammar', templateParams: ['grammar::Grammar'] },
	inputStringObject: { name: 'Object', templateParams: ['object::Object'] },
	inputStringRegexp: { name: 'RegExp', templateParams: ['regexp::RegExp'] },
	inputStringRte: { name: 'RTE', templateParams: ['rte::RTE'] },
	inputStringString: { name: 'String', templateParams: ['string::String'] },
	inputStringTree: { name: 'Tree', templateParams: ['tree::Tree'] }
}

export const inputNodeTypesToParse = [
	'inputStringAutomaton',
	'inputStringGrammar',
	'inputStringObject',
	'inputStringRegexp',
	'inputStringRte',
	'inputStringString',
	'inputStringTree'
] as const

export const inputNodeTypes = [
	'inputString',
	'inputAutomaton',
	'inputGrammar',
	'inputInt',
	'inputBoolean',
	'inputDouble',
	...inputNodeTypesToParse
] as const

export type InputNodeType = typeof inputNodeTypes[number]

export type InputNodeTypesToParse = typeof inputNodeTypesToParse[number]

export const outputNodeTypes = [
	'output',
	'outputAutomaton'
] as const

export const algorithmNodeTypes = [
	'algorithm',
	'customFunction',
	...inputNodeTypes,
	...outputNodeTypes
] as const

export type OutputNodeType = typeof outputNodeTypes[number]

export type AlgorithmNodeType = typeof algorithmNodeTypes[number]

export const isCustomFunction = (
	nodeType: AlgorithmNodeType
) =>
	nodeType === 'customFunction'

export const isInputNode = (
	nodeType: AlgorithmNodeType
): nodeType is InputNodeType =>
	inputNodeTypes.includes(nodeType as InputNodeType)

export const isOutputNode = (
	nodeType: AlgorithmNodeType
): nodeType is OutputNodeType =>
	outputNodeTypes.includes(nodeType as OutputNodeType)

export type AlgorithmNodeValue = string | number | boolean | StateData | GrammarData | TableAutomaton | CustomFunction

export default interface AlgorithmNode {
	id: string
	nodeType: AlgorithmNodeType
	name: string
	position: Position
	height: number
	width: number
	params: AlgorithmParam[]
	templateParams: AlibTemplateType[]
	resultType: AlibType
	overloads?: Overload[]
	value?: AlgorithmNodeValue
	docs?: string
}

export const emptyNode : AlgorithmNode = {
	id: '',
	nodeType: 'algorithm',
	name: '',
	position: {x: 0, y: 0},
	height: 0,
	width: 0,
	params: [],
	templateParams: [],
	resultType: ''
}

export const isAlgorithmNode = (node: any): node is AlgorithmNode => {
	if (!isString(node.id)) return false
	if (!algorithmNodeTypes.includes(node.nodeType)) return false
	if (!isString(node.name)) return false
	if (!isNumber(node?.position?.x)) return false
	if (!isNumber(node?.position?.y)) return false
	if (!isNumber(node?.height)) return false
	if (!isNumber(node?.width)) return false
	if (
		!Array.isArray(node.templateParams) ||
		!node.templateParams.every((template: any) => isString(template))
	)
		return false
	if (!isString(node.resultType)) return false
	if (Array.isArray(node.value)) return false

	return true
}
