import { ImportFn } from 'interfaces/Transformations'
import { parseAutomaton } from 'utils/alt/import/parsers'
import { toStateData } from 'interfaces/StateData'

/**
 * Convert txt-fit data string to data object
 */
const importData: ImportFn = (dataString: string) => {
	return toStateData(parseAutomaton(dataString))
}

export default importData
