import * as React from 'react'

import { toPosition, add } from '../interfaces/Position'
import Position from '../interfaces/Position'
import { CursorMode } from '../interfaces/Canvas'
import throttle from '../utils/throttle'

/**
 * Handles SVG canvas zoom, move and cursor modes
 *
 */
const useCanvas = (
	canvasRef: React.RefObject<SVGSVGElement>,
	offset: Position,
	cursorMode: CursorMode,
	setOffset: (offset: Position) => any,
	setZoom: (factor: number, to?: Position) => any
) => {
	const [isMoving, setMoving] = React.useState(false)

	// Handles moving offset
	const handleSetPosition = React.useCallback(
		(e: MouseEvent) => {
			if (isMoving) {
				const mouse = toPosition(e.movementX, e.movementY)
				setOffset(add(offset, mouse))
			}
		},
		[setOffset, isMoving, offset]
	)

	// Decides when moving is allowed
	const handleSetMoving = React.useCallback(
		(e: MouseEvent) => {
			// Move on middle/right mouse or left mouse when in move mode
			if (e.button === 1 || e.button === 2 || (cursorMode === 'move' && e.button === 0)) {
				setMoving(true)
			}
		},
		[cursorMode]
	)

	const handleUnsetMoving = React.useCallback((e: MouseEvent) => {
		setMoving(false)
	}, [])

	// Zoom on mouse position
	const handleWheel = React.useCallback(
		throttle(16.67, (e: WheelEvent) => {
			const mouse = toPosition(e.clientX, e.clientY)
			const factor = 0.1 * Math.sign(e.deltaY)

			setZoom(factor, mouse)
		}),
		[setZoom]
	)

	const handleDisableContextMenu = React.useCallback((e: MouseEvent) => e.preventDefault(), [canvasRef])

	React.useEffect(() => {
		const canvas = canvasRef.current

		if (canvas) {
			canvas.addEventListener('mousemove', handleSetPosition)
			canvas.addEventListener('mousedown', handleSetMoving)
			canvas.addEventListener('mouseup', handleUnsetMoving)
			canvas.addEventListener('wheel', handleWheel)
			canvas.addEventListener('contextmenu', handleDisableContextMenu)
		}

		return () => {
			if (canvas) {
				canvas.removeEventListener('mousemove', handleSetPosition)
				canvas.removeEventListener('mousedown', handleSetMoving)
				canvas.removeEventListener('mouseup', handleUnsetMoving)
				canvas.removeEventListener('wheel', handleWheel)
				canvas.removeEventListener('contextmenu', handleDisableContextMenu)
			}
		}
	}, [
		canvasRef,
		handleSetPosition,
		handleSetMoving,
		handleUnsetMoving,
		handleWheel
	])
}

export default useCanvas
