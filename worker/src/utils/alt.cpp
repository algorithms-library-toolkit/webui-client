#include <abstraction/ValueHolder.hpp>
#include <common/AlgorithmCategories.hpp>
#include <common/EvalHelper.h>
#include <ext/sstream>
#include <registry/Registry.h>
#include "alt.hpp"
#include "common/TypeQualifiers.hpp"

std::string abstractValueToString(const std::shared_ptr<abstraction::Value>& val)
{
    std::shared_ptr<abstraction::OperationAbstraction> op = abstraction::Registry::getValuePrinterAbstraction(val->getDeclaredType());

    ext::ostringstream oss;
    op->attachInput(val, 0);
    op->attachInput(std::make_shared<abstraction::ValueHolder<ext::ostream&>>(oss, false), 1);
    if (!op->eval())
        throw std::logic_error("Could not convert value to string");
    return oss.str();
}

std::shared_ptr<abstraction::Value> evalAlgorithm(abstraction::TemporariesHolder& environment, const std::string& name, const ext::vector<std::string>& templateParams, const ext::vector<std::shared_ptr<abstraction::Value>>& args)
{
    ext::vector<std::shared_ptr<abstraction::Value>> transformedArgs;
    ext::vector<core::type_details> argTypes;
    ext::vector<abstraction::TypeQualifiers::TypeQualifierSet> argTypeQualifiers;

    for (const std::shared_ptr<abstraction::Value>& param : args) {
        argTypes.push_back(param->getActualType());
        argTypeQualifiers.push_back(param->getTypeQualifiers());
    }

    try {
        auto abstraction = abstraction::Registry::getAlgorithmAbstraction(name, templateParams, argTypes, argTypeQualifiers, abstraction::AlgorithmCategories::AlgorithmCategory::NONE);

        auto paramsCnt = abstraction->numberOfParams();
        for (size_t i = 0; i < paramsCnt; ++i) {
            auto paramType = abstraction->getParamType(i);
            auto paramQual = abstraction->getParamTypeQualifiers(i);

            auto arg = args[i];

            if (abstraction::TypeQualifiers::isRvalueRef(paramQual) && !abstraction::TypeQualifiers::isRvalueRef(arg->getTypeQualifiers())) {
                auto typeQuals = arg->getTypeQualifiers() | abstraction::TypeQualifiers::TypeQualifierSet::RREF;
                abstraction::TypeQualifiers::TypeQualifierSet typeQualifiers = abstraction::TypeQualifiers::TypeQualifierSet::RREF;
                if (abstraction::TypeQualifiers::isConst(arg->getTypeQualifiers()))
                    typeQualifiers = typeQualifiers | abstraction::TypeQualifiers::TypeQualifierSet::CONST;
                arg = std::make_shared<abstraction::ValueReference>(arg, typeQualifiers, true);
            }

            argTypes.push_back(arg->getActualType());
            argTypeQualifiers.push_back(arg->getTypeQualifiers());
            transformedArgs.push_back(arg);
        }

        return abstraction::EvalHelper::evalAbstraction(environment, std::move(abstraction), transformedArgs);
    } catch (const std::exception& executionException) {
        std::throw_with_nested(std::runtime_error("Evaluation of algorithm " + name + " failed."));
    }
}
