import React from 'react'
import { makeStyles } from '@material-ui/styles'
import { SvgIconComponent } from '@material-ui/icons'
import { WebUITheme } from '../../interfaces/Theme'

interface SVGButtonProps {
	className?: string
	x: number
	y: number
	onClick?: (e: React.MouseEvent<SVGGElement, MouseEvent>) => void
	icon: SvgIconComponent
}

const useStyles = makeStyles<WebUITheme>((theme) => ({
	root: {
		display: 'none',
		'&:hover > circle': {
			stroke: theme.palette.primary.light,
			fill: theme.palette.grey[300]
		}
	},
	circle: {
		fill: theme.palette.common.white,
		stroke: theme.palette.grey[400],
		strokeWidth: 2
	}
}))

export const SVGButton = (props: SVGButtonProps) => {
	const { className, x, y, onClick, icon: Icon } = props
	const classes = useStyles()

	return (
		<g
			className={`${classes.root} ${className ?? ''}`}
			onClick={onClick}
			transform={`translate(${x},${y})`}
		>
			<circle
				className={classes.circle}
				pointerEvents={'all'}
				r={16}
			/>
			<Icon
				width={24}
				height={24}
				x={-12}
				y={-12}
				color="action"
				alignmentBaseline="middle"
				textAnchor="middle"
			/>
		</g>
	)
}
