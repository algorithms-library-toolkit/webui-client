import { AlgorithmALT } from 'interfaces/Algorithms'
import { Casts } from 'interfaces/Casts'

export interface WorkerDefinitionsResponse {
	algorithms: AlgorithmALT[]
	casts: Casts
	version: string
}
