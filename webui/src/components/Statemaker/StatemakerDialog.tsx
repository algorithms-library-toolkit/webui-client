import React, { useCallback, useMemo } from 'react'
import { useDispatch, useSelector, batch } from 'react-redux'
import { ReduxState } from '../../reducers'
import {
	Dialog,
	makeStyles,
	DialogContent,
	Fab,
	IconButton,
} from '@material-ui/core'
import DoneIcon from '@material-ui/icons/Done'
import CloseIcon from '@material-ui/icons/Close'
import Statemaker from './Statemaker'
import { algorithmUIActions } from '../../reducers/algorithmUI'
import { algorithmDataActions } from '../../reducers/algorithmData'
import useStatemakerKeyBindings from '../../hooks/useStatemakerKeyBindings'
import { WebUITheme } from '../../interfaces/Theme'

const { setInputAutomaton } = algorithmDataActions
const { setStatemakerOpen } = algorithmUIActions

const useStyles = makeStyles<WebUITheme>((theme) => ({
	dialogContentRoot: {
		height: '100%',
		overflow: 'visible',
		padding: 'unset',
		'&:first-child': {
			padding: 'unset',
		},
	},
	dialogPaper: {
		height: '100%',
		overflow: 'visible',
	},
	applyButton: {
		position: 'absolute',
		right: theme.spacing(1),
		bottom: theme.spacing(1),
		width: '50px',
		height: '50px',
	},
	cancelButton: {
		position: 'absolute',
		right: theme.spacing(1),
		top: theme.spacing(1),
		width: '50px',
		height: '50px',
	},
}))

const StatemakerDialog = () => {
	const dispatch = useDispatch()
	const classes = useStyles()

	const dialogClasses = useMemo(() => ({ paper: classes.dialogPaper }), [
		classes.dialogPaper,
	])

	const dialogContentClasses = useMemo(
		() => ({
			root: classes.dialogContentRoot,
		}),
		[classes.dialogContentRoot]
	)

	const statemakerKeyEventHandlers = useStatemakerKeyBindings()

	const {
		statemakerOpen,
		editedNode,
		stateData,
		showApplyButton,
	} = useSelector((state: ReduxState) => ({
		statemakerOpen: state.algorithmUI.statemakerOpen,
		editedNode: state.algorithmData.present.nodes.get(
			state.algorithmCanvas.selectedNode
		),
		stateData: state.stateData.present,
		showApplyButton: state.algorithmUI.showStatemakerApplyButton,
	}))

	const handleDialogClose = useCallback(
		() => dispatch(setStatemakerOpen(false)),
		[dispatch]
	)

	const handleApply = useCallback(
		() =>
			batch(() => {
				handleDialogClose()

				if (editedNode) dispatch(setInputAutomaton(editedNode.id, stateData))
			}),
		[dispatch, handleDialogClose, editedNode, stateData]
	)

	return (
		<Dialog
			classes={dialogClasses}
			open={statemakerOpen}
			maxWidth={'lg'}
			fullWidth
			onClose={handleDialogClose}
			{...statemakerKeyEventHandlers}
		>
			<DialogContent classes={dialogContentClasses}>
				<Statemaker />
				<IconButton
					className={classes.cancelButton}
					onClick={handleDialogClose}
				>
					<CloseIcon />
				</IconButton>
				{showApplyButton && (
					<Fab
						className={classes.applyButton}
						onClick={handleApply}
						color="primary"
					>
						<DoneIcon />
					</Fab>
				)}
			</DialogContent>
		</Dialog>
	)
}

export default StatemakerDialog
