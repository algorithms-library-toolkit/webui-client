# Worker

Worker consumes requests from the [ActiveMQ](https://activemq.apache.org/) brokers `request` queue and evaluates them. Responses are then sent to the `response` queue.

## Dependencies
- g++
- cmake
- pkg-config
- [ALT](https://alt.fit.cvut.cz/)
- [activemq-cpp](https://activemq.apache.org/components/cms/)
- [jsoncpp](https://github.com/open-source-parsers/jsoncpp)
- [spdlog](https://github.com/gabime/spdlog)
- [catch2](https://github.com/catchorg/Catch2) for tests

## Compiling
Standard CMake.

## Running
To run the worker, pass the broker address as the first argument, evaluator path as the second arg, and the evaluation timeout in miliseconds as the second argument. For example `./alt-webui-worker tcp://localhost:61616 ./alt-webui-worker-evaluator 30000`.
See `./alt-webui-worker -h` for more flags.

## Docker
This subproject contains a `Dockerfile` for building a docker image (pass repository root as the context when building).
