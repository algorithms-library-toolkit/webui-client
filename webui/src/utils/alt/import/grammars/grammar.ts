export const grammar = `
start
      = _ type:type _ "(" _ nonterminals:nonempty_symbol_set _ "," _ terminals:empty_symbol_set _ "," _ rules:rule_set _ "," _ initial:symbol _ ")" _ eof

        {
            return {
                type: type,
                nonterminals: nonterminals,
                terminals: terminals,
                rules: rules,
                initialSymbol: initial
            };
        }

nonempty_symbol_set
    = "{" _ head:symbol tail:( _ "," _ symbol )* _ "}" _

        {
            return tail.reduce(function(result, element) { return result.concat(element[3]); }, [head]);
        }

empty_symbol_set
    = _ "{" _ contents:( symbol ( _ "," _ symbol )* )? _ "}" _

        {
            return contents === null ? [] : contents[1].reduce(function(result, element) {
                return result.concat(element[3]);
            }, [contents[0]])
        }

empty_symbol_sequence
    = _ s:(symbol _ )*

        {
            return s.reduce(function(r,e) { return r.concat(e[0]); }, []);
        }

nonempty_symbol_sequence = _ s:(symbol _ )+

        {
            return s.reduce(function(r,e) { return r.concat(e[0]); }, []);
        }

rule
    = _ lhs:nonempty_symbol_sequence _ "->" _ rhsH:empty_symbol_sequence _ rhsT:( _ "|" _ empty_symbol_sequence _ )* _

        {
            return rhsT.reduce(function(result, element) {
                return result.concat({left: lhs.join(" "), right: element[3].join(" ")});
            }, [{left: lhs.join(" "), right: rhsH.join(" ")}]);
        }

rule_set
    = _ "{" _ contents:( rule ( _ "," _ rule )* )? _ "}" _

        {
            return contents === null ? [] : contents[1].reduce(function(result, element) { return result.concat(element[3]); }, contents[0])
        }

comma_delimited_symbol_sequence
    = _ head:symbol* tail:( _ "," _ symbol )* _

        {
            return tail.reduce(function(result, element) { return result.concat(element[3]); }, [head]);
        }

symbol
	= symbol:symbol_seq cont:symbol_cont?
    { return symbol + (cont !== null ? cont : ""); }

symbol_seq
    = characters:[_a-z0-9]i+ { return characters.join(""); }
    / _ "{" _ symbols:comma_delimited_symbol_sequence _ "}" _ { return "{" + symbols.join(", ") +  "}"; }
    / _ "(" _ symbols:comma_delimited_symbol_sequence _ ")" _ { return "(" + symbols.join(", ") +  ")"; }
    / _ "<" _ symbols:comma_delimited_symbol_sequence _ ">" _ { return "<" + symbols.join(", ") +  ">"; }
    / _ "[" _ symbols:comma_delimited_symbol_sequence _ "]" _ { return "[" + symbols.join(", ") +  "]"; }

symbol_cont
	= [']

type
    = 'RIGHT_RG'
    / 'LEFT_RG'
    / 'RIGHT_LG'
    / 'LEFT_LG'
    / 'LG'
    / 'CFG'
    / 'EPSILON_FREE_CFG'
    / 'GNF'
    / 'CNF'
    / 'CSG'
    / 'NON_CONTRACTING_GRAMMAR'
    / 'CONTEXT_PRESERVING_UNRESTRICTED_GRAMMAR'
    / 'UNRESTRICTED_GRAMMAR'

_
    = [ \\t\\r\\n]*

eof
    = !.
`
