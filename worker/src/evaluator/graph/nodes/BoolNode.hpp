#pragma once
#include <ext/vector>
#include <json/json.h>
#include <string>
#include "graph/nodes/InputNode.hpp"

/**
 * InputNode representing bool value
 */
class BoolNode : public InputNode<bool> {
public:
    BoolNode(const std::string& id, bool value, const ext::vector<std::string>& templateParams);
    BoolNode(const std::string& id, const Json::Value& json);
};
