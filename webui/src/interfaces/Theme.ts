import { createTheme, Theme } from '@material-ui/core/styles'

export interface WebUITheme extends Theme {
	drawerWidth: string
}

export const theme: WebUITheme = { ...createTheme(), drawerWidth: '500px' }
