import Automaton, { EPSILON_CHAR } from 'interfaces/Automaton'
import { classifyAutomaton } from 'interfaces/AutomatonTypes'


test('Classify DFA', () => {
	const automaton: Omit<Automaton, 'type'> = {
		states: ['1', '2'],
		alphabet: ['a'],
		transitions: [{symbol: 'a', from: '1', to: '2'}],
		initialStates: ['1'],
		finalStates: ['2']
	}
	expect(classifyAutomaton(automaton)).toBe('DFA')
})


test('Classify NFA', () => {
	const automaton: Omit<Automaton, 'type'> = {
		states: ['1', '2'],
		alphabet: ['a'],
		transitions: [{symbol: 'a', from: '1', to: '2'}, {symbol: 'a', from: '1', to: '1'}],
		initialStates: ['1'],
		finalStates: ['2']
	}
	expect(classifyAutomaton(automaton)).toBe('NFA')
})

test('Classify ENFA', () => {
	const automaton: Omit<Automaton, 'type'> = {
		states: ['1', '2'],
		alphabet: ['a'],
		transitions: [
			{symbol: 'a', from: '1', to: '2'},
			{symbol: 'a', from: '1', to: '1'},
			{symbol: EPSILON_CHAR, from: '1', to: '1'}],
		initialStates: ['1'],
		finalStates: ['2']
	}
	expect(classifyAutomaton(automaton)).toBe('ENFA')

	const automaton2: Omit<Automaton, 'type'> = {
		states: ['1', '2'],
		alphabet: ['a'],
		transitions: [
			{symbol: EPSILON_CHAR, from: '1', to: '1'}],
		initialStates: ['1'],
		finalStates: ['2']
	}
	expect(classifyAutomaton(automaton2)).toBe('ENFA')
})

test('Classify MISNFA', () => {
	const automaton: Omit<Automaton, 'type'> = {
		states: ['1', '2'],
		alphabet: ['a'],
		transitions: [
			{symbol: 'a', from: '1', to: '2'},
			{symbol: 'a', from: '1', to: '1'}
		],
		initialStates: ['1', '2'],
		finalStates: ['2']
	}
	expect(classifyAutomaton(automaton)).toBe('MISNFA')

	const automaton2: Omit<Automaton, 'type'> = {
		states: ['1', '2'],
		alphabet: ['a'],
		transitions: [
			{symbol: 'a', from: '1', to: '2'},
		],
		initialStates: ['1', '2'],
		finalStates: ['2']
	}
	expect(classifyAutomaton(automaton2)).toBe('MISNFA')
})

test('Classify MISENFA', () => {
	const automaton: Omit<Automaton, 'type'> = {
		states: ['1', '2'],
		alphabet: ['a'],
		transitions: [
			{symbol: 'a', from: '1', to: '2'},
			{symbol: 'a', from: '1', to: '1'},
			{symbol: EPSILON_CHAR, from: '1', to: '1'}
		],
		initialStates: ['1', '2'],
		finalStates: ['2']
	}
	expect(classifyAutomaton(automaton)).toBe('MISENFA')

	const automaton2: Omit<Automaton, 'type'> = {
		states: ['1', '2'],
		alphabet: ['a'],
		transitions: [
			{symbol: 'a', from: '1', to: '2'},
			{symbol: EPSILON_CHAR, from: '2', to: '1'}
		],
		initialStates: ['1', '2'],
		finalStates: ['2']
	}
	expect(classifyAutomaton(automaton2)).toBe('MISENFA')
})
