import { useCallback } from 'react'
import { useDispatch, useSelector } from 'react-redux'

import useCanvas from './useCanvas'
import { ReduxState } from '../reducers'
import Position from '../interfaces/Position'
import { algorithmCanvasActions } from '../reducers/algorithmCanvas'


const { setOffset, zoom } = algorithmCanvasActions

const useAlgorithmCanvas = (canvasRef: React.RefObject<SVGSVGElement>) => {
	const dispatch = useDispatch()
	const { offset, cursorMode } = useSelector((state: ReduxState) => ({
		offset: state.algorithmCanvas.offset,
		cursorMode: state.algorithmCanvas.cursorMode
	}))

	const dispatchOffset = useCallback(
		(offset: Position) => dispatch(setOffset(offset)),
		[dispatch]
	)

	const dispatchZoom = useCallback((value: number) => dispatch(zoom(value)), [
		dispatch
	])

	return useCanvas(canvasRef, offset, cursorMode, dispatchOffset, dispatchZoom)
}

export default useAlgorithmCanvas