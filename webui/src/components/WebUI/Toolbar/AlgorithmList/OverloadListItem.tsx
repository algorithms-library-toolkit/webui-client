import React, { useCallback } from 'react'
import { AlgorithmParam, Overload } from 'interfaces/Algorithms'
import {
	List,
	ListItem,
	ListItemText,
	makeStyles,
	Paper,
	IconButton
} from '@material-ui/core'
import { theme } from '../../../../interfaces/Theme'
import { Param } from './Param'
import { Doxygen } from './Doxygen'
import { InfoOutlined } from '@material-ui/icons'
import { DocsPopover } from 'components/WebUI/DocsPopover'
import { removeTemplate } from 'utils/alt/removeTemplate'

interface OverloadListItemProps {
	name: string
	templateParams: string[]
	overloads: Overload[]
	docs?: string
	onClick?: (overload: Overload[], docs?: string) => void
}

const useStyles = makeStyles({
	root: {
		padding: theme.spacing(1)
	},
	paper: {
		width: '100%'
	},
	listItemText: {
		display: 'flex',
		flexDirection: 'column',
		padding: 'unset',
		'& > *': {
			marginBottom: theme.spacing(0)
		},
		'& > :last-child': {
			marginBottom: theme.spacing(0)
		}
	},
	overload: {
		padding: '0',
	},
})

export function isUnnamedParam(name: string) {
	// Let's consider param names that are named 'arg<i>' as unnamed, i.e., remove 'name' key
	const ARG_REGEXP = /^arg\d+$/g
	return name.match(ARG_REGEXP)
}

export function createParam(param: any, idx: number) {
	if (isUnnamedParam(param.name)) {
		const {name, ...nparam} = param;
		param = nparam;
	}

	return (
		<React.Fragment key={idx}>
			<Param {...param} order={idx} />
		</React.Fragment>
	)
}

export const OverloadListItem = (props: OverloadListItemProps) => {
	const { name, templateParams, overloads, docs, onClick } = props
	const classes = useStyles()
	const [anchorEl, setAnchorEl] = React.useState<HTMLElement | null>(null)

	const params = (): AlgorithmParam[] => {
		if (overloads.length === 1)
			return overloads[0].params

		if (overloads.length > 1) {
			return overloads[0].params.map((param, index) => {
				const prefix = param.type.split("::")[0]
				let paramName = ''
				let type = 'various types'
				const allParameters = overloads.map((overload) => overload.params[index])
				//use type if its the same for all overloads
				if (allParameters.every((parameter) => removeTemplate(parameter.type) === removeTemplate(param.type)))
					type = param.type

				//use common name
				if (!isUnnamedParam(allParameters[0].name) && allParameters.every((parameter) => parameter.name === allParameters[0].name))
					paramName = param.name
				//use common namespace as name of parameter
				else if (allParameters.every((parameter) => parameter.type.startsWith(prefix)))
					paramName = prefix

				return { name: paramName , type: type } as AlgorithmParam
			})
		}

		return []
	}

	const returnType = () : string => {
		if (overloads.every((overload) => overload.resultType === overloads[0].resultType))
			return overloads[0].resultType

		return "various types"
	}

	const getDocs = overloads.length === 1 ? overloads[0].docs : docs

	const handleClick = useCallback(() => onClick?.(overloads, getDocs), [
		onClick,
		overloads,
		getDocs
	])

	const handleClickDocs = (event: React.MouseEvent<HTMLButtonElement>) => {
		event.stopPropagation()
		setAnchorEl(event.currentTarget)
	}

	const handleClose = (event: React.MouseEvent<HTMLButtonElement>) => {
		event.stopPropagation()
		setAnchorEl(null)
	}

	return (
		<Paper className={classes.paper} variant="outlined">
			<ListItem dense className={classes.root} button onClick={handleClick}>
				<ListItemText
					className={classes.listItemText}
					disableTypography
				>
					<List dense className={classes.overload}>
						{params().map((param, paramIndex) => createParam(param, paramIndex + 1))}
						<Param
							type={returnType()}
							order="return"
						/>
						{getDocs && (
							<>
								<Doxygen docstring={getDocs} />
							</>
						)}
					</List>
				</ListItemText>
				{overloads.length > 1 &&
					<React.Fragment>
						<IconButton onClick={handleClickDocs} ><InfoOutlined/></IconButton>
						<DocsPopover
							dataId={'id'}
							anchorEl={anchorEl}
							name={name}
							templateParams={templateParams}
							overloads={overloads}
							docs={docs ?? ''}
							onClose={handleClose}
						/>
					</React.Fragment>}
			</ListItem>
		</Paper>
	)
}
