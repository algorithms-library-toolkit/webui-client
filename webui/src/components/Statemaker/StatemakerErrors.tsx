import React from 'react'
import { useSelector } from 'react-redux'

import { ReduxState } from '../../reducers'
import { getErrors } from '../../utils/errors'
import Error from './StatemakerError'

/**
 * Dispalys all app errors
 *
 * @returns
 */
function Errors() {
	const { canvas, data, toolbarErrors } = useSelector(
		(state: ReduxState) => ({
			canvas: state.stateCanvas,
			data: state.stateData.present,
			toolbarErrors: state.stateUI.errors
		})
	)
	const errors = [...getErrors(data, canvas), ...toolbarErrors]

	if (errors.length === 0) return null

	return (
		<React.Fragment>
			<Error errors={errors} />
		</React.Fragment>
	)
}

export default Errors
