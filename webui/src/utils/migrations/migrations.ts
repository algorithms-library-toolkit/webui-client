import m0001 from 'utils/migrations/m0001'

const MIGRATIONS = [
	m0001,
]

function applyMigrationFrom(graph: any, from: number, currentVersion: number) {
	const foo = MIGRATIONS[from]
	if (!foo) {
		throw Error(`No migration from version ${from}!`)
	}

	console.log(`Migrating JSON from version ${from} to ${from + 1} (target is ${currentVersion})`)

	const res = foo(graph)
	res['version'] = from + 1
	return res
}

export default function applyMigrations(graph: any, currentVersion: number): any {
	const version = graph.version ?? 0

	for (let i = version; i < currentVersion; ++i) {
		graph = applyMigrationFrom(graph, i, currentVersion)
	}
	return graph
}

