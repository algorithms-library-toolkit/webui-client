#include <ext/functional>
#include <ext/ostream>
#include <ext/vector>
#include <global/GlobalData.h>
#include "AlgorithmNode.hpp"
#include "utils/alt.hpp"

using namespace std::string_literals;

AlgorithmNode::AlgorithmNode(const std::string& id, std::string algorithmName, const ext::vector<std::string>& templateParams)
    : AbstractNode(id, templateParams)
    , algorithmName(std::move(algorithmName))
{
}

AlgorithmNode::AlgorithmNode(const std::string& id, const Json::Value& jsonObject)
    : AbstractNode(id, jsonObject)
    , algorithmName(jsonObject["name"].asString())
{
}

bool AlgorithmNode::isOutput() const
{
    return false;
}

bool AlgorithmNode::isInput() const
{
    return false;
}

void AlgorithmNode::evaluate(abstraction::TemporariesHolder& environment)
{
    common::GlobalData::verbose = true;
    ext::ostringstream logStream;
    common::Streams::log = ext::reference_wrapper<ext::ostream>(logStream);

    auto result = evalAlgorithm(environment, algorithmName, templateParams, getParamsVec());
    m_resultType = result->getActualType(); // result is moved from, later. TODO some resource holder?
    passResultToChilds(result);

    if (auto log = logStream.str(); !log.empty()) {
        m_log = log;
    }
}

ext::vector<std::shared_ptr<abstraction::Value>> AlgorithmNode::getParamsVec()
{
    if (std::any_of(params.begin(), params.end(), [](const auto& e) { return e == nullptr; })) {
        throw NodeException("Algorithm node "s + id + " is missing parameter");
    }
    return ext::vector<std::shared_ptr<abstraction::Value>>(params.begin(), params.end());
}

std::optional<std::string> AlgorithmNode::getResult()
{
    return m_log;
}

std::optional<core::type_details> AlgorithmNode::getResultType()
{
    return m_resultType;
}
