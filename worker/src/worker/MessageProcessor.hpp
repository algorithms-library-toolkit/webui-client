#pragma once
#include <chrono>
#include <cms/Connection.h>
#include <filesystem>
#include <json/value.h>
#include "utils/log.hpp"

using message_processor_t = std::function<std::string(long /* message id */, std::string /* message content */)>;

/**
 * Class representing an individual worker
 */
class MessageProcessor {
public:
    /**
     * Abstract queue task processor.
     *
     * @param brokerUrl url of the broker to connect to
     * @param requestQueueName name of the queue producing jobs for us
     * @param responseQueueName name of the queue to put responses to
     * @param workerCb message processing callback (transforms message into response, @see message_processor_t)
     */
    MessageProcessor(std::string brokerURL, const std::string& requestQueueName, const std::string& responseQueueName, message_processor_t workerCb);

    ~MessageProcessor();

    /** @brief starts synchronously receiving messages */
    void listen();

private:
    static const int MESSAGE_RECEIVE_TIMEOUT_MS = 1000;

    std::string m_brokerURL;
    std::unique_ptr<cms::Connection> m_connection;
    std::unique_ptr<cms::Session> m_requestSession, m_responseSession;
    std::unique_ptr<cms::Queue> m_requestQueue, m_responseQueue;
    std::unique_ptr<cms::MessageConsumer> m_requestConsumer;
    std::unique_ptr<cms::MessageProducer> m_responseProducer;
    message_processor_t m_workerCb;
};
