#pragma once
#include <forward_list>
#include <map>
#include <set>
#include <stdexcept>
#include <string>
#include "graph/Edge.hpp"

class AbstractNode;

/**
 * Traverses depth-first through all children nodes deleting children without a path to output node
 * @param node Current node
 * @param visitedEdges Edges visited in current traversal
 * @param validNodes Set of all nodes which are in a path between input node and output node, used for memoization
 * @param topSort Topological sort of all nodes
 * @return True if the node has path to output node, false if not
 */
bool traverseNodes(
    std::shared_ptr<AbstractNode>& node,
    std::set<Edge>& visitedEdges,
    std::set<std::shared_ptr<AbstractNode>>& validNodes,
    std::forward_list<std::shared_ptr<AbstractNode>>& topSort);

/**
 * Topologically sorts nodes and also filters out nodes which are not part of a path between input and output node
 * @param nodes Map of nodes to sort
 * @return Topologically sorted nodes
 */
std::forward_list<std::shared_ptr<AbstractNode>> topSort(const std::map<std::string, std::shared_ptr<AbstractNode>>& nodes);
