import React, { useState } from 'react'
import { makeStyles, Theme } from '@material-ui/core/styles'
import AppBar from '@material-ui/core/AppBar'
import Tabs from '@material-ui/core/Tabs'
import Tab from '@material-ui/core/Tab'
import { a11yProps, TabPanel } from 'components/WebUI/Dialog/TabUtils'

interface Tab {
    label: string,
    component: React.ReactNode,
}

interface Props {
    tabs: Tab[]
}

const useStyles = makeStyles((theme: Theme) => ({
    root: {
        flexGrow: 1,
        padding: '0px 0px 0px 0px',
        width: '1'
    }
}))

const OutputTabs = (props: Props) => {
    const { tabs } = props
    const classes = useStyles()
    const [tabValue, setTabValue] = useState<number>(0)

    const handleTabChange = React.useCallback((event: React.ChangeEvent<{}>, newValue: number) => {
        setTabValue(newValue)
    }, [])

    return (
        <div className={classes.root}>
            <AppBar position='static'>
                <Tabs
                    variant='fullWidth'
                    indicatorColor='secondary'
                    value={tabValue}
                    onChange={handleTabChange}
                >
                    {tabs.map((tab: Tab, i: number) => { return (
						<Tab key={i} label={tab.label} {...a11yProps(i)} />
					) } ) }
                </Tabs>
            </AppBar>

            {tabs.map((tab: Tab, i: number) => { return (
				<TabPanel key={i} value={tabValue} index={i}>{tab.component}</TabPanel>
			)})}
        </div>
    )
}

export default OutputTabs
