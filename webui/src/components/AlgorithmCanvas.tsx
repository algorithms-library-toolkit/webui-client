import React, { useRef, useCallback } from 'react'
import { useDispatch, useSelector } from 'react-redux'
// import useKeyBindings from '../hooks/useKeyBindings'
import Canvas from './Canvas'
import Node from './WebUI/Algorithm/NodeContainer'
import Edge from './WebUI/Algorithm/EdgeContainer'
import GhostNode from './WebUI/Algorithm/GhostNodeContainer'
import GhostEdge from './WebUI/Algorithm/GhostEdgeContainer'
import useAlgorithmCanvas from '../hooks/useAlgorithmCanvas'
import { ReduxState } from '../reducers'
import { algorithmCanvasActions } from '../reducers/algorithmCanvas'

const { setBuildEdge } = algorithmCanvasActions

const AlgorithmCanvas = () => {
	const dispatch = useDispatch()
	const { nodes, edges, showGrid, scale, offset, buildEdge } = useSelector(
		(state: ReduxState) => ({
			nodes: state.algorithmData.present.nodes,
			edges: state.algorithmData.present.edges,
			showGrid: state.algorithmCanvas.gridOn,
			scale: state.algorithmCanvas.scale,
			offset: state.algorithmCanvas.offset,
			buildEdge: state.algorithmCanvas.buildEdge,
		})
	)

	const canvasRef = useRef<SVGSVGElement>(null)

	useAlgorithmCanvas(canvasRef)

	const onMouseUp = useCallback(() => {
		if (buildEdge) dispatch(setBuildEdge(false))
	}, [dispatch, buildEdge])

	return (
		<Canvas
			ref={canvasRef}
			showGrid={showGrid}
			scale={scale}
			offset={offset}
			onMouseUp={onMouseUp}
		>
			{edges
				.valueSeq()
				.toArray()
				.map(({ id }) => (
					<Edge key={id} id={id} canvasRef={canvasRef} />
				))}
			<GhostEdge canvasRef={canvasRef} />
			{nodes
				.valueSeq()
				.toArray()
				.map(({ id }) => (
					<Node key={id} id={id} canvasRef={canvasRef} />
				))}
			<GhostNode canvasRef={canvasRef} />
		</Canvas>
	)
}

export default AlgorithmCanvas
