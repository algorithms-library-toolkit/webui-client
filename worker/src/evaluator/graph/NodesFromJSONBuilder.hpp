#pragma once
#include <json/json.h>
#include <map>
#include <memory>
#include <stdexcept>
#include <string>
#include <utility>
#include "graph/nodes/AbstractNode.hpp"

class AlgorithmGraph;

/** @brief * Builds an AlgorithmGraph from a JSON value */
class NodesFromJSONBuilder {
public:
    NodesFromJSONBuilder& fromJson(const Json::Value& root);

    /** @brief Retrieves the built graph */
    AlgorithmGraph build();

private:
    std::map<std::string, std::shared_ptr<AbstractNode>> nodes;
    std::vector<std::string> functions;

    template <class NodeType>
    void createNodes(const Json::Value& nodesJson);

    template <class OutputNodeType>
    void createOutputNodes(const Json::Value& nodesJson);

    void linkNodes(const Json::Value& edgesJson);

    void withInts(const Json::Value& intNodesJson);
    void withDoubles(const Json::Value& doubleNodesJson);
    void withBools(const Json::Value& boolNodesJson);
    void withStrings(const Json::Value& stringNodesJson);
    void withAlgorithmNodes(const Json::Value& algorithmNodesJson);
    void withOutputs(const Json::Value& outputsJson);
    void withEdges(const Json::Value& edgesJson);
};
