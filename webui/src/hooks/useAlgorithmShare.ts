import {Base64} from 'js-base64'
import {useSnackbar} from 'notistack'
import {useCallback} from 'react'
import {useSelector, useDispatch} from 'react-redux'

import {AlgorithmGraph} from '../interfaces/AlgorithmGraph'
import {ReduxState} from '../reducers'
import { algorithmUIActions } from '../reducers/algorithmUI'


const { openShareDialog, setShareDialogContent } = algorithmUIActions


export const useAlgorithmShare = () => {
  const {nodes, edges} = useSelector((state: ReduxState) => ({
                                       nodes: state.algorithmData.present.nodes,
                                       edges: state.algorithmData.present.edges
                                     }))

  const {enqueueSnackbar} = useSnackbar()
  const dispatch = useDispatch()

  return useCallback(() => {
    const exportData: Omit<AlgorithmGraph, 'outputValues'> = {
      nodes,
      edges
    }

    const json = JSON.stringify(exportData);
    const hash: string = Base64.encode(json)

	dispatch(openShareDialog(hash))

    fetch('https://alt.fit.cvut.cz/shortener/create', {
    	method: 'POST',
    	body: json
    })
    .then(resp => resp.json())
    .then(shortenerData => {
    	dispatch(setShareDialogContent({ key: shortenerData.key }))
	})
    .catch(error => {
    	dispatch(setShareDialogContent({ error: 'Error contacting shortener service.' }))
    	console.error(error)
    })

  }, [nodes, edges, enqueueSnackbar])
}
