import React, { useCallback, useState, useMemo, useLayoutEffect } from 'react'
import { useDispatch, useSelector, batch } from 'react-redux'
import {
	Dialog,
	DialogActions,
	Button,
	DialogTitle,
	IconButton,
	makeStyles,
	Typography,
	Divider,
	TextField,
	DialogContent,
	Select,
	MenuItem,
	InputLabel,
	FormControl,
	Grid,
	ListSubheader
} from '@material-ui/core'
import CloseIcon from '@material-ui/icons/Close'
import WarningIcon from '@material-ui/icons/Warning'
import { ReduxState } from 'reducers'
import { algorithmUIActions } from 'reducers/algorithmUI'
import { algorithmDataActions } from 'reducers/algorithmData'
import InputGrammarField from 'components/WebUI/Dialog/InputDialog/InputGrammarField'
import InputFormatLink from 'components/WebUI/Dialog/InputDialog/InputFormatLink'
import { dataToGrammar, GrammarType, Rule } from 'interfaces/GrammarData'
import { useSyncState } from 'hooks/useSyncState'
import { useSnackbar } from 'notistack'
import {
	validateInitialSymbol,
	validateSymbols,
} from 'utils/validation'
import InputAutomatonField from 'components/WebUI/Dialog/InputDialog/Automaton/InputAutomatonField'

const { closeInputDialog } = algorithmUIActions
const {
	setInputGrammar,
	setInputString,
	setInputNumber: setInputInt,
	setInputBoolean,
	setInputStringToParse
} = algorithmDataActions

const useStyles = makeStyles({
	dialogPaper: {
		minWidth: '600px',
		overflow: 'visible'
	},
	dialogTitle: {
		display: 'flex',
		justifyContent: 'space-between',
		alignItems: 'center',
		padding: '4px 4px 0px 24px'
	},
	textField: {
		width: '100%',
		height: '100%'
	},
	typeSelectFormControl: {
		marginBottom: '20px'
	},
	dialogContent: {
		display: 'flex',
		flexDirection: 'column'
	},
	numberTextField: {
		width: '25%'
	},
	boolSelectFormControl: {
		width: '15%'
	}
})

const validateTextAreaForInputType = (inputType: string, content: string) => {
	switch(inputType) {
		case 'LinearString':
			if (!/^"(.*)"$/.test(content)) {
				return "To obtain LinearString you should encapsulate the string inside quotes. You can ignore this if you know what you are doing."
			}
			if (content.length - 2 > 0 && !/\s/.test(content)) {
				return "Your text contains some characters but no space was found. Are you sure that you separated individual symbols of the string with spaces?"
			}
			break;
		default:
			return undefined;
	}
}

const InputDialog = () => {
	//dropdown menu types
	const primitiveInputTypes = ['string', 'number', 'boolean']
	const parsedInputTypes = ['Automaton', 'Grammar', 'Object', 'RegExp', 'RTE', 'String', 'Tree']
	const inputTypes = [...primitiveInputTypes, parsedInputTypes]
	type InputType = typeof inputTypes[number]
	//type of input
	const [type, setType] = useState<InputType>('Automaton')

	//text field values
	const [stringValue, setStringValue] = useState<string>('')
	const [numberValue, setNumberValue] = useState<string>('')
	const [boolValue, setBoolValue] = useState<boolean>(true)

	//grammar state
	const [initialSymbol, setInitialSymbol] = useSyncState<string>('')
	const [ruleFields, setRuleFields] = useSyncState<Rule[]>([{ left: '', right: '' }])
	const [grammarTabValue, setGrammarTabValue] = useSyncState<number>(0)
	const [grammarType, setGrammarType] = useSyncState<GrammarType>('RIGHT_RG')

	const [doneButtonEnabled, setDoneButtonEnabled] = useState<boolean>(true)

	const [textAreaError, setTextAreaError] = useState<string>()

	const dispatch = useDispatch()
	const classes = useStyles()
	const dialogClasses = useMemo(
		() => ({
			paper: classes.dialogPaper
		}),
		[classes.dialogPaper]
	)

	const { open, dataType, initialValue, editedNode } = useSelector(
		(state: ReduxState) => ({
			open: state.algorithmUI.inputDialog.open,
			dataType: state.algorithmUI.inputDialog.dataType,
			initialValue: state.algorithmUI.inputDialog.initialValue,
			editedNode: state.algorithmData.present.nodes.get(
				state.algorithmUI.inputDialog.editedNodeId
			)
		})
	)
	const { castData } = useSelector((state : ReduxState) => { return { castData: state.workerDefinitions.casts }})

	const { enqueueSnackbar } = useSnackbar()

	const clearForms = () => {
		setGrammarType('RIGHT_RG')
		setInitialSymbol('')
		setRuleFields([{ left: '', right: '' }])
		setGrammarTabValue(0)
	}

	useLayoutEffect(() => {
		if (!open) return

		clearForms()

		switch (dataType) {
			case 'inputString':
				setType('string')
				setStringValue(initialValue)
				break
			case 'inputBoolean':
				setType('boolean')
				setBoolValue(initialValue)
				break
			case 'inputStringAutomaton':
				setType('Automaton')
				setStringValue(initialValue)
				break
			case 'inputStringGrammar':
				setType('Grammar')
				setGrammarTabValue(0)
				setStringValue(initialValue)
				break
			case 'inputGrammar':
				setType('Grammar')
				setGrammarTabValue(1)
				setGrammarType(initialValue.type)
				setInitialSymbol(initialValue.initialSymbol)
				setRuleFields(initialValue.rules)
				break
			case 'inputStringRegexp':
				setType('RegExp')
				setStringValue(initialValue)
				break
			case 'inputStringObject':
				setType('Object')
				setStringValue(initialValue)
				break
			case 'inputStringRte':
				setType('RTE')
				setStringValue(initialValue)
				break
			case 'inputStringString':
				setType('String')
				setStringValue(initialValue)
				break
			case 'inputStringTree':
				setType('Tree')
				setStringValue(initialValue)
				break
			case 'inputInt':
			case 'inputDouble':
				setType('number')
				setNumberValue(initialValue.toString())
				break
		}
	}, [open, initialValue, dataType])

	const handleClose = useCallback(() => {
		dispatch(closeInputDialog())
		setStringValue('')
		setNumberValue('')
		setBoolValue(false)

		clearForms()
	}, [dispatch])

	const handleApply = useCallback(
		() =>
			batch(() => {
				let isError = false

				if (!editedNode) return

				switch (type) {
					case 'string':
						dispatch(setInputString(editedNode.id, stringValue, castData))
						break
					case 'number':
						dispatch(setInputInt(editedNode.id, parseFloat(numberValue), castData))
						break
					case 'boolean':
						dispatch(setInputBoolean(editedNode.id, boolValue, castData))
						break
					case 'Automaton':
						dispatch(setInputStringToParse(editedNode.id, stringValue, 'inputStringAutomaton', castData))
						break
					case 'Grammar':
						if (grammarTabValue() === 0) {
							dispatch(setInputStringToParse(editedNode.id, stringValue, 'inputStringGrammar', castData))
						}
						else {
							try {
								validateInitialSymbol(initialSymbol())
							} catch(e: any) {
								enqueueSnackbar(e.message, { variant: 'error' })
								return
							}

							clearEmptyRules()
							let content = dataToGrammar(initialSymbol(), grammarType(), ruleFields())
							try {
								validateSymbols(content.terminals)
								validateSymbols(content.nonterminals)
							} catch(e: any) {
								enqueueSnackbar(e.message, { variant: 'error' })
								isError = true
							}

							dispatch(setInputGrammar(editedNode.id, content, castData))
						}
						break
					case 'Object':
						dispatch(setInputStringToParse(editedNode.id, stringValue, 'inputStringObject', castData))
						break
					case 'RegExp':
						dispatch(setInputStringToParse(editedNode.id, stringValue, 'inputStringRegexp', castData))
						break
					case 'RTE':
						dispatch(setInputStringToParse(editedNode.id, stringValue, 'inputStringRte', castData))
						break
					case 'String':
						dispatch(setInputStringToParse(editedNode.id, stringValue, 'inputStringString', castData))
						break
					case 'Tree':
						dispatch(setInputStringToParse(editedNode.id, stringValue, 'inputStringTree', castData))
						break
				}

				if (!isError)
					handleClose()
			}),
		[
			dispatch,
			handleClose,
			editedNode,
			stringValue,
			numberValue,
			boolValue,
			type,
			grammarTabValue,
			grammarType,
			initialSymbol,
			ruleFields,
		]
	)

	const handleTextFieldChange = useCallback(
		(e: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>) =>
			setStringValue(e.currentTarget.value),
		[]
	)

	const handleTextFieldChangeALTString = useCallback(
		(e: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>) => {
			setStringValue(e.currentTarget.value)
			const error = validateTextAreaForInputType('LinearString', e.currentTarget.value)
			setTextAreaError(() => error)
		},
		[]
	)

	const handleNumberTextFieldChange = useCallback(
		(e: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>) => {
			const { value } = e.currentTarget
			if (
				value.match(/^-?([0-9]*|[0-9]+\.?[0-9]*)$/) ||
				(value.length < numberValue.length &&
					numberValue.startsWith(value))
			)
				setNumberValue(value)
		},
		[numberValue]
	)

	const handleBoolSelectChange = useCallback(
		(e: React.ChangeEvent<{ value: unknown }>) =>
			setBoolValue(e.target.value === 'true'),
		[]
	)

	const handleKeyDown = useCallback(
		(e: React.KeyboardEvent) => e.stopPropagation(),
		[]
	)

	const handleTypeChange = useCallback(
		(e: React.ChangeEvent<{ value: unknown }>) => {
			setType(e.target.value as typeof type)
			setDoneButtonEnabled(true)
		},
		[type]
	)

	//GRAMMAR
	const handleGrammarTypeChange = (newValue: GrammarType) => {
		setGrammarType(newValue)
	}

	const handleTabChange = (newValue: number) => {
		setGrammarTabValue(newValue)
	}

	const handleAdd = () => {
		setRuleFields([...ruleFields(), { left: '', right: '' }])
	}

	const handleInitSymbolTextField = (text: string) => {
		setInitialSymbol(text)
	}

	const handleLeftInput = (index: number, value: string) => {
		const values = [...ruleFields()]
		values[index].left = value
		setRuleFields(values)
	}

	const handleRightInput = (index: number, value: string) => {
		const values = [...ruleFields()]
		values[index].right = value
		setRuleFields(values)
	}

	const handleDelete =(index: number) => {
		const values = [...ruleFields()]
		values.splice(index, 1)
		setRuleFields(values)
	}

	const clearEmptyRules = () => {
		const values = ruleFields().filter((rule) => rule.left !== '')
		setRuleFields(values)
	}

	if (!editedNode) return null

	return (
		<Dialog
			classes={dialogClasses}
			scroll="body"
			open={open}
			onClose={handleClose}
			onKeyDown={handleKeyDown}
			maxWidth="lg"
		>
			<DialogTitle disableTypography className={classes.dialogTitle}>
				<Typography component="span" variant="h6">Input editor</Typography>
				<IconButton onClick={handleClose}>
					<CloseIcon />
				</IconButton>
			</DialogTitle>
			<Divider />
			<DialogContent className={classes.dialogContent}>
			<Grid container justifyContent="center" spacing={2}>
				<Grid item xs={4}>
					<FormControl className={classes.typeSelectFormControl}>
						<InputLabel id="typeSelectLabel">Type</InputLabel>
						<Select
							labelId="typeSelectLabel"
							value={type}
							onChange={handleTypeChange}
						>
							<ListSubheader>Primitive types</ListSubheader>
								{primitiveInputTypes.map((name) => (
									<MenuItem key={name} value={name}>{name}</MenuItem>
								))}
							<ListSubheader>Parsed types</ListSubheader>
								{parsedInputTypes.map((name) => (
									<MenuItem key={name} value={name}>{name}</MenuItem>
								))}
						</Select>
					</FormControl>
				</Grid>
				<Grid item xs={8}>
					{(type as string === 'string') && (
						<Grid container direction="row" alignItems="center">
							<Grid item xs={2}>
								<WarningIcon style={{color: "red"}} />
							</Grid>
							<Grid item xs={10}>
								<Typography variant="caption">
									This is a raw string. It is usually used only for parsing the text representation of data structures.
									If you want to use string in algorithms (e.g. automaton run) use <strong><code>String</code></strong> type.
								</Typography>
							</Grid>
						</Grid>
					)}
					{parsedInputTypes.includes(type as string) && (
						<InputFormatLink
							showTooltip={type === 'Grammar' && grammarTabValue() === 1}
						/>
					)}
				</Grid>
			</Grid>
				{type === 'number' && (
					<TextField
						className={classes.numberTextField}
						label="Value"
						value={numberValue}
						onChange={handleNumberTextFieldChange}
					/>
				)}
				{type === 'boolean' && (
					<FormControl className={classes.boolSelectFormControl}>
						<InputLabel id="boolValueSelect">Value</InputLabel>
						<Select
							labelId="boolValueSelect"
							value={boolValue.toString()}
							onChange={handleBoolSelectChange}
						>
							<MenuItem value="false">False</MenuItem>
							<MenuItem value="true">True</MenuItem>
						</Select>
					</FormControl>
				)}
				{type === 'Grammar' && (
					<InputGrammarField
						onAdd={handleAdd}
						onDelete={handleDelete}
						stringValue={stringValue}
						fields={ruleFields()}
						type={grammarType()}
						onTypeChange={handleGrammarTypeChange}
						onLeftTextFieldChange={handleLeftInput}
						onRightTextFieldChange={handleRightInput}
						onTextFieldChange={handleTextFieldChange}
						initialSymbol={initialSymbol()}
						onInitSymbolChange={handleInitSymbolTextField}
						tabValue={grammarTabValue()}
						onTabChange={handleTabChange}
					/>
				)}
				{type === 'Automaton' && (
					<InputAutomatonField stringValue={stringValue} onChange={(value: string) => setStringValue(value)} setDoneButtonState={(enabled: boolean) => setDoneButtonEnabled(() => enabled)} />
				)}
				{type !== 'number' && type !== 'boolean' && type !== 'Grammar' && type !== 'Automaton'
				&& (
					<TextField
						className={classes.textField}
						variant="filled"
						multiline
						value={stringValue}
						onChange={type === 'String' ? handleTextFieldChangeALTString : handleTextFieldChange}
						error={textAreaError !== undefined}
						helperText={textAreaError}
					/>
				)}
			</DialogContent>
			<DialogActions>
				<Button onClick={handleApply} color="primary" disabled={!doneButtonEnabled}>
					Done
				</Button>
			</DialogActions>
		</Dialog>
	)
}

export default InputDialog
