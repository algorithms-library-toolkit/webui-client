import { Divider, List } from '@material-ui/core'
import Popover from '@material-ui/core/Popover'
import * as React from 'react'
import Typography from '@material-ui/core/Typography'
import { createParam } from 'components/WebUI//Toolbar/AlgorithmList/OverloadListItem'
import { Param } from 'components/WebUI/Toolbar/AlgorithmList/Param'
import { Doxygen } from 'components/WebUI/Toolbar/AlgorithmList/Doxygen'
import { makeStyles } from '@material-ui/styles'
import { Theme } from '@material-ui/core/styles'
import { AlibTemplateType, Overload } from 'interfaces/Algorithms'

interface DocsPopoverProps {
    dataId: string
    anchorEl: HTMLElement | SVGGElement | null
    name: string
    templateParams: AlibTemplateType[]
    overloads: Overload[]
    docs: string
    onClose: (event: React.MouseEvent<HTMLButtonElement>) => void
}

const useStyles = makeStyles((theme: Theme) => ({
    typography: {
        padding: theme.spacing(2),
    },
    popover: {
    	maxWidth: "600px",
    	maxHeight: "600px",
    }
}))

export const DocsPopover = (props: DocsPopoverProps) => {
    const classes = useStyles()
    const { dataId, anchorEl, name, templateParams, overloads, docs, onClose } = props
    const open = Boolean(anchorEl)
    const id = open ? 'popover_' + dataId : undefined

    let nameTitle = <Typography variant="subtitle2">
        {name}
    </Typography>

    let templateParamsTitles = templateParams.map(param =>
        <Typography variant="body2">
            {param}
        </Typography>
    )

    const overloadsParams = overloads?.map((overload, index) =>
        <React.Fragment>
            <Divider/>
            <List>
                {overload.params.map((param, paramIndex) => createParam(param, paramIndex + 1))}
                <Param
                    type={overload.resultType}
                    order="return"
                />
                {overload.docs ? <Doxygen docstring={overload.docs || ''} /> : ''}
            </List>
        </React.Fragment>
    )

    return (
        <React.Fragment>
            <Popover
                id={id}
                className={classes.popover}
                open={open}
                anchorEl={anchorEl}
                onClose={onClose}
                anchorOrigin={{
                    vertical: 'center',
                    horizontal: 'right',
                }}
                transformOrigin={{
                    vertical: 'center',
                    horizontal: 'left',
                }}>
                <div className={classes.typography}>
                    {nameTitle}
                    {templateParamsTitles}
                    {overloadsParams}
                </div>
            </Popover>
        </React.Fragment>
    )
}
