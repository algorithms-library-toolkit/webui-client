# Algorithms Library Toolkit - WebUI

React frontend for the [Algorithms Library Toolkit](https://alt.fit.cvut.cz/).

## Requirements

- node.js
- npm

## Scripts

* `npm install` - install dependencies
* `npm start` - run app in development mode
* `npm run build` - build application in production mode
* `npm test` - run app tests


## Development
* To run an application in development mode use `npm start`.
* The application needs an API server for evaluation. The API requests are made to URL specified in `REACT_APP_API_PREFIX` (see `.env.development`). You can override this in your `.env.development.local` file.

## Building
To make a production build use `npm run build`

## Docker
The repository contains a `Dockerfile` to build and deploy the app. It uses nginx to host the app. The nginx server can be configured by the `nginx/default.conf` config (pass repository root as the context when building).

## About
The application was forked from [Statemaker](https://gitlab.com/petrdsvoboda/statemaker), which is an application developed as a bachelor's project at the CTU FIT by Petr Svoboda. The Statemaker is used for creating and displaying finite automaton inputs/outputs.
