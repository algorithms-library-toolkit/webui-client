import { AlibType } from 'interfaces/Algorithms'

jest.mock('uuid', () => {
	let id = 0
	return { v4: () => (++id).toString(), reset: () => (id = 0) }
})

import uuid from 'uuid'
import { Map } from 'immutable'
import {
	initialAlgorithmState,
	algorithmReducer,
	algorithmDataActions
} from '../algorithmData'
import AlgorithmNode from '../../interfaces/AlgorithmNode'
import AlgorithmEdge from '../../interfaces/AlgorithmEdge'
import { emptyAutomaton } from '../../interfaces/StateData'
import { AlgorithmGraph } from '../../interfaces/AlgorithmGraph'

const casts: Record<AlibType, AlibType[]> = {
	'abstraction::UnspecifiedType': ['abstraction::UnspecifiedType']
}

const node: AlgorithmNode = {
	height: 0,
	width: 0,
	id: '1',
	name: 'name',
	nodeType: 'algorithm',
	params: [{name: 'param', type: ''}],
	position: { x: 0, y: 0 },
	resultType: '',
	templateParams: []
}

const edge: AlgorithmEdge = {
	startNodeId: '0',
	endNodeId: '1',
	endParamIndex: 0,
	id: '1'
}

const stateWithData = {
	...initialAlgorithmState,
	nodes: Map<string, AlgorithmNode>().set(node.id, node),
	edges: Map<string, AlgorithmEdge>().set(edge.id, edge)
}

describe('algorithm data reducer', () => {
	beforeEach(() => (uuid as any).reset())

	it('creates node', () => {
		const state = algorithmReducer(
			initialAlgorithmState,
			algorithmDataActions.addNode(node)
		)
		expect(state.nodes.size).toBe(1)
		expect(JSON.stringify(state.nodes.first())).toBe(JSON.stringify(node))
	})

	it('creates edge', () => {
		const state = algorithmReducer(
			initialAlgorithmState,
			algorithmDataActions.addEdge(edge, casts)
		)
		expect(state.edges.size).toBe(1)
		expect(JSON.stringify(state.edges.first())).toBe(JSON.stringify(edge))
	})

	it('positions node', () => {
		const position = { x: 1, y: 1 }
		const state = algorithmReducer(
			stateWithData,
			algorithmDataActions.positionNode(node.id, position)
		)

		expect(state.nodes.get(node.id)?.position).toBe(position)
	})

	it('resizes node', () => {
		const state = algorithmReducer(
			stateWithData,
			algorithmDataActions.resizeNode(node.id, 1, 1)
		)
		expect(state.nodes.get(node.id)?.width).toBe(1)
		expect(state.nodes.get(node.id)?.height).toBe(1)
	})

	it('sets input automaton', () => {
		const state = algorithmReducer(
			stateWithData,
			algorithmDataActions.setInputAutomaton('1', emptyAutomaton)
		)
		expect(state.nodes.get(node.id)?.value).toBe(emptyAutomaton)
	})

	it('sets input string', () => {
		const state = algorithmReducer(
			stateWithData,
			algorithmDataActions.setInputString('1', 'test', casts)
		)
		expect(state.nodes.get(node.id)?.value).toBe('test')
	})

	it('sets input number', () => {
		const state = algorithmReducer(
			stateWithData,
			algorithmDataActions.setInputNumber('1', 1, casts)
		)
		expect(state.nodes.get(node.id)?.value).toBe(1)
	})

	it('sets input boolean', () => {
		const state = algorithmReducer(
			stateWithData,
			algorithmDataActions.setInputBoolean('1', true, casts)
		)
		expect(state.nodes.get(node.id)?.value).toBe(true)
	})

	it('sets output values', () => {
		const outputValues = Map<string, string>().set('1', 'test')
		const state = algorithmReducer(
			initialAlgorithmState,
			algorithmDataActions.setOutputValues(outputValues)
		)
		expect(state.outputValues).toBe(outputValues)
	})

	it('deletes node', () => {
		const state = algorithmReducer(
			stateWithData,
			algorithmDataActions.deleteNode(node.id)
		)
		expect(state.nodes.isEmpty()).toBe(true)
	})

	it('deletes edge', () => {
		const state = algorithmReducer(
			stateWithData,
			algorithmDataActions.deleteEdge(edge.id)
		)
		expect(state.edges.isEmpty()).toBe(true)
	})

	it('imports graph', () => {
		const originNode = { ...node, id: '0' }
		const graph: Omit<AlgorithmGraph, 'outputValues'> = {
			...stateWithData,
			nodes: stateWithData.nodes.set(originNode.id, originNode)
    }
    
    const state = algorithmReducer(initialAlgorithmState, algorithmDataActions.importGraph(graph, casts))
    
    expect(state.nodes.contains(originNode)).toBe(true)
    expect(state.nodes.contains(node)).toBe(true)
    expect(state.nodes.size).toBe(2)

    expect(state.edges.contains(edge)).toBe(true)
    expect(state.edges.size).toBe(1)
	})

	afterAll(() => jest.resetModules())
})
