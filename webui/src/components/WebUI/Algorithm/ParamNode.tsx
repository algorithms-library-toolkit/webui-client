import React from 'react'
import { makeStyles } from '@material-ui/core'

import { ParamNodeContainerProps } from './ParamNodeContainer'
import { WebUITheme } from '../../../interfaces/Theme'

export const paramNodeRadius = 6

const useStyles = makeStyles<WebUITheme>((theme) => ({
	param: {
		stroke: theme.palette.grey[800],
		strokeWidth: 2,
		fill: '#fff',
		pointerEvents: 'all',

		'&:hover': {
			stroke: theme.palette.primary.main,
			fill: theme.palette.grey[400]
		},

		'&[data-error="true"]': {
			stroke: '#f00'
		}
	}
}))

interface ParamNodeProps extends ParamNodeContainerProps {
	onMouseDown: (e: React.MouseEvent<SVGCircleElement>) => any
	onMouseUp: (e: React.MouseEvent<SVGCircleElement>) => any
	error: boolean
}

const ParamNode = (props: ParamNodeProps) => {
	const {
		x,
		y,
		onMouseDown,
		onMouseUp,
		name,
		type,
		error
	} = props
	const classes = useStyles()

	return (
		<circle
			className={classes.param}
			cx={x}
			cy={y}
			r={paramNodeRadius}
			onMouseDown={onMouseDown}
			onMouseUp={onMouseUp}
			data-error={error}
		>
		</circle>
	)
}

export default ParamNode
