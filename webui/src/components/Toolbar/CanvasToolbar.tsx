import React from 'react'
import { Paper } from '@material-ui/core'
import ZoomInIcon from '@material-ui/icons/ZoomIn'
import ZoomOutIcon from '@material-ui/icons/ZoomOut'
import MoveIcon from '@material-ui/icons/OpenWith'
import SelectIcon from '@material-ui/icons/TouchApp'
import GridIcon from '@material-ui/icons/GridOn'
import ElementButton from './ElementButton'
import { CursorMode } from '../../interfaces/Canvas'

interface CanvasToolbarProps {
	cursorMode: CursorMode
	gridOn: boolean
	onSelectCursorMode: () => void
	onMoveCursorMode: () => void
	onZoomIn: () => void
	onZoomOut: () => void
	onGrid: () => void
}

export const CanvasToolbar = (props: CanvasToolbarProps) => {
	const {
		cursorMode,
		gridOn: gridSelected,
		onSelectCursorMode,
		onMoveCursorMode,
		onZoomIn,
		onZoomOut,
		onGrid
	} = props

	return (
		<Paper>
			<ElementButton
				Icon={SelectIcon}
				label="Select mode"
				selected={cursorMode === 'select'}
				onAction={onSelectCursorMode}
			/>
			<ElementButton
				Icon={MoveIcon}
				label="Move mode"
				selected={cursorMode === 'move'}
				onAction={onMoveCursorMode}
			/>
			<ElementButton
				Icon={ZoomInIcon}
				label="Zoom in"
				selected={false}
				onAction={onZoomIn}
			/>
			<ElementButton
				Icon={ZoomOutIcon}
				label="Zoom out"
				selected={false}
				onAction={onZoomOut}
			/>
			<ElementButton
				Icon={GridIcon}
				label="Toggle grid"
				selected={gridSelected}
				onAction={onGrid}
			/>
		</Paper>
	)
}
