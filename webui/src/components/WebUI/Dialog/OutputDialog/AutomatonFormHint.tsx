import { Typography } from '@material-ui/core'
import React from 'react'

const AutomatonFormHint = () => {

    return(<>
        <Typography color='inherit'>Epsilon symbol is represented by #E</Typography>
        <Typography color='inherit'>States in the transitions are delimited by comma or pipe symbol</Typography>
        <Typography color='inherit'>New row can be added and removed via buttons or added by pressing the enter key from the last field of the row.</Typography>
        <Typography color='inherit'>New symbols of the alphabet can be added and removed via buttons or deleted with DEL key.</Typography>
    </>)
}

export default AutomatonFormHint
