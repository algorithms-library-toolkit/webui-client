import * as React from 'react'
import { useDispatch, useSelector } from 'react-redux'

import SaveIcon from '@material-ui/icons/Save'

import { ReduxState, Actions } from '../../../reducers'
import { saveState } from '../../../utils/persist'
import ElementButton from '../../Toolbar/ElementButton'

const { setConfirmText } = Actions

/**
 * Saves current state machine to browser local storage
 *
 * @returns
 */
function SaveButton() {
	const dispatch = useDispatch()
	const { data } = useSelector(
		(state: ReduxState) => ({
			data: state.stateData.present
		})
	)

	const handleSave = React.useCallback(() => {
		saveState(data)
		dispatch(setConfirmText('Automata saved'))
	}, [dispatch, data])

	return (
		<ElementButton
			Icon={SaveIcon}
			label="Save to memory"
			onAction={handleSave}
		/>
	)
}

export default SaveButton
