#include "testing.hpp"
#include <environment/Environment.h>
#include <string>
#include "graph/nodes/BoolNode.hpp"
#include "graph/nodes/DoubleNode.hpp"
#include "graph/nodes/IntNode.hpp"
#include "graph/nodes/OutputNode.hpp"
#include "graph/nodes/StringNode.hpp"

TEST_CASE("Input nodes evaluation")
{
    cli::Environment environment;
    auto outputNode = std::make_shared<OutputNode>("output-node");
    std::string expected;

    SECTION("Bool")
    {
        bool value;

        SECTION("true")
        {
            value = true;
            expected = "1";
        }

        SECTION("false")
        {
            value = false;
            expected = "0";
        }

        BoolNode node("bool-node", value, {});

        node.createEdge(outputNode, 0);
        node.evaluate(environment);
    }

    SECTION("String")
    {
        SECTION("A word")
        {
            expected = "execute";
        }

        SECTION("A number as string")
        {
            expected = "123";
        }

        SECTION("A sequence of strings")
        {
            expected = "hello     world";
        }

        SECTION("Random garbage?")
        {
            expected = "12 / * && $@#$ *^ {$% #$!$ $%& ^*  <  ahoj >";
        }

        StringNode node("string-node", expected, {});

        node.createEdge(outputNode, 0);
        node.evaluate(environment);
    }

    SECTION("Int")
    {
        int value;

        SECTION("An ordinary integer")
        {
            value = -187967;
            expected = "-187967";
        }

        SECTION("INT_MAX")
        {
            value = std::numeric_limits<int>::max();
            expected = std::to_string(std::numeric_limits<int>::max());
        }

        SECTION("A zero")
        {
            value = 0;
            expected = "0";
        }

        IntNode node("integer-node", value, {});

        node.createEdge(outputNode, 0);
        node.evaluate(environment);
    }

    SECTION("Double")
    {
        double value;

        SECTION("An ordinary double")
        {
            value = 187967.89673944e15;
            expected = "1.87968e+20";
        }

        SECTION("A zero")
        {
            value = 0;
            expected = "0";
        }

        SECTION("A nan")
        {
            value = NAN;
            expected = "nan";
        }

        DoubleNode node("double-node", value, {});

        node.createEdge(outputNode, 0);
        node.evaluate(environment);
    }


    outputNode->evaluate(environment);
    REQUIRE(outputNode->getResult());
    REQUIRE(outputNode->getResult().value() == expected);
}
