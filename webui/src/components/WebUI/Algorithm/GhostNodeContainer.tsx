import * as React from 'react'
import { batch, useDispatch, useSelector } from 'react-redux'

import useAlgorithmPosition from '../../../hooks/useAlgorithmPosition'

import { Actions, ReduxState } from '../../../reducers'
import Node from './Node'
import { isOutputNode } from '../../../interfaces/AlgorithmNode'
import { v4 as uuidv4 } from 'uuid'
import { getNodeLabelText } from 'utils/textSize'
import useNodeSize from 'hooks/useNodeSize'
import { emptyCustomFunction } from 'interfaces/CustomFunction'

const {
	addNode,
	setBuildNode,
	updateGhostNode,
	set,
	setStatemakerOpen,
	selectNode,
	openInputDialog,
	openCustomFunctionDialog
} = Actions

interface GhostNodeProps {
	canvasRef: React.RefObject<SVGSVGElement>
}

/**
 * Ghost state shown when drawing new state on canvas
 * Uses mouse cursor for position
 *
 * @returns
 */
function GhostNodeContainer(props: GhostNodeProps) {
	const { canvasRef } = props
	const dispatch = useDispatch()

	const { show, data } = useSelector((state: ReduxState) => ({
		show: state.algorithmCanvas.buildNode,
		data: state.algorithmUI.ghostNode,
	}))

	const position = useAlgorithmPosition(canvasRef, show)

	const handleTextChange = React.useCallback(
		(width: number, height: number) => dispatch(updateGhostNode({ width: width, height: height })),
		[dispatch]
	)

	const text = getNodeLabelText(data.name, data.templateParams)

	const textRef = useNodeSize(text, handleTextChange, data.params.length, true)

	// Create new node
	const handleMouseUp = React.useCallback(() => {
		batch(() => {
			const id = uuidv4()
			dispatch(
				addNode({
					id,
					position,
					name: data.name,
					width: data.width,
					height: data.height,
					params: data.params,
					templateParams: data.templateParams,
					resultType: data.resultType,
					nodeType: data.nodeType,
					value: data.value,
					overloads: data.overloads,
					docs: data.docs
				})
			)
			dispatch(setBuildNode(false))
			dispatch(selectNode(id))
			switch (data.nodeType) {
				case 'inputAutomaton':
					dispatch(
						set({
							states: {},
							transitions: {},
							initialStates: [],
							finalStates: [],
						})
					)
					dispatch(setStatemakerOpen(true, true))
					break
				case 'inputString':
					dispatch(openInputDialog(id, 'string'))
					break
				case 'customFunction':
					if (!data.value)
						dispatch(openCustomFunctionDialog(id, emptyCustomFunction))
					break
			}
		})
	}, [dispatch, position, data])

	if (!show) return null

	return (
		<Node
			ref={textRef}
			data={{ ...data, position }}
			isGhost={true}
			onMouseUp={handleMouseUp}
			hasOutput={!isOutputNode(data.nodeType)}
		/>
	)
}

export default GhostNodeContainer
