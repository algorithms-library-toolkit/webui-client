import Automaton, { AutomatonTransition } from 'interfaces/Automaton'
import { classifyAutomaton } from 'interfaces/AutomatonTypes'

export const stateTypes = ['', 'in', 'out', 'inout'] as const
export type StateType = typeof stateTypes[number]

/**
 * Automaton table row
 */
export interface Row {
    type: StateType
    name: string
    transitions: string[]
}

/**
 * Table object
 */
export interface TableAutomaton {
    symbols: string[]
    table: Row[]
}

export interface TableError {
	text: string
	cellType: 'table' | 'symbol'
	cell1DIndex: number
}

/**
 * Returns array of end states for row on the basis of alphabet symbol's order in the header
 * @param state
 * @param automaton
 */
function getTransitions(state: string, automaton: Automaton) {
    let resultTransitions: string[] = []

	const outgoingTransitions = automaton.transitions.filter(tr => tr.from == state)
    automaton.alphabet.forEach(symbol => {
        // find all transitions for the state and the symbol
        const targetNames = outgoingTransitions.filter(tr => tr.from === state && tr.symbol === symbol).map(tr => tr.to)
        resultTransitions.push(targetNames.join(', '))
    })

    return resultTransitions
}

/**
 * Create rows from automaton object
 * @param automaton
 * @param alphabet
 */
export function createTableRows(automaton: Automaton) {
    let rows: Row[] = []
    automaton.states.map((state) => {
        const initial = automaton.initialStates.some((initial) => initial === state)
        const final = automaton.finalStates.some((final) => final === state)
        let type: StateType = ''
        if (initial && final)
            type = 'inout'
        else if (initial)
            type = 'in'
        else if (final)
            type = 'out'

        rows.push({
                type: type,
                name: state,
                transitions : getTransitions(state, automaton)
            })
        }
    )

    return rows
}

export function splitTransitions(states: string): string[] {
    return states.split(/[,|]/g)
        .map((singleState) => singleState.trim())
        .filter((filterState) => filterState !== '')
}


export function rowToAutomaton(automaton: TableAutomaton): Automaton {
	let states = automaton.table.map(row => row.name)
	const initialStates = automaton.table.filter(row => row.type == 'in' || row.type == 'inout').map(row => row.name)
	const finalStates = automaton.table.filter(row => row.type == 'out' || row.type == 'inout').map(row => row.name)

    const transitions = automaton.table.reduce((acc, row: Row) => {
		const transitions = row.transitions.reduce((tracc, column, index) => {
			const singleTransitions = splitTransitions(column)
			const transitions = singleTransitions.map(target => { return {
				symbol: automaton.symbols[index],
				from: row.name,
				to: target
			}})

			transitions.forEach(tr => {
				if (states.indexOf(tr.to) < 0) {
					states.push(tr.to)
				}
			})

			return [...tracc, ...transitions]
		}, [] as AutomatonTransition[])

		return [
			...acc,
			...transitions,
		]
	}, [] as AutomatonTransition[])


	const automatonWithoutType = {
        alphabet: automaton.symbols,
        states: states,
        initialStates: initialStates,
        finalStates: finalStates,
        transitions: transitions,
    }

	return { type: classifyAutomaton(automatonWithoutType), ...automatonWithoutType }
}
