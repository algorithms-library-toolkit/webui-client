import { AlibType } from "interfaces/Algorithms";

export const removeTemplate = (type: AlibType) => type.match(/^[^<]*/)?.[0] ?? ''
