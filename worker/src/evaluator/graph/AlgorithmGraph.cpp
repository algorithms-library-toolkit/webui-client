#include <ast/command/CommandResult.h>
#include <environment/Environment.h>
#include <readline/StringLineInterface.h>
#include "AlgorithmGraph.hpp"
#include "core/type_details.hpp"
#include "graph/nodes/AbstractNode.hpp"
#include "graph/topSort.hpp"

AlgorithmGraph::AlgorithmGraph(const std::map<std::string, std::shared_ptr<AbstractNode>>& nodes)
    : m_nodes(nodes)
{
}

AlgorithmGraph::AlgorithmGraph(const std::map<std::string, std::shared_ptr<AbstractNode>>& nodes, const std::vector<std::string>& functions)
    : m_nodes(nodes)
    , m_functions(functions)
{
}

std::vector<std::string> AlgorithmGraph::getFunctions() const
{
    return m_functions;
}

std::map<std::string, std::shared_ptr<AbstractNode>> AlgorithmGraph::nodes() const
{
    return m_nodes;
}

AlgorithmGraph::EvaluatedGraph AlgorithmGraph::evaluate()
{
    auto sortedNodes = topSort(m_nodes);

    std::map<std::string, std::string> outputs;
    std::map<std::string, std::string> prettyOutputs;
    std::map<std::string, std::string> dotOutputs;
    std::map<std::string, std::string> latexOutputs;
    std::map<std::string, std::string> resultTypes;
    cli::Environment environment;

    for (const auto& function : m_functions) {
        if (auto state = environment.execute(std::make_shared<cli::StringLineInterface>(cli::StringLineInterface(function))); state != cli::CommandResult::OK && state != cli::CommandResult::EOT) {
            throw std::invalid_argument("Custom function has invalid format");
        }
    }

    while (!sortedNodes.empty()) {
        auto node = std::move(sortedNodes.front());
        sortedNodes.pop_front();

        node->evaluate(environment);

        if (auto nodeRes = node->getResult()) {
            outputs[node->getId()] = std::move(*nodeRes);
        }

        if (auto nodeRes = node->getResultStringCompose()) {
            prettyOutputs[node->getId()] = std::move(*nodeRes);
        }

        if (auto nodeRes = node->getResultDot()) {
            dotOutputs[node->getId()] = std::move(*nodeRes);
        }

        if (auto nodeRes = node->getResultLatex()) {
            latexOutputs[node->getId()] = std::move(*nodeRes);
        }

        if (auto nodeType = node->getResultType()) {
            resultTypes[node->getId()] = ext::to_string(nodeType);
        }
    }

    return {outputs, prettyOutputs, dotOutputs, latexOutputs, resultTypes};
}
