import { ExportFn } from 'interfaces/Transformations'

interface ExportData {
	initial: string[]
	final: string[]
	states: {
		[key: string]: {
			[key: string]: string
		}
	}
}
/**
 * Exports data in json format
 *
 * @param {*} data
 * @returns
 */
const exportData: ExportFn = data => {
	let output: ExportData = {
		initial: data.initialStates.map(s => data.states[s].name),
		final: data.finalStates.map(s => data.states[s].name),
		states: {}
	}
	// Create states
	output = Object.values(data.states).reduce(
		(o, s) => ({
			...o,
			states: {
				...o.states,
				[s.name]: {}
			}
		}),
		output
	)
	// Create transitions
	output = Object.values(data.transitions).reduce((o, t) => {
		const startState = data.states[t.startState]
		const endState = data.states[t.endState]
		return {
			...o,
			states: {
				...o.states,
				[startState.name]: {
					...o.states[startState.name],
					[t.name]: endState.name
				}
			}
		}
	}, output)
	return JSON.stringify(output, null, 4)
}

export default exportData
