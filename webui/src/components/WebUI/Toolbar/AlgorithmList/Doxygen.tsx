import React, { useState } from 'react'
import {
	makeStyles,
	Link,
	ListItem,
	ListItemIcon,
	ListItemText,
	Typography
	} from '@material-ui/core'
import {
	InfoOutlined,
} from '@material-ui/icons';

const useStyles = makeStyles({
  item: {
    padding: 0
  }
})

interface DoxygenProps {
	docstring: string
}

function filterSimpleDocLines(line: string) {
	const doxygenTagsAdvanced = ["param", "tparam", "return", "throw", "throws"];

	return doxygenTagsAdvanced.every((tag) => !line.startsWith("@" + tag) && !line.startsWith("\\" + tag))
}

function createDocumentation(lines: string[], more: boolean) {
	let linesToShow = more ? lines : lines.filter((line) => filterSimpleDocLines(line))

	return linesToShow.map((line) => (
		<Typography color="textSecondary" variant="body2">
			{line}
		</Typography>
	))
}

export const Doxygen = (props: DoxygenProps) => {
	const { docstring } = props
	const classes = useStyles()
	const [ more, setMore ] = useState(false);

	const docLines = docstring.split('\n')

	return (
		<ListItem classes={{ root: classes.item }}>
			<ListItemText>
				{ createDocumentation(docLines, more) }
				<Link
					component="button"
					variant="body2"
					onClick={(e: any) => { e.stopPropagation(); setMore(!more); }}
				>
					{more ? "(less)" : "(more)"}
				</Link>
			</ListItemText>
		</ListItem>
		)
}

