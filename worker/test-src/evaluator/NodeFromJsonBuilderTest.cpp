#include "testing.hpp"
#include <json/json.h>
#include "graph/AlgorithmGraph.hpp"
#include "graph/Edge.hpp"
#include "graph/NodesFromJSONBuilder.hpp"
#include "graph/nodes/AlgorithmNode.hpp"
#include "graph/nodes/BoolNode.hpp"
#include "graph/nodes/DoubleNode.hpp"
#include "graph/nodes/IntNode.hpp"
#include "graph/nodes/OutputNode.hpp"
#include "graph/nodes/StringNode.hpp"
#include "utils/alt.hpp"

#define EDGE(FROM, TO, TO_PARAMINDEX) expectedMap[std::to_string(FROM)]->createEdge(expectedMap[std::to_string(TO)], TO_PARAMINDEX);

void checkParams(const std::vector<std::shared_ptr<abstraction::Value>>& a, const std::vector<std::shared_ptr<abstraction::Value>>& b)
{
    REQUIRE(a.size() == b.size());
    for (size_t i = 0; i < a.size(); ++i) {
        INFO(i);
        REQUIRE(abstractValueToString(a[i]) == abstractValueToString(b[i]));
    }
}

void checkNodes(AbstractNode& a, AbstractNode& b);

void checkEdges(const std::set<Edge>& a, const std::set<Edge>& b)
{
    REQUIRE(a.size() == b.size());

    auto aIt = a.begin();
    auto bIt = b.begin();

    for (; aIt != a.end() && bIt != b.end(); ++aIt, ++bIt) {
        REQUIRE(aIt->getParamIndex() == bIt->getParamIndex());
        checkNodes(*(aIt->getNode()), *(bIt->getNode()));
    }
}

void checkNodes(AbstractNode& a, AbstractNode& b)
{
    REQUIRE(a.getId() == b.getId());
    REQUIRE(a.isOutput() == b.isOutput());
    checkParams(a.getParams(), b.getParams());

    checkEdges(a.getEdges(), b.getEdges());
}

TEST_CASE("Build graph from JSON")
{
    Json::Value json;
    std::string jsonString;
    std::map<std::string, std::shared_ptr<AbstractNode>> expectedMap;
    std::vector<std::string> expectedFunctions;

    SECTION("Empty graph")
    {
        jsonString = R"EOF(
{
  "customFunctions": [],
  "graph": {
    "algorithms": {},
    "inputs": {},
    "outputs": [],
    "pipes": []
  }
}
)EOF";
        expectedMap = {};
        expectedFunctions = {};
    }

    SECTION("Example graph")
    {
        jsonString = R"EOF(
{
  "customFunctions": [],
  "graph": {
    "algorithms": {
      "2": {
        "name": "automaton::determinize::Determinize"
      }
    },
    "inputs": {
      "strings": {
        "1": {
          "value": "ENFA\tTransition1\nState1\tState2\nState2\t-"
        }
      }
    },
    "outputs": [
      "3"
    ],
    "pipes": [
      {
        "from": "1",
        "to": "2",
        "paramIndex": 0
      },
      {
        "from": "2",
        "to": "3",
        "paramIndex": 0
      }
    ]
  }
}
)EOF";

        expectedFunctions = {};
        expectedMap = {
            {"1", std::make_shared<StringNode>("1", "ENFA\tTransition1\nState1\tState2\nState2\t-", ext::vector<std::string>())},
            {"2", std::make_shared<AlgorithmNode>("2", "automaton::determinize::Determinize", ext::vector<std::string>())},
            {"3", std::make_shared<OutputNode>("3")},
        };

        EDGE(1, 2, 0);
        EDGE(2, 3, 0);
    }

    SECTION("Algorithm graph with 2 inputs")
    {
        jsonString = R"EOF(
{
  "customFunctions": [],
  "graph": {
    "algorithms": {
      "3": {
        "name": "compare::AutomatonCompare"
      }
    },
    "inputs": {
      "automatons": {
      },
      "strings": {
        "1": {
          "value": "ENFA\tTransition1\nState1\tState2\nState2\t-"
        },
        "2": {
          "value": "foo"
        }
      }
    },
    "outputs": [
      "4"
    ],
    "pipes": [
      {
        "from": "1",
        "to": "3",
        "paramIndex": 0
      },
      {
        "from": "2",
        "to": "3",
        "paramIndex": 1
      },
      {
        "from": "3",
        "to": "4",
        "paramIndex": 0
      }
    ]
  }
}
)EOF";

        expectedFunctions = {};
        expectedMap = {
            {"1", std::make_shared<StringNode>("1", "ENFA\tTransition1\nState1\tState2\nState2\t-", ext::vector<std::string>())},
            {"2", std::make_shared<StringNode>("2", "foo", ext::vector<std::string>())},
            {"3", std::make_shared<AlgorithmNode>("3", "compare::AutomatonCompare", ext::vector<std::string>())},
            {"4", std::make_shared<OutputNode>("4")},
        };

        EDGE(1, 3, 0);
        EDGE(2, 3, 1);
        EDGE(3, 4, 0);
    }

    SECTION("Two components")
    {
        jsonString = R"EOF(
{
  "customFunctions": [],
  "graph": {
    "algorithms": {
      "2": {
        "name": "automaton::determinize::Determinize"
      },
      "5": {
        "name": "string::Parse"
      }
    },
    "inputs": {
      "automatons": {

       },
      "strings": {
        "1": {
          "value": "ENFA\tTransition1\nState1\tState2\nState2\t-"
        },
        "4": {
          "value": "foo"
        }
      }
    },
    "outputs": [
      "3",
      "6"
    ],
    "pipes": [
      {
        "from": "1",
        "to": "2",
        "paramIndex": 0
      },
      {
        "from": "2",
        "to": "3",
        "paramIndex": 0
      },
      {
        "from": "4",
        "to": "5",
        "paramIndex": 0
      },
      {
        "from": "5",
        "to": "6",
        "paramIndex": 0
      }
    ]
  }
}
)EOF";

        expectedFunctions = {};
        expectedMap = {
            {"1", std::make_shared<StringNode>("1", "ENFA\tTransition1\nState1\tState2\nState2\t-", ext::vector<std::string>())},
            {"2", std::make_shared<AlgorithmNode>("2", "automaton::determinize::Determinize", ext::vector<std::string>())},
            {"3", std::make_shared<OutputNode>("3")},
            {"4", std::make_shared<StringNode>("4", "foo", ext::vector<std::string>())},
            {"5", std::make_shared<AlgorithmNode>("5", "string::Parse", ext::vector<std::string>())},
            {"6", std::make_shared<OutputNode>("6")},
        };

        EDGE(1, 2, 0);
        EDGE(2, 3, 0);
        EDGE(4, 5, 0);
        EDGE(5, 6, 0);
    }

    SECTION("Graph with a fork node")
    {
        jsonString = R"EOF(
{
  "customFunctions": [],
  "graph": {
    "algorithms": {},
    "inputs": {
      "strings": {
        "1": {
          "value": "ENFA\tTransition1\nState1\tState2\nState2\t-"
        }
      }
    },
    "outputs": [
      "2",
      "3"
    ],
    "pipes": [
      {
        "from": "1",
        "to": "2",
        "paramIndex": 0
      },
      {
        "from": "1",
        "to": "3",
        "paramIndex": 0
      }
    ]
  }
}
)EOF";
        expectedFunctions = {};
        expectedMap = {
            {"1", std::make_shared<StringNode>("1", "ENFA\tTransition1\nState1\tState2\nState2\t-", ext::vector<std::string>())},
            {"2", std::make_shared<OutputNode>("2")},
            {"3", std::make_shared<OutputNode>("3")},
        };

        EDGE(1, 2, 0);
        EDGE(1, 3, 0);
    }

    SECTION("Two outputs")
    {
        jsonString = R"EOF(
{
  "customFunctions": [],
  "graph": {
    "algorithms": {
      "2": {
        "name": "automaton::determinize::Determinize"
      }
    },
    "inputs": {
      "strings": {
        "1": {
          "value": "NFA\t1\t0\n>0\t1|0\t0\n<1\t-\t-"
        }
      }
    },
    "outputs": ["4", "3"],
    "pipes": [
      {
        "from": "1",
        "to": "2",
        "paramIndex": 0
      },
      {
        "from": "2",
        "to": "4",
        "paramIndex": 0
      },
      {
        "from": "2",
        "to": "3",
        "paramIndex": 0
      }
    ]
  }
}
)EOF";

        expectedFunctions = {};
        expectedMap = {
            {"1", std::make_shared<StringNode>("1", "NFA	1\t0\n>0\t1|0\t0\n<1\t-\t-", ext::vector<std::string>())},
            {"2", std::make_shared<AlgorithmNode>("2", "automaton::determinize::Determinize", ext::vector<std::string>())},
            {"3", std::make_shared<OutputNode>("3")},
            {"4", std::make_shared<OutputNode>("4")},
        };

        EDGE(1, 2, 0);
        EDGE(2, 3, 0);
        EDGE(2, 4, 0);
    }

    SECTION("Graph with custom function")
    {
        jsonString = R"EOF(
{
  "customFunctions": ["function myf ( auto $a ) returning auto begin \nreturn $a;\nend"],
  "graph": {
    "algorithms": {
      "2": {
        "name": "myF"
      }
    },
    "inputs": {
      "strings": {
        "1": {
          "value": "a"
        }
      }
    },
    "outputs": ["3"],
    "pipes": [
      {
        "from": "1",
        "to": "2",
        "paramIndex": 0
      },
      {
        "from": "2",
        "to": "3",
        "paramIndex": 0
      }
    ]
  }
}
)EOF";

        expectedFunctions = {"function myf ( auto $a ) returning auto begin \nreturn $a;\nend"};
        expectedMap = {
            {"1", std::make_shared<StringNode>("1", "a", ext::vector<std::string>())},
            {"2", std::make_shared<AlgorithmNode>("2", "myF", ext::vector<std::string>())},
            {"3", std::make_shared<OutputNode>("3")},
        };

        EDGE(1, 2, 0);
        EDGE(2, 3, 0);
    }

    std::string errs;
    std::istringstream iss(jsonString);
    REQUIRE(Json::parseFromStream(Json::CharReaderBuilder(), iss, &json, &errs));
    REQUIRE(!json.isNull());
    REQUIRE(json.isObject());

    auto graph = NodesFromJSONBuilder().fromJson(json).build();
    auto nodeMap = graph.nodes();

    REQUIRE(nodeMap.size() == expectedMap.size());
    for (auto itA = nodeMap.begin(), itB = expectedMap.begin(); itA != nodeMap.end(); ++itA, ++itB) {
        REQUIRE(itA->first == itB->first);
        checkNodes(*itA->second, *itB->second);
    }

    REQUIRE(graph.getFunctions() == expectedFunctions);
}
