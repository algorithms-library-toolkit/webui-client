import { rowToAutomaton,} from 'interfaces/TableRow'
import { serializeAutomaton } from 'utils/alt/export/serializers'

function convertFromTable(node: any) {
	return {
		...node,
		'nodeType': 'inputStringAutomaton',
		'value': serializeAutomaton(rowToAutomaton(node.value))
	}
}

export default function migrate(graph: any) {
	let res: any = {
		'edges': { ...graph.edges },
		'nodes': {}
	}

	Object.keys(graph.nodes).forEach((key: any) => {
		const node = graph.nodes[key]
		const newNode = node.nodeType === 'inputTableAutomaton' ? convertFromTable(node) : node
		res['nodes'][newNode.id] = newNode
	})

	return res
}
