import React from 'react'
import { useSelector } from 'react-redux'
import {
	makeStyles,
	Dialog,
	CircularProgress,
	DialogContent,
	DialogTitle,
} from '@material-ui/core'
import { ReduxState } from '../../../reducers'

const useStyles = makeStyles({
	dialogContent: {
		display: 'flex',
		justifyContent: 'center',
		minHeight: '75px',
	},
})

export const EvaluatingDialog = () => {
	const { open } = useSelector((state: ReduxState) => ({
		open: state.algorithmUI.evaluatingDialogOpen,
	}))

	const classes = useStyles()

	return (
		<Dialog open={open}>
			<DialogTitle>Evaluating</DialogTitle>
			<DialogContent className={classes.dialogContent}>
				<CircularProgress />
			</DialogContent>
		</Dialog>
	)
}
