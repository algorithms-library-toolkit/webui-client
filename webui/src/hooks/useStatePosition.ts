import React from 'react'
import { useSelector } from 'react-redux'

import usePosition from './usePosition'
import { ReduxState } from '../reducers'

const useStatePosition = (canvas: React.RefObject<SVGSVGElement>, allowChange: boolean) => {
    const { mouseMode, grid, scale, offset } = useSelector((state: ReduxState) => ({
        mouseMode: state.stateUI.mouseMode,
        grid: state.stateCanvas.gridOn,
        scale: state.stateCanvas.scale,
        offset: state.stateCanvas.offset
    }))

    return usePosition(canvas, allowChange, mouseMode, grid, scale, offset)
}

export default useStatePosition