import * as React from 'react'
import {
    Grid,
    makeStyles,
    Paper,
    Table,
    TableBody,
    TableCell,
    TableContainer,
    TableHead,
    TableRow,
    Typography
} from '@material-ui/core'
import {
    SyncAlt,
    ArrowRightAlt,
} from '@material-ui/icons'
import { parseAutomaton } from 'utils/alt/import/parsers'
import { createTableRows } from 'interfaces/TableRow'
import { EPSILON_CHAR } from 'interfaces/Automaton'

const DELTA = '\u03B4'
export const EPSILON = '\u03B5'

interface OutputTableProps {
    content: string
}

const useStyles = makeStyles({
    table: {
        width: '100%'
    },
    icon: {
        verticalAlign: 'middle',
        display: 'inline-flex'
    },
    out: {
        transform: 'rotate(-180deg)',
        verticalAlign: 'middle',
        display: 'inline-flex'
    },
    transition: {
        fontSize: 18
    }
})

export const OutputTable = (props: OutputTableProps) => {
    const { content } = props
    const classes = useStyles()

    const automaton = parseAutomaton(content)
    const rows = createTableRows(automaton)

    return (
        <TableContainer component={Paper}>
            <Table className={classes.table} size="small">
                <TableHead>
                    <TableRow>
                        <TableCell/>
                        <TableCell align="left">
                            <Typography component="span" variant="h6">{DELTA}</Typography>
                        </TableCell>
                        {automaton.alphabet.map((symbol, index) => (
                            <TableCell key={index} align="center">
                                <Typography component="span" variant="h6">{symbol !== EPSILON_CHAR ? symbol : EPSILON}</Typography>
                            </TableCell>
                        ))}
                    </TableRow>
                </TableHead>
                <TableBody>
                    {rows.map((row, rowIndex) => (
                        <TableRow key={rowIndex}>
                            <TableCell align="center">
                                <Grid container justifyContent="center" wrap="nowrap" direction="row"
                                      alignItems="center">
                                    {row.type === 'inout' &&
                                        <SyncAlt className={classes.icon}/>
                                    }
                                    {row.type === 'in' &&
                                        <ArrowRightAlt className={classes.icon}/>
                                    }
                                    {row.type === 'out' &&
                                        <ArrowRightAlt className={classes.out}/>
                                    }
                                </Grid>
                            </TableCell>
                            <TableCell align="left">
                                <Typography component="span" variant="h6">{row.name}</Typography>
                            </TableCell>
                            {row.transitions.map((transition, transitionIndex) =>
                                    <TableCell key={rowIndex + "|" + transitionIndex} align="center">
                                        <Typography component="span" className={classes.transition}>{transition}</Typography>
                                    </TableCell>
                            )}
                        </TableRow>
                    ))}
                </TableBody>
            </Table>
        </TableContainer>
    )
}

export default OutputTable
