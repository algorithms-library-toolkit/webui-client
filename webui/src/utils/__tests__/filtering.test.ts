import { Algorithm, Overload } from 'interfaces/Algorithms'
import { DocsFilter, NameFilter, ParamFilter, ReturnValueFilter } from 'interfaces/Filter'
import { filterAlgorithms, filterAll, filterCategory } from 'utils/filtering'
import { Map } from 'immutable'

const overloads: Overload[] = [
        {
            params: [
                {
                    name: "grammar",
                    type: "grammar::LeftRG"
                },
                {
                    name: "automaton",
                    type: "automaton"
                }
            ],
            resultType: "grammar::LeftRG",
            docs: "fugiat"
        },
    {
        params: [
            {
                name: "automaton",
                type: "automaton::DFA"
            },
            {
                name: "grammar",
                type: "grammar::LeftRG"
            }
        ],
        resultType: "string",
        docs: "eiusmod"
    },
    {
        params: [
            {
                name: "grammar",
                type: "string"
            },
            {
                name: "automaton",
                type: "string"
            }
        ],
        resultType: "regexp",
        docs: "elit"
    }
]

const overload: Overload[] = [{
    params: [
        {
            name: "automaton",
            type: "automaton"
        }
    ],
    resultType: "grammar::LeftRG",
    docs: "fugiat"
}]

const overload2: Overload[] = [
    {
        params: [
            {
                name: "grammar",
                type: "string"
            },
            {
                name: "automaton",
                type: "string"
            }
        ],
        resultType: "regexp",
        docs: "elit"
    }
]

const overload3: Overload[] =  [
    {
        params: [
            {
                name: "grammar",
                type: "grammar::LeftRG"
            },
            {
                name: "automaton",
                type: "automaton::DFA"
            },
            {
                name: "grammar",
                type: "grammar::LeftRG"
            }
        ],
        resultType: "string",
        docs: "eiusmod"
    }
]

const overload4: Overload[] = [
    {
        params: [
            {
                name: "grammar",
                type: "regexp"
            },
            {
                name: "grammar",
                type: "grammar::LeftRG"
            }
        ],

        resultType: "regexp",
        docs: "dolore"
    }
]

const algorithms: Algorithm[] = [
    {
        name: "togrammar",
        templateParams: [],
        overloads: Map<number, Overload[]>()
            .set(1, overload)
            .set(2, overload2)
            .set(3, overload3),
        docs: "foo"
    },
    {
        name: "determinize",
        templateParams: [],
        overloads: Map<number, Overload[]>()
            .set(2, overload4)
    }
]

test('filter params in overload', () => {
    const filters = []
    filters.push(new ParamFilter())

    expect(filterAll(overloads, filters, 'AUTOMATON')).toEqual(true)
    expect(filterAll(overloads, filters, 'regex')).toEqual(false)
})

test('filter docs in overload', () => {
    const filters = []
    filters.push(new DocsFilter())

    expect(filterAll(overloads, filters, 'fu')).toEqual(true)
    expect(filterAll(overloads, filters, 'string')).toEqual(false)
})

test('filter return value in overload', () => {
    const filters = []
    filters.push(new ReturnValueFilter())

    expect(filterAll(overloads, filters, 'gramm')).toEqual(true)
    expect(filterAll(overloads, filters, 'automaton')).toEqual(false)
})

test('filter name in overload', () => {
    const filters = []
    filters.push(new NameFilter())

    //always returns true for name filter
    expect(filterAll(overloads, filters, 'foo')).toEqual(true)
})

test('filter all in overload', () => {
    const filters = []
    filters.push(new ReturnValueFilter(), new DocsFilter(), new ParamFilter())

    expect(filterAll(overloads, filters, 'string')).toEqual(true)
    expect(filterAll(overloads, filters, 'ei')).toEqual(true)
    expect(filterAll(overloads, filters, 'regex')).toEqual(true)
})

test('filter category by name', () => {
    const category = 'ToGrammar'

    expect(filterCategory(category, 'GRAM')).toEqual(true)
    expect(filterCategory(category, 'automat')).toEqual(false)
})

test('filter algorithms overloads by return value', () => {
    const filters = []
    filters.push(new ReturnValueFilter())

    expect(filterAlgorithms(algorithms, filters, 'grammar')).toEqual(true)
    expect(filterAlgorithms(algorithms, filters, 'foo')).toEqual(false)
})

test('filter algorithms oveloads by name', () => {
    const filters = []
    filters.push(new NameFilter())

    //shouldn't filter name
    expect(filterAlgorithms(algorithms, filters, 'determinize')).toEqual(false)
})

test('filter algorithms overloads by parameter', () => {
    const filters = []
    filters.push(new ParamFilter())

    expect(filterAlgorithms(algorithms, filters, 'automaton::DFA')).toEqual(true)
})