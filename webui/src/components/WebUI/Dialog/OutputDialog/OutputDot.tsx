import React, { useCallback, useRef, memo } from 'react'
import {
	Divider,
	Button,
	ButtonGroup
} from '@material-ui/core'
import { Graphviz } from 'components/WebUI/Dialog/Graphviz'
import { makeStyles } from '@material-ui/styles'
import { useSnackbar } from 'notistack'
import { saveAs } from 'file-saver'

const useStyles = makeStyles({
	buttons: {
		display: 'flex',
		justifyContent: 'flex-end'
	}
})

const MemoizedDotDialogContent = memo(
	({ dotContent, onError }: { dotContent: string, onError: any }) => (
		<Graphviz dot={dotContent} onError={onError}/>
	)
)

interface Props {
	content: string;
}

const OutputDot = (props: Props) => {
	const { content } = props
	const { enqueueSnackbar } = useSnackbar()
	const contentRef = useRef<HTMLDivElement>(null)
	const classes = useStyles()

	const handleDOTDownload = useCallback(() => {
		const blob = new Blob([content], {
			type: 'text/plain'
		})

		saveAs(blob, 'output.gv')
	}, [content])

	const handleSVGDownload = useCallback(() => {
		const svgElement = (
			contentRef.current?.firstChild as HTMLDivElement | undefined
		)?.firstChild as SVGSVGElement | undefined

		if (!svgElement || svgElement.nodeName !== 'svg')
			return enqueueSnackbar('DOT SVG output has not been rendered', {
				variant: 'error'
			})

		const blob = new Blob([svgElement.outerHTML], {
			type: 'image/svg+xml'
		})

		saveAs(blob, 'output.svg')
	}, [contentRef, enqueueSnackbar])

    const onGraphvizRenderError = (errorMsg: string) => {
      console.error("Graphviz render error:", errorMsg)
      enqueueSnackbar("Graphviz image could not be rendered.", { variant: 'error' })
    }
    const onGraphvizRenderErrorMemo = useCallback(onGraphvizRenderError, []); // https://stackoverflow.com/questions/54368822/react-memo-components-and-re-render-when-passing-function-as-props

	return (
		<div>
			<div ref={contentRef}>
				<MemoizedDotDialogContent dotContent={content} onError={onGraphvizRenderErrorMemo} />
			</div>
			<Divider />
			<ButtonGroup className={classes.buttons} variant="text" color="primary" aria-label="text primary button group">
				<Button color="primary" aria-controls="saveMenu" aria-haspopup="true" onClick={handleDOTDownload}>Download DOT</Button>
				<Button color="primary" aria-controls="saveMenu" aria-haspopup="true" onClick={handleSVGDownload}>Download SVG</Button>
			</ButtonGroup>
		</div>
	)
}

export default OutputDot
