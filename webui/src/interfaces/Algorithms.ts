import { Map } from 'immutable'

export type AlibType = string
export type AlibTemplateType = string

export interface AlgorithmParam {
  name: string
  type: AlibType
}

export interface Overload {
  params: AlgorithmParam[]
  resultType: AlibType,
  docs?: string
}

export interface AlgorithmALT {
  name: string
  templateParams: AlibTemplateType[]
  overloads: Overload[]
  docs?: string
}

export interface Algorithm {
  name: string
  templateParams: AlibTemplateType[]
  //categorized overloads by number of parameters
  overloads: Map<number, Overload[]>
  docs?: string
}

export interface AlgorithmCategory {
	categoryName: string
	subcategories: Map<string, AlgorithmCategory>
	algorithms: Algorithm[]
	tags: string[]
}
