import * as React from 'react'
import { batch, useDispatch, useSelector } from 'react-redux'
import useDrag from '../../../hooks/useDrag'
import useAlgorithmPosition from '../../../hooks/useAlgorithmPosition'
import { Actions, ReduxState } from '../../../reducers'
import Node from './Node'
import { add, subtract, normalize } from '../../../interfaces/Position'
import { cursorPositionOnCanvas } from '../../../utils/positioning'
import AlgorithmNode, {
	isOutputNode,
	isInputNode,
	isCustomFunction
} from 'interfaces/AlgorithmNode'
import StateData, { toStateData } from 'interfaces/StateData'
import { parseAutomaton } from 'utils/alt/import/parsers'
import { getNodeLabelText } from 'utils/textSize'
import useNodeSize from 'hooks/useNodeSize'
import { OUTPUT_HEADER, VERBOSE_HEADER } from 'components/WebUI/Dialog/OutputDialog/OutputDialog'

const {
	positionNode,
	setBuildNode,
	setPositioningNode,
	selectNode,
	setMouseDownOffset,
	setStatemakerOpen,
	set,
	position: positionAutomaton,
	openOutputDialog,
	openInputDialog,
	openCustomFunctionDialog,
	resizeNode,
	updateGhostNode
} = Actions

interface Props {
	/** State id used to get state data */
	id: string
	canvasRef: React.RefObject<SVGSVGElement>
}

/**
 * Selects attributes from state
 *
 * @param {string} id Selected node's id
 */
const nodeSelector = (id: string) => (state: ReduxState) => {
	let hasTransitionSelected = false

	// If there's selected transition
	if (!!state.stateCanvas.selectedEdge) {
		const t =
			state.stateData.present.transitions[state.stateCanvas.selectedEdge]
		// Mark if selected tansition leads from or to this state
		if (t) {
			hasTransitionSelected = t.startState === id || t.endState === id
		}
	}

	return {
		data: state.algorithmData.present.nodes.get(id, {
			id: '',
			height: 0,
			width: 0,
			name: '',
			params: [],
			templateParams: [],
			position: { x: 0, y: 0 },
			resultType: '',
			nodeType: 'algorithm'
		} as AlgorithmNode),
		isSelected: id === state.algorithmCanvas.selectedNode,
		hasTransitionSelected,
		buildEdge: state.algorithmCanvas.buildEdge,
		cursorMode: state.algorithmCanvas.cursorMode,
		positioningNode: state.algorithmCanvas.positioningNode,
		offset: state.algorithmCanvas.offset,
		scale: state.algorithmCanvas.scale,
		mouseDownOffset: state.algorithmCanvas.mouseDownOffset,
		outputValue: state.algorithmData.present.outputValues.get(id),
		gridOn: state.algorithmCanvas.gridOn
	}
}

/**
 * Represents state machine's individual state
 * It can be dragged around and be selected
 *
 * @param {Props} props
 * @returns
 */
const NodeContainer = (props: Props) => {
	const { id, canvasRef } = props
	const dispatch = useDispatch()
	const [isDragging, setDragging] = useDrag(canvasRef)
	// Indicates mouse moving in drag mode
	const [isMoving, setMoving] = React.useState(false)

	// Binds thi state to update action
	const handleUpdate = React.useCallback(
		(width: number, height: number) => dispatch(resizeNode(id, width, height)),
		[dispatch, id]
	)
	const {
		data,
		isSelected,
		hasTransitionSelected,
		buildEdge,
		cursorMode,
		positioningNode,
		offset,
		scale,
		mouseDownOffset,
		outputValue,
		gridOn
	} = useSelector(nodeSelector(id))

	// When moving around in drag mode, use local position
	const isDraggingAndMoving = isDragging && isMoving
	const position = useAlgorithmPosition(canvasRef, isDraggingAndMoving)

	const text = getNodeLabelText(data.name, data.templateParams)

	// Ref to state name
	const textRef = useNodeSize(text, handleUpdate, data.params.length, false)

	// Dragging state
	const handleMouseMove = React.useCallback(
		(e: React.MouseEvent<SVGRectElement>) => {
			e.preventDefault()
			// Allow only in select mode
			if (
				cursorMode !== 'select' ||
				!isDragging ||
				id === positioningNode
			)
				return

			setMoving(true)
			batch(() => {
				dispatch(setPositioningNode(id))
				dispatch(selectNode(id))
			})
		},
		[dispatch, id, cursorMode, isDragging, positioningNode]
	)

	// Drag on hold
	const handleMouseDown = React.useCallback(
		(e: React.MouseEvent<SVGRectElement>) => {
			e.preventDefault()
			// Allow only in select mode or only if the node is not already being moved
			if (cursorMode !== 'select' || isDraggingAndMoving) return
			e.nativeEvent.stopImmediatePropagation()
			// Signal drag if transition isn't being manipulated
			if (buildEdge) return
			setDragging(true)

			// callback is called after the DOM has been rendered, thus canvasRef.current is not undefined
			const cursorPosition = cursorPositionOnCanvas(
				canvasRef.current!,
				e.clientX,
				e.clientY
			)

			dispatch(
				setMouseDownOffset(
					subtract(
						data.position,
						normalize(cursorPosition, offset, scale)
					)
				)
			)
		},
		[
			dispatch,
			setDragging,
			cursorMode,
			buildEdge,
			canvasRef,
			data.position,
			scale,
			offset,
			isDraggingAndMoving
		]
	)

	// Handle mouse release on node
	const handleMouseUp = React.useCallback(
		(e: React.MouseEvent<SVGRectElement>) => {
			e.preventDefault()
			// Do nothing if not in select mode
			if (cursorMode !== 'select') return

			setDragging(false)
			// If transition is being built
			if (isMoving) {
				// Node is in drag mode
				// Stop dragging and set position
				setMoving(false)
				batch(() => {
					dispatch(setPositioningNode(''))
					dispatch(
						positionNode(
							id,
							add(
								position,
								gridOn ? { x: 0, y: 0 } : mouseDownOffset
							)
						)
					)
				})
			}
			// Select node
			dispatch(selectNode(id))
		},
		[
			dispatch,
			id,
			cursorMode,
			isMoving,
			setDragging,
			position,
			mouseDownOffset,
			gridOn
		]
	)

	const handleEdit = React.useCallback(
		() =>
			batch(() => {
				dispatch(selectNode(id))

				switch (data.nodeType) {
					case 'inputAutomaton':
						dispatch(set(data.value as StateData))
						dispatch(setStatemakerOpen(true, true))
						break
					case 'inputString':
					case 'inputGrammar':
					case 'inputInt':
					case 'inputDouble':
					case 'inputBoolean':
					case 'inputStringAutomaton':
					case 'inputStringGrammar':
					case 'inputStringObject':
					case 'inputStringRegexp':
					case 'inputStringRte':
					case 'inputStringString':
					case 'inputStringTree':
						dispatch(openInputDialog(id, data.nodeType, data.value))
						break
					case "customFunction":
						dispatch(openCustomFunctionDialog(id, data.value))
				}
			}),
		[dispatch, id, data.nodeType, data.value]
	)

	const handleView = React.useCallback(() => {
		if (!outputValue) return

		switch (data.nodeType) {
			case 'outputAutomaton': {
				batch(() => {
					if (outputValue?.result?.pretty) {
						dispatch(set(toStateData(parseAutomaton(outputValue?.result?.pretty))))
						dispatch(positionAutomaton())
						dispatch(setStatemakerOpen(true, false))
					}
				})
				break
			}
			case 'algorithm':
			case 'output':
				if (outputValue !== undefined) {
					dispatch(openOutputDialog(data.nodeType === 'output' ? OUTPUT_HEADER : VERBOSE_HEADER, outputValue, data.nodeType))
				}
				break
		}
	}, [dispatch, data.nodeType, outputValue])

	const handleCopy = React.useCallback(() =>
			batch(() => {
				dispatch(
					updateGhostNode({
						nodeType: data.nodeType,
						name: data.name,
						position: data.position,
						height: data.height,
						width: data.width,
						params: data.params,
						resultType: data.resultType,
						value: data.value
					})
				)
				dispatch(setBuildNode(true))
			}),
		[dispatch, data]
	)

	return (
		<Node
			ref={textRef}
			data={{
				...data,
				position:
					isDraggingAndMoving
						? gridOn ? position : add(position, mouseDownOffset)
						: data.position
			}}
			isSelected={isSelected}
			hasTransitionSelected={hasTransitionSelected}
			showEditButton={isInputNode(data.nodeType) || isCustomFunction(data.nodeType)}
			showCopyButton={isCustomFunction(data.nodeType)}
			hasValue={!!(outputValue?.result)}
			hasOutput={!isOutputNode(data.nodeType)}
			onMouseDown={handleMouseDown}
			onMouseUp={handleMouseUp}
			onMouseMove={handleMouseMove}
			onEdit={handleEdit}
			onCopy={handleCopy}
			onView={handleView}
		/>
	)
}

export default NodeContainer
