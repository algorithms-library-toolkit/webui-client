import React from 'react'
import InputDialog from 'components/WebUI/Dialog/InputDialog/InputDialog'
import StatemakerDialog from '../../Statemaker/StatemakerDialog'
import { EvaluatingDialog } from '../Dialog/EvaluatingDialog'
import { ShareDialog } from '../Dialog/ShareDialog'
import { Fab, makeStyles } from '@material-ui/core'
import { useEvaluate } from '../../../hooks/useEvaluate'
import { AlgorithmDrawer } from './AlgorithmDrawer'
import { WebUITheme } from '../../../interfaces/Theme'
import { AlgorithmCanvasToolbar } from './AlgorithmCanvasToolbar'
import { OutputDialog } from 'components/WebUI/Dialog/OutputDialog/OutputDialog'
import { AlgorithmHints } from '../AlgorithmHints'
import CustomFunctionDialog from 'components/WebUI/Dialog/CustomFunctionDialog'

const useStyles = makeStyles<WebUITheme>((theme) => ({
	root: {
		width: '0px',
	},
	fab: {
		position: 'absolute',
		bottom: theme.spacing(2),
		right: theme.spacing(2),
		'@media (max-height: 640px)': {
			right: theme.spacing(2) + 40
		}
	},
}))

const AlgorithmToolbar = () => {
	const classes = useStyles()
	const evaluate = useEvaluate()

	return (
		<div className={classes.root}>
			<AlgorithmDrawer />
			<Fab
				className={classes.fab}
				color="primary"
				variant="extended"
				onClick={evaluate}
			>
				Evaluate
			</Fab>
			<ShareDialog />
			<InputDialog />
			<CustomFunctionDialog />
			<OutputDialog />
			<StatemakerDialog />
			<EvaluatingDialog />
			<AlgorithmCanvasToolbar />
			<AlgorithmHints />
		</div>
	)
}

export default AlgorithmToolbar
