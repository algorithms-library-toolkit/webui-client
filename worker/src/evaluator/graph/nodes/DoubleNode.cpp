#include <string>
#include "DoubleNode.hpp"

DoubleNode::DoubleNode(const std::string& id, double value, const ext::vector<std::string>& templateParams)
    : InputNode(id, value, templateParams)
{
}

DoubleNode::DoubleNode(const std::string& id, const Json::Value& json)
    : InputNode(id, json["value"].asDouble(), {})
{
}
