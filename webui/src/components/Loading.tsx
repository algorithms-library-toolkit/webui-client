import React from 'react'
import { makeStyles, Backdrop, CircularProgress } from '@material-ui/core'
import { WebUITheme } from 'interfaces/Theme'

const useStyles = makeStyles<WebUITheme>((theme) => ({
    backdrop: {
		zIndex: theme.zIndex.drawer + 1,
		color: '#fff',
    },
}))

interface Props {
	open: boolean
}

export const Loading = (props: Props) => {
	const classes = useStyles()

	return (
		<Backdrop className={classes.backdrop} open={props.open}>
			<CircularProgress color="inherit" />
		</Backdrop>
	)
}
