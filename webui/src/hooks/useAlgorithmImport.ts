import { useCallback } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { ReduxState } from 'reducers'
import { useSnackbar } from 'notistack'
import { algorithmDataActions } from '../reducers/algorithmData'
import {
	AlgorithmGraph,
	algorithmGraphFromJSON
} from '../interfaces/AlgorithmGraph'
import { upload } from '../utils/upload'

const { importGraph } = algorithmDataActions

export const useAlgorithmImport = () => {
	const dispatch = useDispatch()
	const { enqueueSnackbar } = useSnackbar()
	const { castData } = useSelector((state: ReduxState) => { return { castData: state.workerDefinitions.casts }})

	return useCallback(async () => {
		const { filename, content } = await upload()

		let algorithmData: AlgorithmGraph
		try {
			algorithmData = algorithmGraphFromJSON(content)
		} catch (e: any) {
			enqueueSnackbar(e.message ?? "Couldn't parse JSON", {
				variant: 'error'
			})
			return
		}

		enqueueSnackbar(`${filename} imported`, {
			variant: 'info'
		})

		dispatch(importGraph(algorithmData, castData))
	}, [dispatch, enqueueSnackbar, castData])
}
