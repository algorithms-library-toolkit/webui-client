export interface StringNodeOutput {
	plain?: string,
	pretty?: string,
	dot?: string,
	latex?: string,
}

export interface NodeOutputs {
	type?: string,
	result?: StringNodeOutput,
}
