export const isString = (val: any): val is string => typeof val === 'string'
export const isNumber = (val: any): val is number => typeof val === 'number'
export const isObject = (val: any): val is { [key: string]: any } =>
  val !== null && typeof val === 'object' && !Array.isArray(val)

