export class EvaluateResponseError extends Error {
	errorStack: string[];

    constructor(m: string[]) {
        super(m[0]);
		Object.setPrototypeOf(this, EvaluateResponseError.prototype);

		this.errorStack = m;
    }
}
