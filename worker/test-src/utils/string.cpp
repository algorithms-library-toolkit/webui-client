#include "testing.hpp"
#include "utils/string.hpp"

TEST_CASE("string")
{
    REQUIRE(utils::trim("bash") == "bash");
    REQUIRE(utils::trim("  bash") == "bash");
    REQUIRE(utils::trim("bash  ") == "bash");
    REQUIRE(utils::trim("bash\r\n") == "bash");
    REQUIRE(utils::trim("  bash  ") == "bash");
    REQUIRE(utils::trim("  b    ash  ") == "b    ash");
    REQUIRE(utils::trim("\nb \t   ash\n ") == "b \t   ash");
    REQUIRE(utils::trim("\tb    ash  ") == "b    ash");
}
