import { useEffect } from 'react'
import { useSnackbar } from 'notistack'
import { useSelector } from 'react-redux'
import { ReduxState } from '../../reducers'

export const AlgorithmErrorNotifier = () => {
	const { enqueueSnackbar } = useSnackbar()
	const { errors } = useSelector((state: ReduxState) => ({
		errors: state.algorithmData.present.errors
	}))

	useEffect(() => {
		errors.forEach((error) =>
			enqueueSnackbar(error, {
				variant: 'error'
			})
		)
	}, [errors, enqueueSnackbar])

	return null
}
