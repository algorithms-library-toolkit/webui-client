#include <cms/Connection.h>
#include <cms/ConnectionFactory.h>
#include <csignal>
#include <memory>
#include <utility>
#include "worker/MessageProcessor.hpp"

volatile sig_atomic_t g_exit = 0;

MessageProcessor::MessageProcessor(std::string brokerURL, const std::string& requestQueueName, const std::string& responseQueueName, message_processor_t workerCb)
    : m_brokerURL(std::move(brokerURL))
    , m_connection(cms::ConnectionFactory::createCMSConnectionFactory(m_brokerURL)->createConnection())
    // each request message needs to be manually acknowledged once it's processed, this is needed so the worker can only receive at most one message at any given moment
    , m_requestSession(m_connection->createSession(cms::Session::CLIENT_ACKNOWLEDGE))
    , m_responseSession(m_connection->createSession(cms::Session::AUTO_ACKNOWLEDGE))
    // prefetchSize=1 is need so the worker can only receive one unacknowledged message at a time
    , m_requestQueue(m_requestSession->createQueue(requestQueueName + "?consumer.prefetchSize=1"))
    , m_responseQueue(m_responseSession->createQueue(responseQueueName))
    , m_requestConsumer(m_requestSession->createConsumer(m_requestQueue.get()))
    , m_responseProducer(m_requestSession->createProducer(m_responseQueue.get()))
    , m_workerCb(std::move(workerCb))
{
}

MessageProcessor::~MessageProcessor()
{
    m_connection->close();
}

void MessageProcessor::listen()
{
    m_connection->start();

    // Install sighandler for SIGTERM
    struct sigaction sigact;
    memset(&sigact, 0, sizeof(sigact));
    sigact.sa_handler = [](int) { g_exit = 1; };
    sigact.sa_flags = SA_SIGINFO;
    sigaction(SIGTERM, &sigact, nullptr);

    while (true) {
        std::unique_ptr<const cms::TextMessage> message;

        {
            cms::Message* msg = m_requestConsumer->receive(MESSAGE_RECEIVE_TIMEOUT_MS);
            if (msg == nullptr && g_exit == 1) {
                break;
            } else if (msg == nullptr) {
                continue;
            }

            message.reset(dynamic_cast<const cms::TextMessage*>(msg));
        }

        spdlog::debug("Received message at queue '{}'", m_requestQueue->getQueueName());
        if (message.get() == nullptr) {
            spdlog::warn("Not a cms::TextMessage");
            message->acknowledge();
            continue;
        }

        auto taskId = message->getLongProperty("TaskID");
        auto taskType = m_requestQueue->getQueueName();

        spdlog::info("{}: Decoded task {}", taskId, taskType);
        spdlog::trace("{}: {}", taskId, message->getText());

        auto reply = m_workerCb(taskId, message->getText());

        std::unique_ptr<cms::TextMessage> replyMessage(m_responseSession->createTextMessage(reply));
        replyMessage->setLongProperty("TaskID", taskId);
        m_responseProducer->send(replyMessage.get());

        message->acknowledge();
        spdlog::debug("{}: Reply sent ({} bytes)", taskId, reply.size());
        spdlog::info("{}: Acknowledged", taskId);
    }
}
