# Find Algorithms Library Toolkit
#
# Cache Variables: (not for direct use in CMakeLists.txt)
#  ALT_ROOT
#  ALT_LIBRARY
#  ALT_INCLUDE_DIR
#  ALT_a_LIBRARY
#  ALT_a_INCLUDE_DIR
#  ALT_b_LIBRARY
#  ALT_b_INCLUDE_DIR
#  ALT_c_LIBRARY
#  ALT_c_INCLUDE_DIR
#

# Use this module this way:
#  find_package(ALT)
#  find_package(ALT 1.0.0)
#  find_package(ALT COMPONENTS ALGO OPTIONAL_COMPONENTS ALGO_EXPERIMENTAL)
#  ...
#  include_directories(${ALT_INCLUDE_DIR})
#  target_link_libraries(myapp ${ALT_LIBRARIES})

# ALT libraries are components. For example, libalib2algo.so is "algo" component.

# Requires these CMake modules:
#  FindPackageHandleStandardArgs (CMake standard module)
#
# Original Author:
# 2020 Tomas Pecka <tomas.pecka@fit.cvut.cz>
# https://alt.pecka.me

set(_libraries
	xml
	elgo
	graph_algo
	str_cli_integration
	str
	algo_experimental
	std
	measure
	gui
	algo
	common
	data
	raw_cli_integration
	graph_data
	abstraction
	aux
	data_experimental
	dummy
	raw
	cli
	)

set(ALT_ROOT "${ALT_ROOT}" CACHE PATH "ALT install prefix")
# find includes
find_path(ALT_INCLUDE_DIR NAMES version.hpp PATHS "${ALT_ROOT}" PATH_SUFFIXES include/algorithms-library)

# find libs
set(ALT_LIBRARIES "")
foreach(lib ${_libraries})

	# if ALT_ROOT specified, don't look in DEFAULT paths...
	if(ALT_ROOT)
		find_library(ALT_${lib}_FOUND NAMES alib2${lib} PATHS "${ALT_ROOT}" PATH_SUFFIXES lib NO_DEFAULT_PATH)
	else()
		find_library(ALT_${lib}_FOUND NAMES alib2${lib} PATHS "${ALT_ROOT}" PATH_SUFFIXES lib)
	endif()

	if(ALT_${lib}_FOUND)
		set(ALT_LIBRARIES ${ALT_LIBRARIES} ${ALT_${lib}_FOUND})

		string(TOUPPER ${lib} _lib_upper)
		set(ALT_${_lib_upper}_LIBRARY ${ALT_${lib}_FOUND})
		mark_as_advanced(ALT_${_lib_upper}_LIBRARY)
	endif()
endforeach()

# find version
if(ALT_INCLUDE_DIR)
    # Read and parse version header file for version number
	file(STRINGS "${ALT_INCLUDE_DIR}/version.hpp" _alt_version_hpp REGEX "#define ALIB_VERSION[ ]*\"([.0-9]+)\"")
	string(REGEX MATCH "([\\.0-9]+)" ALT_VERSION "${_alt_version_hpp}")

	# report
	# message("Found ALT package. Version " ${ALT_VERSION})
endif()

# handle
include(FindPackageHandleStandardArgs)
find_package_handle_standard_args(ALT
	VERSION_VAR ${ALT_VERSION}
	REQUIRED_VARS ALT_INCLUDE_DIR ALT_LIBRARIES
	HANDLE_COMPONENTS
)

mark_as_advanced(ALT_INCLUDE_DIR)
mark_as_advanced(ALT_LIBRARIES)

if(ALT_FOUND)
	mark_as_advanced(ALT_ROOT)
endif()
