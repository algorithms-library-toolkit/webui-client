#include <boost/algorithm/string/join.hpp>
#include <boost/asio.hpp>
#include <boost/asio/error.hpp>
#include <boost/date_time/posix_time/posix_time_types.hpp>
#include <boost/process.hpp>
#include <boost/process/extend.hpp>
#include <boost/system/detail/error_code.hpp>
#include "utils/log.hpp"
#include "utils/process.hpp"

#define BOOST_DATE_TIME_POSIX_TIME_STD_CONFIG

namespace utils {

std::string execAndWait(const std::string& absolutePath, std::initializer_list<std::string> args, std::optional<std::string_view> std_in, const std::chrono::microseconds& timeoutUs)
{
    namespace bp = boost::process;

    boost::asio::io_context io;
    bp::async_pipe stdinPipe(io);
    boost::asio::deadline_timer deadline(io);
    boost::process::group group;
    bool timeouted = false;
    std::future<std::string> stdoutFuture;
    std::future<std::string> stderrFuture;

    spdlog::trace("exec: {} {}", absolutePath, boost::algorithm::join(args, " "));

    spdlog::trace(" - {}: setting timeout {} us", absolutePath, timeoutUs.count());
    deadline.expires_from_now(boost::posix_time::microseconds(timeoutUs.count()));
    deadline.async_wait([&](const boost::system::error_code& error) {
        if (error == boost::asio::error::operation_aborted) {
            spdlog::trace(" - {}: timeout timer was cancelled", absolutePath);
            return;
        }

        spdlog::trace(" - {}: timeout", absolutePath);
        group.terminate(); // TODO: catch possible exception
        deadline.expires_at(boost::posix_time::pos_infin);
        timeouted = true;
    });

    bp::child c(
        absolutePath,
        boost::process::args = args,
        bp::std_out > stdoutFuture,
        bp::std_err > stderrFuture,
        bp::std_in < stdinPipe,
        io,
        group,
        bp::on_exit([&](int e, std::error_code ec) {
            spdlog::trace(" - {}: exited", absolutePath);
            deadline.cancel();
        }));

    if (std_in) {
        stdinPipe.write_some(bp::buffer(std_in->data(), std_in->size()));
    }
    stdinPipe.close();

    io.run();
    c.wait();

    if (timeouted) {
        throw ExecTimeoutError(absolutePath + " timeouted");
    }

    if (auto nativeExitCode = c.native_exit_code(); WIFSIGNALED(nativeExitCode)) {
        const auto signo = WTERMSIG(nativeExitCode);
        spdlog::critical(" - {}: signalled with {} ({})", absolutePath, signo, strsignal(signo));
        throw ExecReturnCodeError(absolutePath + " signalled with " + std::to_string(signo) + " (" + strsignal(signo) + ")");
    }

    if (c.exit_code()) {
        spdlog::critical(" - {}: ended with a non-zero exit code {}", absolutePath, c.exit_code());
        spdlog::critical(" - {}:  stdout: {}", absolutePath, stdoutFuture.get());
        spdlog::critical(" - {}:  stderr: {}", absolutePath, stderrFuture.get());

        throw ExecReturnCodeError(absolutePath + " returned non-zero exit code " + std::to_string(c.exit_code()));
    }

    return stdoutFuture.get();
}
}
