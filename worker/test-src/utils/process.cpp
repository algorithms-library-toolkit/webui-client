#include "testing.hpp"
#include <catch2/catch.hpp>
#include <chrono>
#include "test_vars.hpp"
#include "utils/log.hpp"
#include "utils/process.hpp"

using namespace std::chrono_literals;

#define TIMEIT(CODE)                                            \
    [] {                                                        \
        auto start = std::chrono::high_resolution_clock::now(); \
        CODE();                                                 \
        auto end = std::chrono::high_resolution_clock::now();   \
        return end - start;                                     \
    }()

static const auto reserve = 2s;

TEST_CASE("process")
{
    spdlog::set_level(spdlog::level::trace);

    REQUIRE(utils::execAndWait("/bin/bash", {"-c", "echo -n 123"}, std::nullopt, 1s) == "123");
    REQUIRE(utils::execAndWait("/bin/bash", {"-c", "cat"}, "123", 1s) == "123");
    REQUIRE(utils::execAndWait("/bin/cat", {}, "123", 1s) == "123");
    REQUIRE_THROWS_MATCHES(utils::execAndWait("/bin/bash", {"-c", "exit 42"}, std::nullopt, 1s), utils::ExecReturnCodeError, Catch::Matchers::Message("/bin/bash returned non-zero exit code 42"));

    utils::execAndWait("/bin/sleep", {"0.2"}, std::nullopt, 300ms);
    REQUIRE(TIMEIT([]() { REQUIRE_THROWS_MATCHES(utils::execAndWait("/bin/sleep", {"50"}, std::nullopt, 200ms), utils::ExecTimeoutError, Catch::Matchers::Message("/bin/sleep timeouted")); }) < 200ms + reserve);
    REQUIRE(TIMEIT([]() { REQUIRE_THROWS_MATCHES(utils::execAndWait("/bin/cat", {"/dev/zero"}, std::nullopt, 333ms), utils::ExecTimeoutError, Catch::Matchers::Message("/bin/cat timeouted")); }) < 333ms + reserve);

    REQUIRE_THROWS_MATCHES(utils::execAndWait(CMAKE_CURRENT_BINARY_DIR "/signal", {}, std::nullopt, 1s), utils::ExecReturnCodeError, Catch::Matchers::Message(CMAKE_CURRENT_BINARY_DIR "/signal signalled with 6 (Aborted)"));
    REQUIRE_THROWS_MATCHES(utils::execAndWait(CMAKE_CURRENT_BINARY_DIR "/signal", {"9"}, std::nullopt, 1s), utils::ExecReturnCodeError, Catch::Matchers::Message(CMAKE_CURRENT_BINARY_DIR "/signal signalled with 9 (Killed)"));
    // TODO: I don't know how to test SIGSEGV because when instrumented with asan the segfault is catched by it and the process exits cleanly
}
