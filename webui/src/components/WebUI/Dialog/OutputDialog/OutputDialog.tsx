import React, { useCallback, useMemo } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import {
	Dialog,
	DialogContent,
	DialogTitle,
	IconButton,
	makeStyles,
	Typography,
	Divider
} from '@material-ui/core'
import CloseIcon from '@material-ui/icons/Close'
import { ReduxState } from 'reducers'
import { algorithmUIActions } from 'reducers/algorithmUI'
import OutputTable from 'components/WebUI/Dialog/OutputDialog/OutputTable'
import OutputGrammar from 'components/WebUI/Dialog/OutputDialog/OutputGrammar'
import OutputTabs from 'components/WebUI/Dialog/OutputDialog/OutputTabs'
import OutputStrings from 'components/WebUI/Dialog/OutputDialog/OutputStrings'
import OutputDot from 'components/WebUI/Dialog/OutputDialog/OutputDot'
import OutputLatex from 'components/WebUI/Dialog/OutputDialog/OutputLatex'
import * as typeConstraints from 'utils/alt/typeConstraints'

const { setOutputDialogOpen } = algorithmUIActions

export const OUTPUT_HEADER = 'Output'
export const VERBOSE_HEADER = 'Algorithm verbose output'

const useStyles = makeStyles({
	dialogPaper: {
		overflow: 'visible'
	},
	dialogTitle: {
		display: 'flex',
		justifyContent: 'space-between',
		alignItems: 'center',
		padding: '4px 4px 0px 24px',
	},
	textField: {
		minWidth: '500px',
		width: '100%',
		height: '100%',
	},
})

export const OutputDialog = () => {
	const dispatch = useDispatch()
	const classes = useStyles()
	const dialogClasses = useMemo(
		() => ({
			paper: classes.dialogPaper
		}),
		[classes.dialogPaper]
	)

	const { open, content, header, nodeType } = useSelector((state: ReduxState) => state.algorithmUI.outputDialog)

	const handleClose = useCallback(
		() => dispatch(setOutputDialogOpen(false)),
		[dispatch]
	)

	const handleKeyDown = useCallback(
		(e: React.KeyboardEvent) => e.stopPropagation(),
		[]
	)

	let dialogContent: React.ReactNode;
	switch(nodeType) {
	case 'algorithm': // verbose output
		dialogContent = (
			<OutputStrings
				pretty={content?.result?.pretty}
				plain={content?.result?.plain}
				verbose
			/> )
		break;

	case 'output': // output node
		let tabs: any[] = [];

		if (content?.type && typeConstraints.isGrammar(content?.type)) {
			tabs.push({ label: "Grammar", component: (<OutputGrammar value={content?.result?.pretty!}/>) })
		}

		if (content?.type && typeConstraints.isFiniteAutomaton(content?.type)) {
			tabs.push({ label: "Table", component: (<OutputTable content={content?.result?.pretty!}/>) })
		}

		if (content?.result?.pretty || content?.result?.plain) {
			tabs.push({ label: "Text", component: (<OutputStrings pretty={content?.result?.pretty} plain={content?.result?.plain} />) })
		}

		if (content?.result?.dot) {
			tabs.push({ label: "Graphviz", component: (<OutputDot content={content?.result?.dot!} />) })
		}

		if (content?.result?.latex) {
			tabs.push({ label: "LaTeX", component: (<OutputLatex content={content?.result?.latex} />) })
		}

		dialogContent = ( <OutputTabs tabs={tabs} /> )
		break;

	default:
		console.error(`Opening OutputDialog on node of type ${nodeType}`)
		dialogContent = ( <div /> )
	}

	return (
		<Dialog
			classes={dialogClasses}
			scroll="body"
			open={open}
			onClose={handleClose}
			onKeyDown={handleKeyDown}
			maxWidth="lg"
		>
			<DialogTitle disableTypography className={classes.dialogTitle}>
				<Typography component="span" variant="h6">{header}</Typography>
				<IconButton onClick={handleClose}>
					<CloseIcon />
				</IconButton>
			</DialogTitle>
			<Divider />
			<DialogContent>
				{dialogContent}
			</DialogContent>
		</Dialog>
	)
}
