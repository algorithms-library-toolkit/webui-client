import React, { useCallback } from 'react'
import { useDispatch, useSelector, batch } from 'react-redux'

import ParamNode from './ParamNode'
import { AlibType } from '../../../interfaces/Algorithms'
import { Actions, ReduxState } from '../../../reducers'

const { updateGhostEdge, setBuildEdge, addEdge, deleteEdge } = Actions

export interface ParamNodeContainerProps {
	nodeId: string
	paramIndex?: number
	x: number
	y: number
	name?: string
	type: AlibType
}

const ParamNodeContainer = (props: ParamNodeContainerProps) => {
	const { nodeId, paramIndex } = props

	const isOutput = paramIndex === undefined

	const dispatch = useDispatch()
	const { edges, ghostEdge, buildEdge, hasError, cursorMode } = useSelector(
		(state: ReduxState) => {
			const edges = state.algorithmData.present.edges.filter(
				isOutput
					? (edge) => edge.startNodeId === nodeId
					: (edge) =>
							edge.endNodeId === nodeId &&
							edge.endParamIndex === paramIndex
			)

			return {
				edges,
				ghostEdge: state.algorithmUI.ghostEdge,
				buildEdge: state.algorithmCanvas.buildEdge,
				hasError: edges.some((edge) =>
					state.algorithmData.present.incompatibleEdges.some(
						({ id }) => id === edge.id
					)
				),
				cursorMode: state.algorithmCanvas.cursorMode
			}
		}
	)

	const { castData } = useSelector((state : ReduxState) => { return { castData: state.workerDefinitions.casts }})

	const handleMouseDown = useCallback(() => {
		if (buildEdge || cursorMode === 'move') return

		batch(() => {
			dispatch(setBuildEdge(true))

			if (isOutput)
				return dispatch(
					updateGhostEdge({ startNodeId: nodeId, endNodeId: '' })
				)

			edges.forEach((edge) => dispatch(deleteEdge(edge.id)))

			dispatch(
				updateGhostEdge({
					endNodeId: nodeId,
					endParamIndex: paramIndex,
					startNodeId: ''
				})
			)
		})
	}, [dispatch, nodeId, paramIndex, isOutput, buildEdge, cursorMode, edges])

	const handleInputMouseUp = useCallback(
		(e: React.MouseEvent) => {
			e.stopPropagation()

			if (
				// is the same node
				ghostEdge.startNodeId === nodeId ||
				// already has an end
				ghostEdge.endNodeId ||
				edges.some((edge) => ghostEdge.startNodeId === edge.startNodeId)
			)
				return

			batch(() => {
				edges.forEach((edge) => dispatch(deleteEdge(edge.id)))

				dispatch(setBuildEdge(false))
				dispatch(
					addEdge({
						...ghostEdge,
						endNodeId: nodeId,
						endParamIndex: paramIndex!
					}, castData)
				)
			})
		},
		[dispatch, ghostEdge, nodeId, edges, paramIndex, castData]
	)

	const handleOutputMouseUp = useCallback(
		(e: React.MouseEvent) => {
			e.stopPropagation()
			// return if mouseup occured on the same node type
			if (
				ghostEdge.endNodeId === nodeId ||
				// return if the edge already has a startNode selected
				ghostEdge.startNodeId ||
				// return if the edge already exists
				edges.some(
					(edge) =>
						edge.startNodeId === nodeId &&
						edge.endNodeId === ghostEdge.endNodeId &&
						edge.endParamIndex === ghostEdge.endParamIndex
				)
			)
				return

			// stop building edge and create a new edge
			batch(() => {
				dispatch(setBuildEdge(false))
				dispatch(
					addEdge({
						...ghostEdge,
						startNodeId: nodeId,
						endParamIndex: ghostEdge.endParamIndex
					}, castData)
				)
			})
		},
		[dispatch, ghostEdge, nodeId, edges, castData]
	)

	const handleMouseUp = useCallback(
		isOutput ? handleOutputMouseUp : handleInputMouseUp,
		[isOutput, handleOutputMouseUp, handleInputMouseUp]
	)

	return (
		<ParamNode
			{...props}
			onMouseDown={handleMouseDown}
			onMouseUp={handleMouseUp}
			error={hasError}
		/>
	)
}

export default ParamNodeContainer
