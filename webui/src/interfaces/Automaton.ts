import { AutomatonType } from 'interfaces/AutomatonTypes'

type State = string
type Symbol = string

export const EPSILON_CHAR = "#E"
type Epsilon = typeof EPSILON_CHAR
type SymbolOrEpsilon = Symbol | Epsilon

export interface AutomatonTransition {
	symbol: SymbolOrEpsilon
	from: State
	to: State
}

export default interface Automaton {
	type: AutomatonType
	states: State[]
	alphabet: Symbol[]
	transitions: AutomatonTransition[]
	initialStates: State[]
	finalStates: State[]
}

export const emptyAutomaton: Automaton = {
	type: 'DFA',
	states: [],
	alphabet: [],
	transitions: [],
	initialStates: [],
	finalStates: [],
}
