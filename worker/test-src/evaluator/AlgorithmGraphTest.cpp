#include "testing.hpp"
#include <abstraction/ValueHolder.hpp>
#include <forward_list>
#include <memory>
#include <string>
#include <utility>
#include "graph/AlgorithmGraph.hpp"
#include "graph/nodes/AbstractNode.hpp"
#include "graph/nodes/AlgorithmNode.hpp"
#include "graph/nodes/InputNode.hpp"
#include "graph/nodes/OutputNode.hpp"
#include "utils/alt.hpp"

std::map<std::string, std::shared_ptr<AbstractNode>> nodesVecToMap(const std::vector<std::shared_ptr<AbstractNode>>& nodes)
{
    std::map<std::string, std::shared_ptr<AbstractNode>> nodeMap;

    for (const auto& node : nodes)
        nodeMap[node->getId()] = node;

    return nodeMap;
}

class DummyNode : public AbstractNode {
public:
    explicit DummyNode(const std::string& id, bool isInput = false, bool isOutput = false, const std::optional<std::string>& log = std::nullopt)
        : AbstractNode(id, ext::vector<std::string>())
        , input(isInput)
        , output(isOutput)
        , log(log)
    {
    }

    explicit DummyNode(bool isInput = false, bool isOutput = false, const std::optional<std::string>& log = std::nullopt)
        : DummyNode(generateId(), isInput, isOutput, log)
    {
    }

    bool isOutput() const override
    {
        return output;
    }

    bool isInput() const override
    {
        return input;
    }

    void evaluate(abstraction::TemporariesHolder& environment) override
    {
        int res = 1;

        for (const auto& param : params) {
            REQUIRE(param != nullptr);
            res += abstractValueToInt(param);
        }

        std::string strResult = std::to_string(res);
        result = strResult;
        passResultToChilds(std::make_shared<abstraction::ValueHolder<std::string>>(std::move(strResult), false));
    }

    std::optional<std::string> getResult() override
    {
        if (isOutput()) {
            return result;
        } else {
            return log;
        }
    }

private:
    bool input, output;
    std::optional<std::string> log;
    std::string result;

    static std::string generateId()
    {
        static int nextId = 0;
        return std::to_string(nextId++);
    }

    static int abstractValueToInt(const std::shared_ptr<abstraction::Value>& val)
    {
        return stoi(abstractValueToString(val));
    }
};

TEST_CASE("Evaluate empty graph")
{
    auto nodes = std::map<std::string, std::shared_ptr<AbstractNode>>();
    auto result = AlgorithmGraph(nodes).evaluate();
    REQUIRE(result.outputs.empty());
    REQUIRE(result.types.empty());
}

TEST_CASE("Evaluate graph with 1 input and 1 output")
{
    auto nodesVec = std::vector<std::shared_ptr<AbstractNode>> {
        std::make_shared<DummyNode>("0", true, false),
        std::make_shared<DummyNode>("1", false, true)};

    nodesVec[0]->createEdge(nodesVec[1], 0);

    auto nodes = nodesVecToMap(nodesVec);
    auto result = AlgorithmGraph(nodes).evaluate();

    REQUIRE(result.outputs == std::map<std::string, std::string> {{"1", "2"}});
    REQUIRE(result.types.empty());
}

TEST_CASE("Evaluate graph with multiple inputs")
{
    auto nodesVec = std::vector<std::shared_ptr<AbstractNode>> {
        std::make_shared<DummyNode>("0", true, false),
        std::make_shared<DummyNode>("1", true, false),
        std::make_shared<DummyNode>("2", false, true)};

    nodesVec[0]->createEdge(nodesVec[2], 0);
    nodesVec[1]->createEdge(nodesVec[2], 1);

    auto nodes = nodesVecToMap(nodesVec);
    auto result = AlgorithmGraph(nodes).evaluate();

    REQUIRE(result.outputs == std::map<std::string, std::string> {{"2", "3"}});
    REQUIRE(result.types.empty());
}

TEST_CASE("Evaluate graph with multiple outputs")
{
    auto nodesVec = std::vector<std::shared_ptr<AbstractNode>> {
        std::make_shared<DummyNode>("0", true, false),
        std::make_shared<DummyNode>("1", false, true),
        std::make_shared<DummyNode>("2", false, true)};

    nodesVec[0]->createEdge(nodesVec[1], 0);
    nodesVec[0]->createEdge(nodesVec[2], 0);

    auto nodes = nodesVecToMap(nodesVec);
    auto result = AlgorithmGraph(nodes).evaluate();

    REQUIRE(result.outputs == std::map<std::string, std::string> {{"1", "2"}, {"2", "2"}});
    REQUIRE(result.types.size() == 0);
}

TEST_CASE("Evaluate graph with multiple inputs and outputs")
{
    auto nodesVec = std::vector<std::shared_ptr<AbstractNode>> {
        std::make_shared<DummyNode>("0", true, false),
        std::make_shared<DummyNode>("1", false, false),
        std::make_shared<DummyNode>("2", false, true),
        std::make_shared<DummyNode>("3", false, false, "logging something\nmultilined\n"),
        std::make_shared<DummyNode>("4", false, true),
        std::make_shared<DummyNode>("5", true, false)};

    /*
     * 0 ------> 1 -----> 3 ----> 4
     *   \       |                ^
     *    \      v                |
     *     \---> 2                |
     *                            |
     * 5 -------------------------/
     */
    nodesVec[0]->createEdge(nodesVec[1], 0);
    nodesVec[0]->createEdge(nodesVec[2], 0);
    nodesVec[1]->createEdge(nodesVec[2], 1);
    nodesVec[1]->createEdge(nodesVec[3], 0);
    nodesVec[3]->createEdge(nodesVec[4], 0);
    nodesVec[5]->createEdge(nodesVec[4], 1);


    try {
        auto nodes = nodesVecToMap(nodesVec);
        auto result = AlgorithmGraph(nodes).evaluate();
    } catch (std::exception& e) {
        std::cerr << e.what() << std::endl;
    }

    auto nodes = nodesVecToMap(nodesVec);
    auto result = AlgorithmGraph(nodes).evaluate();
    REQUIRE(result.outputs == std::map<std::string, std::string> {{"2", "4"}, {"3", "logging something\nmultilined\n"}, {"4", "5"}});
    REQUIRE(result.types.empty());
}

TEST_CASE("Evaluate automaton")
{
    auto inputNode = std::make_shared<InputNode<std::string>>("1", "NFA 0 1\n>0 0|1 1\n<1 1 1\n", ext::vector<std::string>());
    auto parseNode = std::make_shared<AlgorithmNode>("2", "string::Parse", ext::vector<std::string> {"Automaton"});
    auto algorithmNode = std::make_shared<AlgorithmNode>("3", "automaton::determinize::Determinize", ext::vector<std::string>());
    auto outputNode = std::make_shared<OutputNode>("4");

    inputNode->createEdge(parseNode, 0);
    parseNode->createEdge(algorithmNode, 0);
    algorithmNode->createEdge(outputNode, 0);

    auto nodes = nodesVecToMap({inputNode, parseNode, algorithmNode, outputNode});
    auto result = AlgorithmGraph(nodes).evaluate();

    REQUIRE(result.outputs == std::map<std::string, std::string> {{"4", "(DFA states = {{0}, {0, 1}, {1}} inputAlphabet = {0, 1} initialState = {0} finalStates = {{0, 1}, {1}} transitions = {(({0}, 0), {0, 1}), (({0}, 1), {1}), (({0, 1}, 0), {0, 1}), (({0, 1}, 1), {1}), (({1}, 0), {1}), (({1}, 1), {1})})"}});
    REQUIRE(result.types == std::map<std::string, std::string> {{"2", "automaton::NFA<int, int>"}, {"3", "automaton::DFA<int, ext::set<int>>"}, {"4", "automaton::DFA<int, ext::set<int>>"}});
}
