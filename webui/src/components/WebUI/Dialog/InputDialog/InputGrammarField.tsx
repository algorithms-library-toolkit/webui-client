import React from 'react'
import { makeStyles, Theme } from '@material-ui/core/styles'
import AppBar from '@material-ui/core/AppBar'
import Tabs from '@material-ui/core/Tabs'
import Tab from '@material-ui/core/Tab'
import Box from '@material-ui/core/Box'
import { FormControl, Grid, Icon, IconButton, InputLabel, MenuItem, Select, TextField } from '@material-ui/core'
import RemoveIcon from '@material-ui/icons/HighlightOff'
import AddIcon from '@material-ui/icons/Add'
import {
	ArrowRightAlt
} from '@material-ui/icons'
import { GrammarType, grammarTypes, Rule } from 'interfaces/GrammarData'
import GrammarFormHint from 'components/WebUI/Dialog/InputDialog/GrammarFormHint'
import { a11yProps, TabPanel } from 'components/WebUI/Dialog/TabUtils'
import InfoButton from 'components/WebUI/Dialog/InfoButton'

const useStyles = makeStyles((theme: Theme) => ({
	root: {
		flexGrow: 1,
		padding: '0px 0px 0px 0px',
		width: '1'
	},
	textField: {
		minWidth: '500px',
		width: '100%',
		height: '100%',
	},
	symbolTextField: {
		width: '100%',
		height: '100%',
	},
	leftTextField: {
		width: '15%'
	},
	rightTextField: {
		width: '70%'
	},
	div: {
		display: 'flex',
		alignItems: 'center',
		flexWrap: 'wrap'
	},
	formControl: {
		margin: theme.spacing(1),
		minWidth: 120
	},
	selectEmpty: {
		marginTop: theme.spacing(2)
	}
}))

interface InputParseGrammarFieldProps {
	stringValue: string
	onTextFieldChange: (e: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>) => void
	type: GrammarType
	onTypeChange: (newValue: GrammarType) => void
	fields: Rule[]
	onLeftTextFieldChange: (index: number, value: string) => void
	onRightTextFieldChange: (index: number, value: string) => void
	initialSymbol: string
	onInitSymbolChange: (text: string) => void
	onAdd: () => void
	onDelete: (index: number) => void
	tabValue: number
	onTabChange: (newValue: number) => void
}

const InputGrammarField = (props: InputParseGrammarFieldProps) => {
	const {
		stringValue,
		onTextFieldChange,
		type,
		onTypeChange,
		fields,
		onLeftTextFieldChange,
		onRightTextFieldChange,
		initialSymbol,
		onInitSymbolChange,
		onAdd,
		onDelete,
		tabValue,
		onTabChange
	} = props
	const classes = useStyles()

	const handleTypeChange = React.useCallback((event: React.ChangeEvent<{ value: unknown }>) => {
		onTypeChange(event.target.value as GrammarType)
	}, [])

	const handleTabChange = React.useCallback((event: React.ChangeEvent<{}>, newValue: number) => {
		onTabChange(newValue)
	}, [])

	const handleAdd = React.useCallback(() => {
			onAdd()
		}, []
	)

	const handleInitSymbolTextField = React.useCallback((event: React.ChangeEvent<HTMLInputElement>) => {
			onInitSymbolChange(event.currentTarget.value)
		}, []
	)

	const handleDelete = React.useCallback((index: number) => () => {
			onDelete(index)
		}, []
	)

	const handleLeftInput = React.useCallback((index: number) =>
		(event: React.ChangeEvent<HTMLInputElement>) => {
			onLeftTextFieldChange(index, event.currentTarget.value)
		}, []
	)

	const handleRightInput = React.useCallback((index: number) =>
		(event: React.ChangeEvent<HTMLInputElement>) => {
			onRightTextFieldChange(index, event.currentTarget.value)
		}, []
	)

	return (
		<div className={classes.root}>
			<AppBar position='static'>
				<Tabs
					variant='fullWidth'
					indicatorColor='secondary'
					value={tabValue}
					onChange={handleTabChange}
				>
					<Tab label='String' {...a11yProps(0)} />
					<Tab label='Form' {...a11yProps(1)} />
				</Tabs>
			</AppBar>
			<TabPanel value={tabValue} index={0}>
				<TextField
					className={classes.textField}
					variant='filled'
					multiline
					value={stringValue}
					onChange={onTextFieldChange}
				/>
			</TabPanel>
			<TabPanel value={tabValue} index={1}>
				<form autoComplete='off'>
					<Grid container direction={'column'} spacing={2}>
						<Grid item container direction={'row'} justifyContent='space-between'>
							<Grid item xs={6}>
								<InputLabel id='typeGrammarSelectLabel'>Type of grammar</InputLabel>
								<FormControl>
									<Select
										labelId='typeGrammarSelectLabel'
										value={type}
										onChange={handleTypeChange}
									>
										{grammarTypes.map((name) => (
											<MenuItem value={name}>{name}</MenuItem>
										))}
									</Select>
								</FormControl>
							</Grid>
							<Grid item xs={1}>
								<InfoButton
									content={<GrammarFormHint/>}
								/>
							</Grid>
						</Grid>
						<Grid item container direction={'column'} xs={6}>
							<InputLabel id='initialSymbolLabel'>Initial symbol</InputLabel>
							<TextField
								className={classes.symbolTextField}
								variant='filled'
								color='primary'
								name='initialSymbol'
								size='small'
								value={initialSymbol}
								onChange={handleInitSymbolTextField}
							/>
						</Grid>
					</Grid>
					{
						fields.map((field, index) => (
							<div key={index} className={classes.div}>
								<Box
									display='flex'
									alignItems='center'
									width='1'
								>
									<TextField
										className={classes.leftTextField}
										type='left'
										variant='filled'
										color='primary'
										margin='dense'
										name='leftSide'
										size='small'
										value={field.left}
										onChange={handleLeftInput(index)}
									/>
									<Icon>
										<ArrowRightAlt />
									</Icon>
									<TextField
										className={classes.rightTextField}
										type='right'
										variant='filled'
										color='primary'
										margin='dense'
										name='rightSide'
										size='small'
										value={field.right}
										onChange={handleRightInput(index)}
									/>
									<IconButton
										color='default'
										onClick={handleDelete(index)}
									>
										<RemoveIcon />
									</IconButton>
								</Box>
							</div>
						))
					}
					<Grid container justifyContent='flex-end'>
						<IconButton
							color='default'
							onClick={handleAdd}
						>
							<AddIcon />
						</IconButton>
					</Grid>
				</form>
			</TabPanel>
		</div>
	)
}

export default InputGrammarField
