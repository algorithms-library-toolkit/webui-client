#pragma once
#include <memory>
#include <utility>

class AbstractNode;

class Edge {
public:
    explicit Edge(std::shared_ptr<AbstractNode> node, int paramIndex)
        : node(std::move(node))
        , paramIndex(paramIndex)
    {
    }

    /**
     * @return The destination node
     */
    std::shared_ptr<AbstractNode> getNode() const;

    /**
     * @return The destinations parameter index to which the edge points to
     */
    int getParamIndex() const;

    /**
     * Compares node based on the destionation node and parameter index
     * @param other Edge to compare to
     * @return true if the both edges point to the same destination on the same parameter index, false otherwise
     */
    bool operator<(const Edge& other) const;

private:
    std::shared_ptr<AbstractNode> node;
    const int paramIndex;
};
