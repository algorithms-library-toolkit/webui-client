import React, { useState, useCallback } from 'react'
import { Collapse, List, ListItem, ListItemText } from '@material-ui/core'
import ExpandMoreIcon from '@material-ui/icons/ExpandMore'
import { AlgorithmCategory } from 'interfaces/Algorithms'
import { makeStyles } from '@material-ui/styles'
import { WebUITheme } from '../../../../interfaces/Theme'
import { AlgorithmListItem } from './OverloadsList'
import { nextFilterValue } from 'utils/filtering'
import { Filter } from 'interfaces/Filter'

interface AlgorithmCategoryListProps {
	algorithmCategory: AlgorithmCategory
	filters: Filter[]
	filterValue: string
}

const useStyles = makeStyles<WebUITheme>((theme) => ({
	indent: {
		borderLeft: '2px',
		borderLeftColor: theme.palette.primary.light,
		borderLeftStyle: 'solid'
	},
	nestedList: {
		padding: 'unset',
		paddingLeft: theme.spacing(1)
	},
	expandIcon: {
		transition: 'transform 300ms cubic-bezier(0.4, 0, 0.2, 1)',
		transform: 'rotate(-90deg)',
		'&[data-open=true]': {
			transform: 'rotate(-180deg)'
		}
	}
}))

export const AlgorithmCategoryList = (props: AlgorithmCategoryListProps) => {
	const { algorithmCategory, filters, filterValue } = props
	const classes = useStyles()
	const [open, setOpen] = useState<boolean>(false)

	const handleExpand = useCallback(() => setOpen(!open), [open])

	const subcategoryFilter: string | null = nextFilterValue(filterValue, algorithmCategory, filters)

	if (subcategoryFilter === null)
		return null

	return (
		<div className={classes.indent}>
			<ListItem dense button onClick={handleExpand}>
				<ExpandMoreIcon
					className={classes.expandIcon}
					data-open={open}
				/>
				<ListItemText>{algorithmCategory.categoryName}</ListItemText>
			</ListItem>
			<Collapse in={open} unmountOnExit>
				<List dense className={classes.nestedList}>
					{algorithmCategory.subcategories
						.valueSeq()
						.map((subcategory, index) => (
							<AlgorithmCategoryList
								key={index}
								algorithmCategory={subcategory}
								filters={filters}
								filterValue={subcategoryFilter}
							/>
						))
						.toArray()}
					{algorithmCategory.algorithms.map((algorithm, index) => (
						<AlgorithmListItem
							key={`a${index}`}
							algorithm={algorithm}
							filters={filters}
							filterValue={filterValue}
						/>
					))}
				</List>
			</Collapse>
		</div>
	)
}
