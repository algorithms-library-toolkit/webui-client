import { AlibType, Overload, AlgorithmALT } from 'interfaces/Algorithms'
import { Casts } from 'interfaces/Casts'
import { removeTemplate } from 'utils/alt/removeTemplate'
import { Map, Set } from 'immutable'
import AlgorithmNode, {
	AlgorithmNodeType,
	isInputNode,
	isOutputNode
} from 'interfaces/AlgorithmNode'

export const processCasts = (algorithms: AlgorithmALT[], casts: Casts) => {
	let ret : Casts

	ret = Object.fromEntries(Object.entries(casts).map(([typeFrom, typesTo]) => [
		removeTemplate(typeFrom),
		typesTo.map((typeTo: any) => removeTemplate(typeTo))
	]))

	return ret
}

/**
 * Returns set of available result types
 * @param nodes
 * @param nodeId
 */
export const fromTypes = (nodes:  Map<string, AlgorithmNode>, nodeId: string) => {
	const fromNode = nodes.get(nodeId)

	//node without more overloads
	if (fromNode?.overloads === undefined
		|| isInputNode(fromNode?.nodeType as AlgorithmNodeType)
		|| fromNode?.overloads?.length === 1) {

		if(fromNode?.resultType)
			return Set.of(fromNode?.resultType) as Set<AlibType>
		return Set<AlibType>()
	}

	return fromNode?.overloads?.reduce((acc, overload) => {
		if (overload.resultType)
			return acc.add(overload.resultType)
		return acc
	}, Set<AlibType>())
}

/**
 * Returns set of available parameters on selected index
 * @param nodes
 * @param nodeId
 * @param index
 */
export const toTypes = (nodes:  Map<string, AlgorithmNode>, nodeId: string, index: number) => {
	const toNode = nodes.get(nodeId)

	//node without more overloads
	if (toNode?.overloads === undefined
		|| isOutputNode(toNode?.nodeType as AlgorithmNodeType)
		|| toNode?.overloads?.length === 1) {

		if(toNode?.params[index].type)
			return Set.of(toNode?.params[index].type) as Set<AlibType>
		return Set<AlibType>()
	}

	return toNode?.overloads?.reduce((acc, overload) => {
		if (overload.params[index].type)
			return acc.add(overload.params[index].type)
		return acc
	}, Set<AlibType>())
}

export const isCastable = (casts: Casts, from: AlibType, to: AlibType) => {
	return from === 'auto'
		|| to === 'auto'
		|| [from, to].includes('abstraction::UnspecifiedType')
		|| removeTemplate(from) === removeTemplate(to)
		|| casts[removeTemplate(from)]?.includes(removeTemplate(to))
}

/**
 * Check if any type from 'from' set and 'to' set is compatible
 * @param casts
 * @param from
 * @param to
 */
export const areCastable = (casts: Casts, from: Set<AlibType>, to: Set<AlibType>) => {
	const fromWithoutTemplates = from.map((type) => removeTemplate(type))
	const toWithoutTemplates = to.map((type) => removeTemplate(type))

	let isCastable = false
	fromWithoutTemplates.forEach((fromType) => {
		toWithoutTemplates.forEach((toType) => {
			if (casts[fromType]?.includes(toType) || fromType === toType)
				isCastable = true
		} )
	})

	return isCastable || from.union(to).includes('abstraction::UnspecifiedType')
}
