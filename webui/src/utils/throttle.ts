const throttle = <F extends (...args: any[]) => any>(
	interval: number,
	func: F
) => {
	let ready: boolean = true

	return (...args: Parameters<F>) => {
		if (!ready) return

		ready = false
		setTimeout(() => (ready = true), interval)

		func(...args)
	}
}

export default throttle
