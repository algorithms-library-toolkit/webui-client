import React from 'react'
import { useSelector } from 'react-redux'
import { makeStyles, Chip, Link } from '@material-ui/core'
import { Web as WebIcon, Settings as SettingsIcon } from '@material-ui/icons';
import { ReduxState } from 'reducers'

const gitUrl = {
	"webui": "https://gitlab.fit.cvut.cz/algorithms-library-toolkit/webui-client",
	"core": "https://gitlab.fit.cvut.cz/algorithms-library-toolkit/automata-library",
}
const gitSHA = {
	"webui" : process.env.REACT_APP_GIT_HASH ? process.env.REACT_APP_GIT_HASH : undefined,
	"core": undefined
}
const gitDescribe = {
	"webui": process.env.REACT_APP_GIT_DESCRIBE ? process.env.REACT_APP_GIT_DESCRIBE : undefined,
	"core" : undefined
}

const useStyles = makeStyles((theme) => ({
	root: {
		display: 'flex',
		'flex-direction': 'column'
	}
}));


export const WebUIAboutBox = () => {
	const classes = useStyles()
	const { coreDescribe } = useSelector((state: ReduxState) => { return { coreDescribe: state.workerDefinitions.version } })

	const labels = {
		"webui": `WebUI: ${gitDescribe.webui ? gitDescribe.webui : "<unknown>"}`,
		"core" : `Core: ${coreDescribe ? coreDescribe : "<unknonw>"}`,
	}

	const hrefs = {
		"webui": `${gitUrl.webui}/-/blob/${gitSHA.webui}/CHANGELOG.md`,
		"core": `${gitUrl.core}`,
	}

	return (
		<div className={classes.root}>
			<Link href={hrefs.webui}>
				<Chip clickable size="small" color="primary" icon={<WebIcon />} label={labels.webui} />
			</Link>
			<Link href={hrefs.core}>
				<Chip clickable size="small" color="primary" icon={<SettingsIcon />} label={labels.core} />
			</Link>
		</div>
		   )
}
