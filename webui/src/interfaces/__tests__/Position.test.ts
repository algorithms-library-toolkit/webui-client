import { mockRandom } from 'jest-mock-random'

import Position, * as Op from '../Position'

// Mock random
// https://stackoverflow.com/a/43532567/7152861
mockRandom([0.5])

const a = Op.toPosition(1, 2)
const b = Op.toPosition(2, 3)
const c = Op.toPosition(1, -2)
const d = Op.toPosition(-3, 4)
const e = Op.toPosition(1.43231, 1.89984)
const bounds = Op.toPosition(800, 400)

test('(1, 2) + (2, 3) equals (3, 5)', () => {
	expect(Op.add(a, b)).toEqual(Op.toPosition(3, 5))
})
test('(1, 2) + 3 equals (4, 5)', () => {
	expect(Op.addX(a, 3)).toEqual(Op.toPosition(4, 5))
})
test('(1, 2) - (2, 3) equals (-1, -1)', () => {
	expect(Op.subtract(a, b)).toEqual(Op.toPosition(-1, -1))
})
test('(1, 2) - 1 equals (0, 1)', () => {
	expect(Op.subtractX(a, 1)).toEqual(Op.toPosition(0, 1))
})
test('(1, 2) * (2, 3) equals (2, 6)', () => {
	expect(Op.multiply(a, b)).toEqual(Op.toPosition(2, 6))
})
test('(1, 2) * 3 equals (3, 6)', () => {
	expect(Op.multiplyByX(a, 3)).toEqual(Op.toPosition(3, 6))
})
test('(1, 2) / (2, 3) equals (1/2, 2/3)', () => {
	expect(Op.divide(a, b)).toEqual(Op.toPosition(1 / 2, 2 / 3))
})
test('(1, 2) / 3 equals (1/3, 2/3)', () => {
	expect(Op.divideByX(a, 3)).toEqual(Op.toPosition(1 / 3, 2 / 3))
})
test('(1, 2) / (0, 0) equals (0,0)', () => {
	expect(Op.divide(a, Op.toPosition(0, 0))).toEqual(Op.toPosition(0, 0))
})
test('(1, 2) / 0 equals (0, 0)', () => {
	expect(Op.divideByX(a, 0)).toEqual(Op.toPosition(0, 0))
})
test('(1, 2) % 2 equals (1, 0)', () => {
	expect(Op.modX(a, 2)).toEqual(Op.toPosition(1, 0))
})
test('(1, 2)^2 equals (1, 4)', () => {
	expect(Op.toPower(a, 2)).toEqual(Op.toPosition(1, 4))
})
test('(1, 2)^1/2 equals (1, 4)', () => {
	expect(Op.squareRoot(a)).toEqual(Op.toPosition(1, Math.sqrt(2)))
})
test('|(1, -2)| equals (1, 2)', () => {
	expect(Op.absolute(c)).toEqual(a)
})
test('min of (1, 2) and (-3, 4) equals (-3, 2)', () => {
	expect(Op.min(a, d)).toEqual(Op.toPosition(-3, 2))
})
test('min of (-3, 4) and 2 equals (2, 2)', () => {
	expect(Op.minX(d, 2)).toEqual(Op.toPosition(-3, 2))
})
test('max of (1, 2) and (-3, 4) equals (-3, 2)', () => {
	expect(Op.max(a, d)).toEqual(Op.toPosition(1, 4))
})
test('max of (-3, 4) and 2 equals (2, 2)', () => {
	expect(Op.maxX(d, 2)).toEqual(Op.toPosition(2, 4))
})
test('distance of (-3, 4) equals 5', () => {
	expect(Op.distance(d)).toEqual(5)
})
test('round to 3 dec of (1.43231, 1.89984) equals (1.43, 1.9)', () => {
	expect(Op.round(e, 3)).toEqual(Op.toPosition(1.432, 1.9))
})
test('creates Position object', () => {
	expect(Op.toPosition(1, 2)).toEqual(<Position>{ x: 1, y: 2 })
})
test('random(0.5) within bounds (800, 400) centering equals (800, 400)', () => {
	expect(Op.random(bounds)).toEqual(Op.toPosition(800, 400))
})
test('contain (234, -234) within bounds (800, 400) equals (234, 40)', () => {
	expect(Op.toBounds(bounds, Op.toPosition(234, -234))).toEqual(
		Op.toPosition(234, 40)
	)
})
test('zoom (100, -200) with offset (-10, 20) and scale 0.75 by factor -0.25 equals (-35, 70)', () => {
	expect(
		Op.zoom(Op.toPosition(100, -200), Op.toPosition(-10, 20), 0.75, 0.25)
	).toEqual(Op.toPosition(-35, 70))
})
test('normalize (100, -200) with offset (-10, 20) and scale 0.5 equals (220, -440)', () => {
	expect(
		Op.normalize(Op.toPosition(100, -200), Op.toPosition(-10, 20), 0.5)
	).toEqual(Op.toPosition(220, -440))
})
test('snap (-3, 4) to 10 gridsize equals (0, 0)', () => {
	expect(Op.snap(d, 10)).toEqual(Op.toPosition(-0, 0))
})
