import { useCallback } from 'react'
import { useDispatch, useSelector } from 'react-redux'

import useCanvas from './useCanvas'
import { stateCanvasActions } from '../reducers/stateCanvas'
import { ReduxState } from '../reducers'
import Position from '../interfaces/Position'

const { setOffset, zoom } = stateCanvasActions

const useStateCanvas = (canvasRef: React.RefObject<SVGSVGElement>) => {
	const dispatch = useDispatch()
	const { offset, cursorMode } = useSelector((state: ReduxState) => ({
		offset: state.stateCanvas.offset,
		cursorMode: state.stateCanvas.cursorMode
	}))

	const dispatchOffset = useCallback(
		(offset: Position) => dispatch(setOffset(offset)),
		[dispatch]
	)

	const dispatchZoom = useCallback((value: number) => dispatch(zoom(value)), [
		dispatch
	])

	return useCanvas(
		canvasRef,
		offset,
		cursorMode,
		dispatchOffset,
		dispatchZoom
	)
}

export default useStateCanvas
