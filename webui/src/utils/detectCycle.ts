import { Set } from 'immutable'
import AlgorithmEdge from '../interfaces/AlgorithmEdge'

interface PathEntry {
	nodeId: string
	edgeId: string
}

/** Detects cycle in algorithm graph
 *	@returns Set of edge IDs that are in a cycle in a component that contains startNodeId
 */
export const detectCycles = (edges: Set<Omit<AlgorithmEdge, 'endParamIndex'>>): Set<string> => {
	let visited = Set<string>()

	const _detectCycle = (
		path: PathEntry[],
		toVisitId: string
	): Set<string> => {
		const pathIndex = path.findIndex(({ nodeId }) => nodeId === toVisitId)
		if (pathIndex !== -1)
			return Set(path.slice(pathIndex).map(({ edgeId }) => edgeId))

		const nodeEdges = edges.filter((edge) => edge.startNodeId === toVisitId)
		if (nodeEdges.isEmpty()) return Set()

		visited = visited.add(toVisitId)
		return nodeEdges.reduce(
			(acc, edge) =>
				acc.merge(
					_detectCycle(
						[...path, { edgeId: edge.id, nodeId: toVisitId }],
						edge.endNodeId
					)
				),
			Set<string>()
		)
	}

	return edges.reduce(
		(acc, edge) =>
			visited.contains(edge.startNodeId)
				? acc
				: acc.merge(_detectCycle([], edge.startNodeId)),
		Set<string>()
	)
}
