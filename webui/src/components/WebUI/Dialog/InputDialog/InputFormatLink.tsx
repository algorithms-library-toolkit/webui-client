import { IconButton, Link } from '@material-ui/core'
import HelpIcon from '@material-ui/icons/Help'
import React from 'react'

interface InputFormatLinkProps {
	showTooltip: boolean
}

const InputFormatLink = (props: InputFormatLinkProps) => {

	return (
		<Link component='button' variant='body2' target='_blank' href='https://alt.fit.cvut.cz/docs/parse/'
			  onClick={() => window.open('https://alt.fit.cvut.cz/docs/parse/', '_blank')}>
			<IconButton component='span' disabled={true}><HelpIcon /></IconButton>
				Input format documentation
		</Link>
	)
}

export default InputFormatLink