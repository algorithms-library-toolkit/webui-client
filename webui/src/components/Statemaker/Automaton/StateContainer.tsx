import * as React from 'react'
import { batch, useDispatch, useSelector } from 'react-redux'

import useDrag from '../../../hooks/useDrag'
import useStatePosition from '../../../hooks/useStatePosition'
import useTextSize from '../../../hooks/useTextSize'
import { hasStateError } from '../../../utils/errors'

import { Actions, ReduxState } from '../../../reducers'
import { stateCanvasActions } from '../../../reducers/stateCanvas'
import State from './State'

const {
	updateState,
	addTransition,
	incrementTransitionNameId,
	updateGhostTransition,
	setBuildTransition,
	setInput
} = Actions

const { setPositioningNode: setPositioningState, selectNode: selectState } = stateCanvasActions

interface Props {
	/** State id used to get state data */
	id: string
	canvasRef: React.RefObject<SVGSVGElement>
}

/**
 * Selects attributes from state
 *
 * @param {string} id Selected state's id
 */
const stateSelector = (id: string) => (state: ReduxState) => {
	let hasTransitionSelected = false

	// If there's selected transition
	if (!!state.stateCanvas.selectedEdge) {
		const t =
			state.stateData.present.transitions[state.stateCanvas.selectedEdge]
		// Mark if selected tansition leads from or to this state
		if (t) {
			hasTransitionSelected = t.startState === id || t.endState === id
		}
	}

	return {
		data: state.stateData.present.states[id],
		isSelected: id === state.stateCanvas.selectedNode,
		isInitial: state.stateData.present.initialStates.includes(id),
		isFinal: state.stateData.present.finalStates.includes(id),
		hasTransitionSelected,
		hasError: hasStateError(
			state.stateData.present.states,
			state.stateData.present.states[id]
		),
		buildTransition: state.stateUI.buildTransition,
		selectedTransition: state.stateCanvas.selectedEdge,
		ghostTransition: state.stateUI.ghostTransition,
		transitionNameId: state.stateUI.transitionNameId,
		cursorMode: state.stateCanvas.cursorMode,
		positioningState: state.stateCanvas.positioningNode
	}
}

/**
 * Represents state machine's individual state
 * It can be dragged around and be selected
 *
 * @param {Props} props
 * @returns
 */
function StateContainer(props: Props) {
	const { id, canvasRef } = props
	const dispatch = useDispatch()
	const [isDragging, setDragging] = useDrag(canvasRef)
	// Indicates mouse moving in drag mode
	const [isMoving, setMoving] = React.useState(false)
	// Binds thi state to update action
	const handleUpdate = React.useCallback(
		(size: number) => dispatch(updateState(id, { size: size / 2 })),
		[dispatch, id]
	)
	const {
		data,
		isSelected,
		isInitial,
		isFinal,
		hasTransitionSelected,
		hasError,
		buildTransition,
		selectedTransition,
		ghostTransition,
		transitionNameId,
		cursorMode,
		positioningState
	} = useSelector(stateSelector(id))

	// When moving around in drag mode, use local position
	const useLocalMove = isDragging && isMoving
	const position = useStatePosition(canvasRef, useLocalMove)
	// Ref to state name
	const textRef = useTextSize(data.name, handleUpdate, false)

	// Disallow click on elements behind
	const handleClick = React.useCallback(
		(e: React.MouseEvent<SVGCircleElement>) => {
			e.stopPropagation()
		},
		[]
	)

	// Dragging state
	const handleMouseMove = React.useCallback(
		(e: React.MouseEvent<SVGCircleElement>) => {
			e.preventDefault()
			// Allow only in select mode
			if (cursorMode !== 'select' || id === positioningState) return
			if (isDragging) {
				// Signal state is moving
				setMoving(true)
				dispatch(setPositioningState(id))
			}
		},
		[dispatch, id, cursorMode, isDragging, positioningState]
	)

	// Drag on hold
	const handleMouseDown = React.useCallback(
		(e: React.MouseEvent<SVGCircleElement>) => {
			e.preventDefault()
			// Allow only in select mode
			if (cursorMode !== 'select') return
			e.nativeEvent.stopImmediatePropagation()
			// Signal drag if transition isn't being manipulated
			if (selectedTransition || buildTransition) return
			setDragging(true)
		},
		[setDragging, cursorMode, selectedTransition, buildTransition]
	)

	// Handle mouse release on state
	const handleMouseUp = React.useCallback(
		(e: React.MouseEvent<SVGCircleElement>) => {
			e.stopPropagation()
			// Do nothing if not in select mode
			if (cursorMode !== 'select') return
			// If transition is being built
			if (buildTransition) {
				// There's alrady start state
				if (ghostTransition.startState) {
					// Creaate transition
					batch(() => {
						dispatch(
							addTransition({
								...ghostTransition,
								name: ghostTransition.name + transitionNameId,
								endState: id
							})
						)

						dispatch(incrementTransitionNameId())
						dispatch(updateGhostTransition({ startState: '' }))

						dispatch(setBuildTransition(false))
					})
				} else {
					// No start stat ein ghost transition
					dispatch(updateGhostTransition({ startState: id }))
				}
			} else if (isMoving) {
				// State is in drag mode
				// Stop dragging and set position
				batch(() => {
					dispatch(setPositioningState(''))
					dispatch(updateState(id, { position }))
				})
			} else {
				// Select state
				batch(() => {
					dispatch(selectState(id))
					dispatch(setInput(data.name))
				})
			}

			setDragging(false)
			setMoving(false)
		},
		[
			dispatch,
			id,
			data.name,
			cursorMode,
			isMoving,
			setDragging,
			buildTransition,
			ghostTransition,
			transitionNameId,
			position
		]
	)

	return (
		<State
			ref={textRef}
			data={useLocalMove ? { ...data, position } : data}
			isInitial={isInitial}
			isFinal={isFinal}
			isSelected={isSelected}
			hasError={hasError}
			hasTransitionSelected={hasTransitionSelected}
			buildTransition={buildTransition}
			showOverlay={
				cursorMode === 'select' &&
				(!buildTransition || ghostTransition.startState === id) &&
				!isMoving
			}
			onMouseDown={handleMouseDown}
			onMouseUp={handleMouseUp}
			onMouseMove={handleMouseMove}
			onClick={handleClick}
		/>
	)
}

export default StateContainer
