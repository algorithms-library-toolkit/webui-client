import { v4 as uuidv4 } from 'uuid'
import Automaton from 'interfaces/Automaton'
import { classifyAutomaton } from 'interfaces/AutomatonTypes'
import Map from './Map'
import State from './State'
import Transition from './Transition'


/** State machine state map */
export type StateMap = Map<State>
/** State machine transition map */
export type TransitionMap = Map<Transition>

export default interface Data {
	/** Initial machine states */
	initialStates: string[]
	/** Final machine states */
	finalStates: string[]
	/** State machine state map */
	states: StateMap
	/** State machine transition map */
	transitions: TransitionMap
}

export const emptyAutomaton: Data = {
	states: {},
	transitions: {},
	initialStates: [],
	finalStates: []
}

export const toStateData = (automaton: Automaton): Data => {
	type NameIdMapping = { [key: string]: string }
	const nameToId: NameIdMapping = automaton.states.reduce((acc, state) => {
		const id = uuidv4()
		return {
			...acc,
			[state]: id
		}
	}, {})

    const stateMap = automaton.states.reduce((acc, state) => {
		const id = nameToId[state]
		return {
			...acc,
			[id]: { id: id, name: state, position: { x: 0, y: 0 }, size: 0 }
		}
	}, {})

	return {
		states: stateMap,
		initialStates: automaton.initialStates.map((state) => nameToId[state]),
		finalStates: automaton.finalStates.map((state) => nameToId[state]),
		transitions: automaton.transitions.reduce((acc, transition) => {
			const id = uuidv4()
			return {
				...acc,
				[id]: { id: id, name: transition.symbol, startState: nameToId[transition.from], endState: nameToId[transition.to] }
			}
		}, {})
	}
}

const getAlphabetFromTransitions = (transitions: TransitionMap): string[] => {
	return Object.values(transitions).map(tr => tr.name).filter((e, index, arr) => arr.indexOf(e) === index)
}

export const toAutomaton = (data: Data): Automaton => {
	type IdNameMapping = { [key: string]: string }
	const idToName: IdNameMapping = Object.values(data.states).reduce((acc, state) => {
		return {
			...acc,
			[state.id]: state.name
		}
	}, {})

	const automaton = {
		states: Object.values(idToName),
		initialStates: data.initialStates.map((stateId: string) => idToName[stateId]),
		finalStates: data.finalStates.map((stateId: string) => idToName[stateId]),
		alphabet: getAlphabetFromTransitions(data.transitions),
		transitions: Object.values(data.transitions).map(tr => { return {
			symbol: tr.name,
			from: idToName[tr.startState],
			to: idToName[tr.startState],
		}})
	}

	return { type: classifyAutomaton(automaton), ...automaton }
}
