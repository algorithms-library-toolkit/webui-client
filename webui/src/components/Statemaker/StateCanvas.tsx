import React, { useRef } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import Canvas from '../Canvas'
import State from './Automaton/StateContainer'
import Transition from './Automaton/TransitionContainer'
import GhostState from './Automaton/GhostStateContainer'
import GhostTransition from './Automaton/GhostTransitionContainer'
import useStateCanvas from '../../hooks/useStateCanvas'
import { ReduxState, Actions } from '../../reducers'

const { incrementStateNameId, incrementTransitionNameId } = Actions

const StateCanvas = () => {
	const dispatch = useDispatch()

	const {
		states,
		transitions,
		ghostState,
		ghostTransition,
		stateNameId,
		transitionNameId,
		showGrid,
		scale, 
		offset
	} = useSelector((state: ReduxState) => ({
		states: state.stateData.present.states,
		transitions: state.stateData.present.transitions,
		ghostState: state.stateUI.ghostState,
		ghostTransition: state.stateUI.ghostTransition,
		stateNameId: state.stateUI.stateNameId,
		transitionNameId: state.stateUI.transitionNameId,
		showGrid: state.stateCanvas.gridOn,
		scale: state.stateCanvas.scale,
		offset: state.stateCanvas.offset
	}))

	const canvasRef = useRef<SVGSVGElement>(null)

	useStateCanvas(canvasRef)

	// Use numbers to differentiate auto-generated names
	const ghostStateName = ghostState.name + stateNameId
	const ghostTransitionName = ghostTransition.name + transitionNameId

	// Check if name is already used, otherwise increase number
	// Used mainly when importing data
	if (
		Object.values(states)
			.map((s) => s.name)
			.includes(ghostStateName)
	) {
		dispatch(incrementStateNameId())
	}

	if (
		Object.values(transitions)
			.map((s) => s.name)
			.includes(ghostTransitionName)
	) {
		dispatch(incrementTransitionNameId())
	}

	return (
		<Canvas
			id="stateCanvas"
			ref={canvasRef}
			showGrid={showGrid}
			scale={scale}
			offset={offset}
		>
			<GhostTransition canvasRef={canvasRef} />
			{Object.keys(transitions).map((id) => (
				<Transition key={id} id={id} canvasRef={canvasRef} />
			))}
			<GhostState canvasRef={canvasRef} />
			{Object.keys(states).map((id) => (
				<State key={id} id={id} canvasRef={canvasRef} />
			))}
		</Canvas>
	)
}

export default StateCanvas
